Ext.define('Mentor.controller.LeftMenu', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {},
        control: {
            
        }
    },
    //called when the Application is launched, remove if not needed
    launch: function (app) {},

    /***
    Display the Invite tab of Mentee
    ****/
    displayInviteTabMentee: function () {
        var me = this;
        Ext.getCmp('idBtnMeetingHistoryeMenteeLeftMenu').setCls('leftmenu-inactive-btn-cls');
        Ext.getCmp('idBtnProfileMenteeLeftMenu').setCls('leftmenu-inactive-btn-cls');
        Ext.getCmp('idPendingInvitesMenteeLeftMenu').setCls('leftmenu-active-btn-cls');


        Ext.getCmp('idPendingInvitesMenteeLeftMenu').setIconCls('pendingInvitesIconActiveMentorLeftMenu');
        Ext.getCmp('idBtnProfileMenteeLeftMenu').setIconCls('profileIconInActiveMentorLeftMenu');
        Ext.getCmp('idBtnMeetingHistoryeMenteeLeftMenu').setIconCls('historyIconInActiveMentorLeftMenu');

        Ext.getCmp('idMenteeLeftMenu').setActiveItem(0);
    },

    /***
    Display the Pending tab of Mentor
    ****/
    displayPendingTabMentee:function(){
        Ext.getCmp('idBtnProfileMentorLeftMenu').setCls('leftmenu-inactive-btn-cls');
        Ext.getCmp('idBtnMeetingHistoryeMentorLeftMenu').setCls('leftmenu-inactive-btn-cls');
        Ext.getCmp('idPendingInvitesMentorLeftMenu').setCls('leftmenu-active-btn-cls');

        Ext.getCmp('idPendingInvitesMentorLeftMenu').setIconCls('pendingInvitesIconActiveMentorLeftMenu');
        Ext.getCmp('idBtnProfileMentorLeftMenu').setIconCls('profileIconInActiveMentorLeftMenu');
        Ext.getCmp('idBtnMeetingHistoryeMentorLeftMenu').setIconCls('historyIconInActiveMentorLeftMenu');

        Ext.getCmp('idMentorLeftMenu').setActiveItem(0);
    },

    /***
        When Menu open
    ***/
    showMenu : function(params1,params2){
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        if (MentorLoginUser != null) {
            Ext.getCmp('idUserNameMentorLeftMenuScreen').setHtml(MentorLoginUser.MentorName);
            Ext.getCmp('idImgUserImageMentorLeftMenuScreen').setSrc(MentorLoginUser.MentorImage);
            Ext.getCmp('idBtnInstantMeetingMentorLeftMenu').setText(Mentor.Global.IMPROMPTU_NAME+' Meeting');

            var TeamDetail = Ext.decode(localStorage.getItem("TeamAllDetails"));
            try{
                       
                //Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("resources/images/group_avatar.png")');
                Ext.getCmp('idImgTeamImageMentorLeftMenuScreen').setSrc("resources/images/group_avatar.png");
                if (TeamDetail!=null && TeamDetail.OwnTeamDetail != false) {
                    Ext.getCmp('idTeamNameMenuLeftMenuScreen').setHtml(TeamDetail.OwnTeamDetail[0].TeamName);
                    //Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("' + data.getTeamDetail.data.OwnTeamDetail[0].TeamFullImage + '")');
                    Ext.getCmp('idImgTeamImageMentorLeftMenuScreen').setSrc(TeamDetail.OwnTeamDetail[0].TeamFullImage);
                   
                } else if (TeamDetail!=null && TeamDetail.MemberTeamDetail != false) {
                    Ext.getCmp('idTeamNameMenuLeftMenuScreen').setHtml(TeamDetail.MemberTeamDetail[0].TeamName);
                    //Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("' + data.getTeamDetail.data.MemberTeamDetail[0].TeamFullImage + '")');
                     Ext.getCmp('idImgTeamImageMentorLeftMenuScreen').setSrc(TeamDetail.MemberTeamDetail[0].TeamFullImage);
                    
                } else {
                    Ext.getCmp('idTeamNameMenuLeftMenuScreen').setHtml('No Team');
                   
                }
                
            }catch (e){
                console.log(e);
            }

        }
        
        else if (MenteeLoginUser != null) {
            Ext.getCmp('idUserNameMentorLeftMenuScreen').setHtml(MenteeLoginUser.MenteeName);
            Ext.getCmp('idImgUserImageMentorLeftMenuScreen').setSrc(MenteeLoginUser.MenteeImage);

            var TeamDetail = Ext.decode(localStorage.getItem("TeamAllDetails"));
            try{
                       
                //Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("resources/images/group_avatar.png")');
                Ext.getCmp('idImgTeamImageMentorLeftMenuScreen').setSrc("resources/images/group_avatar.png");
                if (TeamDetail!=null && TeamDetail.OwnTeamDetail != false) {
                    Ext.getCmp('idTeamNameMenuLeftMenuScreen').setHtml(TeamDetail.OwnTeamDetail[0].TeamName);
                    //Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("' + data.getTeamDetail.data.OwnTeamDetail[0].TeamFullImage + '")');
                    Ext.getCmp('idImgTeamImageMentorLeftMenuScreen').setSrc(TeamDetail.OwnTeamDetail[0].TeamFullImage);
                   
                } else if (TeamDetail!=null && TeamDetail.MemberTeamDetail != false) {
                    Ext.getCmp('idTeamNameMenuLeftMenuScreen').setHtml(TeamDetail.MemberTeamDetail[0].TeamName);
                    //Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("' + data.getTeamDetail.data.MemberTeamDetail[0].TeamFullImage + '")');
                     Ext.getCmp('idImgTeamImageMentorLeftMenuScreen').setSrc(TeamDetail.MemberTeamDetail[0].TeamFullImage);
                    
                } else {
                    Ext.getCmp('idTeamNameMenuLeftMenuScreen').setHtml('No Team');
                   
                }
             }
             catch (e){
                console.log(e);
            }
        }
    }
    
    
});