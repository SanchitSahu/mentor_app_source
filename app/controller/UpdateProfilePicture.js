Ext.define('Mentor.controller.UpdateProfilePicture', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {},
        control: {
            "mentorprofile button[action=takeProfilePhoto]": {
                tap: "takeProfilePhoto"
            },
            "addeditteam button[action=takeTeamProfilePhoto]": {
                tap: "takeTeamProfilePhoto"
            },
            "mentorprofile button[action=btnUpdateProfile]": {
                tap: "editProfileWithPhoto"
            },
            "mentorprofile button[action=btnSaveProfile]": {
                tap: "btnSaveProfile"
            },
            "mentorprofile button[action=btnGetUpdatedProfile]": {
                tap: "btnGetUpdatedProfile"
            }
        }
    },
    //called when the Application is launched, remove if not needed
    launch: function (app) {

    },
    takeProfilePhoto: function (btnCmp) {
        var me = this;
        Ext.Msg.show({
            title: "Profile Image",
            message: "Select an option",
            buttons: [
                {
                    itemId: "0",
                    text: "Gallery",
                    ui: "action"
                },
                {
                    itemId: "1",
                    text: "Camera",
                    ui: "action"
                },
                {
                    itemId: "cancel",
                    text: "Cancel",
                    ui: "decline"
                }
            ],
            promptConfig: false,
            scope: me,
            fn: function (btn) {
                if (btn == "cancel") {
                    return;
                }
                this.openCordovaPicker.call(this, btnCmp, Number(btn));
            }
        });
    },
    takeTeamProfilePhoto: function (btnCmp) {
        var me = this;
        Ext.Msg.show({
            title: "Team Profile Image",
            message: "Select an option",
            buttons: [
                {
                    itemId: "0",
                    text: "Gallery",
                    ui: "action"
                },
                {
                    itemId: "1",
                    text: "Camera",
                    ui: "action"
                },
                {
                    itemId: "cancel",
                    text: "Cancel",
                    ui: "decline"
                }
            ],
            promptConfig: false,
            scope: me,
            fn: function (btn) {
                if (btn == "cancel") {
                    return;
                }
                this.openCordovaPickerForTeam.call(this, btnCmp, Number(btn));
            }
        });
    },
    openCordovaPickerForTeam: function (component, sourceType) {
        var me = this,
                width = (Ext.getBody().getWidth() * 2 * sourceType),
                height = (Ext.getBody().getHeight() * 2 * sourceType);
        navigator.camera.getPicture(function (fileURL) {
            component.element.setStyle("backgroundImage", 'url("' + fileURL + '")');
            component.element.setStyle("backgroundRepeat", 'no-repeat');
            //component.up('mentorprofile').setProfilePickImageUrl(fileURL);
            //me.uploadProfilePhoto(fileURL);
            Mentor.Global.PROFILE_IMAGE = fileURL;
            me.editTeamProfileWithPhoto(fileURL);
        }, function (error) {
            console.log(Ext.encode(error));
            Ext.Msg.alert("Error", Ext.encode(error));
            // alert("Fail " + Ext.encode(error));
        }, {
            quality: 50,
            destinationType: 1,
            /*  DATA_URL : 0,      // Return image as base64-encoded string
             FILE_URI : 1,      // Return image file URI
             NATIVE_URI : 2     // Return image native URI (e.g., assets-library:// on iOS or content:// on Android)	*/
            sourceType: sourceType, //	0,
            /*	PHOTOLIBRARY : 0,
             CAMERA : 1,
             SAVEDPHOTOALBUM : 2	*/
            //allowEdit : true,
            correctOrientation: true,
            targetWidth: width,
            targetHeight: height,
            encodingType: 1,
            /* JPEG : 0,               // Return JPEG encoded image
             PNG : 1	*/
            saveToPhotoAlbum: true,
            mediaType: 0
        });
    },
    openCordovaPicker: function (component, sourceType) {
        var me = this,
                width = (Ext.getBody().getWidth() * 2 * sourceType),
                height = (Ext.getBody().getHeight() * 2 * sourceType);
        navigator.camera.getPicture(function (fileURL) {
            component.element.setStyle("backgroundImage", 'url("' + fileURL + '")');
            component.element.setStyle("backgroundRepeat", 'no-repeat');
            //component.up('mentorprofile').setProfilePickImageUrl(fileURL);
            //me.uploadProfilePhoto(fileURL);
            Mentor.Global.PROFILE_IMAGE = fileURL;
            me.editProfileWithPhoto(fileURL);
        }, function (error) {
            console.log(Ext.encode(error));
            Ext.Msg.alert("Error", Ext.encode(error));
            // alert("Fail " + Ext.encode(error));
        }, {
            quality: 50,
            destinationType: 1,
            /*  DATA_URL : 0,      // Return image as base64-encoded string
             FILE_URI : 1,      // Return image file URI
             NATIVE_URI : 2     // Return image native URI (e.g., assets-library:// on iOS or content:// on Android)	*/
            sourceType: sourceType, //	0,
            /*	PHOTOLIBRARY : 0,
             CAMERA : 1,
             SAVEDPHOTOALBUM : 2	*/
            //allowEdit : true,
            correctOrientation: true,
            targetWidth: width,
            targetHeight: height,
            encodingType: 1,
            /* JPEG : 0,               // Return JPEG encoded image
             PNG : 1	*/
            saveToPhotoAlbum: true,
            mediaType: 0
        });
    },
    //uploadProfilePhoto: function(btn){
    /*uploadProfilePhoto: function(url){
     var me = this,
     //formPanel = btn.up("mentorprofile"),
     fileURL = formPanel.getProfilePickImageUrl();
     if(Ext.isEmpty(fileURL)){
     Ext.Msg.alert("Message", "Tap the image to select photo then click on upload button");
     return;
     }
     var options = new FileUploadOptions(),
     viewport = Ext.Viewport;
     options.fileKey = "files[]";
     options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
     options.mimeType = "image/png";
     
     var params = {};
     params.method = "mentorIamge";
     params.MentorID = "1";			
     options.params = params;
     
     var ft = new FileTransfer(),
     viewport = Ext.Viewport,
     ftURL =  GLOBAL.SERVER_URL;
     ft.onprogress = function(progressEvent) {
     if (progressEvent.lengthComputable) {
     var mask = viewport.getMasked();
     mask.setMessage("Uploading..." + Ext.Number.toFixed((progressEvent.loaded / progressEvent.total),1) * 100 + "%");
     //mask.setMessage("Uploading..." + Ext.Number.toFixed((progressEvent.loaded / progressEvent.total),2) * 100 + "%");
     }
     };				
     viewport.setMasked({
     xtype: "loadmask",
     message: "Uploading..."
     });
     ft.upload(fileURL, encodeURI(ftURL), function(obj){
     viewport.setMasked(false);
     console.log("Success: "+ Ext.encode(obj));
     var response = Ext.decode(obj.response),
     data = Ext.isArray(response) ? response[0]: response,
     url = data && data.name; 
     formPanel.setProfilePickImageUrl(null);
     me.updateProfilePicture("http://images.allinworldsportsapp.com/"+url);
     // component.element.setStyle("backgroundImage", 'url("http://images.allinworldsportsapp.com/' + url + '")');
     }, function(obj){
     viewport.setMasked(false);
     console.log("Fail: "+ Ext.encode(obj));
     Ext.Msg.alert("Error", "Image upload failed");
     }, options);
     },*/


    editProfileWithPhoto: function (url) {
        var me = this,
                //formPanel = btn.up("mentorprofile"),
                fileURL = Mentor.Global.PROFILE_IMAGE;

        /*var Email = Ext.getCmp('email').getValue();
         var Phone = Ext.getCmp('phone').getValue();
         var UserName = Ext.getCmp('userName').getValue();
         var AccessToken = Ext.getCmp('password').getValue();
         
         if(Email == ""){
         Ext.Msg.alert('MELS', "Please enter email address");
         return;
         }
         else if(Phone == ""){
         Ext.Msg.alert('MELS', "Please enter phone number");
         return;
         }
         else if(UserName == ""){
         Ext.Msg.alert('MELS', "Please enter username");
         return;
         }
         else if(AccessToken == ""){
         Ext.Msg.alert('MELS', "Please enter password");
         return;
         }
         else if(me.ValidateEmail(Email)==false){
         Ext.Msg.alert('MELS',"You have entered an invalid email address.");
         return;
         }
         else if(AccessToken.length < 4){
         Ext.Msg.alert('MELS',"Password length must be four characters.");
         return;
         }*/
        /*if(Ext.isEmpty(fileURL)){
         Ext.Msg.alert("Message", "Tap the image to select photo then click on upload button");
         return;
         }*/

        var GLOBAL = Mentor.Global;

        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0"
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1"
        }

        var data = {
            /*UserName : UserName,
             PhoneNumber : Phone,
             Pass : AccessToken,
             Email : Email*/
            UserId: UserID,
            UserType: UserType
        };


        var options = new FileUploadOptions(), viewport = Ext.Viewport;
        options.fileKey = "files[]";
        options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
        options.mimeType = "image/png";

        var params = {};
        params.method = "editProfile";
        params.UserId = UserID;
        params.UserType = UserType;
        /*params.UserName = UserName;
         params.PhoneNumber = Phone;
         params.Pass = AccessToken;
         params.Email = Email;*/
        options.params = params;

        var ft = new FileTransfer(),
                viewport = Ext.Viewport,
                ftURL = GLOBAL.SERVER_URL;
        ft.onprogress = function (progressEvent) {
            if (progressEvent.lengthComputable) {
                var mask = viewport.getMasked();
                mask.setMessage("Uploading..." + Ext.Number.toFixed((progressEvent.loaded / progressEvent.total), 1) * 100 + "%");
                //mask.setMessage("Uploading..." + Ext.Number.toFixed((progressEvent.loaded / progressEvent.total),2) * 100 + "%");
            }
        };
        viewport.setMasked({
            xtype: "loadmask",
            message: "Uploading..."
        });
        ft.upload(fileURL, encodeURI(ftURL), function (obj) {
            viewport.setMasked(false);
            console.log("Success: " + Ext.encode(obj));
            var response = Ext.decode(obj.response),
                    data = Ext.isArray(response) ? response[0] : response,
                    url = data && data.name;
            //formPanel.setProfilePickImageUrl(null);

            //me.updateProfilePicture("http://images.allinworldsportsapp.com/"+url);

            //var data = Ext.decode(obj.responseText);
            console.log(data);
            d = data.editProfile.data;
            if (data.editProfile.Error == 1 || data.editProfile.Error == 2) {
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', data.editProfile.Message);
                }, 100);
                return;
            } else {
                if (UserType == "0") {//Mentor
                    localStorage.setItem("idMentorLoginDetail", Ext.encode(data.editProfile.data));
                    var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
                    Ext.ComponentQuery.query('#idTopHeaderProfileScreen')[0].element.setStyle("backgroundImage",'linear-gradient( rgba(81,43,128, 0.70), rgba(81,43,128, 0.70) ) ,url("' + MentorLoginUser.MentorImage + '")');
                    Ext.ComponentQuery.query('#idTopHeaderProfileScreen')[0].element.setStyle("backgroundRepeat", 'no-repeat');
                    Ext.ComponentQuery.query('#idTopHeaderProfileScreen')[0].element.setStyle("background-position", 'center');
                    Ext.ComponentQuery.query('#idTopHeaderProfileScreen')[0].element.setStyle("background-size", 'cover');
                    Ext.ComponentQuery.query('#idToolbarProfileScreen')[0].element.setStyle("background", 'transparent');
                    Ext.ComponentQuery.query('#idImagePanelProfileScreen')[0].element.setStyle("background", 'transparent');
                } else if (UserType == "1") {//Mentee
                    localStorage.setItem("idMenteeLoginDetail", Ext.encode(data.editProfile.data));
                    var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
                    Ext.ComponentQuery.query('#idTopHeaderProfileScreen')[0].element.setStyle("backgroundImage",'linear-gradient( rgba(81,43,128, 0.70), rgba(81,43,128, 0.70) ) ,url("' + MenteeLoginUser.MenteeImage + '")');
                    Ext.ComponentQuery.query('#idTopHeaderProfileScreen')[0].element.setStyle("backgroundRepeat", 'no-repeat');
                    Ext.ComponentQuery.query('#idTopHeaderProfileScreen')[0].element.setStyle("background-position", 'center');
                    Ext.ComponentQuery.query('#idTopHeaderProfileScreen')[0].element.setStyle("background-size", 'cover');

                    Ext.ComponentQuery.query('#idToolbarProfileScreen')[0].element.setStyle("background", 'transparent');
                    Ext.ComponentQuery.query('#idImagePanelProfileScreen')[0].element.setStyle("background", 'transparent');
                }
                Mentor.app.application.getController('LeftMenu').showMenu("", ""); //Update left menu image also
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', "Profile Image updated sucessfully.", function () {
                       /* if (MentorLoginUser != null) {
                            Ext.getCmp('idMentorBottomTabView').setActiveItem(0);
                        } else {
                            Ext.getCmp('idMenteeBottomTabView').setActiveItem(0);
                        }*/
                    });
                }, 100);
            }
            // component.element.setStyle("backgroundImage", 'url("http://images.allinworldsportsapp.com/' + url + '")');
        }, function (obj) {
            viewport.setMasked(false);
            console.log("Fail: " + Ext.encode(obj));
            Ext.Msg.alert("Error", "Profile Image not update please try later.");
        }, options);
    },
    editTeamProfileWithPhoto: function (url) {
        var me = this, fileURL = Mentor.Global.PROFILE_IMAGE;

        var GLOBAL = Mentor.Global;

        var options = new FileUploadOptions(), viewport = Ext.Viewport;
        options.fileKey = "files[]";
        options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
        options.mimeType = "image/png";

        var params = {};
        params.method = "editTeamProfile";
        options.params = params;

        var ft = new FileTransfer(),
                viewport = Ext.Viewport,
                ftURL = GLOBAL.SERVER_URL;
        ft.onprogress = function (progressEvent) {
            if (progressEvent.lengthComputable) {
                var mask = viewport.getMasked();
                mask.setMessage("Uploading..." + Ext.Number.toFixed((progressEvent.loaded / progressEvent.total), 1) * 100 + "%");
                //mask.setMessage("Uploading..." + Ext.Number.toFixed((progressEvent.loaded / progressEvent.total),2) * 100 + "%");
            }
        };
        viewport.setMasked({
            xtype: "loadmask",
            message: "Uploading..."
        });
        ft.upload(fileURL, encodeURI(ftURL), function (obj) {
            viewport.setMasked(false);
            console.log("Success: " + Ext.encode(obj));
            var response = Ext.decode(obj.response),
                    data = Ext.isArray(response) ? response[0] : response,
                    url = data && data.name;

            console.log(data);
            d = data.editTeamProfile.data;
            if (data.editTeamProfile.Error == 1 || data.editTeamProfile.Error == 2) {
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', data.editTeamProfile.Message);
                }, 100);
                return;
            } else {
                Ext.getCmp('id_TeamProfilePictureHidden').setHtml(data.editTeamProfile.data);
            }
        }, function (obj) {
            viewport.setMasked(false);
            console.log("Fail: " + Ext.encode(obj));
            Ext.Msg.alert("Error", "Team Image not update please try later.");
        }, options);
    },
    btnUpdateProfile: function (btn) {
        var Email = Ext.getCmp('email').getValue();
        var Phone = Ext.getCmp('phone').getValue();
        var UserName = Ext.getCmp('userName').getValue();
        var AccessToken = Ext.getCmp('password').getValue();

        if (Email == "") {
            Ext.Msg.alert('MELS', "Please enter email address");
        } else if (Phone == "") {
            Ext.Msg.alert('MELS', "Please enter phone number");
        } else if (UserName == "") {
            Ext.Msg.alert('MELS', "Please enter username");
        } else if (AccessToken == "") {
            Ext.Msg.alert('MELS', "Please enter password");
        } else if (me.ValidateEmail(Email) == false) {
            Ext.Msg.alert('MELS', "You have entered an invalid email address.");
        } else if (AccessToken.length < 4) {
            Ext.Msg.alert('MELS', "Password length must be four characters.");
        } else {
            var GLOBAL = Mentor.Global;

            var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
            var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
            var UserID;
            var UserType;

            if (MentorLoginUser != null) {
                UserID = MentorLoginUser.Id;
                UserType = "0";
            } else if (MenteeLoginUser != null) {
                UserID = MenteeLoginUser.Id;
                UserType = "1";
            }

            var data = {
                UserId: UserID,
                UserType: UserType,
                UserName: UserName,
                PhoneNumber: Phone,
                Pass: AccessToken,
                Email: Email
            };
            Ext.Viewport.setMasked({
                xtype: "loadmask",
                message: "Please wait"
            });

            Ext.Ajax.request({
                url: GLOBAL.SERVER_URL,
                method: "POST",
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/json'
                },
                xhr2: true,
                disableCaching: false,
                jsonData: {
                    method: "editProfile",
                    body: data
                },
                success: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    var data = Ext.decode(responce.responseText);
                    console.log(data);
                    d = data.editProfile.data;
                    if (data.editProfile.Error == 1 || data.editProfile.Error == 2) {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.editProfile.Message);
                        }, 100);
                        return;
                    } else {
                        if (UserType == "0") {//Mentor
                            localStorage.setItem("idMentorLoginDetail", Ext.encode(data.editProfile.data));
                        } else if (UserType == "1") {//Mentee
                            localStorage.setItem("idMenteeLoginDetail", Ext.encode(data.editProfile.data));
                        }

                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', "Profile updated sucessfully.");
                        }, 100);
                        Mentor.app.application.getController('LeftMenu').showMenu("", "");
                    }
                },
                failure: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    console.log(responce);
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            });
        }
    },
    btnGetUpdatedProfile: function (popup) {
        var GLOBAL = Mentor.Global;
        var me = this;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
        }

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var data = {
            UserId: UserID,
            UserType: UserType
        };

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getProfile",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getProfile.data;
                if (data.getProfile.Error == 1 || data.getProfile.Error == 2) {
                    if (typeof popup == 'undefined') {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.getProfile.Message);
                        }, 100);
                    }
                    return;
                } else {
                    if (data.getProfile.data.UserType == "0") { //Mentor
                        localStorage.setItem("idMentorLoginDetail", Ext.encode(data.getProfile.data));
                        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
                    } else if (data.getProfile.data.UserType == "1") {//Mentee
                        localStorage.setItem("idMenteeLoginDetail", Ext.encode(data.getProfile.data));
                    }
                    var SkillsStore = Ext.getStore('Skills').load();
                    var SkillsPanel = Ext.getCmp('idSkillsProfile');
                    SkillsPanel.removeAll();

                    var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
                    var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
                    var Skills;
                    if (MentorLoginUser != null) {
                        Ext.getCmp('idUserNameProfileScreen').setHtml(MentorLoginUser.MentorName);
                        Ext.getCmp('userName').setValue(MentorLoginUser.MentorName);
                        Ext.getCmp('email').setValue(MentorLoginUser.MentorEmail);
                        Ext.getCmp('phone').setValue(MentorLoginUser.MentorPhone);
                        //Ext.getCmp('password').setValue(MentorLoginUser.Password);
                        Ext.getCmp('password').setValue('');
                        Ext.getCmp('cnfpassword').setValue('');
                        Skills = MentorLoginUser.Skills;
                        if (MentorLoginUser.MentorImage != "") {
                            Ext.ComponentQuery.query('#id_ProfilePicture')[0].element.setStyle("backgroundImage", 'url("' + MentorLoginUser.MentorImage + '")');
                            Ext.ComponentQuery.query('#id_ProfilePicture')[0].element.setStyle("backgroundRepeat", 'no-repeat');

                            Ext.ComponentQuery.query('#idTopHeaderProfileScreen')[0].element.setStyle("backgroundImage",'linear-gradient( rgba(81,43,128, 0.70), rgba(81,43,128, 0.70) ) ,url("' + MentorLoginUser.MentorImage + '")');
                            Ext.ComponentQuery.query('#idTopHeaderProfileScreen')[0].element.setStyle("backgroundRepeat", 'no-repeat');
                            Ext.ComponentQuery.query('#idTopHeaderProfileScreen')[0].element.setStyle("background-position", 'center');
                            Ext.ComponentQuery.query('#idTopHeaderProfileScreen')[0].element.setStyle("background-size", 'cover');

                            Ext.ComponentQuery.query('#idToolbarProfileScreen')[0].element.setStyle("background", 'transparent');
                            Ext.ComponentQuery.query('#idImagePanelProfileScreen')[0].element.setStyle("background", 'transparent');

                            //Avinash (LeftMenu)
                            Ext.getCmp('idImgUserImageMentorLeftMenuScreen').setSrc(MentorLoginUser.MentorImage);
                             
                        }

                        Ext.getCmp('addInterestProfile').setHtml('Skills Possessed');
                        //when Checkbox
                        //Ext.getCmp('idIsPhoneSMSCapableProfileScreen').setChecked(MentorLoginUser.DisableSMS); // Default Unchecked

                        //when Radio button
                        if (MentorLoginUser.DisableSMS == 1) {
                            Ext.ComponentQuery.query('radiofield[name=idIsPhoneSMSCapableProfileScreen]')[0].setGroupValue(false); // Default Unchecked
                        } else {
                            Ext.ComponentQuery.query('radiofield[name=idIsPhoneSMSCapableProfileScreen]')[0].setGroupValue(true); // Default Unchecked
                        }

                        if (MentorLoginUser.MentorSourceID != null) {
                            Ext.getCmp('idSourceProfileScreen').setValue(MentorLoginUser.MentorSourceID);
                        }
                    } else if (MenteeLoginUser != null) {
                        Ext.getCmp('idUserNameProfileScreen').setHtml(MenteeLoginUser.MenteeName);
                        Ext.getCmp('userName').setValue(MenteeLoginUser.MenteeName);
                        Ext.getCmp('email').setValue(MenteeLoginUser.MenteeEmail);
                        Ext.getCmp('phone').setValue(MenteeLoginUser.MenteePhone);
                        //Ext.getCmp('password').setValue(MenteeLoginUser.Password);
                        Ext.getCmp('password').setValue('');
                        Ext.getCmp('cnfpassword').setValue('');

                        Ext.getCmp('idLabelAddSkillProfile').setHtml("Add Interests");
                        //Ext.getCmp('idButtonAddSkillProfile').setText("Add Interests");
                        Ext.getCmp('addInterestProfile').setHtml('Skills Needed');

                        Skills = MenteeLoginUser.Skills;
                        if (MenteeLoginUser.MenteeImage != "") {
                            Ext.ComponentQuery.query('#id_ProfilePicture')[0].element.setStyle("backgroundImage", 'url("' + MenteeLoginUser.MenteeImage + '")');
                            Ext.ComponentQuery.query('#id_ProfilePicture')[0].element.setStyle("backgroundRepeat", 'no-repeat');

                            Ext.ComponentQuery.query('#idTopHeaderProfileScreen')[0].element.setStyle("backgroundImage",'linear-gradient( rgba(81,43,128, 0.70), rgba(81,43,128, 0.70) ) ,url("' + MenteeLoginUser.MenteeImage + '")');
                            Ext.ComponentQuery.query('#idTopHeaderProfileScreen')[0].element.setStyle("backgroundRepeat", 'no-repeat');
                            Ext.ComponentQuery.query('#idTopHeaderProfileScreen')[0].element.setStyle("background-position", 'center');
                            Ext.ComponentQuery.query('#idTopHeaderProfileScreen')[0].element.setStyle("background-size", 'cover');

                            Ext.ComponentQuery.query('#idToolbarProfileScreen')[0].element.setStyle("background", 'transparent');
                            Ext.ComponentQuery.query('#idImagePanelProfileScreen')[0].element.setStyle("background", 'transparent');
                        }
                        //when Checkbox
                        //Ext.getCmp('idIsPhoneSMSCapableProfileScreen').setChecked(MenteeLoginUser.DisableSMS); // Default Unchecked

                        //when Radio button
                        if (MenteeLoginUser.DisableSMS == 1) {
                            Ext.ComponentQuery.query('radiofield[name=idIsPhoneSMSCapableProfileScreen]')[0].setGroupValue(false); // Default Unchecked
                        } else {
                            Ext.ComponentQuery.query('radiofield[name=idIsPhoneSMSCapableProfileScreen]')[0].setGroupValue(true); // Default Unchecked
                        }

                        if (MenteeLoginUser.MenteeSourceID != null) {
                            Ext.getCmp('idSourceProfileScreen').setValue(MenteeLoginUser.MenteeSourceID);
                        }
                    }

                    for (i = 0; i < SkillsStore.data.length; i++) {
                        var checked = false;
                        var avtiveSkillCLS = 'profileSkillsInActiveCLS';

                        var SkillsArray = Skills.split(',');
                        for (j = 0; j < SkillsArray.length; j++) {
                            var SkillsID = SkillsArray[j].split('~')
                            if (SkillsID[0] == SkillsStore.data.getAt(i).data.SkillID)
                            {
                                checked = true;
                                avtiveSkillCLS = 'profileSkillsActiveCLS';
                                break;
                            }
                        }

                        var radioField = Ext.create('Ext.Button', {
                            //name: 'recorded_stream',
                            id: SkillsStore.data.getAt(i).data.SkillID,
                            text: SkillsStore.data.getAt(i).data.SkillName,
                            //labelCls : 'profileSkillsCLS',
                            //labelWidth: 250,
                            //checked : checked,
                            cls: avtiveSkillCLS,
                            //margin : 2,
                            action: 'addInterest'
                        });
                        SkillsPanel.add(radioField);
                    }
                }
                //me.btnGetUpdatedTeamData();
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                if (typeof popup == 'undefined') {
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            }
        });
    },
    btnGetUpdatedTeamData: function () {
        var GLOBAL = Mentor.Global;
        var me = this;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
        }

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var data = {
            UserId: UserID,
            UserType: UserType
        };

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getTeamDetail",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getTeamDetail.data;
                Ext.getStore('TeamListing').clearData();
                Ext.getStore('TeamDropdown').clearData();
                if (data.getTeamDetail.Error == 1 || data.getTeamDetail.Error == 2) {
                    if (typeof popup == 'undefined') {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.getTeamDetail.Message);
                        }, 100);
                    }
                    return;
                } else {
                    localStorage.setItem("TeamAllDetails", Ext.encode(data.getTeamDetail.data));

                    if (data.getTeamDetail.data.AllTeamDetail == false) {
                        var defaultVal = {TeamId: "", TeamName: "-- No Team Available --"};
                        Ext.getStore('TeamDropdown').add(defaultVal);
                    } else {
                        var defaultVal = {TeamId: "", TeamName: "-- Select Team --"};
                        Ext.getStore('TeamDropdown').add(defaultVal);
                        Ext.getStore('TeamDropdown').add(data.getTeamDetail.data.AllTeamDetail);
                    }

                    Ext.getStore('TeamListing').add(data.getTeamDetail.data.AllTeamDetail);

                    try{
                        Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("resources/images/group_avatar.png")');
                        if (data.getTeamDetail.data.OwnTeamDetail != false) {
                            Ext.getCmp('idTeamNameMenuLeftMenuScreen').setHtml(data.getTeamDetail.data.OwnTeamDetail[0].TeamName);
                            Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("' + data.getTeamDetail.data.OwnTeamDetail[0].TeamFullImage + '")');
                            Ext.getCmp('idTeamNameProfileScreen').setHtml(data.getTeamDetail.data.OwnTeamDetail[0].TeamName);
                           
                        } else if (data.getTeamDetail.data.MemberTeamDetail != false) {
                            Ext.getCmp('idTeamNameMenuLeftMenuScreen').setHtml(data.getTeamDetail.data.MemberTeamDetail[0].TeamName);
                            Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("' + data.getTeamDetail.data.MemberTeamDetail[0].TeamFullImage + '")');
                            Ext.getCmp('idTeamNameProfileScreen').setHtml(data.getTeamDetail.data.MemberTeamDetail[0].TeamName);
                            
                        } else {
                            Ext.getCmp('idTeamNameMenuLeftMenuScreen').setHtml('No Team');
                            Ext.getCmp('idTeamNameProfileScreen').setHtml('No Team');
                            
                        }
                        
                    }catch (e){
                        console.log(e);
                    }
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                if (typeof popup == 'undefined') {
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            }
        });
    },
    //Get Source of SignUp
    getSourceList: function () {
        var me = this;
        var GLOBAL = Mentor.Global;

        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserType;
        if (MentorLoginUser != null) {
            UserType = "0";
        } else if (MenteeLoginUser != null) {
            UserType = "1";
        }
        var data = {
            usertype: UserType
        };

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });
        Ext.Ajax.request({
            url: GLOBAL.URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getSourceList",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false);
                var data = Ext.decode(responce.responseText);
                console.log(data);
                if (data.getSourceList.Error == 1 || data.getSourceList.Error == 2) {
                    Ext.getStore('SchoolDetail').removeAll();
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getSourceList.Message);
                    }, 100);
                    return;
                } else {
                    Ext.getStore('SourceList').removeAll();
                    /*Ext.getStore('SchoolDetail').insert(0,{"subdomain_id":"-1","subdomain_name":"Select School","school_name":"Select School"});*/
                    Ext.getStore('SourceList').add(data.getSourceList.data);

                    //var SourceListStore = Ext.getStore('SourceList').load();//idSchoolDetailSelectFieldLoginScreen

                    var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
                    var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));

                    if (MentorLoginUser != null) {
                        if (MentorLoginUser.MentorSourceID != null)
                            Ext.getCmp('idSourceProfileScreen').setValue(MentorLoginUser.MentorSourceID);
                    } else if (MenteeLoginUser != null) {
                        if (MenteeLoginUser.MenteeSourceID != null) {
                            Ext.getCmp('idSourceProfileScreen').setValue(MenteeLoginUser.MenteeSourceID);
                        }
                    }
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    ValidateEmail: function (mail) {
        if (/^\w+([\+.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
        {
            return (true);
        }
        return (false);
        //return (true);
    },
    btnSaveProfile: function () {

    },
    /***
     Call this api after login
    ****/
    getUpdatedTeamData: function () {
        var GLOBAL = Mentor.Global;
        var me = this;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
        }

      /*  Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });*/

        var data = {
            UserId: UserID,
            UserType: UserType
        };

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getTeamDetail",
                body: data
            },
            success: function (responce) {
                //Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getTeamDetail.data;
                if (data.getTeamDetail.Error == 1 || data.getTeamDetail.Error == 2) {
                    if (typeof popup == 'undefined') {
                        Ext.Function.defer(function () {
                            
                        }, 100);
                    }
                    return;
                } else {
                    localStorage.setItem("TeamAllDetails", Ext.encode(data.getTeamDetail.data));

                    if (data.getTeamDetail.data.AllTeamDetail == false) {
                        var defaultVal = {TeamId: "", TeamName: "-- No Team Available --"};
                        Ext.getStore('TeamDropdown').add(defaultVal);
                    } else {
                        var defaultVal = {TeamId: "", TeamName: "-- Select Team --"};
                        Ext.getStore('TeamDropdown').add(defaultVal);
                        Ext.getStore('TeamDropdown').add(data.getTeamDetail.data.AllTeamDetail);
                    }

                    Ext.getStore('TeamListing').add(data.getTeamDetail.data.AllTeamDetail);

                    
                }
            },
            failure: function (responce) {
                //Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                if (typeof popup == 'undefined') {
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        //Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            }
        });
    }

});