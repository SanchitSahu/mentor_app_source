Ext.define('Mentor.controller.AppDetail', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {},
        control: {
            "mentorprofile button[action=openWebview]": {
                tap: "openWebview"
            },
            "mentorreviewform button[action=btnBackMentorReviewForm]": {
                tap: "btnBackMentorReviewForm"
            },
            "meetinghistory button[action=btnAddMeetingDetail]": {
                tap: "btnAddMeetingDetail"
            },
            "mentorPendingMeetingDetail button[action=btnAddMeetingDetail]": {
                tap: "btnAddMeetingDetail"
            },
            "meetinghistory button[action=btnProfileMeetingDetail]": {
                tap: "btnProfileMeetingDetail"
            },
            "mentorPendingRequest button[action=btnGetPendingRequest]": {
                tap: "btnGetPendingRequest"
            },
            "meetinghistory button[action=btnRefreshMeetingHistory]": {
                tap: "btnRefreshMeetingHistory"
            },
            "teamlisting button[action=btnRefreshTeamListing]": {
                tap: "btnRefreshTeamListing"
            },
            "meetinghistorymentee button[action=btnProfileMeetingDetail]": {
                tap: "btnProfileMeetingDetail"
            },
            "meetinghistorymentee button[action=btnInviteForMeeting]": {
                tap: "btnInviteForMeeting"
            },
            "meetinghistorymentee button[action=btnRefreshMeetingHistory]": {
                tap: "btnRefreshMeetingHistory"
            },
            "mentorreviewform button[action=doSubmitMentorReviewForm]": {
                tap: "doSubmitMentorReviewForm"
            },
            "menteereviewform button[action=doSubmitMenteeReviewForm]": {
                tap: "doSubmitMenteeReviewForm"
            },
            "menteereviewform button[action=btnBackMenteeReviewForm]": {
                tap: "btnBackMenteeReviewForm"
            },
            "mentorprofile button[action=btnBackMentorProfile]": {
                tap: "btnBackMentorProfile"
            },
            "mentorprofile button[action=btnAddSkills]": {
                tap: "btnAddSkills"
            },
            "addeditteam button[action=btnAddTeam]": {
                tap: "btnAddTeam"
            },
            "invitementor button[action=btnBackInviteBack]": {
                tap: "btnBackInviteBack"
            },
            "mentorPendingRequest button[action=btnBackMentorPendingRequest]": {
                tap: "btnBackMentorPendingRequest"
            },
            "meetinghistory button[action=btnBackMentorInvitesTabView]": {
                tap: "btnBackMentorInvitesTabView"
            },
            "acceptRejectPendingRequest button[action=doAcceptRequest]": {
                tap: "doAcceptRequest"
            },
            "acceptRejectPendingRequest button[action=doDeclineRequest]": {
                tap: "doDeclineRequest"
            },
            "mentorPendingMeetingDetail button[action=doReschedule]": {
                tap: "doReschedule"
            },
            "mentorPendingMeetingDetail button[action=doCancel]": {
                tap: "doCancel"
            },
            "mentorRescheduleMeeting button[action=doRescheduleRequest]": {
                tap: "doRescheduleRequest"
            },
            "mentorCancelMeeting button[action=btnCancelPendingMeeting]": {
                tap: "btnCancelPendingMeeting"
            },
            "acceptRejectPendingRequest button[action=btnBackAcceptRejectPendingRequest]": {
                tap: "btnBackAcceptRejectPendingRequest"
            },
            "waitscreen button[action=btnBackWaitScreen]": {
                tap: "btnBackWaitScreen"
            },
            "waitscreen button[action=AcceptRescheduledMeetingByMentor]": {
                tap: "AcceptRescheduledMeetingByMentor"
            },
            "waitscreen button[action=AcceptAcceptedMeetingByMentor]": {
                tap: "AcceptAcceptedMeetingByMentor"
            },
            "invitementor button[action=btnSendInviteMentorScreen]": {
                tap: "btnInviteMentor"
            },
            "invitementee button[action=btnSendInviteMenteeScreen]": {
                tap: "btnInviteMentee"
            },
            "invitementor button[action=btnGotoWaitScreenInviteMentorScreen]": {
                tap: "btnGotoWaitScreenInviteMentorScreen"
            },
            "waitscreen button[action=btnRefreshWaitScreen]": {
                tap: "getMettingWaitScreenInfo"
            },
            "waitscreen button[action=btnHistoryScreenWaitScreen]": {
                tap: "btnHistoryScreenWaitScreen"
            },
            "waitscreen button[action=btnCancelMeetingMentee]": {
                tap: "btnCancelMeetingMentee"
            },
            "waitscreen button[action=btnRescheduleMeetingMentee]": {
                tap: "btnRescheduleMeetingMentee"
            },
            "waitscreen button[action=btnAcceptMeetingMentee]": {
                tap: "btnAcceptMeetingMentee"
            },
            "waitscreen button[action=btnDeclineMeetingMentee]": {
                tap: "btnDeclineMeetingMentee"
            },
            "meetinghistorymentee button[action=btnInviteStatusMeetingHistory]": {
                tap: "btnGotoWaitScreenInviteMentorScreen"
            },
            'meetinghistory searchfield[itemId=searchBoxMentorMettingHistory]': {
                clearicontap: 'onClearSearchMentorMettingHistory',
                keyup: 'onSearchKeyUpMentorMettingHistory'
            },
            'teamlisting searchfield[itemId=searchBoxMentorTeams]': {
                clearicontap: 'onClearSearchMentorTeams',
                keyup: 'onSearchKeyUpMentorTeams'
            },
            'meetinghistorymentee searchfield[itemId=searchBoxMenteeMettingHistory]': {
                clearicontap: 'onClearSearchMenteeMettingHistory',
                keyup: 'onSearchKeyUpMenteeMettingHistory'
            },
            "mentorPendingMeetingDetail button[action=btnBackMentorPendingMeetingDetail]": {
                tap: "btnBackMentorPendingMeetingDetail"
            },
            "mentorRescheduleMeeting button[action=btnBackMentorRescheduleMeeting]": {
                tap: "btnBackMentorRescheduleMeeting"
            },
            "mentorCancelMeeting button[action=btnBackMentorCancelMeeting]": {
                tap: "btnBackMentorCancelMeeting"
            },
            "acceptresponses button[action=btnBackAcceptDeclineResponses]": {
                tap: "btnBackAcceptDeclineResponses"
            },
            "declineresponses button[action=btnBackAcceptDeclineResponses]": {
                tap: "btnBackAcceptDeclineResponses"
            },
            "acceptresponses button[action=doSubmitAcceptRequest]": {
                tap: "doSubmitAcceptRequest"
            },
            "acceptresponses button[action=doSubmitIssueInviteRequest]": {
                tap: "doSubmitIssueInviteRequest"
            },
            "declineresponses button[action=doSubmitDeclineRequest]": {
                tap: "doSubmitDeclineRequest"
            },
            "button[action=addnotes]": {
                tap: "addnotes"
            },
            "mentor_notes button[action=btnBackMentorNotes]": {
                tap: "btnBackMentorNotes"
            },
            "subadminwebview button[action=btnBackMentorNotes]": {
                tap: "btnBackMentorNotes"
            },
            "mentor_notes button[action=btnRefreshUserNotes]": {
                tap: "btnRefreshUserNotes"
            },
            'mentor_notes searchfield[itemId=searchBoxMentorNotes]': {
                clearicontap: 'onClearSearchUserNotes',
                keyup: 'onSearchKeyUpUserNotes'
            },
            'addnotepopview button[action=doSubmitAddEditNote]': {
                tap: 'doSubmitAddEditNote'
            },
            'addcommentpopview button[action=doSubmitAddEditComment]': {
                tap: 'doSubmitAddEditComment'
            },
            'instantMeeting button[action=doBtnNextInstantMeeting]': {
                tap: 'doBtnNextInstantMeeting'
            },
            'instantMeeting button[action=btnBackInstantMeetingScreen]': {
                tap: 'btnBackInstantMeetingScreen'
            },
            'action_instant_meeting button[action=btnBackActionInstantMeeting]': {
                tap: 'btnBackActionInstantMeeting'
            },
            'action_mentor_instant_meeting button[action=btnBackActionMentorInstantMeeting]': {
                tap: 'btnBackActionMentorInstantMeeting'
            },
            'teamlisting button[action=btnBackActionTeamListing]': {
                tap: 'btnBackActionTeamListing'
            },
            'teamdetail button[action=btnBackActionTeamDetail]': {
                tap: 'btnBackActionTeamDetail'
            },
            'teamdetail button[action=teamDetailJoinTeam]': {
                tap: 'teamDetailJoinTeam'
            },
            'action_instant_meeting [action=btnNextActionInstantMeeting]': {
                tap: 'btnNextActionInstantMeeting'
            },
            'action_mentor_instant_meeting [action=btnSubmitActionInstantMeeting]': {
                tap: 'btnSubmitActionInstantMeeting'
            },
            'addeditteam [action=btnViewTeamMembers]': {
                tap: 'btnViewTeamMembers'
            }, 'teamdetail [action=btnViewTeamMembers]': {
                tap: 'btnViewTeamMembers'
            },
            'viewteammembers [action=btnRefreshTeamMembers]': {
                tap: 'btnRefreshTeamMembers'
            },
            'viewteammembers searchfield[itemId=searchBoxTeamMember]': {
                clearicontap: 'onClearSearchTeam',
                keyup: 'onSearchKeyUpTeam'
            },
            'addteammembers [action=btnSubmitAddTeamMember]': {
                tap: 'btnSubmitAddTeamMember'
            }
        }
    },
    getMeetingListing: function () {
        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));

        var data = {
            MentorID: MentorLoginUser.Id
        };
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getMeetingList",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false);
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getMeetingList.data;
                if (data.getMeetingList.Error == 1 || data.getMeetingList.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getMeetingList.Message);
                    }, 100);
                    return;
                } else {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getMeetingList.Message, function () {

                        });
                    }, 100);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    btnSubmitAddTeamMember: function () {
        var GLOBAL = Mentor.Global;

        var TeamId = Ext.getCmp('btnSubmitAddTeamMember').getItemId();
        var selectedMentors = Ext.getCmp('idMentorActionItem');
        var selectedMentees = Ext.getCmp('idMenteeTeamActionItem');

        var selectedMentorID = '', selectedMentorName = '';
        var selectedMenteeID = '', selectedMenteeName = '';

        var selectedMentorChecked = 0;
        var selectedMenteeChecked = 0;

        for (i = 0; i < selectedMentees.getItems().length; i++) {
            if (selectedMentees.getItems().getAt(i).isChecked()) {
                if (selectedMenteeChecked == 0) {
                    selectedMenteeID = selectedMentees.getItems().getAt(i).getItemId();
                    selectedMenteeName = selectedMentees.getItems().getAt(i).getValue();
                    selectedMenteeChecked++;
                } else {
                    selectedMenteeID = selectedMenteeID + "," + selectedMentees.getItems().getAt(i).getItemId();
                    selectedMenteeName = selectedMenteeName + ";" + selectedMentees.getItems().getAt(i).getValue();
                    selectedMenteeChecked++;
                }
            }
        }

        for (var i = 0; i < selectedMentors.getItems().length; i++) {
            if (selectedMentors.getItems().getAt(i).isChecked()) {
                if (selectedMentorChecked == 0) {
                    selectedMentorID = selectedMentors.getItems().getAt(i).getItemId();
                    selectedMentorName = selectedMentors.getItems().getAt(i).getValue();
                    selectedMentorChecked++;
                } else {
                    selectedMentorID = selectedMentorID + "," + selectedMentors.getItems().getAt(i).getItemId();
                    selectedMentorName = selectedMentorName + ";" + selectedMentors.getItems().getAt(i).getValue();
                    selectedMentorChecked++;
                }
            }
        }

        console.log(selectedMenteeID);
        console.log(selectedMenteeName);
        console.log(selectedMentorID);
        console.log(selectedMentorName);

        if (selectedMenteeID == '' && selectedMentorID == '') {
            Ext.Msg.alert('MELS', "Please select at least one Member");
            return false;
        }

        var data = {
            TeamID: TeamId,
            MenteeID: selectedMenteeID,
            MentorID: selectedMentorID
        };

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "addTeamMember",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false);
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.addTeamMember.data;
                if (data.addTeamMember.Error == 1 || data.addTeamMember.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.addTeamMember.Message);
                    }, 100);
                    return;
                } else {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.addTeamMember.Message, function () {
                            var View = Mentor.Global.NavigationStack.pop();
                            var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                            mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
                        });
                    }, 100);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    btnAddTeamMembers: function (teamId) {

        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("addteammembers");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "addteammembers"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

        Ext.getCmp('btnSubmitAddTeamMember').setItemId(teamId);
    },
    btnViewTeamMembers: function () {
        // alert(Ext.getCmp('idButtonViewTeamMember').getItemId());
        this.getTeamMembers();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("viewteammembers");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "viewteammembers"});
        }
        var buttonID = Ext.getCmp('idButtonViewTeamMember').getData();
        Ext.getCmp('viewteammembers').removeCls('showListing');
        if (buttonID == 'sanchit') {
            console.log('-->' + buttonID);
            Ext.getCmp('addTeamMembersToTeam').hide();
            Ext.getCmp('viewteammembers').addCls('showListing');

            // Ext.getCmp('searchBoxTeam').hide();

        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

        Ext.getCmp('addTeamMembersToTeam').setItemId(Ext.getCmp('idButtonViewTeamMember').getItemId());
    },

    getTeamMembers: function () {
        var GLOBAL = Mentor.Global;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
        }

        var TeamID = Ext.getCmp('idButtonViewTeamMember').getItemId();

        var data = {
            UserID: UserID,
            UserType: UserType,
            TeamId: TeamID
        };

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getTeamMembers",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getTeamMembers.data;
                if (data.getTeamMembers.Error == 1 || data.getTeamMembers.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getTeamMembers.Message);
                    }, 100);
                    return;
                } else {
                    Ext.Function.defer(function () {
                        Ext.getStore('TeamMembers').clearData();
                        Ext.getStore('TeamMembers').add(data.getTeamMembers.data);
                    }, 100);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    teamDetailJoinTeam: function () {
        var GLOBAL = Mentor.Global;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
        }

        var TeamID = Ext.getCmp('teamDetailJoin').getItemId();

        var data = {
            UserID: UserID,
            UserType: UserType,
            TeamId: TeamID
        };

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "joinTeam",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.joinTeam.data;
                if (data.joinTeam.Error == 1 || data.joinTeam.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.joinTeam.Message);
                    }, 100);
                    return;
                } else {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.joinTeam.Message, function () {
                            /*var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
                             if (MentorLoginUser != null) {
                             var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                             var mainMenu = mainPanel.down("mentorBottomTabView");
                             if (!mainMenu) {
                             mainMenu = mainPanel.add({xtype: "mentorBottomTabView"});
                             }
                             Ext.getCmp('idMentorBottomTabView').setActiveItem(3);
                             Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
                             mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
                             } else {
                             var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                             var mainMenu = mainPanel.down("menteeBottomTabView");
                             if (!mainMenu) {
                             mainMenu = mainPanel.add({xtype: "menteeBottomTabView"});
                             }
                             Ext.getCmp('idMenteeBottomTabView').setActiveItem(2);
                             Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
                             mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
                             
                             
                             }*/
                            Mentor.Global.NavigationStack.pop();
                            var View = Mentor.Global.NavigationStack.pop();
                            var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                            mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
                        });
                    }, 100);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    leaveTeam: function (teamId, redirectToAddScreen, gotoPage) {
        var GLOBAL = Mentor.Global;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
        }

        var data = {
            UserID: UserID,
            UserType: UserType,
            TeamId: teamId
        };

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "leaveTeam",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.leaveTeam.data;
                if (data.leaveTeam.Error == 1 || data.leaveTeam.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.leaveTeam.Message);
                    }, 100);
                    return;
                } else {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.leaveTeam.Message, function () {
                            if (redirectToAddScreen) {
                                if (gotoPage == 'add') {
                                    Mentor.app.application.getController('AppDetail').addEditTeam();
                                } else if (gotoPage == 'join') {
                                    Mentor.app.application.getController('AppDetail').teamListing();
                                }
                            } else {
                                Mentor.app.application.getController('UpdateProfilePicture').btnGetUpdatedTeamData();
                            }
                        });
                    }, 100);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    displayTeamDetail: function (view, index, item, record, e) {
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
        }

        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("teamdetail");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "teamdetail"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});


        record = view.getStore().getAt(index).data;
        console.log(record);

        Ext.ComponentQuery.query('#id_TeamProfilePictureDisplay')[0].element.setStyle("backgroundImage", 'url("' + record.TeamFullImage + '")');
        // Ext.ComponentQuery.query('#id_TeamOwnerPictureDisplay')[0].element.setStyle("backgroundImage", 'url("' + record.OwnerFullImage + '")');
        Ext.getCmp('idTeamNameDetailScreen').setHtml(record.TeamName);
        Ext.getCmp('teamDetailJoin').setItemId(record.TeamId);
        Ext.getCmp('idTeamDescDetailScreen').setValue(record.TeamDescription);
        // Ext.getCmp('idTeamOwnerNameDetailScreen').setHtml(record.OwnerName);
        Ext.getCmp('idTeamOpenSlotsDetailScreen').setValue(record.MaxMembers);
        Ext.getCmp('idButtonViewTeamMember').setItemId(record.TeamId);
        Ext.getCmp('teamDetailJoin').show();
        Ext.getCmp('idToolbarTeamDetail').setTitle('Join a Team');
        Ext.getCmp('idSkillsLabelTeamReqSkillsDetailScreen').setHtml('One or more skills required by ' + record.TeamName + ' members');


        var SkillsStore = Ext.getStore('Skills').load();
        var SkillsPanel = Ext.getCmp('idTeamReqSkillsDetailScreen');
        SkillsPanel.removeAll();
        var Skills = record.Skills;

        for (i = 0; i < SkillsStore.data.length; i++) {
            var SkillsArray = Skills.split(',');
            for (j = 0; j < SkillsArray.length; j++) {
                var SkillsID = SkillsArray[j].split('~');
                if (SkillsID[0] == SkillsStore.data.getAt(i).data.SkillID) {
                    var radioField = Ext.create('Ext.Button', {
                        text: SkillsStore.data.getAt(i).data.SkillName,
                        cls: 'profileSkillsActiveCLS',
                        disabled: true
                    });
                    SkillsPanel.add(radioField);
                    break;
                }
            }
        }
    },

    /***
     Display Team detail to Joim meber list
     ****/
    viewTeamDetail: function (teamId) {

        if (typeof teamId != 'undefined') {
            var AllTeamDetail = Ext.decode(localStorage.getItem('TeamAllDetails')).AllTeamDetail;
            for (i = 0; i < AllTeamDetail.length; i++) {
                if (AllTeamDetail[i].TeamId == teamId) {

                    var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                    var mainMenu = mainPanel.down("teamdetail");
                    if (!mainMenu) {
                        mainMenu = mainPanel.add({xtype: "teamdetail"});
                    }
                    Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
                    mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});


                    record = AllTeamDetail[i];
                    console.log(record);

                    Ext.ComponentQuery.query('#id_TeamProfilePictureDisplay')[0].element.setStyle("backgroundImage", 'url("' + record.TeamFullImage + '")');
                    // Ext.ComponentQuery.query('#id_TeamOwnerPictureDisplay')[0].element.setStyle("backgroundImage", 'url("' + record.OwnerFullImage + '")');
                    Ext.getCmp('idTeamNameDetailScreen').setHtml(record.TeamName);
                    Ext.getCmp('teamDetailJoin').setItemId(record.TeamId);
                    Ext.getCmp('idTeamDescDetailScreen').setValue(record.TeamDescription);
                    // Ext.getCmp('idTeamOwnerNameDetailScreen').setHtml(record.OwnerName);
                    Ext.getCmp('idTeamOpenSlotsDetailScreen').setValue(record.MaxMembers);
                    Ext.getCmp('idButtonViewTeamMember').setItemId(record.TeamId);
                    Ext.getCmp('teamDetailJoin').hide();
                    Ext.getCmp('idToolbarTeamDetail').setTitle('Team Detail');
                    Ext.getCmp('idSkillsLabelTeamReqSkillsDetailScreen').setHtml('One or more skills required by ' + record.TeamName + ' members');

                    var SkillsStore = Ext.getStore('Skills').load();
                    var SkillsPanel = Ext.getCmp('idTeamReqSkillsDetailScreen');
                    SkillsPanel.removeAll();
                    var Skills = record.Skills;

                    for (i = 0; i < SkillsStore.data.length; i++) {
                        var SkillsArray = Skills.split(',');
                        for (j = 0; j < SkillsArray.length; j++) {
                            var SkillsID = SkillsArray[j].split('~');
                            if (SkillsID[0] == SkillsStore.data.getAt(i).data.SkillID) {
                                var radioField = Ext.create('Ext.Button', {
                                    text: SkillsStore.data.getAt(i).data.SkillName,
                                    cls: 'profileSkillsActiveCLS',
                                    disabled: true
                                });
                                SkillsPanel.add(radioField);
                                break;
                            }
                        }
                    }
                    break;
                }

            }
        } else {
            Ext.Msg.alert('MELS', "Not able to fetch Team detail, Please try later");
        }

    },

    teamListing: function () {
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("teamlisting");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "teamlisting"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
    },
    btnAddTeam: function () {
        var GLOBAL = Mentor.Global;
        var SkillsPanel = Ext.getCmp('idSkillsTeam');
        var SkillID;
        var SkillName;
        var isChecked = 0;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;

        var TeamName = Ext.getCmp('idTeamName').getValue();
        var TeamDescription = Ext.getCmp('idTeamDesc').getValue();
        var MaxMemberCount = Ext.getCmp('idSliderValue').getValue();
        var TeamImage = Ext.getCmp('id_TeamProfilePictureHidden').getHtml();

        if (TeamName == "") {
            Ext.Msg.alert('MELS', "Please enter Team Name");
            return;
        } else if (TeamDescription == "") {
            Ext.Msg.alert('MELS', "Please enter Team Description");
            return;
        } else if (MaxMemberCount == "") {
            Ext.Msg.alert('MELS', "Please enter Max Member Count");
            return;
        }

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
        }

        for (i = 0; i < SkillsPanel.getItems().length; i++) {
            if (SkillsPanel.getItems().getAt(i).getCls() == 'profileSkillsActiveCLS') {
                if (isChecked == 0) {
                    SkillID = SkillsPanel.getItems().getAt(i).getId().split('_')[1];
                    SkillName = SkillsPanel.getItems().getAt(i).getText();
                    isChecked = isChecked + 1;
                } else {
                    SkillID = SkillID + "," + SkillsPanel.getItems().getAt(i).getId().split('_')[1];
                    SkillName = SkillName + "," + SkillsPanel.getItems().getAt(i).getText();
                    isChecked = isChecked + 1;
                }
            }
        }

        if (isChecked <= 0) {
            Ext.Msg.alert('MELS', "Please select at least one skill");
        } else {
            var data = {
                UserId: UserID,
                UserType: UserType,
                SkillID: SkillID,
                TeamName: TeamName,
                TeamDescription: TeamDescription,
                MaxMemberCount: MaxMemberCount,
                TeamId: '',
                TeamImage: TeamImage
            };

            var ownerTeamInfo = Ext.decode(localStorage.getItem('TeamAllDetails'));
            if (ownerTeamInfo.OwnTeamDetail != false) {
                data.TeamId = ownerTeamInfo.OwnTeamDetail[0].TeamId;
            }

            Ext.Viewport.setMasked({
                xtype: "loadmask",
                message: "Please wait"
            });

            Ext.Ajax.request({
                url: GLOBAL.SERVER_URL,
                method: "POST",
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/json'
                },
                xhr2: true,
                disableCaching: false,
                jsonData: {
                    method: "addTeam",
                    body: data
                },
                success: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    var data = Ext.decode(responce.responseText);
                    console.log(data);
                    d = data.addTeam.data;
                    if (data.addTeam.Error == 1 || data.addTeam.Error == 2) {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.addTeam.Message);
                        }, 100);
                        return;
                    } else {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.addTeam.Message, function () {
                                /* if (UserType == 0) {
                                 var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                                 var mainMenu = mainPanel.down("mentorBottomTabView");
                                 if (!mainMenu) {
                                 mainMenu = mainPanel.add({xtype: "mentorBottomTabView"});
                                 }
                                 Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
                                 mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
                                 Ext.getCmp('idMentorBottomTabView').setActiveItem(Ext.getCmp('idMentorBottomTabView').getItems().length - 2);
                                 } else {
                                 var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                                 var mainMenu = mainPanel.down("menteeBottomTabView");
                                 if (!mainMenu) {
                                 mainMenu = mainPanel.add({xtype: "menteeBottomTabView"});
                                 }
                                 Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
                                 mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
                                 Ext.getCmp('idMenteeBottomTabView').setActiveItem(Ext.getCmp('idMenteeBottomTabView').getItems().length - 2);
                                 }*/
                                var View = Mentor.Global.NavigationStack.pop();
                                var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                                mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
                            });
                        }, 100);
                    }
                },
                failure: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    console.log(responce);
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            });
        }
    },
    deleteTeam: function (TeamId, redirect) {
        var GLOBAL = Mentor.Global;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
        }

        var data = {
            UserId: UserID,
            UserType: UserType,
            TeamId: TeamId
        };

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "deleteTeam",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.deleteTeam.data;
                if (data.deleteTeam.Error == 1 || data.deleteTeam.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.deleteTeam.Message);
                    }, 100);
                    return;
                } else {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.deleteTeam.Message, function () {
                            if (redirect) {
                                Mentor.app.application.getController('AppDetail').teamListing();
                            } else {
                                Mentor.app.application.getController('UpdateProfilePicture').btnGetUpdatedTeamData();
                            }
                        });
                    }, 100);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    teamMenuPopup: function (action) {
        var popup = new Ext.Panel({
            id: "TeamMenuPopupView",
            floating: true,
            centered: true,
            modal: true,
            hideOnMaskTap: true,
            autoDestroy: true,
            width: 310,
            padding: '0 0 10 0',
            //height: 335,
            items: [
                {
                    xtype: 'teammenupopupview',
                    id: 'idTeammenupopupview'
                }
            ],
            listeners: {
                hide: function (a, b) {
                    popup.hide();
                    Ext.Viewport.remove(popup);
                }
            }
        });

        var getTeamDetail = Ext.decode(localStorage.getItem('TeamAllDetails'));
        var btnCount = 0;

        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));

        if (getTeamDetail.OwnTeamDetail != false) {
            btnCount = 2;
            if (getTeamDetail.AllTeamDetail != false) {
                btnCount = 3;
                Ext.getCmp('teamMenuJoin').setText('Switch Team');
                Ext.getCmp('teamMenuJoin').show();
            } else {
                btnCount = 2;
                Ext.getCmp('teamMenuJoin').hide();
            }
            Ext.getCmp('teamMenuChange').hide();
            Ext.getCmp('teamMenuLeave').hide();
            Ext.getCmp('teamMenuAdd').hide();
            Ext.getCmp('teamMenuEdit').show();
            Ext.getCmp('teamMenuView').hide(); // Hide when team is created by login user
            Ext.getCmp('teamMenuEdit').setItemId(getTeamDetail.OwnTeamDetail[0].TeamId);
            Ext.getCmp('teamMenuDelete').show();
            Ext.getCmp('teamMenuDelete').setItemId(getTeamDetail.OwnTeamDetail[0].TeamId);

        } else if (getTeamDetail.MemberTeamDetail != false) {
            btnCount = 3;
            Ext.getCmp('teamMenuJoin').hide();
            if (getTeamDetail.AllTeamDetail.length > 1) {
                btnCount = 3;
                Ext.getCmp('teamMenuChange').show()
                Ext.getCmp('teamMenuChange').setItemId(getTeamDetail.MemberTeamDetail[0].TeamId)
            } else {
                btnCount = 2;
                Ext.getCmp('teamMenuChange').hide();
            }

            Ext.getCmp('teamMenuLeave').show().setText("Leave " + getTeamDetail.MemberTeamDetail[0].TeamName);
            Ext.getCmp('teamMenuLeave').setItemId(getTeamDetail.MemberTeamDetail[0].TeamId);
            Ext.getCmp('teamMenuView').show(); // Show to other team members
            Ext.getCmp('teamMenuView').setItemId(getTeamDetail.MemberTeamDetail[0].TeamId);

            if (MentorLoginUser != null) {
                Ext.getCmp('teamMenuAdd').hide();
                btnCount = btnCount - 1;
            } else {
                Ext.getCmp('teamMenuAdd').show();
            }
            Ext.getCmp('teamMenuEdit').hide();
            Ext.getCmp('teamMenuDelete').hide();
        } else {
            Ext.getCmp('teamMenuChange').hide();
            Ext.getCmp('teamMenuLeave').hide();
            Ext.getCmp('teamMenuDelete').hide();
            Ext.getCmp('teamMenuEdit').hide();
            Ext.getCmp('teamMenuView').hide();

            if (getTeamDetail.AllTeamDetail != false) {
                btnCount = 2;
                Ext.getCmp('teamMenuJoin').show();
            } else {
                btnCount = 1;
                Ext.getCmp('teamMenuJoin').hide();
            }
            if (MentorLoginUser != null) {
                Ext.getCmp('teamMenuAdd').hide();
                btnCount = btnCount - 1;
            } else {
                Ext.getCmp('teamMenuAdd').show();
            }
        }

//        Ext.getCmp('teamMenuJoin').show();
//        Ext.getCmp('teamMenuChange').show();
//        Ext.getCmp('teamMenuLeave').show();
//        Ext.getCmp('teamMenuAdd').show();
//        Ext.getCmp('teamMenuDelete').show();
//        btnCount = 5;

        //popup.setHeight((++btnCount * 69) + 5);

        return popup;
    },
    getAllMentorsMenteesForTeam: function () {
        var GLOBAL = Mentor.Global;

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getAllMentorsForTeam",
                body: {}
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                if (data.getAllMentorsForTeam.Error == 0) {
                    Ext.getStore('MentorForTeam').clearData();
                    Ext.getStore('MentorForTeam').add(data.getAllMentorsForTeam.data);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                if (typeof popup == 'undefined') {
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            }
        });
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getAllMenteesForTeam",
                body: {}
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                if (data.getAllMenteesForTeam.Error == 0) {
                    Ext.getStore('MenteeForTeam').clearData();
                    Ext.getStore('MenteeForTeam').add(data.getAllMenteesForTeam.data);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                if (typeof popup == 'undefined') {
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            }
        });
    },
    addEditTeam: function (teamId) {
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("addeditteam");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "addeditteam"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

        Ext.getCmp('idTeamName').setValue('');
        Ext.getCmp('idTeamDesc').setValue('');
        Ext.getCmp('idRequiredMembers').setValue(3);
        Ext.getCmp('idSliderValue').setValue(3);
        Ext.ComponentQuery.query('#id_TeamProfilePicture')[0].element.setStyle("backgroundImage", 'url("resources/images/group_avatar.png")');

        Ext.getCmp('id_TeamProfilePictureHidden').setHtml('group_avatar.png');
        Ext.getCmp('addEditTeamScreenTitle').setTitle('Add a New Team');
        Ext.getCmp('idButtonViewTeamMember').setHidden(true);

        if (typeof teamId != 'undefined') {
            var ownerTeamInfo = Ext.decode(localStorage.getItem('TeamAllDetails'));
            Ext.getCmp('addEditTeamScreenTitle').setTitle('Edit ' + ownerTeamInfo.OwnTeamDetail[0].TeamName);
            Ext.getCmp('idTeamName').setValue(ownerTeamInfo.OwnTeamDetail[0].TeamName);
            Ext.getCmp('idTeamDesc').setValue(ownerTeamInfo.OwnTeamDetail[0].TeamDescription);
            Ext.getCmp('idRequiredMembers').setValue(ownerTeamInfo.OwnTeamDetail[0].MaxMembers);
            Ext.getCmp('idSliderValue').setValue(ownerTeamInfo.OwnTeamDetail[0].MaxMembers);
            Ext.getCmp('idButtonViewTeamMember').setItemId(teamId).setHidden(false);
            Ext.ComponentQuery.query('#id_TeamProfilePicture')[0].element.setStyle("backgroundImage", 'url("' + ownerTeamInfo.OwnTeamDetail[0].TeamFullImage + '")');
            Ext.getCmp('id_TeamProfilePictureHidden').setHtml(ownerTeamInfo.OwnTeamDetail[0].TeamImage);
            Ext.getCmp('idButtonViewTeamMember').setItemId(teamId);
            Ext.getCmp('idAddEditSkillForTeam').setHtml('One or more skills required by ' + ownerTeamInfo.OwnTeamDetail[0].TeamName + ' members');
        }
    },
    removeCancelledMeetingEntry: function (view, index) {
        var record = '';
        record = view.getStore().getAt(index).data;
        console.log(record);
        var GLOBAL = Mentor.Global;
        var data = {
            InvitationId: record.InvitationId
        };

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "removeCancelledMeetingEntry",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                if (data.removeCancelledMeetingEntry.Error == 0) {
                    Mentor.app.application.getController('AppDetail').getMentorWaitScreenRejected();
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                if (typeof popup == 'undefined') {
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            }
        });
    },
    btnBackActionInstantMeeting: function () {
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    btnBackActionMentorInstantMeeting: function () {
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    btnBackInstantMeetingScreen: function () {
        //Ext.getCmp('idMentorBottomTabView').setActiveItem(0);
        this.doClearInstantMeetingForm();
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport,
                mainPanel = viewport.down("#mainviewport");

        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    btnBackActionTeamListing: function () {
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    btnBackActionTeamDetail: function () {
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    btnRefreshTeamMembers: function () {
        this.getTeamMembers();
    },
    doBtnNextInstantMeeting: function () {
        var radioValue = '';
        var radioValueID = '';

        if (Ext.getCmp('radioMentee').getChecked() == true) {
            radioValue = 'mentee';
            if (Ext.getCmp('idMenteeSelectFieldInstantMeetingScreen').getValue() == -1) {
                Ext.Msg.alert('MELS', "Please select a " + Mentor.Global.MENTEE_NAME);
                return;
            } else {
                radioValueID = Ext.getCmp('idMenteeSelectFieldInstantMeetingScreen').getValue();
            }
        } else if (Ext.getCmp('radioTeam').getChecked() == true) {
            radioValue = 'team';
            if (Ext.getCmp('idTeamSelectFieldInstantMeetingScreen').getValue() == '') {
                Ext.Msg.alert('MELS', "Please select a Team");
                return;
            } else {
                radioValueID = Ext.getCmp('idTeamSelectFieldInstantMeetingScreen').getValue();
            }
        } else {
            Ext.Msg.alert('MELS', "Please select whom are you meeting with.");
            return;
        }
        var InstantMeetingMainTopic = Ext.getCmp('idTopicsSelectFieldInstantMeetingScreen').getValue();
        var InstantMeetingType = Ext.getCmp('idMeetingTypeSelectFieldInstantMeetingScreen').getValue();
        var InstantMeetingPlace = Ext.getCmp('idMeetingPlaceSelectFieldInstantMeetingScreen').getValue();

        if (InstantMeetingMainTopic == '') {
            Ext.Msg.alert('MELS', "Please select Main Topic.");
            return;
        } else if (InstantMeetingType == '') {
            Ext.Msg.alert('MELS', "Please select Meeting Type.");
            return;
        } else if (InstantMeetingPlace == '') {
            Ext.Msg.alert('MELS', "Please select Meeting Place.");
            return;
        }

        var InstantMeetingData = {
            MeetingWith: radioValue,
            MeetingWithID: radioValueID,
            MainTopic: InstantMeetingMainTopic,
            MeetingType: InstantMeetingType,
            MeetingPlace: InstantMeetingPlace,
            MeetingStartTime: Ext.getCmp('meetingStartTimeMentorInstantMeeting').getValue(),
            MeetingEndTime: Ext.getCmp('meetingEndTimeMentorInstantMeeting').getValue(),
            MeetingElapsedTime: Ext.getCmp('meetingElapsedTimeMentorInstantMeeting').getValue()
        };
        Mentor.Global.INSTANT_MEETING_DATA = InstantMeetingData;

        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("action_instant_meeting");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "action_instant_meeting"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
        Ext.getStore('InviteMentee').load();
        if (radioValue == 'mentee') {
            Ext.getCmp('action_instant_meeting_items_title').setTitle(Mentor.Global.MENTEE_NAME + ' Action Items');
            Ext.getCmp('idLabelTeamMenteeActionInstantMeeting').setHtml(Mentor.Global.MENTEE_NAME + ' Action Items');
            //Ext.getCmp('idLabelTeamMenteeActionInstantMeeting').setHidden(true);
        } else if (radioValue == 'team') {
            Ext.getCmp('action_instant_meeting_items_title').setTitle('Team Action Items');
            Ext.getCmp('idLabelTeamMenteeActionInstantMeeting').setHtml('Team Action Items');
            //Ext.getCmp('idLabelTeamMenteeActionInstantMeeting').setHidden(false);
        }
    },

    /****
     Clear form 
     ****/
    doClearInstantMeetingForm: function () {
        //Clear value
        Ext.getCmp('idTopicsSelectFieldInstantMeetingScreen').setValue('');
        Ext.getCmp('idMeetingTypeSelectFieldInstantMeetingScreen').setValue('');
        Ext.getCmp('idMenteeSelectFieldInstantMeetingScreen').setValue('');
        Ext.getCmp('idTeamSelectFieldInstantMeetingScreen').setValue('');
        Ext.getCmp('meetingStartTimeMentorInstantMeeting').setValue('');
        Ext.getCmp('idMeetingPlaceSelectFieldInstantMeetingScreen').setValue('');
        Ext.getCmp('meetingEndTimeMentorInstantMeeting').setValue('');

    },
    /****
     }
     From Mentee Action Instant Meeting, move to Mentor Action
     ****/
    btnNextActionInstantMeeting: function () {
        var MenteeTeamActionItem = Ext.getCmp('idMenteeTeamActionItem');
        var MenteeTeamActionItemDone = Ext.getCmp('idMenteeTeamActionItemDoneOnMeetingUpdate');
        var MenteeTeamActionItemID = '', MenteeTeamActionItemDoneID = '', MentorTeamActionItemID = '', MentorTeamActionItemDoneID = '';
        var MenteeTeamActionItemName = '', MenteeTeamActionItemDoneName = '', MentorTeamActionItemName = '', MentorTeamActionItemDoneName = '';
        var MenteeTeamActionItemIsChecked = 0;
        var MenteeTeamActionItemDoneIsChecked = 0;

        for (i = 0; i < MenteeTeamActionItem.getItems().length; i++) {
            if (MenteeTeamActionItem.getItems().getAt(i).isChecked()) {
                if (MenteeTeamActionItemIsChecked == 0) {
                    MenteeTeamActionItemID = MenteeTeamActionItem.getItems().getAt(i).getValue();
                    MenteeTeamActionItemName = MenteeTeamActionItem.getItems().getAt(i).getItemId();
                    MenteeTeamActionItemIsChecked++;
                } else {
                    MenteeTeamActionItemID = MenteeTeamActionItemID + "," + MenteeTeamActionItem.getItems().getAt(i).getValue();
                    MenteeTeamActionItemName = MenteeTeamActionItemName + ";" + MenteeTeamActionItem.getItems().getAt(i).getItemId();
                    MenteeTeamActionItemIsChecked++;
                }
            }
        }

        for (i = 0; i < MenteeTeamActionItemDone.getItems().length; i++) {
            if (MenteeTeamActionItemDone.getItems().getAt(i).isChecked()) {
                if (MenteeTeamActionItemDoneIsChecked == 0) {
                    MenteeTeamActionItemDoneID = MenteeTeamActionItemDone.getItems().getAt(i).getValue();
                    MenteeTeamActionItemDoneName = MenteeTeamActionItemDone.getItems().getAt(i).getItemId();
                    MenteeTeamActionItemDoneIsChecked++;
                } else {
                    MenteeTeamActionItemDoneID = MenteeTeamActionItemDoneID + "," + MenteeTeamActionItemDone.getItems().getAt(i).getValue();
                    MenteeTeamActionItemDoneName = MenteeTeamActionItemDoneName + ";" + MenteeTeamActionItemDone.getItems().getAt(i).getItemId();
                    MenteeTeamActionItemDoneIsChecked++;
                }
            }
        }

        if (MenteeTeamActionItemID == '') {
            if (Mentor.Global.INSTANT_MEETING_DATA.MeetingWith == 'mentee') {
                Ext.Msg.alert('MELS', "Please select atleast one " + Mentor.Global.MENTEE_NAME + " ToDo Item");
            } else {
                Ext.Msg.alert('MELS', "Please select atleast one Team ToDo Item");
            }
            return;
        } else {
            var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
            var mainMenu = mainPanel.down("action_mentor_instant_meeting");
            if (!mainMenu) {
                mainMenu = mainPanel.add({xtype: "action_mentor_instant_meeting"});
            }
            Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
            mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

        }



    },

    btnSubmitActionInstantMeeting: function () {
        var GLOBAL = Mentor.Global;
        var UserID = '';
        var MenteeTeamActionItem = Ext.getCmp('idMenteeTeamActionItem');
        var MenteeTeamActionItemDone = Ext.getCmp('idMenteeTeamActionItemDoneOnMeetingUpdate');
        var MentorTeamActionItem = Ext.getCmp('idMentorActionItemInstantMetting');
        var MentorTeamActionItemDone = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');

        var MenteeTeamActionItemID = '', MenteeTeamActionItemDoneID = '', MentorTeamActionItemID = '', MentorTeamActionItemDoneID = '';
        var MenteeTeamActionItemName = '', MenteeTeamActionItemDoneName = '', MentorTeamActionItemName = '', MentorTeamActionItemDoneName = '';
        var MenteeTeamActionItemIsChecked = 0;
        var MenteeTeamActionItemDoneIsChecked = 0;
        var MentorTeamActionItemIsChecked = 0;
        var MentorTeamActionItemDoneIsChecked = 0;

        for (i = 0; i < MenteeTeamActionItem.getItems().length; i++) {
            if (MenteeTeamActionItem.getItems().getAt(i).isChecked()) {
                if (MenteeTeamActionItemIsChecked == 0) {
                    MenteeTeamActionItemID = MenteeTeamActionItem.getItems().getAt(i).getValue();
                    MenteeTeamActionItemName = MenteeTeamActionItem.getItems().getAt(i).getItemId();
                    MenteeTeamActionItemIsChecked++;
                } else {
                    MenteeTeamActionItemID = MenteeTeamActionItemID + "," + MenteeTeamActionItem.getItems().getAt(i).getValue();
                    MenteeTeamActionItemName = MenteeTeamActionItemName + ";" + MenteeTeamActionItem.getItems().getAt(i).getItemId();
                    MenteeTeamActionItemIsChecked++;
                }
            }
        }

        for (i = 0; i < MenteeTeamActionItemDone.getItems().length; i++) {
            if (MenteeTeamActionItemDone.getItems().getAt(i).isChecked()) {
                if (MenteeTeamActionItemDoneIsChecked == 0) {
                    MenteeTeamActionItemDoneID = MenteeTeamActionItemDone.getItems().getAt(i).getValue();
                    MenteeTeamActionItemDoneName = MenteeTeamActionItemDone.getItems().getAt(i).getItemId();
                    MenteeTeamActionItemDoneIsChecked++;
                } else {
                    MenteeTeamActionItemDoneID = MenteeTeamActionItemDoneID + "," + MenteeTeamActionItemDone.getItems().getAt(i).getValue();
                    MenteeTeamActionItemDoneName = MenteeTeamActionItemDoneName + ";" + MenteeTeamActionItemDone.getItems().getAt(i).getItemId();
                    MenteeTeamActionItemDoneIsChecked++;
                }
            }
        }

        for (i = 0; i < MentorTeamActionItem.getItems().length; i++) {
            if (MentorTeamActionItem.getItems().getAt(i).isChecked()) {
                if (MentorTeamActionItemIsChecked == 0) {
                    MentorTeamActionItemID = MentorTeamActionItem.getItems().getAt(i).getValue();
                    MentorTeamActionItemName = MentorTeamActionItem.getItems().getAt(i).getItemId();
                    MentorTeamActionItemIsChecked++;
                } else {
                    MentorTeamActionItemID = MentorTeamActionItemID + "," + MentorTeamActionItem.getItems().getAt(i).getValue();
                    MentorTeamActionItemName = MentorTeamActionItemName + ";" + MentorTeamActionItem.getItems().getAt(i).getItemId();
                    MentorTeamActionItemIsChecked++;
                }
            }
        }

        for (i = 0; i < MentorTeamActionItemDone.getItems().length; i++) {
            if (MentorTeamActionItemDone.getItems().getAt(i).isChecked()) {
                if (MentorTeamActionItemDoneIsChecked == 0) {
                    MentorTeamActionItemDoneID = MentorTeamActionItemDone.getItems().getAt(i).getValue();
                    MentorTeamActionItemDoneName = MentorTeamActionItemDone.getItems().getAt(i).getItemId();
                    MentorTeamActionItemDoneIsChecked++;
                } else {
                    MentorTeamActionItemDoneID = MentorTeamActionItemDoneID + "," + MentorTeamActionItemDone.getItems().getAt(i).getValue();
                    MentorTeamActionItemDoneName = MentorTeamActionItemDoneName + ";" + MentorTeamActionItemDone.getItems().getAt(i).getItemId();
                    MentorTeamActionItemDoneIsChecked++;
                }
            }
        }

        if (MenteeTeamActionItemID == '') {
            if (Mentor.Global.INSTANT_MEETING_DATA.MeetingWith == 'mentee') {
                Ext.Msg.alert('MELS', "Please select atleast one " + Mentor.Global.MENTEE_NAME + " ToDo Item");
            } else {
                Ext.Msg.alert('MELS', "Please select atleast one Team ToDo Item");
            }
            return;
        } else {
            Mentor.Global.INSTANT_MEETING_DATA.MenteeTeamActionItemID = MenteeTeamActionItemID;
            Mentor.Global.INSTANT_MEETING_DATA.MenteeTeamActionItemName = MenteeTeamActionItemName;
            Mentor.Global.INSTANT_MEETING_DATA.MenteeTeamActionItemDoneID = MenteeTeamActionItemDoneID;
            Mentor.Global.INSTANT_MEETING_DATA.MenteeTeamActionItemDoneName = MenteeTeamActionItemDoneName;
        }

        if (MentorTeamActionItemID == '') {
            Ext.Msg.alert('MELS', "Please select atleast one " + Mentor.Global.MENTOR_NAME + " ToDo Item");
            return;
        } else {
            Mentor.Global.INSTANT_MEETING_DATA.MentorTeamActionItemID = MentorTeamActionItemID;
            Mentor.Global.INSTANT_MEETING_DATA.MentorTeamActionItemName = MentorTeamActionItemName;
            Mentor.Global.INSTANT_MEETING_DATA.MentorTeamActionItemDoneID = MentorTeamActionItemDoneID;
            Mentor.Global.INSTANT_MEETING_DATA.MentorTeamActionItemDoneName = MentorTeamActionItemDoneName;
        }


        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
        }

        Mentor.Global.INSTANT_MEETING_DATA.MentorID = UserID;

        var data = Mentor.Global.INSTANT_MEETING_DATA;

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "saveInstantMeeting",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.saveInstantMeeting.data;
                if (data.saveInstantMeeting.Error == 1 || data.saveInstantMeeting.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.saveInstantMeeting.Message);
                    }, 100);
                    return;
                } else {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.saveInstantMeeting.Message, function () {
                            //Avinash (Tab)
                            /*var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                             var mainMenu = mainPanel.down("mentorBottomTabView");
                             if (!mainMenu) {
                             mainMenu = mainPanel.add({xtype: "mentorBottomTabView"});
                             }
                             Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
                             mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
                             Ext.getCmp('idMentorBottomTabView').setActiveItem(1);*/

                            //Avinash (Left Menu)
                            Mentor.Global.NavigationStack.pop();
                            Mentor.Global.NavigationStack.pop();
                            var View = Mentor.Global.NavigationStack.pop();
                            var viewport = Ext.Viewport,
                                    mainPanel = viewport.down("#mainviewport");

                            mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});

                            //Avinash (Left Menu)
                            Mentor.Global.INSTANT_MEETING_DATA = "";
                            clearInterval(Mentor.Global.INSTANT_MEETING_INTERVAL_ID);
                        });
                    }, 100);
                    return;
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false);
                if (typeof popup == 'undefined') {
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            }
        });
        return;
    },
    openWebview: function () {
        var viewport = Ext.Viewport,
                mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("subadminwebview");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "subadminwebview"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
    },
    doSubmitAddEditNote: function () {
        me = this;
        var GLOBAL = Mentor.Global;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID, UserType;

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
        }

        var idAddEditNoteID = Ext.getCmp('idAddEditNoteID').getValue();
        var idAddEditNoteMeetingID = Ext.getCmp('idAddEditNoteMeetingID').getValue();
        var idAddEditNoteTitle = Ext.getCmp('idAddEditNoteTitle').getValue();
        var idAddEditNoteDescription = Ext.getCmp('idAddEditNoteDescription').getValue();

        var data = {
            UserID: UserID,
            UserType: UserType,
            NoteID: idAddEditNoteID,
            NoteTitle: idAddEditNoteTitle,
            NoteDescription: idAddEditNoteDescription,
            NoteMeetingID: idAddEditNoteMeetingID
        };
        //console.log(data);

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "addUserNote",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.addUserNote.data;
                if (data.addUserNote.Error == 1 || data.addUserNote.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.addUserNote.Message);
                    }, 100);
                    return;
                } else {
                    Ext.Function.defer(function () {
                        //Ext.Msg.alert('MELS', data.addUserNote.Message, function () {
                        var popupObject = Ext.getCmp('addEditNotePopUp');
                        popupObject.hide();
                        Mentor.app.application.getController('Main').getUserNotes();
                        //});
                    }, 100);
                    return;
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                if (typeof popup == 'undefined') {
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            }
        });
        return;
    },
    addnotes: function () {
        var UserNoteStore = Ext.getStore('UserNotes');
        if (UserNoteStore.data.length === 0) {
            var popup = Mentor.app.application.getController('AppDetail').addNotePopup();
            Ext.Viewport.add(popup);
            popup.show();
        } else {
            var viewport = Ext.Viewport,
                    mainPanel = viewport.down("#mainviewport");
            var mainMenu = mainPanel.down("mentor_notes");
            if (!mainMenu) {
                mainMenu = mainPanel.add({xtype: "mentor_notes"});
            }
            Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
            mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
        }
    },
    btnBackMentorNotes: function () {
        /*me = this;
         var GLOBAL = Mentor.Global;
         var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
         var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
         var UserID;
         var UserType;
         */

        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
        /*
         if (MentorLoginUser != null) {
         var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
         var mainMenu = mainPanel.down("mentorBottomTabView");
         if (!mainMenu) {
         mainMenu = mainPanel.add({xtype: "mentorBottomTabView"});
         }
         } else if (MenteeLoginUser != null) {
         var viewport = Ext.Viewport,
         mainPanel = viewport.down("#mainviewport");
         var mainMenu = mainPanel.down("menteeBottomTabView");
         if (!mainMenu) {
         mainMenu = mainPanel.add({xtype: "menteeBottomTabView"});
         }
         }
         
         Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
         mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
         */
    },
    getMeetingHistory: function (popup) {
        me = this;
        var GLOBAL = Mentor.Global;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
            /*Ext.getCmp('idUserNameMentorMeetingHistory').setHtml(MentorLoginUser.MentorName);
             Ext.getCmp('idVersionMeetingHistory').setHtml(GLOBAL.VERSION_NAME);*/
            localStorage.setItem('SaveMeetingStartDateTime', Ext.encode({StartTime: null}));
            localStorage.setItem('SaveStartedMeetingID', Ext.encode({SaveStartedMeetingID: null}));
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
            Ext.getCmp('idUserName').setHtml(MenteeLoginUser.MenteeName);
            //Ext.getCmp('idVersionMenteeMeetingHistory').setHtml(GLOBAL.VERSION_NAME);
            localStorage.setItem('SaveMeetingStartDateTime', Ext.encode({StartTime: null}));
            localStorage.setItem('SaveStartedMeetingID', Ext.encode({SaveStartedMeetingID: null}));
        }

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var data = {
            UserId: UserID,
            UserType: UserType,
            CurrentPage: "1",
            PageSize: "10"
        };
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getMeetingHistory",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getMeetingHistory.data;
                Ext.getStore('MeetingHistory').clearData();
                if (data.getMeetingHistory.Error == 1) {
                    if (typeof popup == 'undefined') {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.getMeetingHistory.Message);
                        }, 100);
                    }

                    if (UserType != "1") {
                        Ext.getCmp('invitesTab').setTitle(Mentor.Global.MENTOR_PENDING_TITLE + " (" +
                                data.getMeetingHistory.pendingMeeting + "/" + data.getMeetingHistory.pending + ")");

                        /*Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(0).setTitle(Mentor.Global.MENTOR_PENDING_TITLE + "(" +
                         data.getMeetingHistory.pendingMeeting + "/" + data.getMeetingHistory.pending + ")");*/
                        Ext.getCmp('idPendingInvitesMentorLeftMenu').setText(Mentor.Global.MENTOR_PENDING_TITLE + " (" +
                                data.getMeetingHistory.pendingMeeting + "/" + data.getMeetingHistory.pending + ")");

                        var MentorPendingRequest = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().getComponent(1);
                        var MentorPendingMeetingList = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().getComponent(0)

                        MentorPendingRequest.setTitle(
                                Mentor.Global.MENTOR_PENDING_REQUEST_TITLE + " (" + data.getMeetingHistory.pending + ")");
                        MentorPendingMeetingList.setTitle(
                                Mentor.Global.MENTOR_PENDING_MEETING_TITLE + " (" + data.getMeetingHistory.pendingMeeting + ")");
                    }
                    return;
                } else if (data.getMeetingHistory.Error == 2) {
                    if (UserType == "1") {
                        Ext.Msg.alert('MELS', "Click Invites tab to issue meeting invite.", function () {
                            Ext.getCmp('idMenteeBottomTabView').setActiveItem(1);
                        });
                        me.btnInviteForMeeting();
                    } else {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', "Click OK to start a new meeting.");
                        }, 100);
                        Ext.getCmp('invitesTab').setTitle(Mentor.Global.MENTOR_PENDING_TITLE + " (" +
                                data.getMeetingHistory.pendingMeeting + "/" + data.getMeetingHistory.pending + ")");

                        /*Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(0).setTitle(Mentor.Global.MENTOR_PENDING_TITLE + "(" +
                         data.getMeetingHistory.pendingMeeting + "/" + data.getMeetingHistory.pending + ")");*/
                        Ext.getCmp('idPendingInvitesMentorLeftMenu').setText(Mentor.Global.MENTOR_PENDING_TITLE + " (" +
                                data.getMentorPendingMeeting.pendingMeeting + "/" + data.getMentorPendingMeeting.pending + ")");

                        var MentorPendingRequest = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().getComponent(1);
                        var MentorPendingMeetingList = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().getComponent(0)

                        MentorPendingRequest.setTitle(
                                Mentor.Global.MENTOR_PENDING_REQUEST_TITLE + " (" + data.getMeetingHistory.pending + ")");
                        MentorPendingMeetingList.setTitle(
                                Mentor.Global.MENTOR_PENDING_MEETING_TITLE + " (" + data.getMeetingHistory.pendingMeeting + ")");
                    }
                } else {
                    Ext.getStore('MeetingHistory').clearData();
                    Ext.getStore('MeetingHistory').add(data.getMeetingHistory.data);

                    Mentor.app.application.getController('Main').getActionItemComments();

                    if (UserType == "0") {
                        /*if(data.getMeetingHistory.pending == "0")
                         Ext.getCmp("idBtnPendingInvitesMeetingHistoryMentorScreen").setText("No Pending Invites");
                         else{*/


                        //Avinash (LeftMenu)
                        /*Ext.getCmp("idBtnPendingInvitesMeetingHistoryMentorScreen").setText(data.getMeetingHistory.pending + " Pending Invites");
                         
                         Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(0).setTitle(Mentor.Global.MENTOR_PENDING_TITLE + "(" +
                         data.getMeetingHistory.pendingMeeting + "/" + data.getMeetingHistory.pending + ")");*/
                        Ext.getCmp('idPendingInvitesMentorLeftMenu').setText(Mentor.Global.MENTOR_PENDING_TITLE + " (" +
                                data.getMeetingHistory.pendingMeeting + "/" + data.getMeetingHistory.pending + ")");


                        Ext.getCmp('invitesTab').setTitle(Mentor.Global.MENTOR_PENDING_TITLE + " (" +
                                data.getMeetingHistory.pendingMeeting + "/" + data.getMeetingHistory.pending + ")");

                        var MentorPendingRequest = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().getComponent(1);
                        var MentorPendingMeetingList = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().getComponent(0)

                        MentorPendingRequest.setTitle(
                                Mentor.Global.MENTOR_PENDING_REQUEST_TITLE + " (" + data.getMeetingHistory.pending + ")");
                        MentorPendingMeetingList.setTitle(
                                Mentor.Global.MENTOR_PENDING_MEETING_TITLE + " (" + data.getMeetingHistory.pendingMeeting + ")");
                        //}
                    }
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                if (typeof popup == 'undefined') {
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            }
        });
    },
    displayDetail: function (view, index, item, record, e) {
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        localStorage.setItem("storeMentorActionItems", null);
        localStorage.setItem("storeEntrepreneurActionItems", null);
        localStorage.setItem("storeMeetingDetailSubTopics", null);
        var UserID;
        var UserType;

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0"
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1"
        }

        if (UserType == "0") { // mentor
            var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
            var mainMenu = mainPanel.down("mentorreviewform");
            if (!mainMenu) {
                mainMenu = mainPanel.add({xtype: "mentorreviewform"});
            }
            Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
            mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

            Ext.getCmp('mentorMeetingFollowUpMeetingPanel').setHidden(true);
            Ext.getCmp("mentorMeetingTypePanel").setHidden(false);
            Ext.getCmp("mentorMeetingPlacePanel").setHidden(false);
            Ext.getCmp("mentorMeetingSubTopicPanel").setHidden(false);
            Ext.getCmp("mentorMeetingStartTimePanel").setHidden(false);
            Ext.getCmp("mentorMeetingEndTimePanel").setHidden(false);
            Ext.getCmp("mentorMeetingElapsedPanel").setHidden(false);
            Ext.getCmp("mentorMeetingMentorActionItemPanel").setHidden(false);
            Ext.getCmp("mentorMeetingMenteeActionItemPanel").setHidden(false);
            Ext.getCmp("mentorMeetingRatingPanel").setHidden(false);

            Ext.getCmp('meetingSubjectMentorReviewForm').setDisabled(false);
            Ext.getCmp('mentorCommentMentorReviewForm').setDisabled(false);
            Ext.getCmp('idBtnSubmitMentorReviewForm').setHidden(false);
            Ext.getCmp('idBtnBackMentorReviewForm').setHidden(false);


            Ext.getCmp('idMentorReviewFormScreen').reset(); // Reset all Fields
            Ext.getCmp('mainIssueDiscussedMentorReviewForm').setMaxRows(2);
            Ext.getCmp('actionItemsByMentorMentorReviewForm').setMaxRows(2);
            Ext.getCmp('actionItemsByMenteeMentorReviewForm').setMaxRows(2);
            Ext.getCmp('mentorCommentMentorReviewForm').setMaxRows(2);

            record = view.getStore().getAt(index).data;
            Mentor.Global.MEETING_DETAIL = record;

            if (record.FollowUpMeetingFor > 0) {
                Ext.getCmp('mentorMeetingFollowUpMeetingPanel').setHidden(false);
                Ext.getCmp('meetingFollowUpMeetingMentorReviewForm').setValue(record.MeetingDetail);
            }

            Ext.getCmp('meetingWithWhomMentorReviewForm').setValue(record.MenteeName);
            Ext.getCmp('meetingTypeMentorReviewForm').setValue(record.MeetingTypeID);
            Ext.getCmp('meetingPlaceMentorReviewForm').setValue(record.MeetingPlaceID);
            Ext.getCmp('meetingSubjectMentorReviewForm').setValue(record.TopicDescription);
            Ext.getCmp('meetingStartTimeMentorReviewForm').setValue(record.MeetingStartDatetime);
            Ext.getCmp('meetingEndTimeReviewForm').setValue(record.MeetingEndDatetime);
            Ext.getCmp('meetingElapsedTimeMentorReviewForm').setValue(record.MeetingElapsedTime);

            var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
            Ext.getCmp('idUserNameEnterpreneur').setHtml(MentorLoginDetail.MentorName);
            Ext.getCmp('idMentorReviewFormTitle').setTitle("Meeting Edit Form");

            var SatificationValuePanel = Ext.getCmp('idSatisfactionWithMeeting');
            //SatificationValuePanel.setValue(record.SatisfactionIndex);
            SatificationValuePanel.setValue(record.average);

            Ext.getCmp('mentorCommentMentorReviewForm').setValue(record.MentorComment);
            Ext.getCmp('menteeCommentMenteeReviewForm').setValue(record.MenteeComment);

            //Set Value for MentorActionItems
            if (record.MentorActionDetail != "") {
                var MentorActionItems = record.MentorActionDetail.split(',');
                var MentorActionName;
                var MentorActionID;
                var MentorActionNameStorage;
                for (i = 0; i < MentorActionItems.length; i++) {
                    var MentorAction = MentorActionItems[i].split('~');
                    if (i == 0) {
                        MentorActionID = MentorAction[0];
                        MentorActionName = MentorAction[1];
                        MentorActionNameStorage = MentorAction[1];
                    } else {
                        MentorActionID = MentorActionID + "," + MentorAction[0];
                        MentorActionName = MentorActionName + ",\n" + MentorAction[1];
                        MentorActionNameStorage = MentorActionNameStorage + "," + MentorAction[1];
                    }
                }
                Ext.getCmp('actionItemsByMentorMentorReviewForm').setValue(MentorActionName);
                Ext.getCmp('actionItemsByMentorMentorReviewForm').setMaxRows(MentorActionItems.length);
            }

            if (record.MentorActionDoneDetail != "") {
                var MentorActionDoneItems = record.MentorActionDoneDetail.split(',');
                var MentorActionDoneName;
                var MentorActionDoneID;
                for (i = 0; i < MentorActionDoneItems.length; i++) {
                    var MentorActionDone = MentorActionDoneItems[i].split('~');
                    if (i == 0) {
                        MentorActionDoneID = MentorActionDone[0];
                        MentorActionDoneName = MentorActionDone[1];
                    } else {
                        MentorActionDoneID = MentorActionDoneID + "," + MentorActionDone[0];
                        MentorActionDoneName = MentorActionDoneName + "," + MentorActionDone[1];
                    }
                }
            }

            var MentorActionItems = {
                MentorActionItemID: MentorActionID,
                MentorActionItemName: MentorActionNameStorage,
                MentorActionItemIDDone: MentorActionDoneID,
                MentorActionItemNameDone: MentorActionDoneName
            };
            localStorage.setItem("storeMentorActionItems", Ext.encode(MentorActionItems));

            //Set Value for MenteeActionItems
            if (record.MenteeActionDetail != "") {
                var MenteeActionItems = record.MenteeActionDetail.split(',');
                var MenteeActionName;
                for (i = 0; i < MenteeActionItems.length; i++) {
                    var MenteeAction = MenteeActionItems[i].split('~');
                    if (i == 0) {
                        MenteeActionName = MenteeAction[1];
                    } else {
                        MenteeActionName = MenteeActionName + ",\n" + MenteeAction[1];
                    }
                }
                Ext.getCmp('actionItemsByMenteeMentorReviewForm').setValue(MenteeActionName);
                Ext.getCmp('actionItemsByMenteeMentorReviewForm').setMaxRows(MenteeActionItems.length);
            }

            //Set Value for Meeting Detail (Sub Topics)
            if (record.SubTopicDescription != "") {
                var SubTopics = record.SubTopicDescription.split(',');
                var SubTopicsName;
                var SubTopicsID;
                var SubTopicsNameStorage;
                for (i = 0; i < SubTopics.length; i++) {
                    var subTopicItems = SubTopics[i].split('~');
                    if (i == 0) {
                        SubTopicsID = subTopicItems[0];
                        SubTopicsName = subTopicItems[1];
                        SubTopicsNameStorage = subTopicItems[1];
                    } else {
                        SubTopicsID = SubTopicsID + "," + subTopicItems[0];
                        SubTopicsName = SubTopicsName + ",\n" + subTopicItems[1];
                        SubTopicsNameStorage = SubTopicsNameStorage + "," + subTopicItems[1];
                    }
                }
                Ext.getCmp('mainIssueDiscussedMentorReviewForm').setValue(SubTopicsName);
                Ext.getCmp('mainIssueDiscussedMentorReviewForm').setMaxRows(SubTopics.length);

                var subtopicData = {
                    SubTopicsID: SubTopicsID,
                    SubTopicsTitle: SubTopicsNameStorage
                };
                localStorage.setItem("storeMeetingDetailSubTopics", Ext.encode(subtopicData));
            }

            Ext.getCmp('mentorCommentMentorReviewForm').setInputCls('inputCLS');

            if (record.Status == 'Rejected' || record.Status == 'Cancelled') {
                Ext.getCmp('mentorCommentMentorReviewForm').setInputCls('');
                Ext.getCmp("mentorMeetingTypePanel").setHidden(true);
                Ext.getCmp("mentorMeetingPlacePanel").setHidden(true);
                Ext.getCmp("mentorMeetingSubTopicPanel").setHidden(true);
                Ext.getCmp("mentorMeetingStartTimePanel").setHidden(true);
                Ext.getCmp("mentorMeetingEndTimePanel").setHidden(true);
                Ext.getCmp("mentorMeetingElapsedPanel").setHidden(true);
                Ext.getCmp("mentorMeetingMentorActionItemPanel").setHidden(true);
                Ext.getCmp("mentorMeetingMenteeActionItemPanel").setHidden(true);
                Ext.getCmp("mentorMeetingRatingPanel").setHidden(true);

                Ext.getCmp('meetingSubjectMentorReviewForm').setDisabled(true);
                Ext.getCmp('mentorCommentMentorReviewForm').setDisabled(true);
                Ext.getCmp('idBtnSubmitMentorReviewForm').setHidden(true);
                Ext.getCmp('idBtnBackMentorReviewForm').setHidden(false);
            }
        } else if (UserType == "1") { //mentee

            var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
            var mainMenu = mainPanel.down("menteereviewform");
            if (!mainMenu) {
                mainMenu = mainPanel.add({xtype: "menteereviewform"});
            }
            Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
            mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

            Ext.getCmp("menteeMeetingTypePanel").setHidden(false);
            Ext.getCmp("menteeMeetingPlacePanel").setHidden(false);
            Ext.getCmp("menteeMeetingSubTopicPanel").setHidden(false);
            Ext.getCmp("menteeMeetingStartTimePanel").setHidden(false);
            Ext.getCmp("menteeMeetingEndTimePanel").setHidden(false);
            Ext.getCmp("menteeMeetingElapsedPanel").setHidden(false);
            Ext.getCmp("menteeMeetingMentorActionItemPanel").setHidden(false);
            Ext.getCmp("menteeMeetingMenteeActionItemPanel").setHidden(false);
            Ext.getCmp("menteeMeetingRatingPanel").setHidden(false);
            Ext.getCmp('idSatisfactionClearAndUnderstandableMenteeReviewForm').setEditable(true);
            Ext.getCmp('idSatisfactionOverallSubjectMenteeReviewForm').setEditable(true);
            Ext.getCmp('idSatisfactionReleveantProblemMenteeReviewForm').setEditable(true);
            Ext.getCmp('idSatisfactionAdviceAndFeedbackMenteeReviewForm').setEditable(true);
            Ext.getCmp('idBtnSubmitMenteeReviewForm').setHidden(false);
            Ext.getCmp('idBtnBackMenteeReviewForm').setHidden(false);
            Ext.getCmp('actionItemsByMenteeMenteeReviewForm').setDisabled(true);
            Ext.getCmp('menteeCommentMenteeReviewForm').setDisabled(true);

            Ext.getCmp('idMenteeReviewFormScreen').reset(); // Reset all Fields
            Ext.getCmp('mainIssueDiscussedMenteeReviewForm').setMaxRows(2);
            Ext.getCmp('actionItemsByMentorMenteeReviewForm').setMaxRows(2);
            Ext.getCmp('actionItemsByMenteeMenteeReviewForm').setMaxRows(2);
            Ext.getCmp('menteeCommentMenteeReviewForm').setDisabled(false);
            Ext.getCmp('menteeCommentMenteeReviewForm').setMaxRows(2);

            record = view.getStore().getAt(index).data;
            Mentor.Global.MEETING_DETAIL = record;

            Ext.getCmp('meetingWithWhomMenteeReviewForm').setValue(record.MentorName);
            Ext.getCmp('meetingTypeMenteeReviewForm').setValue(record.MeetingTypeName);
            Ext.getCmp('meetingPlaceMenteeReviewForm').setValue(record.MeetingPlaceName);
            Ext.getCmp('meetingSubjectMenteeReviewForm').setValue(record.TopicDescription);
            Ext.getCmp('meetingStartTimeMenteeReviewForm').setValue(record.MeetingStartDatetime);
            Ext.getCmp('meetingEndTimeMenteeReviewForm').setValue(record.MeetingEndDatetime);
            Ext.getCmp('meetingElapsedTimeMenteeReviewForm').setValue(record.MeetingElapsedTime);
            Ext.getCmp('mentorCommentMentorReviewForm').setValue(record.MentorComment);
            Ext.getCmp('idMenteeReviewFormTitle').setTitle("Meeting Edit Form");

            //meetingElapsedTimeMenteeReviewForm
            var MenteeLoginDetail = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
            Ext.getCmp('idUserNameEnterpreneur').setHtml(MenteeLoginDetail.MenteeName);

            //Care C
            var SatisfactionClearAndUnderstandablePanel = Ext.getCmp('idSatisfactionClearAndUnderstandableMenteeReviewForm');
            SatisfactionClearAndUnderstandablePanel.setValue(record.CareC);

            //Care A
            var SatisfactionOverallSubjectPanel = Ext.getCmp('idSatisfactionOverallSubjectMenteeReviewForm');
            SatisfactionOverallSubjectPanel.setValue(record.CareA);

            //Care R
            var SatisfactionReleveantProblemPanel = Ext.getCmp('idSatisfactionReleveantProblemMenteeReviewForm');
            SatisfactionReleveantProblemPanel.setValue(record.CareR);

            //Care E
            var SatisfactionAdviceAndFeedbackPanel = Ext.getCmp('idSatisfactionAdviceAndFeedbackMenteeReviewForm');
            SatisfactionAdviceAndFeedbackPanel.setValue(record.CareE);

            Ext.getCmp('menteeCommentMenteeReviewForm').setValue(record.MenteeComment);

            if (record.MentorActionDetail != "") {
                var MentorActionItems = record.MentorActionDetail.split(',');
                var MentorActionName;
                for (i = 0; i < MentorActionItems.length; i++) {
                    var MentorAction = MentorActionItems[i].split('~');
                    if (i == 0) {
                        MentorActionName = MentorAction[1];
                    } else {
                        MentorActionName = MentorActionName + ",\n" + MentorAction[1];
                    }
                }
                Ext.getCmp('actionItemsByMentorMenteeReviewForm').setValue(MentorActionName);
                Ext.getCmp('actionItemsByMentorMenteeReviewForm').setMaxRows(MentorActionItems.length);
            }

            if (record.MenteeActionDetail != "") {
                var MenteeActionItems = record.MenteeActionDetail.split(',');
                var MenteeActionName;
                var MenteeActionID;
                var MenteeActionNameStorage;
                for (i = 0; i < MenteeActionItems.length; i++) {
                    var MenteeAction = MenteeActionItems[i].split('~');
                    if (i == 0) {
                        MenteeActionID = MenteeAction[0];
                        MenteeActionNameStorage = MenteeAction[1];
                        MenteeActionName = MenteeAction[1];
                    } else {
                        MenteeActionID = MenteeActionID + "," + MenteeAction[0];
                        MenteeActionNameStorage = MenteeActionNameStorage + "," + MenteeAction[1];
                        MenteeActionName = MenteeActionName + ",\n" + MenteeAction[1];
                    }
                }
                Ext.getCmp('actionItemsByMenteeMenteeReviewForm').setValue(MenteeActionName);
                Ext.getCmp('actionItemsByMenteeMenteeReviewForm').setMaxRows(MenteeActionItems.length);
            }

            if (record.MenteeActionDoneDetail != "") {
                var MenteeActionDoneItems = record.MenteeActionDoneDetail.split(',');
                var MenteeActionDoneName;
                var MenteeActionDoneID;
                for (i = 0; i < MenteeActionDoneItems.length; i++) {
                    var MenteeActionDone = MenteeActionDoneItems[i].split('~');
                    if (i == 0) {
                        MenteeActionDoneID = MenteeActionDone[0];
                        MenteeActionDoneName = MenteeActionDone[1];
                    } else {
                        MenteeActionDoneID = MenteeActionDoneID + "," + MenteeActionDone[0];
                        MenteeActionDoneName = MenteeActionDoneName + "," + MenteeActionDone[1];
                    }
                }
            }

            var MenteeActionItems = {
                MenteeActionItemID: MenteeActionID,
                MenteeActionItemName: MenteeActionNameStorage,
                MenteeActionItemIDDone: MenteeActionDoneID,
                MenteeActionItemNameDone: MenteeActionDoneName
            };
            localStorage.setItem("storeEntrepreneurActionItems", Ext.encode(MenteeActionItems));

            //Set Value for Meeting Detail (Sub Topics)
            if (record.SubTopicDescription != "") {
                var SubTopics = record.SubTopicDescription.split(',');
                var SubTopicsName;
                for (i = 0; i < SubTopics.length; i++) {
                    var subTopicItems = SubTopics[i].split('~');
                    if (i == 0) {
                        SubTopicsName = subTopicItems[1];
                    } else {
                        SubTopicsName = SubTopicsName + ",\n" + subTopicItems[1];
                    }
                }
                Ext.getCmp('mainIssueDiscussedMenteeReviewForm').setValue(SubTopicsName);
                Ext.getCmp('mainIssueDiscussedMenteeReviewForm').setMaxRows(SubTopics.length);
            }

            if (record.Status == 'Rejected' || record.Status == 'Cancelled') {
                Ext.getCmp("menteeMeetingTypePanel").setHidden(true);
                Ext.getCmp("menteeMeetingPlacePanel").setHidden(true);
                Ext.getCmp("menteeMeetingSubTopicPanel").setHidden(true);
                Ext.getCmp("menteeMeetingStartTimePanel").setHidden(true);
                Ext.getCmp("menteeMeetingEndTimePanel").setHidden(true);
                Ext.getCmp("menteeMeetingElapsedPanel").setHidden(true);
                Ext.getCmp("menteeMeetingMentorActionItemPanel").setHidden(true);
                Ext.getCmp("menteeMeetingMenteeActionItemPanel").setHidden(true);
                Ext.getCmp("menteeMeetingRatingPanel").setHidden(true);

                Ext.getCmp('idSatisfactionClearAndUnderstandableMenteeReviewForm').setEditable(false);
                Ext.getCmp('idSatisfactionOverallSubjectMenteeReviewForm').setEditable(false);
                Ext.getCmp('idSatisfactionReleveantProblemMenteeReviewForm').setEditable(false);
                Ext.getCmp('idSatisfactionAdviceAndFeedbackMenteeReviewForm').setEditable(false);
                Ext.getCmp('idBtnSubmitMenteeReviewForm').setHidden(true);
                Ext.getCmp('idBtnBackMenteeReviewForm').setHidden(false);

                Ext.getCmp('actionItemsByMenteeMenteeReviewForm').setDisabled(true);
                Ext.getCmp('menteeCommentMenteeReviewForm').setDisabled(true);
            }
        }
    },
    doSubmitMentorReviewForm: function (btn) {
        me = this;
        var GLOBAL = Mentor.Global;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;
        var SatificationValue;

        var SatificationRating = Ext.getCmp('idSatisfactionWithMeeting');

        var MeetingTypeID = Ext.getCmp('meetingTypeMentorReviewForm').getValue();
        var MeetingPlaceID = Ext.getCmp('meetingPlaceMentorReviewForm').getValue();
        var MeetingTopicID = Ext.getCmp('meetingSubjectMentorReviewForm').getValue();
        var MeetingStartTime = Ext.getCmp('meetingStartTimeMentorReviewForm').getValue();
        var MeetingEndTime = Ext.getCmp('meetingEndTimeReviewForm').getValue();
        var MeetingElapsedTime = Ext.getCmp('meetingElapsedTimeMentorReviewForm').getValue();
        var MentorComment = Ext.ComponentQuery.query('#mentorCommentMentorReviewForm')[0].getValue();

        var StartSessionDate = Date.parse(Ext.getCmp('meetingStartTimeMentorReviewForm').getValue());
        var EndSessionDate = Date.parse(Ext.getCmp('meetingEndTimeReviewForm').getValue());
        var MeetingTimeValidate = "";

        if (StartSessionDate == 'Invalid Date' || EndSessionDate == 'Invalid Date') {
            MeetingTimeValidate = "invalidDate";
        } else {
            var DateDiff = EndSessionDate - StartSessionDate;
            var diffMins = parseInt(DateDiff / 60000);

            if (diffMins < 0) {
                MeetingTimeValidate = "invalidDate";
            }
        }

        if (MeetingTimeValidate == "invalidDate") {
            Ext.Msg.alert('MELS', "Please input proper Meeting Start time and End time.");
            return;
        } else if (SatificationRating.getValue() == "" || SatificationRating.getValue() == "0") {
            Ext.Msg.alert('MELS', "Please select satisfaction rating.");
            return;
        } else {
            SatificationValue = SatificationRating.getValue();
        }

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
        }

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var SubtopicData = Ext.decode(localStorage.getItem('storeMeetingDetailSubTopics'));
        var SubTopicId = "";
        if (SubtopicData != null) {
            SubTopicId = SubtopicData.SubTopicsID;
        }

        var ActionItemData = Ext.decode(localStorage.getItem('storeMentorActionItems'));
        var MentorActionItemID = "";
        var MentorActionItemName = "";
        var MentorActionItemIDDone = "";
        var MentorActionItemNameDone = "";
        if (ActionItemData != null) {
            MentorActionItemID = ActionItemData.MentorActionItemID;
            MentorActionItemIDDone = ActionItemData.MentorActionItemIDDone;
        }

        var data = {
            MentorID: GLOBAL.MEETING_DETAIL.MentorID,
            MenteeID: GLOBAL.MEETING_DETAIL.MenteeID,
            MeetingID: GLOBAL.MEETING_DETAIL.MeetingID,
            MeetingTypeID: MeetingTypeID,
            MeetingPlaceID: MeetingPlaceID,
            MeetingTopicID: MeetingTopicID,
            MeetingSubTopicID: SubTopicId,
            MeetingStartTime: MeetingStartTime,
            MeetingEndTime: MeetingEndTime,
            MeetingElapsedTime: MeetingElapsedTime,
            MentorComment: MentorComment,
            SatisfactionIndex: SatificationValue,
            MentorActionIDs: MentorActionItemID,
            MentorActionItemIDDone: MentorActionItemIDDone
        };
        console.log(data);
//        return;

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "mentorComment",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.mentorComment.data;

                if (data.mentorComment.Error == 1 || data.mentorComment.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.mentorComment.Message);
                    }, 100);
                    return;
                } else {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.mentorComment.Message, function () {
                            /*var viewport = Ext.Viewport,
                             mainPanel = viewport.down("#entorBottomTabView");
                             var mainMenu = mainPanel.down("meetinghistory");
                             if(!mainMenu){
                             mainMenu = mainPanel.add({xtype: "meetinghistory"});
                             }
                             mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});*/
                            var View = Mentor.Global.NavigationStack.pop();
                            var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                            mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});

                            Mentor.Global.MEETING_DETAIL = null;
                            me.getMeetingHistory();
                        });
                    }, 100);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    doSubmitMenteeReviewForm: function (btn) {
        var GLOBAL = Mentor.Global;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;

        /*var SatificationCareCValue = Ext.getCmp('idSatisfactionClearAndUnderstandableMenteeReviewForm').getValue();
         var SatificationCareAValue = Ext.getCmp('idSatisfactionOverallSubjectMenteeReviewForm').getValue();
         var SatificationCareRValue = Ext.getCmp('idSatisfactionReleveantProblemMenteeReviewForm').getValue();
         var SatificationCareEValue = Ext.getCmp('idSatisfactionAdviceAndFeedbackMenteeReviewForm').getValue();*/

        //Care C
        /*var SatificationCareCValuePanel = Ext.getCmp('idSatisfactionClearAndUnderstandableMenteeReviewForm');
         var SatificationCareCValue;
         var isChecked = 0;
         var isCheckedStatus = false;
         for(i = 0 ; i <SatificationCareCValuePanel.getItems().length;i++){
         if(SatificationCareCValuePanel.getItems().getAt(i).isChecked())
         {
         if(isChecked ==0){
         SatificationCareCValue = SatificationCareCValuePanel.getItems().getAt(i).getValue();
         isCheckedStatus = true;
         }
         }
         }
         if(isCheckedStatus == false)
         {
         Ext.Msg.alert('MELS', "Please select clear and understandable feedback and advice rating.");
         return;
         }*/

        var SatificationCareCValueRatting = Ext.getCmp('idSatisfactionClearAndUnderstandableMenteeReviewForm');
        var SatificationCareCValue;
        if (SatificationCareCValueRatting.getValue() == "" || SatificationCareCValueRatting.getValue() == "0") {
            Ext.Msg.alert('MELS', "Please select clear and understandable feedback and advice rating.");
            return;
        } else {
            SatificationCareCValue = SatificationCareCValueRatting.getValue();
        }


        //Care A
        /*var SatificationCareAValuePanel = Ext.getCmp('idSatisfactionOverallSubjectMenteeReviewForm');
         var SatificationCareAValue;
         var isChecked = 0;
         isCheckedStatus = false;
         for(i = 0 ; i <SatificationCareAValuePanel.getItems().length;i++){
         if(SatificationCareAValuePanel.getItems().getAt(i).isChecked())
         {
         if(isChecked ==0){
         SatificationCareAValue = SatificationCareAValuePanel.getItems().getAt(i).getValue();
         isCheckedStatus = true;
         }
         }
         }
         if(isCheckedStatus == false)
         {
         Ext.Msg.alert('MELS', "Please select applicable to overall subject area rating.");
         return;
         }*/
        var SatificationCareAValueRatting = Ext.getCmp('idSatisfactionOverallSubjectMenteeReviewForm');
        var SatificationCareAValue;
        if (SatificationCareAValueRatting.getValue() == "" || SatificationCareAValueRatting.getValue() == "0") {
            Ext.Msg.alert('MELS', "Please select applicable to overall subject area rating.");
            return;
        } else {
            SatificationCareAValue = SatificationCareAValueRatting.getValue();
        }


        //Care R
        /*var SatificationCareRValuePanel = Ext.getCmp('idSatisfactionReleveantProblemMenteeReviewForm');
         var SatificationCareRValue;
         var isChecked = 0;
         isCheckedStatus = false;
         for(i = 0 ; i <SatificationCareRValuePanel.getItems().length;i++){
         if(SatificationCareRValuePanel.getItems().getAt(i).isChecked())
         {
         if(isChecked ==0){
         SatificationCareRValue = SatificationCareRValuePanel.getItems().getAt(i).getValue();
         isCheckedStatus = true;
         }
         }
         }
         if(isCheckedStatus == false)
         {
         Ext.Msg.alert('MELS', "Please select relevant to my particular problem rating.");
         return;
         }*/
        var SatificationCareRValueRatting = Ext.getCmp('idSatisfactionReleveantProblemMenteeReviewForm');
        var SatificationCareRValue;
        if (SatificationCareRValueRatting.getValue() == "" || SatificationCareRValueRatting.getValue() == "0") {
            Ext.Msg.alert('MELS', "Please select relevant to my particular problem rating.");
            return;
        } else {
            SatificationCareRValue = SatificationCareRValueRatting.getValue();
        }


        //Care E
        /*var SatificationCareEValuePanel = Ext.getCmp('idSatisfactionAdviceAndFeedbackMenteeReviewForm');
         var SatificationCareEValue;
         var isChecked = 0;
         isCheckedStatus = false;
         for(i = 0 ; i <SatificationCareEValuePanel.getItems().length;i++){
         if(SatificationCareEValuePanel.getItems().getAt(i).isChecked())
         {
         if(isChecked ==0){
         SatificationCareEValue = SatificationCareEValuePanel.getItems().getAt(i).getValue();
         isCheckedStatus = true;
         }
         }
         }
         if(isCheckedStatus == false)
         {
         Ext.Msg.alert('MELS', "Please select Executable/Actionable practical advice and feedback rating.");
         return;
         }*/
        var SatificationCareEValueRatting = Ext.getCmp('idSatisfactionAdviceAndFeedbackMenteeReviewForm');
        var SatificationCareEValue;
        if (SatificationCareEValueRatting.getValue() == "" || SatificationCareEValueRatting.getValue() == "0") {
            Ext.Msg.alert('MELS', "Please select Executable/Actionable practical advice and feedback rating.");
            return;
        } else {
            SatificationCareEValue = SatificationCareEValueRatting.getValue();
        }


        var MenteeComment = Ext.ComponentQuery.query('#menteeCommentMenteeReviewForm')[0].getValue();
        var ActionItemData = Ext.decode(localStorage.getItem('storeEntrepreneurActionItems'));
        var MenteeActionItemID = "";
        var MenteeActionItemIDDone = "";
        if (ActionItemData != null) {
            MenteeActionItemID = ActionItemData.MenteeActionItemID;
            MenteeActionItemIDDone = ActionItemData.MenteeActionItemIDDone;
        }

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var data = {
            MentorID: GLOBAL.MEETING_DETAIL.MentorID,
            MenteeID: GLOBAL.MEETING_DETAIL.MenteeID,
            MeetingID: GLOBAL.MEETING_DETAIL.MeetingID,
            MenteeComment: MenteeComment,
            CareC: SatificationCareCValue,
            CareA: SatificationCareAValue,
            CareR: SatificationCareRValue,
            CareE: SatificationCareEValue,
            MenteeActionItemID: MenteeActionItemID,
            MenteeActionItemIDDone: MenteeActionItemIDDone
        };
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "menteeComment",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.menteeComment.data;

                if (data.menteeComment.Error == 1 || data.menteeComment.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.menteeComment.Message);
                    }, 100);
                    return;
                } else {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.menteeComment.Message, function () {
                            /*var viewport = Ext.Viewport,
                             mainPanel = viewport.down("#mainviewport");
                             var mainMenu = mainPanel.down("meetinghistorymentee");
                             if(!mainMenu){
                             mainMenu = mainPanel.add({xtype: "meetinghistorymentee"});
                             }
                             mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});*/
                            var View = Mentor.Global.NavigationStack.pop();
                            var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                            mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});

                            Mentor.Global.MEETING_DETAIL = null;
                            me.getMeetingHistory();
                        });
                    }, 100);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    btnBackMentorReviewForm: function (btn) {
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});

        /*var viewport = Ext.Viewport,
         mainPanel = viewport.down("#mainviewport");
         var mainMenu = mainPanel.down("meetinghistory");
         if(!mainMenu){
         mainMenu = mainPanel.add({xtype: "meetinghistory"});
         }
         mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});*/

        Mentor.Global.MEETING_DETAIL = null;
    },
    btnBackMenteeReviewForm: function (btn) {
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});

        /*var viewport = Ext.Viewport,
         mainPanel = viewport.down("#mainviewport");
         var mainMenu = mainPanel.down("meetinghistorymentee");
         if(!mainMenu){
         mainMenu = mainPanel.add({xtype: "meetinghistorymentee"});
         }
         mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});*/

        Mentor.Global.MEETING_DETAIL = null;
    },
    //From Meeting History
    /*btnAddMeetingDetail : function(btn){
     me = this;
     var viewport = Ext.Viewport,
     mainPanel = viewport.down("#mainviewport");
     var mainMenu = mainPanel.down("entrepreneur");
     
     //mainMenu = mainPanel.remove({xtype: "entrepreneur"});
     //mainMenu = mainPanel.add({xtype: "entrepreneur"});
     
     if(!mainMenu){
     mainMenu = mainPanel.add({xtype: "entrepreneur"});
     }
     Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
     mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
     Mentor.Global.MEETING_DETAIL = null;
     me.clearLocalStoragePreviousMeetingDetail();
     
     MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
     Mentor.app.application.getController('Main').getMentee(MentorLoginDetail.Id,false);
     btnAddMeetingDetail
     localStorage.setItem("storeStartEndSession",null); //Clear Previous Meeting Start and End Session
     },*/
    //From Pending Meeting Detail
    btnAddMeetingDetail: function (btn) {
        me = this;
        var btnStartNewMeeting = Ext.getCmp('idBtnStartNewMeetingMentorPendingMeetingDetail');
        var PendingMeetingRecord = Ext.getStore('MentorPendingMeeting').getAt(parseInt(btnStartNewMeeting.getItemId())).data;

        var FollowUpMeetingCheckbox = Ext.getCmp('idIsFollowUpMeetingMentorPendingMeetingDetailYes');
        var FollowUpMeetingID = 0;
        if (FollowUpMeetingCheckbox.isChecked()) {
            FollowUpMeetingID = Ext.getCmp('idFollowUpListMentorPendingMeetingDetail').getValue();
            if (FollowUpMeetingID == null) {
                FollowUpMeetingID = 0;
            }
        }

        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("entrepreneur");

        Mentor.Global.MEETING_DETAIL = null;
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "entrepreneur"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

        me.clearLocalStoragePreviousMeetingDetail();

        MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        me.getMentee(MentorLoginDetail.Id, PendingMeetingRecord);

        Ext.getCmp('FollowUpMeetingID').setValue(FollowUpMeetingID);

        localStorage.setItem("storeStartEndSession", null); //Clear Previous Meeting Start and End Session
        Ext.getCmp('idBtnNextEnterpreneur').setDisabled(true);
        Ext.getCmp('idLabelMenteeNameEnterpreneurScreen').setHtml("Meeting with " + PendingMeetingRecord.MenteeName);
    },
    //Get Mentee For Start new Meeting from Pending Meeting Detail
    getMentee: function (MentorID, PendingMeetingRecord) {
        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var data = {
            MentorID: MentorID
        };

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getMentee",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getMentee.data;
                if (data.getMentee.Error == 1) {
                    Ext.Function.defer(function () {
                        Ext.getStore('Mentee').removeAll();
                        Ext.Msg.alert('MELS', data.getMentee.Message);
                    }, 100);
                    return;
                } else if (data.getMentee.Error == 2) {
                    Ext.getStore('Mentee').removeAll();
                    if (isLogin) {// If Service at a time Of Login
                        me.getTopic();
                    }
                } else {
                    //localStorage.setItem("idMentor",Ext.encode(data.getMentee.data));
                    Ext.getStore('Mentee').removeAll();
                    Ext.getStore('Mentee').add(data.getMentee.data);
                    //Check if user comes from meeting detail
                    var MeetingDetail = Mentor.Global.MEETING_DETAIL;
                    if (MeetingDetail != null) {
                        Ext.getCmp('idEnterpreneurSelectField').setValue(MeetingDetail.MenteeID);
                    }

                    //Set All fields related to mentee
                    Ext.getCmp('idEnterpreneurSelectField').setValue(PendingMeetingRecord.MenteeID);
                    if (PendingMeetingRecord.MeetingTypeID != "0") {
                        Ext.getCmp('idMettingTypeSelectField').setValue(PendingMeetingRecord.MeetingTypeID);
                    }
                    if (PendingMeetingRecord.TopicID != "0") {
                        Ext.getCmp('idTopicsTimingScreen').setValue(PendingMeetingRecord.TopicID);
                    }
                    if (PendingMeetingRecord.MeetingPlaceID != "0") {
                        Ext.getCmp('idMeetingPlaceSelectField').setValue(PendingMeetingRecord.MeetingPlaceID);
                    }
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    changeMenteeDetail: function () {
        me = this;
        me.storeMeetingDetailForUpdate(); // Store the Meeting Detail for Update;
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("entrepreneur");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "entrepreneur"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
    },
    changeTimingDetail: function () {
        me = this;
        me.storeMeetingDetailForUpdate(); // Store the Meeting Detail for Update;
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("timing");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "timing"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
    },
    changeMeetingDetail: function (topicID) {
        me = this;
        me.storeMeetingDetailForUpdate(); // Store the Meeting Detail for Update;

        //Clear the store and Dispaly according to the Metting MainTopics
        var SubTopicStore = Ext.getStore('SubTopic');
        SubTopicStore.clearFilter();
        SubTopicStore.filterBy(function (rec) {
            return rec.get('TopicID') === topicID;
        });
        SubTopicStore.load();

        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("subtopicdiscusses");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "subtopicdiscusses"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
    },
    changeMenteeActionItemsDetail: function () {
        me = this;
        me.storeMeetingDetailForUpdate(); // Store the Meeting Detail for Update;

        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("meetingedit_mentee_action_items");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "meetingedit_mentee_action_items"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
    },
    changeMentorActionItemsDetail: function () {
        me = this;
        me.storeMeetingDetailForUpdate(); // Store the Meeting Detail for Update;
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("meetingedit_mentor_action_items");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "meetingedit_mentor_action_items"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
    },
    clearLocalStoragePreviousMeetingDetail: function () {
        localStorage.setItem("storeMeetingDetail", null);
        localStorage.setItem("storeMeetingTiming", null);
        localStorage.setItem("storeMeetingDetailSubTopics", null);
        localStorage.setItem("storeEntrepreneurActionItems", null);
        localStorage.setItem("storeMentorActionItems", null);
    },
    btnProfileMeetingDetail: function (btn) {
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("mentorprofile");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "mentorprofile"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
    },
    btnBackMentorProfile: function (btn) {
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    storeMeetingDetailForUpdate: function () {
        var MEETING_DETAIL = Mentor.Global.MEETING_DETAIL;
        localStorage.setItem("storeMeetingDetail", null);
        //localStorage.setItem("storeMeetingTiming",null);
        //localStorage.setItem("storeMeetingDetailSubTopics", null);
        //localStorage.setItem("storeEntrepreneurActionItems", null);
        //localStorage.setItem("storeMentorActionItems", null);

        //EnterpreneurScreen (Select Mentee and Meeting Type)
        var MeenteeDetail = {
            "MeetngTypeID": MEETING_DETAIL.MeetingTypeID,
            "MeetingTypeTitle": MEETING_DETAIL.MeetingTypeName,
            "MenteeName": MEETING_DETAIL.MenteeName,
            "MenteeID": MEETING_DETAIL.MenteeID,
            "StartDate": MEETING_DETAIL.MeetingStartDatetime,
            "MeetingEndDatetime": MEETING_DETAIL.MeetingEndDatetime,
            "TopicName": MEETING_DETAIL.TopicDescription,
            "TopicID": MEETING_DETAIL.MeetingTopicID,
            "MeetingElapsedTime": MEETING_DETAIL.MeetingElapsedTime,
            "MeetingPlaceID": MEETING_DETAIL.MeetingPlaceID,
            "MeetingPlaceTitle": MEETING_DETAIL.MeetingPlaceName
        };
        localStorage.setItem("storeMeetingDetail", Ext.encode(MeenteeDetail));

        /*** 22/02/2016 Move Timing Screen to Enterpreneur Screen***/
        //Timing Screen (Topics)
        /*var TopicsTimingDetail = {"StartDate" : MEETING_DETAIL.MeetingStartDatetime,
         "MeetingEndDatetime" : MEETING_DETAIL.MeetingEndDatetime,
         "TopicName" : MEETING_DETAIL.TopicDescription,
         "TopicID" : MEETING_DETAIL.MeetingTopicID,
         "MeetingElapsedTime" : MEETING_DETAIL.MeetingElapsedTime}
         
         localStorage.setItem("storeMeetingTiming",Ext.encode(TopicsTimingDetail));*/
        /** End 22/02/2016 Move Timing Screen to Enterpreneur Screen***/


        // Store Metting Detail Screen (Sub Topics)
        /*var MeetingDetailSubTopics = {"SubTopicsID": MEETING_DETAIL.MeetingSubTopicID,
         "SubTopicsTitle": MEETING_DETAIL.SubTopicDescription}
         
         localStorage.setItem("storeMeetingDetailSubTopics", Ext.encode(MeetingDetailSubTopics));*/


        // Store Mentee Action item (Mentee Action Item)
        var MenteeActionItems = MEETING_DETAIL.MenteeActionDetail.split(',');
        var MenteeActionName;
        var MenteeActionID;
        for (j = 0; j < MenteeActionItems.length; j++) {
            var MenteeAction = MenteeActionItems[j].split('~');
            if (j == 0) {
                MenteeActionName = MenteeAction[1];
                MenteeActionID = MenteeAction[0];
            } else {
                MenteeActionName = MenteeActionName + "," + MenteeAction[1];
                MenteeActionID = MenteeActionID + "," + MenteeAction[0];
            }
        }
        var EntrepreneurActionItems = {
            "MenteeActionItemID": MenteeActionID,
            "MenteeActionItemName": MenteeActionName
        };

        //localStorage.setItem("storeEntrepreneurActionItems", Ext.encode(EntrepreneurActionItems));

        // Store Menor Action item (Mentee Action Item)
        var MentorActionItems = MEETING_DETAIL.MentorActionDetail.split(',');
        var MentorActionName;
        var MentorActionID;
        for (j = 0; j < MentorActionItems.length; j++) {
            var MentorAction = MentorActionItems[j].split('~');
            if (j == 0) {
                MentorActionName = MentorAction[1];
                MentorActionID = MentorAction[0];
            } else {
                MentorActionName = MentorActionName + "," + MentorAction[1];
                MentorActionID = MentorActionID + "," + MentorAction[0];
            }
        }
        var MentorActionItems = {
            "MentorActionItemID": MentorActionID,
            "MentorActionItemName": MentorActionName,
            "Feedback": MEETING_DETAIL.MeetingFeedback,
            "MeetingEndDatetime": MEETING_DETAIL.MeetingEndDatetime
        };
        //localStorage.setItem("storeMentorActionItems", Ext.encode(MentorActionItems));
    },
    btnAddSkills: function (btn) {
        var GLOBAL = Mentor.Global;
        var SkillsPanel = Ext.getCmp('idSkillsProfile');
        var SkillID;
        var SkillName;
        var isChecked = 0;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;
        var SMSDisable = ""; // For mentee

        var Email = Ext.getCmp('email').getValue();
        var Phone = Ext.getCmp('phone').getValue();
        var UserName = Ext.getCmp('userName').getValue();
        var AccessToken = Ext.getCmp('password').getValue();
        var CnfAccessToken = Ext.getCmp('cnfpassword').getValue();
        var Source = Ext.getCmp('idSourceProfileScreen').getValue();

        if (Email == "") {
            Ext.Msg.alert('MELS', "Please enter email address");
            return;
        } else if (UserName == "") {
            Ext.Msg.alert('MELS', "Please enter username");
            return;
        } else if (this.ValidateEmail(Email) == false) {
            Ext.Msg.alert('MELS', "You have entered an invalid email address.");
            return;
        }

        //SMSDisable = Ext.getCmp('idIsPhoneSMSCapableProfileScreen').getChecked();
        SMSDisable = Ext.ComponentQuery.query('radiofield[name=idIsPhoneSMSCapableProfileScreen]')[0].getGroupValue();
        if (SMSDisable && Phone == "") {
            Ext.Msg.alert('MELS', "Please enter phone number");
            return;
        }

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
        }

        for (i = 0; i < SkillsPanel.getItems().length; i++) {
            if (SkillsPanel.getItems().getAt(i).getCls() == 'profileSkillsActiveCLS') {
                if (isChecked == 0) {
                    SkillID = SkillsPanel.getItems().getAt(i).getId();
                    SkillName = SkillsPanel.getItems().getAt(i).getText();
                    isChecked = isChecked + 1;
                } else {
                    SkillID = SkillID + "," + SkillsPanel.getItems().getAt(i).getId();
                    SkillName = SkillName + "," + SkillsPanel.getItems().getAt(i).getText();
                    isChecked = isChecked + 1;
                }
            }
        }

        if (isChecked <= 0) {
            Ext.Msg.alert('MELS', "Please select at least one skills");
        } else {
            var data = {
                UserId: UserID,
                UserType: UserType,
                SkillID: SkillID,
                UserName: UserName,
                PhoneNumber: Phone,
                Email: Email,
                DisableSMS: SMSDisable,
                SourceID: Source
            };

            if (AccessToken != "" && CnfAccessToken != "") {
                if (AccessToken.length < 4) {
                    Ext.Msg.alert('MELS', "Password length must be four characters.");
                    return;
                } else if (AccessToken != CnfAccessToken) {
                    Ext.Msg.alert('MELS', "Both Passwords do not match");
                    return;
                } else {
                    data["Pass"] = AccessToken;
                }
            } else if (AccessToken == "" && CnfAccessToken != "") {
                Ext.Msg.alert('MELS', "Please enter Password");
                return;
            } else if (AccessToken != "" && CnfAccessToken == "") {
                Ext.Msg.alert('MELS', "Please enter Confirm Password");
                return;
            }

            Ext.Viewport.setMasked({
                xtype: "loadmask",
                message: "Please wait"
            });

            Ext.Ajax.request({
                url: GLOBAL.SERVER_URL,
                method: "POST",
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/json'
                },
                xhr2: true,
                disableCaching: false,
                jsonData: {
                    method: "addSkill",
                    body: data
                },
                success: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    var data = Ext.decode(responce.responseText);
                    console.log(data);
                    d = data.addSkill.data;
                    if (data.addSkill.Error == 1 || data.addSkill.Error == 2) {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.addSkill.Message);
                        }, 100);
                        return;
                    } else {
                        if (UserType == "0") {//Mentor
                            localStorage.setItem("idMentorLoginDetail", Ext.encode(data.addSkill.data));

                            Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(0).setDisabled(false);
                            Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(1).setDisabled(false);
                            Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(2).setDisabled(false);
                            Ext.getCmp('profileBackBtn').setDisabled(false);
                        } else if (UserType == "1") {//Mentee
                            localStorage.setItem("idMenteeLoginDetail", Ext.encode(data.addSkill.data));

                            Ext.getCmp('idMenteeBottomTabView').getTabBar().getComponent(0).setDisabled(false);
                            Ext.getCmp('idMenteeBottomTabView').getTabBar().getComponent(1).setDisabled(false);
                            Ext.getCmp('profileBackBtn').setDisabled(false);
                        }

                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', "Profile updated sucessfully.", function () {
                                if (MentorLoginUser != null) {
                                    Ext.getCmp('idMentorBottomTabView').setActiveItem(0);
                                } else {
                                    Ext.getCmp('idMenteeBottomTabView').setActiveItem(0);
                                }
                            });
                        }, 100);
                    }
                },
                failure: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    console.log(responce);
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            });
        }
    },
    btnInviteForMeeting: function () {
        var GLOBAL = Mentor.Global;

        /*var viewport = Ext.Viewport,
         mainPanel = viewport.down("#mainviewport");
         var mainMenu = mainPanel.down("invitementor");
         if(!mainMenu){
         mainMenu = mainPanel.add({xtype: "invitementor"});
         }
         Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
         mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});*/

        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        Ext.getCmp('idUserName').setHtml(MenteeLoginUser.MenteeName);

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var data = {
            MenteeID: MenteeLoginUser.Id
        };
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getMentorForInvitation",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getMentorForInvitation.data;

                var MenteeAcceptedInvite = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar().getComponent(0);
                var MenteePendingInvite = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar().getComponent(1);
                var MenteeRejectedInvite = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar().getComponent(2);

                /* Ext.getCmp('idMenteeBottomTabView').getTabBar().getComponent(0).setTitle(Mentor.Global.MENTEE_INVITES_TITLE + "(" +
                 data.getMentorForInvitation.AcceptedCount + "/" + data.getMentorForInvitation.PendingCount + "/" + data.getMentorForInvitation.DeclinedCount + ")");*/
                Ext.getCmp('idPendingInvitesMenteeLeftMenu').setText(Mentor.Global.MENTEE_INVITES_TITLE + " (" +
                        data.getMentorForInvitation.AcceptedCount + "/" + data.getMentorForInvitation.PendingCount + "/" + data.getMentorForInvitation.DeclinedCount + ")");

                MenteePendingInvite.setTitle(
                        Mentor.Global.MENTEE_PENDING_TITLE + " (" + data.getMentorForInvitation.PendingCount + ")");
                MenteeAcceptedInvite.setTitle(
                        Mentor.Global.MENTEE_ACCEPTED_TITLE + " (" + data.getMentorForInvitation.AcceptedCount + ")");
                MenteeRejectedInvite.setTitle(
                        Mentor.Global.MENTEE_DECLINED_TITLE + " (" + data.getMentorForInvitation.DeclinedCount + ")");

                if (data.getMentorForInvitation.Error == 1) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getMentorForInvitation.Message);
                    }, 100);
                    return;
                } else if (data.getMentorForInvitation.Error == 2) {
//                    Ext.Function.defer(function () {
//                        Ext.Msg.alert('MELS', "No mentor found");
//                    }, 100);
                    Ext.getCmp('idNoMentorAvailable').setHidden(false);
                } else {
                    var selectedValue = Ext.getCmp('idMentorSelectFieldInviteMentorScreen').getValue();
                    Ext.getCmp('idNoMentorAvailable').setHidden(true);
                    Ext.getStore('InviteMentor').clearData();
                    Ext.getStore('InviteMentor').insert(0, {
                        "MentorID": "-1",
                        "MentorName": "-- Select " + Mentor.Global.MENTOR_NAME + " --",
                        "school_name": "-- Select " + Mentor.Global.MENTOR_NAME + " --"
                    });
                    Ext.getStore('InviteMentor').add(data.getMentorForInvitation.data);

                    var mentorData = Ext.getStore('InviteMentor').data;
                    var defaultSelect = true;
                    for (i = 0; i < mentorData.length; i++) {
                        if (mentorData.getAt(i).data.MentorID == selectedValue) {
                            defaultSelect = false;
                            break;
                        }
                    }
                    if (defaultSelect === false) {
                        Ext.getCmp('idMentorSelectFieldInviteMentorScreen').setValue(selectedValue);
                    }
                }
                Mentor.app.application.getController('Main').getActionItemComments();
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    btnInviteForMeetingMentor: function () {
        var GLOBAL = Mentor.Global;

        localStorage.setItem('CacheAcceptResponse', null);
        localStorage.setItem('CacheDeclineResponse', null);

        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        Ext.getCmp('idUserName').setHtml(MentorLoginUser.MentorName);

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var data = {
            MentorID: MentorLoginUser.Id
        };
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getMenteeForInvitation",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getMenteeForInvitation.data;

                Ext.getCmp('invitesTab').setTitle(Mentor.Global.MENTOR_PENDING_TITLE + " (" +
                        data.getMenteeForInvitation.pendingMeeting + "/" + data.getMenteeForInvitation.pending + ")");

                /*Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(0).setTitle(Mentor.Global.MENTOR_PENDING_TITLE + "(" +
                 data.getMenteeForInvitation.pendingMeeting + "/" + data.getMenteeForInvitation.pending + ")");*/
                Ext.getCmp('idPendingInvitesMentorLeftMenu').setText(Mentor.Global.MENTOR_PENDING_TITLE + " (" +
                        data.getMenteeForInvitation.pendingMeeting + "/" + data.getMenteeForInvitation.pending + ")");

                var MentorPendingRequest = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().getComponent(1);
                var MentorPendingMeetingList = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().getComponent(0)

                MentorPendingRequest.setTitle(
                        Mentor.Global.MENTOR_PENDING_REQUEST_TITLE + " (" + data.getMenteeForInvitation.pending + ")");
                MentorPendingMeetingList.setTitle(
                        Mentor.Global.MENTOR_PENDING_MEETING_TITLE + " (" + data.getMenteeForInvitation.pendingMeeting + ")");

                if (data.getMenteeForInvitation.Error == 1) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getMenteeForInvitation.Message);
                    }, 100);
                    return;
                } else if (data.getMenteeForInvitation.Error == 2) {
//                    Ext.Function.defer(function () {
//                        Ext.Msg.alert('MELS', "No mentor found");
//                    }, 100);
                    Ext.getCmp('idNoMenteeAvailable').setHidden(false);
                } else {
                    var selectedValue = Ext.getCmp('idMenteeSelectFieldInviteMenteeScreen').getValue();
                    Ext.getCmp('idNoMenteeAvailable').setHidden(true);
                    Ext.getStore('InviteMentee').clearData();
                    Ext.getStore('InviteMentee').insert(0, {
                        "MenteeID": "-1",
                        "MenteeName": "-- Select " + Mentor.Global.MENTEE_NAME + " --",
                        "rStatus": ""
                    });
                    Ext.getStore('InviteMentee').add(data.getMenteeForInvitation.data);

                    var menteeData = Ext.getStore('InviteMentee').data;
                    var defaultSelect = true;
                    for (i = 0; i < menteeData.length; i++) {
                        if (menteeData.getAt(i).data.MenteeID == selectedValue) {
                            defaultSelect = false;
                            break;
                        }
                    }
                    if (defaultSelect === false) {
                        Ext.getCmp('idMenteeSelectFieldInviteMenteeScreen').setValue(selectedValue);
                    }
                    Mentor.app.application.getController('Main').getActionItemComments();
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    btnInviteMentor: function () {
        var GLOBAL = Mentor.Global;
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var MentorSelectField = Ext.getCmp('idMentorSelectFieldInviteMentorScreen');
        var TopicsSelectField = Ext.getCmp('idTopicsSelectFieldInviteMentorScreen');
        var MenteeCommentReasonForMeeting = Ext.getCmp('menteeCommentInviteMentorScreen');
        var MenteeEmail = Ext.getCmp('idMeenteeEmailInviteMentorScreen');
        var MenteePhoneNo = Ext.getCmp('idMeenteePhoneNoInviteMentorScreen');
        var IsPhoneSMSCapable = Ext.getCmp('idIsPhoneSMSCapableInviteMentorScreen');
        var FollowUpMeetingFor = 0;
        if (Ext.getCmp('idIsFollowUpMeetingInviteMentorYes').isChecked()) {
            FollowUpMeetingFor = Ext.getCmp('idFollowUpListInviteMentor').getValue();
            if (FollowUpMeetingFor == null) {
                FollowUpMeetingFor = 0;
            }
        }

        if (MentorSelectField.getValue() == '-1') {
            Ext.Msg.alert('MELS', "Please select " + Mentor.Global.MENTOR_NAME + " .");
            return;
        } else if (TopicsSelectField.getValue() == null || TopicsSelectField.getValue() == '' || TopicsSelectField.getValue() == -1) {
            Ext.Msg.alert('MELS', "Please select main topic.");
            return;
        } else if (MenteeCommentReasonForMeeting.getValue() == "") {
            Ext.Msg.alert('MELS', "Reason for meeting is empty.");
            return;
        }

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var data = {
            MentorID: MentorSelectField.getValue(),
            MenteeID: MenteeLoginUser.Id,
            TopicID: TopicsSelectField.getValue(),
            MenteeComments: MenteeCommentReasonForMeeting.getValue(),
            MenteeEmail: MenteeEmail.getValue(),
            MenteePhone: MenteePhoneNo.getValue(),
            SmsCapable: IsPhoneSMSCapable.getChecked(),
            FollowUpMeetingFor: FollowUpMeetingFor
        };

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "mentorInvitation",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.mentorInvitation.data;
                MenteeCommentReasonForMeeting.setValue('');
                if (data.mentorInvitation.Error == 1) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.mentorInvitation.Message, function () {
                            Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').setActiveItem(1);
                        });
                    }, 100);
                    return;
                } else if (data.mentorInvitation.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.mentorInvitation.Message, function () {
                            Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').setActiveItem(1);
                        });
                    }, 100);
                } else {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.mentorInvitation.Message, function () {
                            //me.btnGotoWaitScreenInviteMentorScreen();
                            Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').setActiveItem(1);
                        });
                    }, 100);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    btnInviteMenteeSend: function () {
        var GLOBAL = Mentor.Global;
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "menteeInvitation",
                body: {}
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.menteeInvitation.data;
                MentorCommentReasonForMeeting.setValue('');
                if (data.menteeInvitation.Error == 1) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.menteeInvitation.Message, function () {
                            //Ext.getCmp('idMenteeInvitesTabView').setActiveItem(1);
                            Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').setActiveItem(1);
                        });
                    }, 100);
                    return;
                } else if (data.menteeInvitation.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.menteeInvitation.Message, function () {
                            //Ext.getCmp('idMenteeInvitesTabView').setActiveItem(1);
                            Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').setActiveItem(1);
                        });
                    }, 100);
                } else {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.menteeInvitation.Message, function () {
                            //me.btnGotoWaitScreenInviteMentorScreen();
                            //Ext.getCmp('idMenteeInvitesTabView').setActiveItem(1);
                            Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').setActiveItem(1);
                        });
                    }, 100);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    btnInviteMentee: function () {
        var GLOBAL = Mentor.Global;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeSelectField = Ext.getCmp('idMenteeSelectFieldInviteMenteeScreen');
        var TopicsSelectField = Ext.getCmp('idTopicsSelectFieldInviteMenteeScreen');
        var MentorCommentReasonForMeeting = Ext.getCmp('mentorCommentInviteMenteeScreen');
        var MentorEmail = Ext.getCmp('idMentorEmailInviteMenteeScreen');
        var MentorPhoneNo = Ext.getCmp('idMentorPhoneNoInviteMenteeScreen');
        var IsPhoneSMSCapable = Ext.getCmp('idIsPhoneSMSCapableInviteMenteeScreen');
        var InvitationDate = Ext.getCmp('dateTimeIssueInviteMentee');
        var InvitationTime = Ext.getCmp('timeIssueInviteMentee');
        var MeetingTypeField = Ext.getCmp('idMeetingTypeSelectFieldInviteMenteeScreen');
        var MeetingPlaceField = Ext.getCmp('idMeetingPlaceSelectFieldInviteMenteeScreen');

        if (MenteeSelectField.getValue() == '-1') {
            Ext.Msg.alert('MELS', "Please select " + Mentor.Global.MENTEE_NAME + " .");
            return;
        } else if (TopicsSelectField.getValue() == null || TopicsSelectField.getValue() == '' || TopicsSelectField.getValue() == -1) {
            Ext.Msg.alert('MELS', "Please select main topic.");
            return;
        } else if (InvitationDate.getValue() == "" || InvitationTime.getValue() == "") {
            Ext.Msg.alert('MELS', "Both date and time fields need to be set when you are issuing an invite.");
            return;
        } else if (MeetingTypeField.getValue() == null || MeetingTypeField.getValue() == '' || MeetingTypeField.getValue() == -1) {
            Ext.Msg.alert('MELS', "Please select meeting type.");
            return;
        } else if (MeetingPlaceField.getValue() == null || MeetingPlaceField.getValue() == '' || MeetingPlaceField.getValue() == -1) {
            Ext.Msg.alert('MELS', "Please select meeting place.");
            return;
        } else if (MentorCommentReasonForMeeting.getValue() == "") {
            Ext.Msg.alert('MELS', "You must specify a reason for requesting a meeting.");
            return;
        }

        var data = {
            MenteeID: MenteeSelectField.getValue(),
            MentorID: MentorLoginUser.Id,
            TopicID: TopicsSelectField.getValue(),
            MentorComments: MentorCommentReasonForMeeting.getValue(),
            MentorEmail: MentorEmail.getValue(),
            MentorPhone: MentorPhoneNo.getValue(),
            SmsCapable: IsPhoneSMSCapable.getChecked(),
            MeetingDateTime: InvitationDate.getValue() + " " + InvitationTime.getValue(),
            MeetingType: MeetingTypeField.getValue(),
            MeetingPlace: MeetingPlaceField.getValue()
        };

        localStorage.setItem("mentorIssueInvitationDetail", null);
        localStorage.setItem("mentorIssueInvitationDetail", Ext.encode(data));

        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("acceptresponses");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "acceptresponses"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

        Ext.getCmp('idDoSubmitIssueInviteRequest').setHidden(false);
        Ext.getCmp('idDoSubmitAcceptRequest').setHidden(true);

        Ext.getCmp('idMentorCommetnsFieldSetAcceptRejectPendingRequest').setTitle(Mentor.Global.MENTOR_NAME + ' Comments');
        Ext.getCmp('commentsAcceptPendingRequest').setPlaceHolder(Mentor.Global.MENTOR_NAME + ' Comments');
    },
    btnBackInviteBack: function (btn) {
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    btnGetPendingRequest: function (popup) {
        var GLOBAL = Mentor.Global;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        localStorage.setItem('SaveMeetingStartDateTime', Ext.encode({StartTime: null}));
        localStorage.setItem('SaveStartedMeetingID', Ext.encode({SaveStartedMeetingID: null}));

        /*var viewport = Ext.Viewport,
         mainPanel = viewport.down("#mainviewport");
         var mainMenu = mainPanel.down("mentorPendingRequest");
         if(!mainMenu){
         mainMenu = mainPanel.add({xtype: "mentorPendingRequest"});
         }
         //Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
         mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});*/

        Ext.getCmp('idUserNameMentorPendingRequest').setHtml(MentorLoginUser.MentorName);

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var data = {
            MentorID: MentorLoginUser.Id
        };

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getMentorPendingRequest",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getMentorPendingRequest.data;

                if (data.getMentorPendingRequest.pendingMeeting != 0 && data.getMentorPendingRequest.pending == 0) {
                    Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().setActiveTab(0);
                    return false;
                }

                if (data.getMentorPendingRequest.Error == 1) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getMentorPendingRequest.Message);
                    }, 100);
                    return;
                } else if (data.getMentorPendingRequest.Error == 2) {
                    Ext.getStore('PendingRequest').clearData();
                    Ext.getStore('PendingRequest').load();
                    Ext.getStore('PendingRequest').sync();

                    if (typeof popup == 'undefined') {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', "No pending invitations. Check back later.", function () {
                                /*var View = Mentor.Global.NavigationStack.pop();
                                 var viewport = Ext.Viewport,
                                 mainPanel = viewport.down("#mainviewport");
                                 
                                 mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});*/
                                //Ext.getCmp('idMentorBottomTabView').setActiveItem(0);
                                //Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(1).setBadgeText("");
                            });
                        }, 100);
                    }

                    Ext.getCmp('invitesTab').setTitle(Mentor.Global.MENTOR_PENDING_TITLE + " (" +
                            data.getMentorPendingRequest.pendingMeeting + "/" + data.getMentorPendingRequest.pending + ")");

                    /*Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(0).setTitle(Mentor.Global.MENTOR_PENDING_TITLE + "(" +
                     data.getMentorPendingRequest.pendingMeeting + "/" + data.getMentorPendingRequest.pending + ")");*/
                    Ext.getCmp('idPendingInvitesMentorLeftMenu').setText(Mentor.Global.MENTOR_PENDING_TITLE + " (" +
                            data.getMentorPendingRequest.pendingMeeting + "/" + data.getMentorPendingRequest.pending + ")");


                    var MentorPendingRequest = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().getComponent(1);
                    var MentorPendingMeetingList = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().getComponent(0)

                    MentorPendingRequest.setTitle(
                            Mentor.Global.MENTOR_PENDING_REQUEST_TITLE + " (" + data.getMentorPendingRequest.pending + ")");
                    MentorPendingMeetingList.setTitle(
                            Mentor.Global.MENTOR_PENDING_MEETING_TITLE + " (" + data.getMentorPendingRequest.pendingMeeting + ")");
                } else {
                    Ext.getStore('PendingRequest').clearData();
                    Ext.getStore('PendingRequest').load();
                    Ext.getStore('PendingRequest').sync();
                    Ext.getStore('PendingRequest').add(data.getMentorPendingRequest.data);
                    //Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(1).setBadgeText(data.getMentorPendingRequest.data.length);

                    Ext.getCmp('invitesTab').setTitle(Mentor.Global.MENTOR_PENDING_TITLE + " (" +
                            data.getMentorPendingRequest.pendingMeeting + "/" + data.getMentorPendingRequest.pending + ")");

                    /*Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(0).setTitle(Mentor.Global.MENTOR_PENDING_TITLE + "(" +
                     data.getMentorPendingRequest.pendingMeeting + "/" + data.getMentorPendingRequest.pending + ")");*/
                    Ext.getCmp('idPendingInvitesMentorLeftMenu').setText(Mentor.Global.MENTOR_PENDING_TITLE + " (" +
                            data.getMentorPendingRequest.pendingMeeting + "/" + data.getMentorPendingRequest.pending + ")");

                    var MentorPendingRequest = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().getComponent(1);
                    var MentorPendingMeetingList = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().getComponent(0)

                    MentorPendingRequest.setTitle(
                            Mentor.Global.MENTOR_PENDING_REQUEST_TITLE + " (" + data.getMentorPendingRequest.pending + ")");
                    MentorPendingMeetingList.setTitle(
                            Mentor.Global.MENTOR_PENDING_MEETING_TITLE + " (" + data.getMentorPendingRequest.pendingMeeting + ")");

                    Mentor.app.application.getController('Main').getActionItemComments();
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                if (typeof popup == 'undefined') {
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            }
        });
    },
    btnBackMentorPendingRequest: function (btn) {
        /*var View = Mentor.Global.NavigationStack.pop();
         var viewport = Ext.Viewport,
         mainPanel = viewport.down("#mainviewport");
         
         mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});*/
        Ext.getCmp('idMentorBottomTabView').setActiveItem(0);
    },
    btnBackMentorInvitesTabView: function (btn) {
        //Avinash (Left Menu)
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport,
                mainPanel = viewport.down("#mainviewport");

        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
        //Avinash (Tab)
        /*var TabBar = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar();
         TabBar.setActiveTab(0);
         Ext.getCmp('idMentorBottomTabView').setActiveItem(0);*/
    },
    respondPendingRequest: function (view, index, item, e) {
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("acceptRejectPendingRequest");
        //mainPanel.remove({xtype: "acceptRejectPendingRequest"})
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "acceptRejectPendingRequest"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

        record = view.getStore().getAt(index).data;
        Mentor.Global.MENTOR_PENDING_REQUEST = record;
        Mentor.Global.Current_date = new Date();

        localStorage.setItem('CacheAcceptResponse', null);
        localStorage.setItem('CacheDeclineResponse', null);

        //Ext.getCmp('idMeetingFromReject').setHtml("Meeting Request from " + record.MenteeName);

        Ext.getCmp('idMenteeNameAcceptReject').setValue(record.MenteeName);


        if (record.Status == "Rescheduled") {
            Ext.getCmp('idInfoMenteeNameAcceptRejectPendingRequest').setHtml('Reschedule Request from ' + record.MenteeName)
            Ext.getCmp('idRespondPendingInvitePageTitle').setTitle('Reschedule Request');
        } else {
            Ext.getCmp('idInfoMenteeNameAcceptRejectPendingRequest').setHtml('Meeting Request from ' + record.MenteeName)
            Ext.getCmp('idRespondPendingInvitePageTitle').setTitle('Respond Pending Invite');
        }

        Ext.getCmp('idMeetingTopicAcceptRejectPendingRequest').setValue(record.TopicDescription);
        Ext.getCmp('menteeCommentAcceptRejectPendingRequest').setValue(record.ManteeComments);

        Ext.getCmp('idMenteeEmailAcceptRejectPendingRequest').setValue("");
        Ext.getCmp('idMenteeEmailAcceptRejectPendingRequest').setValue(record.InviteeEmail);

        Ext.getCmp('idMenteePhoneNoAcceptRejectPendingRequest').setValue("");
        Ext.getCmp('idMenteePhoneNoAcceptRejectPendingRequest').setValue(record.InviteePhone);

        Ext.getCmp('idIsPhoneSMSCapableAcceptRejectPendingRequest').setChecked(false);
        Ext.getCmp('idIsPhoneSMSCapableAcceptRejectPendingRequest').setChecked(record.SmsCapable);

        Ext.getCmp('dateTimeAcceptRejectPendingRequest').setValue('');
        Ext.getCmp('timePickerAcceptRejectPendingRequest').setValue('');
        Ext.getCmp('timeAcceptRejectPendingRequest').setValue('');

        //Ext.getCmp('idMeetingTypeSelectFieldAcceptRejectPendingRequest').setValue(null);
        //Ext.getCmp('idMeetingPlaceSelectFieldAcceptRejectPendingRequest').setValue(null);

        // Ext.getCmp('idMeetingTypeSelectFieldAcceptRejectPendingRequest').resetOriginalValue();
        // Ext.getCmp('idMeetingTypeSelectFieldAcceptRejectPendingRequest').reset();
        // Ext.getCmp('idMeetingPlaceSelectFieldAcceptRejectPendingRequest').resetOriginalValue();
        // Ext.getCmp('idMeetingPlaceSelectFieldAcceptRejectPendingRequest').reset();
//        Ext.getCmp('commentsAcceptRejectPendingRequest').setValue('');
        Ext.getCmp('idMeetingTypeSelectFieldAcceptRejectPendingRequest').setValue('');
        Ext.getCmp('idMeetingPlaceSelectFieldAcceptRejectPendingRequest').setValue('')

        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));

        Ext.getCmp('idVersionAcceptRejectPendingRequest').setHtml(Mentor.Global.VERSION_NAME);
        //idLabelMenteeRescheduleCommentsAcceptRejectPendingRequest
        //menteeRescheduleCommentAcceptRejectPendingRequest
        Ext.getCmp('RescheduleAcceptReject').setHidden(true);
        Ext.getCmp('NormalAcceptReject').setHidden(true);
        if (record.rescheduledBy == '1') {
            Ext.getCmp('idLabelMenteeRescheduleCommentsAcceptRejectPendingRequest').setHtml(Mentor.Global.MENTEE_NAME + ' Reason for Reschedule').setHidden(false);
            Ext.getCmp('menteeRescheduleCommentAcceptRejectPendingRequest').setValue(record.RescheduleComments).setHidden(false);
            Ext.getCmp('RescheduleAcceptReject').setHidden(false);
        } else {
            Ext.getCmp('NormalAcceptReject').setHidden(false);
        }
        if (MentorLoginUser != null) {
            Ext.getCmp('idUserName').setHtml(MentorLoginUser.MentorName);
        } else if (MenteeLoginUser != null) {
            Ext.getCmp('idUserName').setHtml(MenteeLoginUser.MenteeName);
        }
    },
    btnBackAcceptDeclineResponses: function () {
        localStorage.setItem("mentorAcceptInvitationDetail", null);
        localStorage.setItem("mentorDeclineInvitationDetail", null);
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport,
                mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    doAcceptRequest: function () {
        me = this;
        var GLOBAL = Mentor.Global;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var Record = Mentor.Global.MENTOR_PENDING_REQUEST;
        var Comment = Ext.getCmp('mentorCommentAcceptRejectPendingRequest').getValue();
        var Status = "Accepted";
        var MettingPlaceID = Ext.getCmp('idMeetingPlaceSelectFieldAcceptRejectPendingRequest').getValue();
        var MeetingDate = Ext.getCmp('dateTimeAcceptRejectPendingRequest').getValue();
        var MeetingTime = Ext.getCmp('timeAcceptRejectPendingRequest').getValue();
        var MeetingTypeID = Ext.getCmp('idMeetingTypeSelectFieldAcceptRejectPendingRequest').getValue();

        if (MeetingTypeID == null || MeetingTypeID == -1 || MeetingTypeID == '') {
            Ext.Msg.alert('MELS', "Please select meeting type.");
            return;
        } else if (MettingPlaceID == null || MettingPlaceID == -1 || MettingPlaceID == '') {
            Ext.Msg.alert('MELS', "Please select meeting place.");
            return;
        } else if (MeetingDate == "" || MeetingTime == "") {
            Ext.Msg.alert('MELS', "Both date and time fields need to be set when you accept the meeting invite.");
            return;
        } else if (Comment == "") {
            Ext.Msg.alert('MELS', "Please enter comment");
            return;
        } else {
//            Ext.Viewport.setMasked({
//                xtype: "loadmask",
//                message: "Please wait"
//            });

            var data = {
                InvitationId: Record.InvitationId,
                Status: Status,
                MeetingPlaceID: MettingPlaceID,
                MeetingDateTime: MeetingDate + " " + MeetingTime,
                MeetingTypeID: MeetingTypeID,
                Comments: Comment
            };
            localStorage.setItem("mentorAcceptInvitationDetail", null);
            localStorage.setItem("mentorAcceptInvitationDetail", Ext.encode(data));
            var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
            var mainMenu = mainPanel.down("acceptresponses");
            if (!mainMenu) {
                mainMenu = mainPanel.add({xtype: "acceptresponses"});
            }
            Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
            mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
            Ext.getCmp('commentsAcceptPendingRequest').setValue('');
            Ext.getCmp('idDoSubmitAcceptRequest').setHidden(false);
            Ext.getCmp('idDoSubmitIssueInviteRequest').setHidden(true);
            /*Ext.Ajax.request({
             url: GLOBAL.SERVER_URL,
             method: "POST",
             dataType: 'json',
             headers: {
             'Content-Type': 'application/json'
             },
             xhr2: true,
             disableCaching: false,
             jsonData: {
             method: "mentorResponse",
             body: data
             },
             success: function (responce) {
             Ext.Viewport.setMasked(false); //Avinash
             var data = Ext.decode(responce.responseText);
             console.log(data);
             d = data.mentorResponse.data;
             
             if (data.mentorResponse.Error == 1) {
             Ext.Function.defer(function () {
             Ext.Msg.alert('MELS', data.mentorResponse.Message);
             }, 100);
             return;
             } else if (data.mentorResponse.Error == 2) {
             Ext.Function.defer(function () {
             Ext.Msg.alert('MELS', data.mentorResponse.Message);
             }, 100);
             
             } else {
             Ext.Function.defer(function () {
             Ext.Msg.alert('MELS', data.mentorResponse.Message, function () {
             var View = Mentor.Global.NavigationStack.pop();
             var viewport = Ext.Viewport,
             mainPanel = viewport.down("#mainviewport");
             
             mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
             
             me.btnGetPendingRequest();
             });
             }, 100);
             
             }
             
             },
             failure: function (responce) {
             Ext.Viewport.setMasked(false); //Avinash
             Ext.Function.defer(function () {
             Ext.Msg.alert('MELS', Mentor.Global.ERROR_MESSAGE);
             }, 100);
             
             }
             });*/
        }
    },
    doSubmitIssueInviteRequest: function () {
        var GLOBAL = Mentor.Global;
        var MentorAcceptResponses = Ext.getCmp('idMentorAcceptResponses');
        var Comment = Ext.getCmp('commentsAcceptPendingRequest').getValue();
        var MentorAcceptResponsesID;
        var MentorAcceptResponsesName;
        var isChecked = 0;
        for (i = 0; i < MentorAcceptResponses.getItems().length; i++) {
            if (MentorAcceptResponses.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    MentorAcceptResponsesID = MentorAcceptResponses.getItems().getAt(i).getValue();
                    MentorAcceptResponsesName = MentorAcceptResponses.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                } else {
                    MentorAcceptResponsesID = MentorAcceptResponsesID + "," + MentorAcceptResponses.getItems().getAt(i).getValue();
                    MentorAcceptResponsesName = MentorAcceptResponsesName + ";" + MentorAcceptResponses.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                }
            }
        }

        /* if (Comment == "" && isChecked == 0) {
         Ext.Msg.alert('MELS', 'You must select the preparations that you expect the mentee to take before the meeting. If you select Other for Meeting Type, Meeting Place or Preparations, you must enter a comment about what you mean');
         } else {*/
        var IssueInvitationData = Ext.decode(localStorage.getItem('mentorIssueInvitationDetail'));
        IssueInvitationData['MentorAcceptResponsesID'] = MentorAcceptResponsesID;
        IssueInvitationData['MentorAcceptResponsesName'] = MentorAcceptResponsesName;
        IssueInvitationData['Comments'] = Comment;
        localStorage.setItem("mentorIssueInvitationDetail", Ext.encode(IssueInvitationData));
        var data = IssueInvitationData;
        console.log(data);

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "menteeInvitation",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.menteeInvitation.data;
                //MentorCommentReasonForMeeting.setValue('');
                if (data.menteeInvitation.Error == 1) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.menteeInvitation.Message);
                    }, 100);
                    return;
                } else if (data.menteeInvitation.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.menteeInvitation.Message);
                    }, 100);
                } else {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.menteeInvitation.Message, function () {
                            /*var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                             var mainMenu = mainPanel.down("mentorBottomTabView");
                             if (!mainMenu) {
                             mainMenu = mainPanel.add({xtype: "mentorBottomTabView"});
                             }
                             Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
                             mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
                             Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').setActiveItem(0);*/
                            var View = Mentor.Global.NavigationStack.pop();
                            var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                            mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
                            Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().setActiveTab(0);
                        });
                    }, 100);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
        //}
    },
    doSubmitAcceptRequest: function () {
        var GLOBAL = Mentor.Global;
        var MentorAcceptResponses = Ext.getCmp('idMentorAcceptResponses');
        var Comment = Ext.getCmp('commentsAcceptPendingRequest').getValue();
        var MentorAcceptResponsesID;
        var MentorAcceptResponsesName;
        var isChecked = 0;
        for (i = 0; i < MentorAcceptResponses.getItems().length; i++) {
            if (MentorAcceptResponses.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    MentorAcceptResponsesID = MentorAcceptResponses.getItems().getAt(i).getValue();
                    MentorAcceptResponsesName = MentorAcceptResponses.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                } else {
                    MentorAcceptResponsesID = MentorAcceptResponsesID + "," + MentorAcceptResponses.getItems().getAt(i).getValue();
                    MentorAcceptResponsesName = MentorAcceptResponsesName + ";" + MentorAcceptResponses.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                }
            }
        }

        if (Comment == "" && isChecked == 0) {
            Ext.Msg.alert('MELS', 'You must select the preparations that you expect the mentee to take before the meeting. If you select Other for Meeting Type, Meeting Place or Preparations, you must enter a comment about what you mean');
        } else {
            var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
            var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));

            var AcceptInviteDetail = Ext.decode(localStorage.getItem("mentorAcceptInvitationDetail"));
            AcceptInviteDetail['MentorAcceptResponsesID'] = MentorAcceptResponsesID;
            AcceptInviteDetail['MentorAcceptResponsesName'] = MentorAcceptResponsesName;
            //AcceptInviteDetail['Comments'] = Comment;
            localStorage.setItem("mentorAcceptInvitationDetail", Ext.encode(AcceptInviteDetail));
            var data = AcceptInviteDetail;

            Ext.Viewport.setMasked({
                xtype: "loadmask",
                message: "Please wait"
            });

            Ext.Ajax.request({
                url: GLOBAL.SERVER_URL,
                method: "POST",
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/json'
                },
                xhr2: true,
                disableCaching: false,
                jsonData: {
                    method: "mentorResponse",
                    body: data
                },
                success: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    var data = Ext.decode(responce.responseText);
                    console.log(data);
                    d = data.mentorResponse.data;

                    if (data.mentorResponse.Error == 1) {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.mentorResponse.Message);
                        }, 100);
                        return;
                    } else if (data.mentorResponse.Error == 2) {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.mentorResponse.Message);
                        }, 100);
                    } else {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.mentorResponse.Message, function () {
                                var View = Mentor.Global.NavigationStack.pop();
                                View = Mentor.Global.NavigationStack.pop();
                                var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                                mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
                                //me.btnGetPendingRequest();
                            });
                        }, 100);
                    }
                },
                failure: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    console.log(responce);
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            });
        }
    },
    doDeclineRequest: function () {
        var Record = Mentor.Global.MENTOR_PENDING_REQUEST;
        //var Comment = Ext.getCmp('commentsAcceptRejectPendingRequest').getValue();
        var Status = "Rejected";

        var data = {
            InvitationId: Record.InvitationId,
            Status: Status
        }

        localStorage.setItem("mentorDeclineInvitationDetail", null);
        localStorage.setItem("mentorDeclineInvitationDetail", Ext.encode(data));
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("declineresponses");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "declineresponses"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
        Ext.getCmp('commentsRejectPendingRequest').setValue('');
    },
    doSubmitDeclineRequest: function () {
        var GLOBAL = Mentor.Global;
        var MentorDeclineResponses = Ext.getCmp('idMentorDeclineResponses');
        var Comment = Ext.getCmp('commentsRejectPendingRequest').getValue();
        var MentorDeclineResponsesID;
        var MentorDeclineResponsesName;
        var isChecked = 0;
        for (i = 0; i < MentorDeclineResponses.getItems().length; i++) {
            if (MentorDeclineResponses.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    MentorDeclineResponsesID = MentorDeclineResponses.getItems().getAt(i).getValue();
                    MentorDeclineResponsesName = MentorDeclineResponses.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                } else {
                    MentorDeclineResponsesID = MentorDeclineResponsesID + "," + MentorDeclineResponses.getItems().getAt(i).getValue();
                    MentorDeclineResponsesName = MentorDeclineResponsesName + ";" + MentorDeclineResponses.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                }
            }
        }

        if (isChecked == 0) {
            Ext.Msg.alert('MELS', 'Please select one or more reasons from the list of reasons or specify a reason in the ' + Mentor.Global.MENTOR_NAME + ' comments');
        } else {
            var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
            var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));

            if (MentorLoginUser != null) {
                var DeclineInviteDetail = Ext.decode(localStorage.getItem("mentorDeclineInvitationDetail"));
                DeclineInviteDetail['MentorDeclineResponsesID'] = MentorDeclineResponsesID;
                DeclineInviteDetail['MentorDeclineResponsesName'] = MentorDeclineResponsesName;
                DeclineInviteDetail['Comments'] = Comment;
                localStorage.setItem("mentorDeclineInvitationDetail", Ext.encode(DeclineInviteDetail));
                var data = DeclineInviteDetail;

                Ext.Viewport.setMasked({
                    xtype: "loadmask",
                    message: "Please wait"
                });

                Ext.Ajax.request({
                    url: GLOBAL.SERVER_URL,
                    method: "POST",
                    dataType: 'json',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    xhr2: true,
                    disableCaching: false,
                    jsonData: {
                        method: "mentorResponse",
                        body: data
                    },
                    success: function (responce) {
                        Ext.Viewport.setMasked(false); //Avinash
                        var data = Ext.decode(responce.responseText);
                        console.log(data);
                        d = data.mentorResponse.data;

                        if (data.mentorResponse.Error == 1) {
                            Ext.Function.defer(function () {
                                Ext.Msg.alert('MELS', data.mentorResponse.Message);
                            }, 100);
                            return;
                        } else if (data.mentorResponse.Error == 2) {
                            Ext.Function.defer(function () {
                                Ext.Msg.alert('MELS', data.mentorResponse.Message);
                            }, 100);

                        } else {
                            Ext.Function.defer(function () {
                                Ext.Msg.alert('MELS', data.mentorResponse.Message, function () {
                                    var View = Mentor.Global.NavigationStack.pop();
                                    View = Mentor.Global.NavigationStack.pop();
                                    var viewport = Ext.Viewport,
                                            mainPanel = viewport.down("#mainviewport");

                                    mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
                                });
                            }, 100);
                        }
                    },
                    failure: function (responce) {
                        Ext.Viewport.setMasked(false); //Avinash
                        console.log(responce);
                        var connectionExist = Mentor.Global.doesConnectionExist();
                        var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', error);
                        }, 100);
                    }
                });
            } else if (MenteeLoginUser != null) {
                var DeclineInviteDetail = {};
                DeclineInviteDetail['MenteeDeclineResponsesID'] = MentorDeclineResponsesID;
                DeclineInviteDetail['MenteeDeclineResponsesName'] = MentorDeclineResponsesName;
                DeclineInviteDetail['Comments'] = Comment;
                DeclineInviteDetail['Status'] = "Rejected";
                var InvitationIndex = Ext.getCmp('idbtnAcceptMeetingMentee').getItemId();
                var InvitationData = Ext.getStore('WaitScreenMentorPending').data.getAt(InvitationIndex).data;
                DeclineInviteDetail['InvitationId'] = InvitationData.InvitationId;
                var data = DeclineInviteDetail;
                console.log(data);
                Ext.Viewport.setMasked({
                    xtype: "loadmask",
                    message: "Please wait"
                });

                Ext.Ajax.request({
                    url: GLOBAL.SERVER_URL,
                    method: "POST",
                    dataType: 'json',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    xhr2: true,
                    disableCaching: false,
                    jsonData: {
                        method: "menteeResponse",
                        body: data
                    },
                    success: function (responce) {
                        Ext.Viewport.setMasked(false); //Avinash
                        var data = Ext.decode(responce.responseText);
                        console.log(data);
                        d = data.menteeResponse.data;

                        if (data.menteeResponse.Error == 1) {
                            Ext.Function.defer(function () {
                                Ext.Msg.alert('MELS', data.menteeResponse.Message);
                            }, 100);
                            return;
                        } else if (data.menteeResponse.Error == 2) {
                            Ext.Function.defer(function () {
                                Ext.Msg.alert('MELS', data.menteeResponse.Message);
                            }, 100);

                        } else {
                            Ext.Function.defer(function () {
                                Ext.Msg.alert('MELS', data.menteeResponse.Message, function () {
                                    var View = Mentor.Global.NavigationStack.pop();
                                    var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                                    mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
                                });
                            }, 100);
                        }
                    },
                    failure: function (responce) {
                        Ext.Viewport.setMasked(false); //Avinash
                        console.log(responce);
                        var connectionExist = Mentor.Global.doesConnectionExist();
                        var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', error);
                        }, 100);
                    }
                });
            }
        }
    },
    btnBackAcceptRejectPendingRequest: function () {
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    btnBackWaitScreen: function () {
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    AcceptRescheduledMeetingByMentor: function () {
        var GLOBAL = Mentor.Global;
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var RescheduleMeetingButtonMentee = Ext.getCmp('idbtnRescheduleMeetingMentee');
        var WaitScreenData = Ext.getStore(Ext.getCmp('idStoreName').getValue()).getAt(parseInt(RescheduleMeetingButtonMentee.getItemId())).data;

        var data = {
            InvitationID: WaitScreenData.InvitationId
        };

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "acceptRescheduledMeetingByMentor",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.acceptRescheduledMeetingByMentor.data;

                if (data.acceptRescheduledMeetingByMentor.Error == 1 || data.acceptRescheduledMeetingByMentor.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.acceptRescheduledMeetingByMentor.Message);
                    }, 100);
                    return;
                } else {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.acceptRescheduledMeetingByMentor.Message, function () {
                            var View = Mentor.Global.NavigationStack.pop();
                            var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                            mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
                        });
                    }, 100);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    AcceptAcceptedMeetingByMentor: function () {
        var GLOBAL = Mentor.Global;
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var RescheduleMeetingButtonMentee = Ext.getCmp('idbtnRescheduleMeetingMentee');
        var WaitScreenData = Ext.getStore(Ext.getCmp('idStoreName').getValue()).getAt(parseInt(RescheduleMeetingButtonMentee.getItemId())).data;
        console.log(WaitScreenData);
        var data = {
            InvitationID: WaitScreenData.InvitationId,
            MentorID: WaitScreenData.MentorID,
            MenteeID: MenteeLoginUser.Id,
            MenteeName: MenteeLoginUser.MenteeName,
            MentorName: WaitScreenData.MentorName,
            MentorEmail: WaitScreenData.MentorEmail,
            MeetingDateTime: WaitScreenData.MeetingDateTime,
            MeetingTypeName: WaitScreenData.MeetingTypeName,
            TopicDescription: WaitScreenData.TopicDescription,
            MentorPhone: WaitScreenData.MentorPhone
        };

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "acceptAcceptedMeetingByMentor",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false);

                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.acceptAcceptedMeetingByMentor.data;

                if (data.acceptAcceptedMeetingByMentor.Error == 1 || data.acceptAcceptedMeetingByMentor.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.acceptAcceptedMeetingByMentor.Message);
                    }, 100);
                    return;
                } else {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.acceptAcceptedMeetingByMentor.Message, function () {
                            var View = Mentor.Global.NavigationStack.pop();
                            var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                            mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
                        });
                    }, 100);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    btnGotoWaitScreenInviteMentorScreen: function () {
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("waitscreen");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "waitscreen"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

        me.getMentorForWaitScreen();
    },
    getMentorForWaitScreen: function () {
        var GLOBAL = Mentor.Global;
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var data = {
            MenteeID: MenteeLoginUser.Id
        };

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getMentorWaitScreen",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getMentorWaitScreen.data;

                if (data.getMentorWaitScreen.Error == 1) {
                    Ext.getStore('WaitScreenMentor').clearData();
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getMentorWaitScreen.Message, function () {
                            Ext.getCmp('idMenteeInvitesTabView').setActiveItem(0);
                        });
                    }, 100);
                    return;
                } else if (data.getMentorWaitScreen.Error == 2) {
                    Ext.getStore('WaitScreenMentor').clearData();
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getMentorWaitScreen.Message);
                    }, 100);

                    var View = Mentor.Global.NavigationStack.pop();
                    var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                    mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
                    //me.btnGetPendingRequest();
                } else {
                    Ext.getStore('WaitScreenMentor').clearData();
                    Ext.getStore('WaitScreenMentor').add(data.getMentorWaitScreen.data);
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    getMentorWaitScreenAccepted: function (popup) {
        var GLOBAL = Mentor.Global;
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var data = {
            MenteeID: MenteeLoginUser.Id
        };
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getMentorWaitScreenAccepted",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getMentorWaitScreenAccepted.data;

                var MenteeAcceptedInvite = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar().getComponent(0);
                var MenteePendingInvite = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar().getComponent(1);
                var MenteeRejectedInvite = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar().getComponent(2);

                /* Ext.getCmp('idMenteeBottomTabView').getTabBar().getComponent(0).setTitle(Mentor.Global.MENTEE_INVITES_TITLE + "(" +
                 data.getMentorWaitScreenAccepted.AcceptedCount + "/" + data.getMentorWaitScreenAccepted.PendingCount + "/" + data.getMentorWaitScreenAccepted.DeclinedCount + ")");*/
                Ext.getCmp('idPendingInvitesMenteeLeftMenu').setText(Mentor.Global.MENTEE_INVITES_TITLE + " (" +
                        data.getMentorWaitScreenAccepted.AcceptedCount + "/" + data.getMentorWaitScreenAccepted.PendingCount + "/" + data.getMentorWaitScreenAccepted.DeclinedCount + ")");

                MenteePendingInvite.setTitle(
                        Mentor.Global.MENTEE_PENDING_TITLE + " (" + data.getMentorWaitScreenAccepted.PendingCount + ")");
                MenteeAcceptedInvite.setTitle(
                        Mentor.Global.MENTEE_ACCEPTED_TITLE + " (" + data.getMentorWaitScreenAccepted.AcceptedCount + ")");
                MenteeRejectedInvite.setTitle(
                        Mentor.Global.MENTEE_DECLINED_TITLE + " (" + data.getMentorWaitScreenAccepted.DeclinedCount + ")");

                if (data.getMentorWaitScreenAccepted.AcceptedCount == 0) {
                    Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').setActiveItem(1);
                    //Ext.getCmp('idMenteeInvitesTabView').setActiveItem(1);
                    return false;
                }

                if (data.getMentorWaitScreenAccepted.Error == 1) {
                    Ext.getStore('WaitScreenMentorAccepted').clearData();
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getMentorWaitScreenAccepted.Message, function () {
                            Ext.getCmp('idMenteeInvitesTabView').setActiveItem(0);
                        });
                    }, 100);
                    return;
                } else if (data.getMentorWaitScreenAccepted.Error == 2) {
                    Ext.getStore('WaitScreenMentorAccepted').clearData();
                    if (typeof popup == 'undefined') {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.getMentorWaitScreenAccepted.Message);
                        }, 100);
                    }

                    var View = Mentor.Global.NavigationStack.pop();
                    var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                    mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
                    //me.btnGetPendingRequest();
                } else {
                    Ext.getStore('WaitScreenMentorAccepted').clearData();
                    Ext.getStore('WaitScreenMentorAccepted').add(data.getMentorWaitScreenAccepted.data);
                }
                Mentor.app.application.getController('Main').getActionItemComments();
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                if (typeof popup == 'undefined') {
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            }
        });
    },
    getMentorWaitScreenPending: function (popup) {
        var GLOBAL = Mentor.Global;
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var data = {
            MenteeID: MenteeLoginUser.Id
        }
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getMentorWaitScreenPending",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getMentorWaitScreenPending.data;

                var MenteeAcceptedInvite = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar().getComponent(0);
                var MenteePendingInvite = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar().getComponent(1);
                var MenteeRejectedInvite = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar().getComponent(2);

                /* Ext.getCmp('idMenteeBottomTabView').getTabBar().getComponent(0).setTitle(Mentor.Global.MENTEE_INVITES_TITLE + "(" +
                 data.getMentorWaitScreenPending.AcceptedCount + "/" + data.getMentorWaitScreenPending.PendingCount + "/" + data.getMentorWaitScreenPending.DeclinedCount + ")");*/
                Ext.getCmp('idPendingInvitesMenteeLeftMenu').setText(Mentor.Global.MENTEE_INVITES_TITLE + " (" +
                        data.getMentorWaitScreenPending.AcceptedCount + "/" + data.getMentorWaitScreenPending.PendingCount + "/" + data.getMentorWaitScreenPending.DeclinedCount + ")");

                MenteePendingInvite.setTitle(
                        Mentor.Global.MENTEE_PENDING_TITLE + " (" + data.getMentorWaitScreenPending.PendingCount + ")");
                MenteeAcceptedInvite.setTitle(
                        Mentor.Global.MENTEE_ACCEPTED_TITLE + " (" + data.getMentorWaitScreenPending.AcceptedCount + ")");
                MenteeRejectedInvite.setTitle(
                        Mentor.Global.MENTEE_DECLINED_TITLE + " (" + data.getMentorWaitScreenPending.DeclinedCount + ")");


                if (data.getMentorWaitScreenPending.PendingCount == 0 && data.getMentorWaitScreenPending.AcceptedCount > 0) {
                    Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').setActiveItem(0);
                    return false;
                } else if (data.getMentorWaitScreenPending.PendingCount == 0) {
                    Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').setActiveItem(2);
                    return false;
                }

                if (data.getMentorWaitScreenPending.Error == 1) {
                    Ext.getStore('WaitScreenMentorPending').clearData();
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getMentorWaitScreenPending.Message, function () {
                            Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').setActiveItem(0);
                        });
                    }, 100);
                    return;
                } else if (data.getMentorWaitScreenPending.Error == 2) {
                    Ext.getStore('WaitScreenMentorPending').clearData();
                    if (typeof popup == 'undefined') {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.getMentorWaitScreenPending.Message);
                        }, 100);
                    }

                    var View = Mentor.Global.NavigationStack.pop();
                    var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                    mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
                    //me.btnGetPendingRequest();
                } else {
                    Ext.getStore('WaitScreenMentorPending').clearData();
                    Ext.getStore('WaitScreenMentorPending').add(data.getMentorWaitScreenPending.data);
                }
                Mentor.app.application.getController('Main').getActionItemComments();
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                if (typeof popup == 'undefined') {
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            }
        });
    },
    getMentorWaitScreenRejected: function (popup) {
        var GLOBAL = Mentor.Global;
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var data = {
            MenteeID: MenteeLoginUser.Id
        };
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getMentorWaitScreenRejected",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getMentorWaitScreenRejected.data;

                var MenteeAcceptedInvite = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar().getComponent(0);
                var MenteePendingInvite = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar().getComponent(1);
                var MenteeRejectedInvite = Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').getTabBar().getComponent(2);

                /*Ext.getCmp('idMenteeBottomTabView').getTabBar().getComponent(0).setTitle(Mentor.Global.MENTEE_INVITES_TITLE + "(" +
                 data.getMentorWaitScreenRejected.AcceptedCount + "/" + data.getMentorWaitScreenRejected.PendingCount + "/" + data.getMentorWaitScreenRejected.DeclinedCount + ")");*/
                Ext.getCmp('idPendingInvitesMenteeLeftMenu').setText(Mentor.Global.MENTEE_INVITES_TITLE + " (" +
                        data.getMentorWaitScreenRejected.AcceptedCount + "/" + data.getMentorWaitScreenRejected.PendingCount + "/" + data.getMentorWaitScreenRejected.DeclinedCount + ")");

                MenteePendingInvite.setTitle(
                        Mentor.Global.MENTEE_PENDING_TITLE + " (" + data.getMentorWaitScreenRejected.PendingCount + ")");
                MenteeAcceptedInvite.setTitle(
                        Mentor.Global.MENTEE_ACCEPTED_TITLE + " (" + data.getMentorWaitScreenRejected.AcceptedCount + ")");
                MenteeRejectedInvite.setTitle(
                        Mentor.Global.MENTEE_DECLINED_TITLE + " (" + data.getMentorWaitScreenRejected.DeclinedCount + ")");


                if (data.getMentorWaitScreenRejected.DeclinedCount == 0 && data.getMentorWaitScreenRejected.AcceptedCount > 0) {
                    Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').setActiveItem(0);
                    return false;
                } else if (data.getMentorWaitScreenRejected.DeclinedCount == 0 && data.getMentorWaitScreenRejected.PendingCount > 0) {
                    Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').setActiveItem(1);
                    return false;
                } else if (data.getMentorWaitScreenRejected.DeclinedCount == 0) {
                    Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').setActiveItem(3);
                    return false;
                }

                if (data.getMentorWaitScreenRejected.Error == 1) {
                    Ext.getStore('WaitScreenMentorRejected').clearData();
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getMentorWaitScreenRejected.Message, function () {
                            Ext.getCmp('idPendingInvitesTabInvitesMenteeBottomTabView').setActiveItem(0);
                        });
                    }, 100);
                    return;
                } else if (data.getMentorWaitScreenRejected.Error == 2) {
                    Ext.getStore('WaitScreenMentorRejected').clearData();
                    if (typeof popup == 'undefined') {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.getMentorWaitScreenRejected.Message);
                        }, 100);
                    }

                    var View = Mentor.Global.NavigationStack.pop();
                    var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                    mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
                    //me.btnGetPendingRequest();
                } else {
                    Ext.getStore('WaitScreenMentorRejected').clearData();
                    Ext.getStore('WaitScreenMentorRejected').add(data.getMentorWaitScreenRejected.data);
                }
                Mentor.app.application.getController('Main').getActionItemComments();
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                if (typeof popup == 'undefined') {
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            }
        });
    },
    getMettingWaitScreenInfo: function () {
        var GLOBAL = Mentor.Global;
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var MentorID = Ext.getCmp('idMentorSelectField').getValue();
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var data = {
            MenteeID: MenteeLoginUser.Id,
            MentorID: MentorID
        };
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getMettingWaitScreenInfo",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getMettingWaitScreenInfo.data;

                if (data.getMettingWaitScreenInfo.Error == 1) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getMettingWaitScreenInfo.Message);
                    }, 100);
                    return;
                } else if (data.getMettingWaitScreenInfo.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getMettingWaitScreenInfo.Message);
                    }, 100);
                    return;
                } else {
                    var record = data.getMettingWaitScreenInfo.data[0];
                    if (record.Status == "Accepted") {
                        Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(true);
                        Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(false);
                        Ext.getCmp('waitScreenMeetingStatus').setValue('Invitation has accepted by mentor.');
                    } else if (record.Status == "Rejected") {
                        Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(true);
                        Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(false);
                        Ext.getCmp('waitScreenMeetingStatus').setValue('Invitation has declined by mentor.');
                    } else if (record.Status == "Pending") {
                        Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(false);
                        Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(false);
                        Ext.getCmp('waitScreenMeetingStatus').setValue('Invitation has not accepted by mentor yet.');
                    }

                    if (record.Meeting_status == 1) {
                        Ext.getCmp('waitScreenMeetingStatus').setValue('Meeting has started.');
                    } else if (record.Meeting_status == 3) {
                        Ext.getCmp('waitScreenMeetingStatus').setValue('Meeting has ended.');
                    }
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    btnHistoryScreenWaitScreen: function () {

    },
    btnRefreshMeetingHistory: function () {
        this.getMeetingHistory();
    },
    btnRefreshTeamListing: function () {
        Mentor.app.application.getController('UpdateProfilePicture').btnGetUpdatedTeamData();
    },
    btnRefreshUserNotes: function () {
        Mentor.app.application.getController('Main').getUserNotes(true);
    },
    onSearchKeyUpMentorMettingHistory: function (searchField) {
        queryString = searchField.getValue();
        console.log(this, 'Please search by: ' + queryString);

        var store = Ext.getStore('MeetingHistory');
        store.clearFilter();

        if (queryString) {
            var thisRegEx = new RegExp(queryString, "i");
            store.filterBy(function (record) {
                if (thisRegEx.test(record.get('MenteeName')) ||
                        thisRegEx.test(record.get('SubTopicName')) ||
                        thisRegEx.test(record.get('TopicDescription'))) {
                    return true;
                }
                return false;
            });
        }
    },
    onSearchKeyUpMentorTeams: function (searchField) {
        queryString = searchField.getValue();
        console.log(this, 'Please search by: ' + queryString);

        var store = Ext.getStore('TeamListing');
        store.clearFilter();

        if (queryString) {
            var thisRegEx = new RegExp(queryString, "i");
            store.filterBy(function (record) {
                if (thisRegEx.test(record.get('TeamName')) ||
                        thisRegEx.test(record.get('TeamDescription')) ||
                        thisRegEx.test(record.get('MaxMembers')) ||
                        thisRegEx.test(record.get('OwnerName')) ||
                        thisRegEx.test(record.get('Skills'))) {
                    return true;
                }
                return false;
            });
        }
    },
    //On Search Mentee Metting History
    onSearchKeyUpMenteeMettingHistory: function (searchField) {
        queryString = searchField.getValue();
        console.log(this, 'Please search by: ' + queryString);

        var store = Ext.getStore('MeetingHistory');
        store.clearFilter();

        if (queryString) {
            var thisRegEx = new RegExp(queryString, "i");
            store.filterBy(function (record) {
                if (thisRegEx.test(record.get('MentorName')) ||
                        thisRegEx.test(record.get('SubTopicName')) ||
                        thisRegEx.test(record.get('TopicDescription'))) {
                    return true;
                }
                return false;
            });
        }
    },
    onSearchKeyUpTeam: function (searchField) {
        queryString = searchField.getValue();
        console.log(this, 'Please search by: ' + queryString);

        var store = Ext.getStore('TeamMembers');
        store.clearFilter();

        if (queryString) {
            var thisRegEx = new RegExp(queryString, "i");
            store.filterBy(function (record) {
                if (thisRegEx.test(record.get('UserName'))) {
                    return true;
                }
                return false;
            });
        }
    },
    onSearchKeyUpUserNotes: function (searchField) {
        queryString = searchField.getValue();
        console.log(this, 'Please search by: ' + queryString);

        var store = Ext.getStore('UserNotes');
        store.clearFilter();

        if (queryString) {
            var thisRegEx = new RegExp(queryString, "i");
            store.filterBy(function (record) {
                if (thisRegEx.test(record.get('NoteTitle')) ||
                        thisRegEx.test(record.get('CreatedDate')) ||
                        thisRegEx.test(record.get('UpdatedDate')) ||
                        thisRegEx.test(record.get('NoteDescription'))) {
                    return true;
                }
                return false;
            });
        }
    },
    // From Invite List from Mentee Login go to Wait Screen
    displayInvitationSendDetailInWaitScreen: function (view, index, item, e) {
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));

        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("waitscreen");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "waitscreen"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

        //UnHide Meeting Place
        Ext.getCmp('idLabelMeetingPlaceWaitScreen').setHidden(false);
        Ext.getCmp('idMeetingPlaceWaitScreen').setHidden(false);

        //UnHide Meeting Type
        Ext.getCmp('idLabelMeetingTypeWaitScreen').setHidden(false);
        Ext.getCmp('idMeetingTypeWaitScreen').setHidden(false);

        //UnHide Meeting DateTime
        Ext.getCmp('idLabelMeetingDateTimeWaitScreen').setHidden(false);
        Ext.getCmp('idMeetingDateTimeWaitScreen').setHidden(false);

        //Hide Meeting Cancellation comment
        Ext.getCmp('idLabelMentorCancellationCommentsWaitScreen').setHidden(true);
        Ext.getCmp('mentorCancellationCommentWaitScreen').setHidden(true);
        Ext.getCmp('mentorCancellationCommentWaitScreen').setValue("");

        //Hide Meeting Reschedule comment
        Ext.getCmp('idLabelRescheduleCommentsWaitScreen').setHidden(true);
        Ext.getCmp('idRescheduleCommentsWaitScreen').setHidden(true);
        Ext.getCmp('idRescheduleCommentsWaitScreen').setValue("");

        //UnHide Cancel Meeting Button and set the index for using while cancelling meeting
        var CancelMeetingButtonMentee = Ext.getCmp('idBtnCancelMeetingMentee');
        CancelMeetingButtonMentee.setHidden(false);
        CancelMeetingButtonMentee.setItemId("" + index);

        //UnHide Rechedule Meeting Button and set the index for using while rescheduling meeting
        var RescheduleMeetingButtonMentee = Ext.getCmp('idbtnRescheduleMeetingMentee');
        RescheduleMeetingButtonMentee.setHidden(false);
        RescheduleMeetingButtonMentee.setItemId("" + index);

        var AcceptMeetingButtonMentee = Ext.getCmp('idbtnAcceptMeetingMentee');
        var DeclineMeetingButtonMentee = Ext.getCmp('idBtnDeclineMeetingMentee');
        AcceptMeetingButtonMentee.setHidden(true);
        DeclineMeetingButtonMentee.setHidden(true);
        AcceptMeetingButtonMentee.setItemId("" + index);
        DeclineMeetingButtonMentee.setItemId("" + index);

        var WaitScreenRecord = view.getStore().getAt(index).data;
        var StoreName = view.getStore().getStoreId();
        Ext.getCmp('idStoreName').setValue(StoreName);
        Ext.getCmp('idMentorSelectField').setStore(StoreName);

        var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginDetail = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));

        if (MentorLoginDetail != null) {
            Ext.getCmp('idUserNameWaitScreen').setHtml(MentorLoginDetail.MentorName);
        } else if (MenteeLoginDetail != null) {
            Ext.getCmp('idUserNameWaitScreen').setHtml(MenteeLoginDetail.MenteeName);
        }

        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        Ext.getCmp('idLabelMentorCommentsWaitScreen').setHtml(Mentor.Global.MENTOR_NAME + ' Comment');
        Ext.getCmp('idLabelMentorNameListWaitScreen').setTitle(Mentor.Global.MENTOR_NAME + ' Name');
        Ext.getCmp('idLabelMenteeCommentsWaitScreen').setHtml(Mentor.Global.MENTEE_NAME + ' Comment');

        Ext.getCmp('idLabelMentorEmailWaitScreen').setHtml(Mentor.Global.MENTOR_NAME + ' Email');
        Ext.getCmp('idLabelMentorPhoneNoWaitScreen').setHtml(Mentor.Global.MENTOR_NAME + ' Phone No');


        Ext.getCmp('idMentorSelectField').setValue(WaitScreenRecord.InvitationId);
        Ext.getCmp('idInfoMentorNameWaitScreen').setHtml('Meeting Request to ' + WaitScreenRecord.MentorName); //Avinash 

        Ext.getCmp('mentorCommentWaitScreen').setValue(WaitScreenRecord.MentorComments);
        Ext.getCmp('menteeCommentWaitScreen').setValue(WaitScreenRecord.MenteeComment);

        Ext.getCmp('idMeetingPlaceWaitScreen').setValue(WaitScreenRecord.MeetingPlaceName);
        Ext.getCmp('idMeetingDateTimeWaitScreen').setValue(WaitScreenRecord.MeetingDateTime);
        Ext.getCmp('waitScreenMeetingMainTopic').setValue(WaitScreenRecord.TopicDescription);
        Ext.getCmp('idMeetingTypeWaitScreen').setValue(WaitScreenRecord.MeetingTypeName);

        Ext.getCmp('idMentorEmailWaitScreen').setValue(WaitScreenRecord.MentorEmail).setHidden(true);
        Ext.getCmp('idMentorPhoneWaitScreen').setValue(WaitScreenRecord.MentorPhone).setHidden(true);
        Ext.getCmp('idIsPhoneSMSCapableWaitScreen').setChecked(parseInt(WaitScreenRecord.MentorSmsCapable));
        Ext.getCmp('idIsPhoneSMSCapableWaitScreen').setHidden(true);
        Ext.getCmp('idLabelMentorEmailWaitScreen').setHidden(true);
        Ext.getCmp('idLabelMentorPhoneNoWaitScreen').setHidden(true);

        Ext.getCmp('idBtnRefreshWaitScreen').setHidden(false);

        Ext.getCmp('idLabelMeetingTypeWaitScreen').setHidden(false);
        Ext.getCmp('idMeetingTypeWaitScreen').setHidden(false);
        Ext.getCmp('idLabelMeetingPlaceWaitScreen').setHidden(false);
        Ext.getCmp('idMeetingPlaceWaitScreen').setHidden(false);
        Ext.getCmp('idLabelMeetingDateTimeWaitScreen').setHidden(false);
        Ext.getCmp('idMeetingDateTimeWaitScreen').setHidden(false);
        Ext.getCmp('mentorCommentWaitScreen').setHidden(false);
        Ext.getCmp('idLabelMentorCommentsWaitScreen').setHidden(false);
        Ext.getCmp('idAcceptRescheduledMeetingByMentor').setHidden(true);
        Ext.getCmp('idbtnAcceptMeetingMenteeNew').setHidden(true);
        //Ext.getCmp('idAcceptAcceptedMeetingByMentor').setHidden(true);

        Ext.getCmp('idMentorAcceptDeclineResponsesDisplay').setValue("").setHidden(true);
        Ext.getCmp('idLabelMentorAcceptDeclineResponsesDisplay').setHidden(true);
        Ext.getCmp('idBtnCancelMeetingMentee').setText('CANCEL INVITE');

        Ext.getCmp('idOKMentor').setHidden(false);
        //Ext.getCmp('idBtnOkMeetingMentee').setHidden(true);

        if (WaitScreenRecord.Status == "Pending" && WaitScreenRecord.InitiatedBy == 0) {

            Ext.getCmp('idTitleWaitscreen').setTitle('Pending Meeting');

            //Ext.getCmp('idAcceptAcceptedMeetingByMentor').setHidden(false);

            Ext.getCmp('idLabelMeetingTypeWaitScreen').setHidden(false);
            Ext.getCmp('idMeetingTypeWaitScreen').setHidden(false);
            Ext.getCmp('idLabelMeetingPlaceWaitScreen').setHidden(false);
            Ext.getCmp('idMeetingPlaceWaitScreen').setHidden(false);
            Ext.getCmp('idLabelMeetingDateTimeWaitScreen').setHidden(false);
            Ext.getCmp('idMeetingDateTimeWaitScreen').setHidden(false);
            Ext.getCmp('mentorCommentWaitScreen').setHidden(false);
            Ext.getCmp('idLabelMentorCommentsWaitScreen').setHidden(false);
            Ext.getCmp('idbtnRescheduleMeetingMentee').setHidden(false);

            Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(false);
            Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(false);
            Ext.getCmp('waitScreenMeetingStatus').setValue(MenteeLoginUser.MenteeName + " has not yet responded.");

            Ext.getCmp('idbtnAcceptMeetingMentee').setHidden(true);
            Ext.getCmp('idbtnAcceptMeetingMenteeNew').setHidden(false);
            Ext.getCmp('idBtnDeclineMeetingMentee').setHidden(true);
            Ext.getCmp('idbtnRescheduleMeetingMentee').setHidden(false);
            Ext.getCmp('idBtnCancelMeetingMentee').setHidden(false);

            Ext.getCmp('idLabelMenteeCommentsWaitScreen').setHidden(false);
            Ext.getCmp('menteeCommentWaitScreen').setHidden(false);
            Ext.getCmp('menteeCommentWaitScreen').setDisabled(false);
            Ext.getCmp('idBtnCancelMeetingMentee').setText('DECLINE MEETING');
            Ext.getCmp('menteeCommentWaitScreen').setCls('textFieldDisableCLS');


        } else if (WaitScreenRecord.Status == "Accepted") {
            if (WaitScreenRecord.InitiatedBy == 0) {
                //Ext.getCmp('idAcceptAcceptedMeetingByMentor').setHidden(true);
            } else {
                //Ext.getCmp('idAcceptAcceptedMeetingByMentor').setHidden(false);
            }
            Ext.getCmp('menteeCommentWaitScreen').setDisabled(true);
            //Ext.getCmp('idAcceptAcceptedMeetingByMentor').setHidden(false);

            Ext.getCmp('idTitleWaitscreen').setTitle('Accepted Invite');
            Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(true);
            Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(false);
            Ext.getCmp('waitScreenMeetingStatus').setValue(WaitScreenRecord.MentorName + ' accepted your invitation.');

            Ext.getCmp('idLabelMentorAcceptDeclineResponsesDisplay').setHtml('Pre-Meeting Prep').setHidden(false);
            Ext.getCmp('idMentorAcceptDeclineResponsesDisplay').setHidden(false).setValue(WaitScreenRecord.AcceptResponsesName.replace(/;/g, "\n"));
            //Ext.getCmp('idBtnCancelMeetingMentee').setText('CANCEL MEETING');
            Ext.getCmp('idBtnCancelMeetingMentee').setText('CANCEL MEET');

        } else if (WaitScreenRecord.Status == "Rejected") {
            Ext.getCmp('idOKMentor').setHidden(false);
            Ext.getCmp('menteeCommentWaitScreen').setDisabled(true);
            Ext.getCmp('idTitleWaitscreen').setTitle('Declined Invite');
            Ext.getCmp('idLabelMeetingTypeWaitScreen').setHidden(true);
            Ext.getCmp('idMeetingTypeWaitScreen').setHidden(true);
            Ext.getCmp('idLabelMeetingPlaceWaitScreen').setHidden(true);
            Ext.getCmp('idMeetingPlaceWaitScreen').setHidden(true);
            Ext.getCmp('idLabelMeetingDateTimeWaitScreen').setHidden(true);
            Ext.getCmp('idMeetingDateTimeWaitScreen').setHidden(true);

            Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(true);
            Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(false);
            Ext.getCmp('waitScreenMeetingStatus').setValue(WaitScreenRecord.MentorName + ' declined your invitation.');
            Ext.getCmp('idBtnRefreshWaitScreen').setHidden(true);

            //Hide Meeting Place
            Ext.getCmp('idLabelMeetingPlaceWaitScreen').setHidden(true);
            Ext.getCmp('idMeetingPlaceWaitScreen').setHidden(true);

            //Hide Meeting Type
            Ext.getCmp('idLabelMeetingTypeWaitScreen').setHidden(true);
            Ext.getCmp('idMeetingTypeWaitScreen').setHidden(true);

            //Hide Meeting DateTime
            Ext.getCmp('idLabelMeetingDateTimeWaitScreen').setHidden(true);
            Ext.getCmp('idMeetingDateTimeWaitScreen').setHidden(true);
            Ext.getCmp('idBtnCancelMeetingMentee').setHidden(true);
            Ext.getCmp('idbtnRescheduleMeetingMentee').setHidden(true);

            Ext.getCmp('idLabelMentorAcceptDeclineResponsesDisplay').setHtml('Decline Responses').setHidden(false);
            Ext.getCmp('idMentorAcceptDeclineResponsesDisplay').setHidden(false).setValue(WaitScreenRecord.DeclineResponsesName.replace(/;/g, "\n"));

        } else if (WaitScreenRecord.Status == "Pending") {

            Ext.getCmp('idTitleWaitscreen').setTitle('Pending Invite');

            if (WaitScreenRecord.InitiatedBy == 0) {
                Ext.getCmp('idLabelMeetingTypeWaitScreen').setHidden(false);
                Ext.getCmp('idMeetingTypeWaitScreen').setHidden(false);
                Ext.getCmp('idLabelMeetingPlaceWaitScreen').setHidden(false);
                Ext.getCmp('idMeetingPlaceWaitScreen').setHidden(false);
                Ext.getCmp('idLabelMeetingDateTimeWaitScreen').setHidden(false);
                Ext.getCmp('idMeetingDateTimeWaitScreen').setHidden(false);
                Ext.getCmp('mentorCommentWaitScreen').setHidden(false);
                Ext.getCmp('idLabelMentorCommentsWaitScreen').setHidden(false);
                Ext.getCmp('idbtnRescheduleMeetingMentee').setHidden(false);

                Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(false);
                Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(false);
                Ext.getCmp('waitScreenMeetingStatus').setValue(MenteeLoginUser.MenteeName + " has not yet responded.");

                Ext.getCmp('idbtnAcceptMeetingMentee').setHidden(false);
                Ext.getCmp('idBtnDeclineMeetingMentee').setHidden(false);
                Ext.getCmp('idbtnRescheduleMeetingMentee').setHidden(true);
                Ext.getCmp('idBtnCancelMeetingMentee').setHidden(true);

                Ext.getCmp('idLabelMenteeCommentsWaitScreen').setHidden(false);
                Ext.getCmp('menteeCommentWaitScreen').setHidden(false);
                Ext.getCmp('menteeCommentWaitScreen').setDisabled(false);

            } else {
                Ext.getCmp('idLabelMenteeCommentsWaitScreen').setHidden(false);
                Ext.getCmp('menteeCommentWaitScreen').setHidden(false);
                Ext.getCmp('menteeCommentWaitScreen').setDisabled(true);
                Ext.getCmp('idbtnAcceptMeetingMentee').setHidden(true);
                Ext.getCmp('idBtnDeclineMeetingMentee').setHidden(true);
                Ext.getCmp('idBtnCancelMeetingMentee').setHidden(false);
                Ext.getCmp('idbtnRescheduleMeetingMentee').setHidden(false);

                Ext.getCmp('idLabelMeetingTypeWaitScreen').setHidden(true);
                Ext.getCmp('idMeetingTypeWaitScreen').setHidden(true);
                Ext.getCmp('idLabelMeetingPlaceWaitScreen').setHidden(true);
                Ext.getCmp('idMeetingPlaceWaitScreen').setHidden(true);
                Ext.getCmp('idLabelMeetingDateTimeWaitScreen').setHidden(true);
                Ext.getCmp('idMeetingDateTimeWaitScreen').setHidden(true);
                Ext.getCmp('mentorCommentWaitScreen').setHidden(true);
                Ext.getCmp('idLabelMentorCommentsWaitScreen').setHidden(true);
                Ext.getCmp('idbtnRescheduleMeetingMentee').setHidden(true);

                Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(false);
                Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(false);
                Ext.getCmp('waitScreenMeetingStatus').setValue(WaitScreenRecord.MentorName + " has not yet responded.");
            }

        } else if (WaitScreenRecord.Status == "Cancelled") {
            Ext.getCmp('idOKMentor').setHidden(false);
            if (WaitScreenRecord.InitiatedBy == '0') {
                Ext.getCmp('idTitleWaitscreen').setTitle('Cancelled Detail');
            } else if (WaitScreenRecord.InitiatedBy == '1') {
                if (WaitScreenRecord.cancelledBy == '0') {
                    Ext.getCmp('idTitleWaitscreen').setTitle('Cancelled Detail');
                } else if (WaitScreenRecord.cancelledBy == '1') {
                    Ext.getCmp('idTitleWaitscreen').setTitle('Cancelled Invite');
                }
            }
            if (WaitScreenRecord.MeetingDateTime == "") {
                Ext.getCmp('idLabelMeetingTypeWaitScreen').setHidden(true);
                Ext.getCmp('idMeetingTypeWaitScreen').setHidden(true);
                Ext.getCmp('idLabelMeetingPlaceWaitScreen').setHidden(true);
                Ext.getCmp('idMeetingPlaceWaitScreen').setHidden(true);
                Ext.getCmp('idLabelMeetingDateTimeWaitScreen').setHidden(true);
                Ext.getCmp('idMeetingDateTimeWaitScreen').setHidden(true);
                Ext.getCmp('mentorCommentWaitScreen').setHidden(true);
                Ext.getCmp('idLabelMentorCommentsWaitScreen').setHidden(true);
            }
            if (WaitScreenRecord.cancelledBy == '1') {
                Ext.getCmp('idLabelMentorCancellationCommentsWaitScreen').setHidden(false);
                Ext.getCmp('mentorCancellationCommentWaitScreen').setHidden(false);
                Ext.getCmp('idLabelMentorCancellationCommentsWaitScreen').setHtml(Mentor.Global.MENTEE_NAME + ' Comment for Cancellation');
                Ext.getCmp('mentorCancellationCommentWaitScreen').setValue(WaitScreenRecord.cancellationReason);
            } else if (WaitScreenRecord.cancelledBy == '0') {
                Ext.getCmp('idLabelMentorCancellationCommentsWaitScreen').setHidden(false);
                Ext.getCmp('idLabelMentorCancellationCommentsWaitScreen').setHtml(Mentor.Global.MENTOR_NAME + ' Comment for Cancellation');
                Ext.getCmp('mentorCancellationCommentWaitScreen').setHidden(false);
                Ext.getCmp('mentorCancellationCommentWaitScreen').setValue(WaitScreenRecord.cancellationReason);
            }

            Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(false);
            Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(false);

            if (WaitScreenRecord.cancelledBy == '1') {
                Ext.getCmp('waitScreenMeetingStatus').setValue(MenteeLoginDetail.MenteeName + " has cancelled the invitation.");
            } else if (WaitScreenRecord.cancelledBy == '0') {
                Ext.getCmp('waitScreenMeetingStatus').setValue(WaitScreenRecord.MentorName + " has cancelled the invitation.");
            }

            Ext.getCmp('idLabelMentorCancellationCommentsWaitScreen').setHidden(false);
            Ext.getCmp('mentorCancellationCommentWaitScreen').setHidden(false);
            Ext.getCmp('mentorCancellationCommentWaitScreen').setValue(WaitScreenRecord.cancellationReason);
            Ext.getCmp('idBtnCancelMeetingMentee').setHidden(true);
            RescheduleMeetingButtonMentee.setHidden(true);

//            Ext.getCmp('idBtnCancelMeetingMentee').setHidden(true);
//            Ext.getCmp('idbtnRescheduleMeetingMentee').setHidden(true);
        } else if (WaitScreenRecord.Status == "Rescheduled") {
            console.log(WaitScreenRecord);
//            Ext.getCmp('idTitleWaitscreen').setTitle('Pending Edit Details');

            if (WaitScreenRecord.MeetingDateTime == "") {
                Ext.getCmp('idLabelMeetingTypeWaitScreen').setHidden(true);
                Ext.getCmp('idMeetingTypeWaitScreen').setHidden(true);
                Ext.getCmp('idLabelMeetingPlaceWaitScreen').setHidden(true);
                Ext.getCmp('idMeetingPlaceWaitScreen').setHidden(true);
                Ext.getCmp('idLabelMeetingDateTimeWaitScreen').setHidden(true);
                Ext.getCmp('idMeetingDateTimeWaitScreen').setHidden(true);
                Ext.getCmp('mentorCommentWaitScreen').setHidden(true);
                Ext.getCmp('idLabelMentorCommentsWaitScreen').setHidden(true);
            }
            if (WaitScreenRecord.rescheduledBy == '1') {
                Ext.getCmp('idTitleWaitscreen').setTitle('Requested Reschedule');
                Ext.getCmp('idLabelRescheduleCommentsWaitScreen').setHidden(false);
                Ext.getCmp('idLabelRescheduleCommentsWaitScreen').setHtml(Mentor.Global.MENTEE_NAME + ' Comment for Reschedule');

                Ext.getCmp('idRescheduleCommentsWaitScreen').setHidden(false);
                Ext.getCmp('idRescheduleCommentsWaitScreen').setValue(WaitScreenRecord.RescheduleComments);
            } else if (WaitScreenRecord.rescheduledBy == '0') {
                Ext.getCmp('idTitleWaitscreen').setTitle('Reschedule Requested');
                Ext.getCmp('idAcceptRescheduledMeetingByMentor').setHidden(false);
                Ext.getCmp('idLabelRescheduleCommentsWaitScreen').setHidden(false);
                Ext.getCmp('idLabelRescheduleCommentsWaitScreen').setHtml(Mentor.Global.MENTOR_NAME + ' Comment for Reschedule');

                Ext.getCmp('idRescheduleCommentsWaitScreen').setHidden(false);
                Ext.getCmp('idRescheduleCommentsWaitScreen').setValue(WaitScreenRecord.RescheduleComments);
            }

            Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(false);
            Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(false);

            if (WaitScreenRecord.rescheduledBy == '1') {
                //Ext.getCmp('waitScreenMeetingStatus').setValue(MenteeLoginDetail.MenteeName + " has rescheduled the invitation.");
                Ext.getCmp('waitScreenMeetingStatus').setValue("Requested reschedule");
            } else if (WaitScreenRecord.rescheduledBy == '0') {
                //Ext.getCmp('waitScreenMeetingStatus').setValue(WaitScreenRecord.MentorName + " has rescheduled the invitation.");
                Ext.getCmp('waitScreenMeetingStatus').setValue("Requested reschedule");
            }

            Ext.getCmp('idBtnCancelMeetingMentee').setHidden(false);
            //RescheduleMeetingButtonMentee.setHidden(true);
            //Ext.getCmp('idBtnCancelMeetingMentee').setHidden(true);
            //Ext.getCmp('idbtnRescheduleMeetingMentee').setHidden(true);
        }
    },
    btnCancelMeetingMentee: function () {

        //var CancelMeetingButtonMentee = Ext.getCmp('idBtnCancelMeetingMentee');
        //CancelMeetingButtonMentee.setHidden(false);
        //CancelMeetingButtonMentee.setItemId(index);

        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;

        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("mentorCancelMeeting");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "mentorCancelMeeting"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

        var CancellationCommentLabel = Ext.getCmp("idLabelMenteeCommentCancelMeetingDetail");
        CancellationCommentLabel.setHtml("Reason For Cancellation<span class='spanAsterisk'>*</span>");

        if (MentorLoginUser != null) {
            var btnCancelMeeting = Ext.getCmp('idBtnCancelMeetingMentorPendingMeetingDetail');
            var InvitationData = Ext.getStore('MentorPendingMeeting').getAt(parseInt(btnCancelMeeting.getItemId())).data;

            var CancelSubmitMeeting = Ext.getCmp('idbtnCancelPendingMeeting');
            CancelSubmitMeeting.setItemId("" + btnCancelMeeting.getItemId());

            var MainTopics = Ext.getCmp('idMainTopicsMentorCancelMeetingDetail');
            var MentorMenteeTag = Ext.getCmp('idLabelMenteeNameMentorCancelMeetingDetail');
            var MentorMenteeName = Ext.getCmp('idMenteeNameMentorCancelMeetingDetail');
            var CancelationReason = Ext.getCmp('menteeCancellationCommentCancelMeetingScreen');

            MainTopics.setValue("");
            MentorMenteeTag.setHtml("");
            MentorMenteeName.setValue("");
            CancelationReason.setValue("");

            MainTopics.setValue(InvitationData.TopicDescription);


            MentorMenteeTag.setHtml(Mentor.Global.MENTEE_NAME + " Name");
            MentorMenteeName.setValue(InvitationData.MenteeName);
        } else if (MenteeLoginUser != null) {
            var MainTopics = Ext.getCmp('idMainTopicsMentorCancelMeetingDetail');
            var MentorMenteeTag = Ext.getCmp('idLabelMenteeNameMentorCancelMeetingDetail');
            var MentorMenteeName = Ext.getCmp('idMenteeNameMentorCancelMeetingDetail');
            var CancelationReason = Ext.getCmp('menteeCancellationCommentCancelMeetingScreen');

            MainTopics.setValue("");
            MentorMenteeTag.setHtml("");
            MentorMenteeName.setValue("");
            CancelationReason.setValue("");

            var btnCancelMeeting = Ext.getCmp('idBtnCancelMeetingMentee');
            //var WaitScreenData = Ext.getStore('WaitScreenMentor').getAt(parseInt(btnCancelMeeting.getItemId())).data;
            var WaitScreenData = Ext.getStore(Ext.getCmp('idStoreName').getValue()).getAt(parseInt(btnCancelMeeting.getItemId())).data;
            console.log(WaitScreenData.Status);
            if (WaitScreenData.Status == "Accepted" || WaitScreenData.Status == "Rescheduled") {
                Ext.getCmp('cancelMeetingTitle').setTitle('Cancel Meeting');
                Ext.getCmp('idbtnBackCancelPendingMeeting').setText('DO NOT CANCEL MEETING');
            } else if (WaitScreenData.Status == "Pending") {
                Ext.getCmp('cancelMeetingTitle').setTitle('Cancel Invite');
                Ext.getCmp('idbtnBackCancelPendingMeeting').setText('DO NOT CANCEL INVITE');
            }

            MainTopics.setValue(WaitScreenData.TopicDescription);
            Ext.getCmp('idInfoMenteeNameMentorCancelMeetingDetail').setHtml('Meeting Request to ' + WaitScreenData.MentorName); //Avinash

            if (WaitScreenData.MeetingDateTime == "") {
                Ext.getCmp("idLabelMeetingDateTimeMentorCancelMeetingDetail").setHidden(true);
                Ext.getCmp("idMeetingDateTimeMentorCancelMeetingDetail").setHidden(true);
            } else {
                Ext.getCmp("idLabelMeetingDateTimeMentorCancelMeetingDetail").setHidden(false);
                Ext.getCmp("idMeetingDateTimeMentorCancelMeetingDetail").setHidden(false).setValue(WaitScreenData.MeetingDateTime);
            }

            MentorMenteeTag.setHtml(Mentor.Global.MENTOR_NAME + " Name");
            MentorMenteeName.setValue(WaitScreenData.MentorName);
        }
    },
    btnAcceptMeetingMentee: function () {
        var GLOBAL = Mentor.Global;
        var InvitationIndex = Ext.getCmp('idbtnAcceptMeetingMentee').getItemId();
        //var InvitationData = Ext.getStore('WaitScreenMentorPending').data.getAt(InvitationIndex).data;
        var InvitationData = Ext.getStore('WaitScreenMentorAccepted').data.getAt(InvitationIndex).data;
        var Comment = Ext.getCmp('menteeCommentWaitScreen').getValue();
        if (Comment == "") {
            Ext.Msg.alert('MELS', "Comment is required");
            return;
        } else {
            var data = {
                Comments: Comment,
                Status: "Accepted",
                InvitationId: InvitationData.InvitationId
            };
            console.log(data);

            Ext.Viewport.setMasked({
                xtype: "loadmask",
                message: "Please wait"
            });

            Ext.Ajax.request({
                url: GLOBAL.SERVER_URL,
                method: "POST",
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/json'
                },
                xhr2: true,
                disableCaching: false,
                jsonData: {
                    method: "menteeResponse",
                    body: data
                },
                success: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    var data = Ext.decode(responce.responseText);
                    console.log(data);
                    d = data.menteeResponse.data;

                    if (data.menteeResponse.Error == 1) {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.menteeResponse.Message);
                        }, 100);
                        return;
                    } else if (data.menteeResponse.Error == 2) {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.menteeResponse.Message);
                        }, 100);
                    } else {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.menteeResponse.Message, function () {
                                var View = Mentor.Global.NavigationStack.pop();
                                var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                                mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});

//                                var viewport = Ext.Viewport,
//                                        mainPanel = viewport.down("#mainviewport");
//                                var mainMenu = mainPanel.down("menteeInvitesTabView");
//                                if (!mainMenu) {
//                                    mainMenu = mainPanel.add({xtype: "menteeInvitesTabView"});
//                                }
//                                Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
//                                mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
//                                Ext.getCmp('idMenteeInvitesTabView').getTabBar().setActiveTab(0);
                            });
                        }, 100);
                    }
                },
                failure: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    console.log(responce);
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            });
        }
    },
    btnDeclineMeetingMentee: function () {
        var viewport = Ext.Viewport,
                mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("declineresponses");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "declineresponses"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

        Ext.getCmp('idMentorCommetnsFieldSetAcceptRejectPendingRequest').setTitle(Mentor.Global.MENTEE_NAME + ' Comments');
        Ext.getCmp('commentsRejectPendingRequest').setPlaceHolder(Mentor.Global.MENTEE_NAME + ' Comments')
    },
    btnRescheduleMeetingMentee: function () {
        var viewport = Ext.Viewport,
                mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("mentorRescheduleMeeting");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "mentorRescheduleMeeting"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

        var RescheduleMeetingButtonMentee = Ext.getCmp('idbtnRescheduleMeetingMentee');
        //var WaitScreenData = Ext.getStore('WaitScreenMentor').getAt(parseInt(RescheduleMeetingButtonMentee.getItemId())).data;
        var WaitScreenData = Ext.getStore(Ext.getCmp('idStoreName').getValue()).getAt(parseInt(RescheduleMeetingButtonMentee.getItemId())).data;
//        console.log(WaitScreenData);
        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        Ext.getCmp('idLabelMenteeNameMentorRescheduleMeetingDetail').setHtml(Mentor.Global.MENTOR_NAME + ' Name');
        Ext.getCmp('idLabelMenteeEmailMentorRescheduleMeetingDetail').setHtml(Mentor.Global.MENTOR_NAME + ' Email');
        Ext.getCmp('idLabelMenteePhoneNoMentorRescheduleMeetingDetail').setHtml(Mentor.Global.MENTOR_NAME + ' Phone No');
        Ext.getCmp('idLabelMenteeCommentsMentorRescheduleMeetingDetail').setHtml(Mentor.Global.MENTEE_NAME + ' Reason for Requested Reschedule');
        Ext.getCmp('idLabelMentorCommentsMentorRescheduleMeetingDetail').setHtml(Mentor.Global.MENTOR_NAME + ' Comment');
        Ext.getCmp('idLabelMenteeContactMentorRescheduleMeetingDetail').setHtml('Contact ' + Mentor.Global.MENTEE_NAME);

        Ext.getCmp('idLabelRescheduleCommentRescheduleMeetingDetail').setHtml(Mentor.Global.MENTEE_NAME + " Reason for Reschedule<span class='spanAsterisk'>*</span>");
        Ext.getCmp('idRescheduleCommentRescheduleMeetingDetail').setValue("");
        Ext.getStore('MeetingType').load();

        var MenteeName = Ext.getCmp('idMenteeNameMentorRescheduleMeetingDetail');
        var MainTopics = Ext.getCmp('idMainTopicsMentorRescheduleMeetingDetail');
        var MeetingPlace = Ext.getCmp('idMeetingPlaceMentorRescheduleMeetingDetail');
        var MenteeEmail = Ext.getCmp('idMeenteeEmailMentorRescheduleMeetingDetail');
        var MenteePhoneNo = Ext.getCmp('idMeenteePhoneMentorRescheduleMeetingDetail');
        var isPhoneDisable = Ext.getCmp('idIsPhoneSMSCapableMentorRescheduleMeetingDetail');
        var MenteeComment = Ext.getCmp('idMenteeCommentMentorRescheduleMeetingDetail');
        var MentorComment = Ext.getCmp('idMentorCommentMentorRescheduleMeetingDetail');
        var MeetingType = Ext.getCmp('idMainTypeMentorRescheduleMeetingDetail');

        var MeetingTypeMentor = Ext.getCmp('idLabelMainTypeMentorRescheduleMeetingDetail').setHidden(true);
        var MeetingType = Ext.getCmp('idMainTypeMenteeRescheduleMeetingDetail');
        var MeetingPlaceMentor = Ext.getCmp('idLabelSelectMeetingPlaceReschedulePendingRequest').setHidden(true);
        var MeetingPlace = Ext.getCmp('idMeetingPlaceMenteeRescheduleMeetingDetail');

        var RescheduleSubmitButton = Ext.getCmp('idBtnRescheduleMeetingSubmit');
        RescheduleSubmitButton.setItemId("" + RescheduleMeetingButtonMentee.getItemId());

        var MeetingDateViewOnly = Ext.getCmp('dateTimeReschedulePendingRequestViewOnly');
        var MeetingDate = Ext.getCmp('dateTimeReschedulePendingRequest');
        MeetingDate.hide();
        var MeetingTimeViewOnly = Ext.getCmp('timePickerReschedulePendingRequestViewOnly');
        var MeetingTime = Ext.getCmp('timeReschedulePendingRequest');
        MeetingTime.hide();
        Ext.getCmp('idContainerMenteeContactMentorRescheduleMeetingDetail').setHidden(true); //Avinash , Hide connect to student section
        Ext.getCmp('idLabelMenteeContactMentorRescheduleMeetingDetail').setHidden(true);//Avinash  , Hide connect to student section

        MenteeName.setValue("");
        MainTopics.setValue("");
        MeetingPlace.setValue("");
        MenteeEmail.setValue("");
        MenteePhoneNo.setValue("");
        isPhoneDisable.setChecked(0);
        MenteeComment.setValue("");
        //MenteeComment.setDisabled(false);
        MentorComment.setValue("");
        MeetingType.setValue("");

        //MeetingDate.setValue("");
        MeetingDateViewOnly.setValue("");
        MeetingTimeViewOnly.setValue("");

        MenteeName.setValue(WaitScreenData.MentorName);
        Ext.getCmp('idInfoMentorNameMentorRescheduleMeetingDetail').setHtml('Meeting Request to ' + WaitScreenData.MentorName); //Avinash
        MainTopics.setValue(WaitScreenData.TopicDescription);
        MeetingPlace.setValue(WaitScreenData.MeetingPlaceName);
        MeetingType.setValue(WaitScreenData.MeetingTypeName);

        var DateTime = WaitScreenData.MeetingDateTime.split(" ");
        Mentor.Global.Current_date = Date.parse(WaitScreenData.MeetingDateTime);

        //MeetingDate.setValue(Date.parse(InvitationData.MeetingDateTime));
        MeetingDateViewOnly.setValue((DateTime[0])).show();
        MeetingTimeViewOnly.setValue(DateTime[1]).show();

        MenteeEmail.setValue(WaitScreenData.MentorEmail);
        MenteePhoneNo.setValue(WaitScreenData.MentorPhone);
        isPhoneDisable.setValue(WaitScreenData.MentorSmsCapable);
        MenteeComment.setValue(WaitScreenData.MenteeComment);
        MentorComment.setValue(WaitScreenData.MentorComments);

    },
    //Get Mentor Pending Metting that are not started yet.
    getMentorPendingMeeting: function (popup) {
        localStorage.setItem('SaveMeetingStartDateTime', Ext.encode({StartTime: null}));
        localStorage.setItem('SaveStartedMeetingID', Ext.encode({SaveStartedMeetingID: null}));
        var GLOBAL = Mentor.Global;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        var data = {
            MentorID: MentorLoginUser.Id
        }
        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "getMentorPendingMeeting",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.getMentorPendingMeeting.data;
                Ext.getStore('MentorPendingMeeting').clearData();
                if (data.getMentorPendingMeeting.pendingMeeting == 0 && data.getMentorPendingMeeting.pending != 0) {
                    Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().setActiveTab(1);
                    return false;
                }

                if (data.getMentorPendingMeeting.Error == 1) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.getMentorPendingMeeting.Message, function () {
                            Ext.getCmp('idMenteeInvitesTabView').setActiveItem(0);
                        });
                    }, 100);
                    return;
                } else if (data.getMentorPendingMeeting.Error == 2) {
                    Ext.getStore('MentorPendingMeeting').clearData();
                    if (typeof popup == 'undefined') {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.getMentorPendingMeeting.Message);
                        }, 100);
                    }

                    /*var View = Mentor.Global.NavigationStack.pop();
                     var viewport = Ext.Viewport,
                     mainPanel = viewport.down("#mainviewport");
                     
                     mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
                     
                     me.btnGetPendingRequest();*/

                    Ext.getCmp('invitesTab').setTitle(Mentor.Global.MENTOR_PENDING_TITLE + " (" +
                            data.getMentorPendingMeeting.pendingMeeting + "/" + data.getMentorPendingMeeting.pending + ")");

                    /*Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(0).setTitle(Mentor.Global.MENTOR_PENDING_TITLE + "(" +
                     data.getMentorPendingMeeting.pendingMeeting + "/" + data.getMentorPendingMeeting.pending + ")");*/
                    Ext.getCmp('idPendingInvitesMentorLeftMenu').setText(Mentor.Global.MENTOR_PENDING_TITLE + " (" +
                            data.getMentorPendingMeeting.pendingMeeting + "/" + data.getMentorPendingMeeting.pending + ")");

                    var MentorPendingRequest = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().getComponent(1);
                    var MentorPendingMeetingList = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().getComponent(0)

                    MentorPendingRequest.setTitle(
                            Mentor.Global.MENTOR_PENDING_REQUEST_TITLE + " (" + data.getMentorPendingMeeting.pending + ")");
                    MentorPendingMeetingList.setTitle(
                            Mentor.Global.MENTOR_PENDING_MEETING_TITLE + " (" + data.getMentorPendingMeeting.pendingMeeting + ")");
                } else {
                    Ext.getStore('MentorPendingMeeting').clearData();
                    Ext.getStore('MentorPendingMeeting').add(data.getMentorPendingMeeting.data);

                    Ext.getCmp('invitesTab').setTitle(Mentor.Global.MENTOR_PENDING_TITLE + " (" +
                            data.getMentorPendingMeeting.pendingMeeting + "/" + data.getMentorPendingMeeting.pending + ")");

                    /*Ext.getCmp('idMentorBottomTabView').getTabBar().getComponent(0).setTitle(Mentor.Global.MENTOR_PENDING_TITLE + "(" +
                     data.getMentorPendingMeeting.pendingMeeting + "/" + data.getMentorPendingMeeting.pending + ")");*/
                    Ext.getCmp('idPendingInvitesMentorLeftMenu').setText(Mentor.Global.MENTOR_PENDING_TITLE + " (" +
                            data.getMentorPendingMeeting.pendingMeeting + "/" + data.getMentorPendingMeeting.pending + ")");


                    var MentorPendingRequest = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().getComponent(1);
                    var MentorPendingMeetingList = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar().getComponent(0);

                    MentorPendingRequest.setTitle(
                            Mentor.Global.MENTOR_PENDING_REQUEST_TITLE + " (" + data.getMentorPendingMeeting.pending + ")");
                    MentorPendingMeetingList.setTitle(
                            Mentor.Global.MENTOR_PENDING_MEETING_TITLE + " (" + data.getMentorPendingMeeting.pendingMeeting + ")");

                    Mentor.app.application.getController('Main').getActionItemComments();
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                if (typeof popup == 'undefined') {
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            }
        });
    },
    calculateHelpPopupHeight: function (helpIndex) {
        var windowWidth = window.screen.width - 30;
        var windowHeight = window.screen.height - 30;
        var height = '60%';
        var lines = '';
        var helpData = Ext.decode(localStorage.getItem('helpConfigData'));
        var HelpTextToDisplay = (typeof helpData[helpIndex] != "undefined" && helpData[helpIndex] != "") ? helpData[helpIndex] : helpData.no_help;
        if (windowWidth < 320) {
            lines = (HelpTextToDisplay.length / 45);
        } else if (windowWidth >= 320 && windowWidth < 480) {
            lines = (HelpTextToDisplay.length / 60);
        } else if (windowWidth >= 480 && windowWidth < 640) {
            lines = (HelpTextToDisplay.length / 75);
        } else if (windowWidth >= 640 && windowWidth < 800) {
            lines = (HelpTextToDisplay.length / 90);
        } else if (windowWidth >= 800) {
            lines = (HelpTextToDisplay.length / 105);
        }
        var returnLines = (lines * 14) + 20;
        return {
            height: (lines !== '') ? (returnLines + 100) + 'px' : height,
            windowHeight: windowHeight,
            textHeight: returnLines,
            HelpTextToDisplay: HelpTextToDisplay
        };
    },
    addHelpPopup: function (helpIndex) {
        console.log(helpIndex);
        var popupHeight = this.calculateHelpPopupHeight(helpIndex);
        var popup = new Ext.Panel({
            id: 'idHelpTextPopupWindow',
            floating: true,
            centered: true,
            modal: true,
            hideOnMaskTap: true,
            scrollable: true,
            autoDestroy: true,
            padding: '10 10 10 10',
            width: '100%',
            minHeight: (popupHeight.textHeight > popupHeight.windowHeight) ? popupHeight.windowHeight + 'px' : popupHeight.height,
            maxWidth: (window.screen.width - 30),
            maxHeight: (window.screen.height - 100),
            items: [
                {
                    xtype: 'helptextpopup',
                    listeners: {
                        activate: function () {
                            Ext.getCmp('helptextID').setHtml(popupHeight.HelpTextToDisplay);
                        }
                    }
                },
                {
                    xtype: 'button',
                    cls: 'help-ok-btn-cls',
                    pressedCls: 'press-btn-cls',
                    style: 'margin-bottom:10px;margin-top:20px;',
                    html: 'OK',
                    handler: function () {
                        popup.hide();
                        Ext.Viewport.remove(popup);
                    }
                }
            ],
            listeners: {
                hide: function (a, b) {
                    popup.hide();
                    Ext.Viewport.remove(popup);
                }
            }
        });
        return popup;
    },
    addNotePopup: function (action) {
        var popup = new Ext.Panel({
            id: "addEditNotePopUp",
            floating: true,
            centered: true,
            modal: true,
            hideOnMaskTap: true,
            autoDestroy: true,
            /*width: 310,
             height: 338,
             style: "background-color: #FFFF76;",*/
            width: '90%',
            //height: 338,
            style: "background-color: #ffffff;",
            items: [
                {
                    xtype: 'addnotepopview',
                    id: 'idAddnotepopview',
                    listeners: {}
                }
            ],
            listeners: {
                hide: function (a, b) {
                    popup.hide();
                    Ext.Viewport.remove(popup);
                }
            }
        });
        if (action != 'add') {
            if (Mentor.Global.MEETING_DETAIL != null) {
                var MeetingID = Mentor.Global.MEETING_DETAIL.MeetingID;
                var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
                var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
                var UserType = '';
                if (MentorLoginUser != null) {
                    UserType = MentorLoginUser.UserType;
                } else if (MenteeLoginUser != null) {
                    UserType = MenteeLoginUser.UserType;
                }

                Ext.getCmp('idAddEditNoteMeetingID').setValue(MeetingID);
                var store = Ext.getStore('UserNotes');
                store.clearFilter();

                if (MeetingID) {
                    var thisRegMeetingID = new RegExp(MeetingID, "i");
                    var thisRegUserType = new RegExp(UserType, "i");
                    store.filterBy(function (record) {
                        if (thisRegMeetingID.test(record.get('MeetingID')) && thisRegUserType.test(record.get('NoteUserType'))) {
                            return true;
                        }
                        return false;
                    });
                }
                if (typeof store.getAt(0) != "undefined") {
                    var record = store.getAt(0).data;
                    store.clearFilter();
                    Ext.getCmp('idAddEditNoteID').setValue(record.NoteID);
                    Ext.getCmp('idAddEditNoteMeetingID').setValue(record.MeetingID);
                    Ext.getCmp('idAddEditNoteTitle').setValue(record.NoteTitle.replace(/\\/g, ""));
                    Ext.getCmp('idAddEditNoteDescription').setValue(record.NoteDescription.replace(/\\/g, ""));
                }
            }
        }
        return popup;
    },
    editNotePopup: function (view, index, NoteID) {
        var record = '';
        if (NoteID !== '' && typeof NoteID !== 'undefined') {

            var store = Ext.getStore('UserNotes');
            store.clearFilter();

            if (NoteID) {
                var thisRegMeetingID = new RegExp(NoteID, "i");
                store.filterBy(function (record) {
                    if (thisRegMeetingID.test(record.get('NoteID'))) {
                        return true;
                    }
                    return false;
                });
            }
            record = store.getAt(0).data;
            store.clearFilter();
        } else {
            record = view.getStore().getAt(index).data;
        }
        var popup = new Ext.Panel({
            id: "addEditNotePopUp",
            floating: true,
            centered: true,
            modal: true,
            hideOnMaskTap: true,
            autoDestroy: true,
            width: 310,
            height: 338,
            style: "background-color: #FFFF76;",
            items: [
                {
                    xtype: 'addnotepopview',
                    id: 'idAddnotepopview',
                    listeners: {}
                }
            ],
            listeners: {
                hide: function (a, b) {
                    popup.hide();
                    Ext.Viewport.remove(popup);
                }
            }
        });

        Ext.getCmp('idAddEditNoteID').setValue(record.NoteID);
        Ext.getCmp('idAddEditNoteMeetingID').setValue(record.MeetingID);
        Ext.getCmp('idAddEditNoteTitle').setValue(record.NoteTitle.replace(/\\/g, ""));
        Ext.getCmp('idAddEditNoteDescription').setValue(record.NoteDescription.replace(/\\/g, ""));

        return popup;
    },
    viewNotePopup: function (view, index, NoteID) {
        var record = '';
        if (NoteID !== '' && typeof NoteID !== 'undefined') {

            var store = Ext.getStore('UserNotes');
            store.clearFilter();

            if (NoteID) {
                var thisRegMeetingID = new RegExp(NoteID, "i");
                store.filterBy(function (record) {
                    if (thisRegMeetingID.test(record.get('NoteID'))) {
                        return true;
                    }
                    return false;
                });
            }
            record = store.getAt(0).data;
            store.clearFilter();
        } else {
            record = view.getStore().getAt(index).data;
        }
        var popup = new Ext.Panel({
            id: "viewNotePopUp",
            floating: true,
            centered: true,
            modal: true,
            hideOnMaskTap: true,
            autoDestroy: true,
            //width: 310,
            width: '90%',
            height: 400,
            style: "background-color: #ffffff;",
            items: [
                {
                    xtype: 'viewnotepopview',
                    id: 'idViewnotepopview',
                    listeners: {}
                }
            ],
            listeners: {
                hide: function (a, b) {
                    popup.hide();
                    Ext.Viewport.remove(popup);
                }
            }
        });
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID, UserType;

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
        }

        Ext.getCmp('idShareBtnViewNotePopup').setHidden(true);
        Ext.getCmp('idDeleteBtnViewNotePopup').setHidden(true);
        Ext.getCmp('idEditBtnViewNotePopup').setHidden(true);
        if (UserID == record.NoteAuthor && UserType == record.NoteUserType) { //When Note Author/Creator Views the note
            Ext.getCmp('idShareBtnViewNotePopup').setHidden(false);
            Ext.getCmp('idDeleteBtnViewNotePopup').setHidden(false);
            Ext.getCmp('idEditBtnViewNotePopup').setHidden(false);
        } else if ((UserID != record.NoteAuthor || UserType != record.NoteUserType) && record.Access == 1) {
            Ext.getCmp('idShareBtnViewNotePopup').setHidden(true);
            Ext.getCmp('idDeleteBtnViewNotePopup').setHidden(true);
            Ext.getCmp('idEditBtnViewNotePopup').setHidden(false);
        }

        if (MenteeLoginUser != null) {
            Ext.getCmp('idShareBtnViewNotePopup').setHidden(true);
        }

        Ext.getCmp('idViewNoteID').setValue(record.NoteID);
        Ext.getCmp('idViewNoteMeetingID').setValue(record.MeetingID);
        Ext.getCmp('idViewNoteTitle').setHtml(record.NoteTitle.replace(/\\/g, ""));
        Ext.getCmp('idViewNoteDescription').setValue(record.NoteDescription.replace(/\\/g, "")).setReadOnly(true);

        return popup;
    },
    blockTeamMember: function (record) {
        var GLOBAL = Mentor.Global;
        var me = this;
        var data = {};
        Ext.Msg.prompt('MELS', 'Please enter Reason (optional)', function (text, value) {
            data.TeamID = record.data.TeamID;
            data.UserID = record.data.UserID;
            data.UserType = record.data.UserType;
            data.Comment = value;

            Ext.Ajax.request({
                url: GLOBAL.SERVER_URL,
                method: "POST",
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/json'
                },
                xhr2: true,
                disableCaching: false,
                jsonData: {
                    method: "blockTeamMember",
                    body: data
                },
                success: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    var data = Ext.decode(responce.responseText);
                    console.log(data);

                    if (data.blockTeamMember.Error == 1 || data.blockTeamMember.Error == 2) {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.blockTeamMember.Message);
                        }, 100);
                        return;
                    } else {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.blockTeamMember.Message, function () {
                                me.getTeamMembers();
                            });
                        }, 100);
                        return;
                    }
                },
                failure: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    console.log(responce);
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            });

        });
    },
    removeTeamMember: function (record) {
        var GLOBAL = Mentor.Global;
        var me = this;
        var data = {};
        Ext.Msg.prompt('MELS', 'Please enter Reason (optional)', function (text, value) {
            data.TeamID = record.data.TeamID;
            data.UserID = record.data.UserID;
            data.UserType = record.data.UserType;
            data.Comment = value;

            Ext.Ajax.request({
                url: GLOBAL.SERVER_URL,
                method: "POST",
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/json'
                },
                xhr2: true,
                disableCaching: false,
                jsonData: {
                    method: "removeTeamMember",
                    body: data
                },
                success: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    var data = Ext.decode(responce.responseText);
                    console.log(data);

                    if (data.removeTeamMember.Error == 1 || data.removeTeamMember.Error == 2) {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.removeTeamMember.Message);
                        }, 100);
                        return;
                    } else {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.removeTeamMember.Message, function () {
                                // Mentor.app.application.getController('').getTeamMembers();
                                me.getTeamMembers();
                            });
                        }, 100);
                        return;
                    }
                },
                failure: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    console.log(responce);
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            });

        });
    },

    shareNotePopup: function (buttonId, NoteID) {
        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "shareUserNote",
                body: {
                    Share: buttonId,
                    NoteID: NoteID
                }
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);

                if (data.shareUserNote.Error == 1 || data.shareUserNote.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.shareUserNote.Message);
                    }, 100);
                    return;
                } else {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.shareUserNote.Message, function () {
                            Mentor.app.application.getController('Main').getUserNotes();
                        });
                    }, 100);
                    return;
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    deleteNotePopup: function (view, index, NoteID) {
        var record = '';
        if (NoteID !== '' && typeof NoteID !== 'undefined') {

            var store = Ext.getStore('UserNotes');
            store.clearFilter();

            if (NoteID) {
                var thisRegMeetingID = new RegExp(NoteID, "i");
                store.filterBy(function (record) {
                    if (thisRegMeetingID.test(record.get('NoteID'))) {
                        return true;
                    }
                    return false;
                });
            }
            record = store.getAt(0).data;
            store.clearFilter();
        } else {
            record = view.getStore().getAt(index).data;
        }

        var GLOBAL = Mentor.Global;
        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "deleteUserNote",
                body: {
                    NoteID: record.NoteID
                }
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);

                if (data.deleteUserNote.Error == 1 || data.deleteUserNote.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.deleteUserNote.Message);
                    }, 100);
                    return;
                } else {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.deleteUserNote.Message, function () {
                            Mentor.app.application.getController('Main').getUserNotes();
                        });
                    }, 100);
                    return;
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                var connectionExist = Mentor.Global.doesConnectionExist();
                var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', error);
                }, 100);
            }
        });
    },
    doSubmitAddEditComment: function () {
        me = this;
        var GLOBAL = Mentor.Global;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID, UserType;

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
        }

        var idAddEditCommentID = Ext.getCmp('idAddEditCommentID').getValue();
        var idAddEditActionItemID = Ext.getCmp('idAddEditActionItemID').getValue();
        var idAddEditActionItemType = Ext.getCmp('idAddEditActionItemType').getValue();
        var idAddEditCommentMeetingID = Ext.getCmp('idAddEditCommentMeetingID').getValue();
        var idAddEditCommentDescription = Ext.getCmp('idAddEditCommentDescription').getValue();

        var data = {
            UserID: UserID,
            UserType: UserType,
            CommentID: idAddEditCommentID,
            ActionItemID: idAddEditActionItemID,
            ActionItemType: idAddEditActionItemType,
            MeetingID: idAddEditCommentMeetingID,
            CommentDescription: idAddEditCommentDescription
        };
        //console.log(data);

        Ext.Viewport.setMasked({
            xtype: "loadmask",
            message: "Please wait"
        });

        Ext.Ajax.request({
            url: GLOBAL.SERVER_URL,
            method: "POST",
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            xhr2: true,
            disableCaching: false,
            jsonData: {
                method: "addActionItemComment",
                body: data
            },
            success: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                var data = Ext.decode(responce.responseText);
                console.log(data);
                d = data.addActionItemComment.data;
                if (data.addActionItemComment.Error == 1 || data.addActionItemComment.Error == 2) {
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', data.addActionItemComment.Message);
                    }, 100);
                    return;
                } else {
                    Ext.Function.defer(function () {
                        var popupObject = Ext.getCmp('addEditCommentPopUp');
                        popupObject.hide();
                        Mentor.app.application.getController('Main').getActionItemComments();
                        //});
                    }, 100);
                    return;
                }
            },
            failure: function (responce) {
                Ext.Viewport.setMasked(false); //Avinash
                console.log(responce);
                if (typeof popup == 'undefined') {
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            }
        });
        return;
    },
    addCommentPopup: function (ActionItemType, ActionItemId, EditActionItemID, MeetingID) {
        var popup = new Ext.Panel({
            id: "addEditCommentPopUp",
            floating: true,
            centered: true,
            modal: true,
            hideOnMaskTap: true,
            autoDestroy: true,
            width: '90%',
            //height: 338,
            style: "background-color: #ffffff;",
            items: [
                {
                    xtype: 'addcommentpopview',
                    id: 'idAddcommentpopview',
                    listeners: {}
                }
            ],
            listeners: {
                hide: function (a, b) {
                    popup.hide();
                    Ext.Viewport.remove(popup);
                }
            }
        });

        Ext.getCmp('idAddEditCommentID').setValue();
        Ext.getCmp('idAddEditActionItemID').setValue(ActionItemId);
        Ext.getCmp('idAddEditActionItemType').setValue(ActionItemType);
        Ext.getCmp('idAddEditCommentMeetingID').setValue(MeetingID);
        Ext.getCmp('idAddEditCommentDescription').setValue();

        if (EditActionItemID != 'addnote') {
            var record = '';
            var EditActionItemID = EditActionItemID.split('_')[1];
            if (ActionItemType == 1) { //For Mentee
                if (typeof EditActionItemID !== 'undefined' && EditActionItemID !== '') {
                    var store = Ext.getStore('MenteeActionComments');
                    store.clearFilter();

                    if (EditActionItemID) {
                        var thisRegMeetingID = new RegExp(EditActionItemID, "i");
                        store.filterBy(function (record) {
                            if (thisRegMeetingID.test(record.get('MenteeActionItemCommentID'))) {
                                return true;
                            }
                            return false;
                        });
                    }
                    record = store.getAt(0).data;
                    store.clearFilter();

                    Ext.getCmp('idAddEditCommentID').setValue(record.MenteeActionItemCommentID);
                    Ext.getCmp('idAddEditActionItemID').setValue(record.MenteeActionItemID);
                    Ext.getCmp('idAddEditActionItemType').setValue(ActionItemType);
                    Ext.getCmp('idAddEditCommentMeetingID').setValue(record.MeetingID);
                    Ext.getCmp('idAddEditCommentDescription').setValue(record.Comment);
                }
            } else if (ActionItemType == 0) { //For Mentor
                if (typeof EditActionItemID !== 'undefined' && EditActionItemID !== '') {
                    var store = Ext.getStore('MentorActionComments');
                    store.clearFilter();

                    if (EditActionItemID) {
                        var thisRegMeetingID = new RegExp(EditActionItemID, "i");
                        store.filterBy(function (record) {
                            if (thisRegMeetingID.test(record.get('MentorActionItemCommentID'))) {
                                return true;
                            }
                            return false;
                        });
                    }
                    record = store.getAt(0).data;
                    store.clearFilter();

                    Ext.getCmp('idAddEditCommentID').setValue(record.MentorActionItemCommentID);
                    Ext.getCmp('idAddEditActionItemID').setValue(record.MentorActionItemID);
                    Ext.getCmp('idAddEditActionItemType').setValue(ActionItemType);
                    Ext.getCmp('idAddEditCommentMeetingID').setValue(record.MeetingID);
                    Ext.getCmp('idAddEditCommentDescription').setValue(record.Comment);
                }
            }
        }

        return popup;
    },
    viewCommentPopup: function (view, index, NoteID) {
        var record = '';
        if (NoteID !== '' && typeof NoteID !== 'undefined') {

            var store = Ext.getStore('UserNotes');
            store.clearFilter();

            if (NoteID) {
                var thisRegMeetingID = new RegExp(NoteID, "i");
                store.filterBy(function (record) {
                    if (thisRegMeetingID.test(record.get('NoteID'))) {
                        return true;
                    }
                    return false;
                });
            }
            record = store.getAt(0).data;
            store.clearFilter();
        } else {
            record = view.getStore().getAt(index).data;
        }
        var popup = new Ext.Panel({
            id: "viewCommentPopUp",
            floating: true,
            centered: true,
            modal: true,
            hideOnMaskTap: true,
            autoDestroy: true,
            width: 310,
            height: 400,
            style: "background-color: #AFEEEE;",
            items: [
                {
                    xtype: 'viewcommentpopview',
                    id: 'idViewcommentpopview',
                    listeners: {}
                }
            ],
            listeners: {
                hide: function (a, b) {
                    popup.hide();
                    Ext.Viewport.remove(popup);
                }
            }
        });
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID, UserType;

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
        }

        Ext.getCmp('idShareBtnViewNotePopup').setHidden(true);
        Ext.getCmp('idDeleteBtnViewNotePopup').setHidden(true);
        Ext.getCmp('idEditBtnViewNotePopup').setHidden(true);
        if (UserID == record.NoteAuthor && UserType == record.NoteUserType) { //When Note Author/Creator Views the note
            Ext.getCmp('idShareBtnViewNotePopup').setHidden(false);
            Ext.getCmp('idDeleteBtnViewNotePopup').setHidden(false);
            Ext.getCmp('idEditBtnViewNotePopup').setHidden(false);
        } else if ((UserID != record.NoteAuthor || UserType != record.NoteUserType) && record.Access == 1) {
            Ext.getCmp('idShareBtnViewNotePopup').setHidden(true);
            Ext.getCmp('idDeleteBtnViewNotePopup').setHidden(true);
            Ext.getCmp('idEditBtnViewNotePopup').setHidden(false);
        }

        if (MenteeLoginUser != null) {
            Ext.getCmp('idShareBtnViewNotePopup').setHidden(true);
        }

        Ext.getCmp('idViewNoteID').setValue(record.NoteID);
        Ext.getCmp('idViewNoteMeetingID').setValue(record.MeetingID);
        Ext.getCmp('idViewNoteTitle').setHtml(record.NoteTitle.replace(/\\/g, ""));
        Ext.getCmp('idViewNoteDescription').setValue(record.NoteDescription.replace(/\\/g, "")).setReadOnly(true);

        return popup;
    },
    // From Mentor Pending Meetng List from Mentor Login go to Detail View
    displayMentorPendingMeeting: function (view, index, item, e) {
        var viewport = Ext.Viewport,
                mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("mentorPendingMeetingDetail");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "mentorPendingMeetingDetail"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

        //Mentor.app.getController('AppDetail').getMeetingHistory();

        var PendingMeetingRecord = view.getStore().getAt(index).data;
        var btnStartNewMeeting = Ext.getCmp('idBtnStartNewMeetingMentorPendingMeetingDetail');
        btnStartNewMeeting.setItemId("" + index);
        var btnRescheduleMeeting = Ext.getCmp('idBtnRescheduleMeetingMentorPendingMeetingDetail');
        btnRescheduleMeeting.setItemId("" + index);
        var btnCancelMeeting = Ext.getCmp('idBtnCancelMeetingMentorPendingMeetingDetail');
        btnCancelMeeting.setItemId("" + index);

        //var IsFollowUpMeeting = Ext.getCmp('idIsFollowUpMeetingMentorPendingMeetingDetailNo');
        var IsFollowUpMeetingYes = Ext.getCmp('idIsFollowUpMeetingMentorPendingMeetingDetailYes');
        var IsFollowUpMeetingNo = Ext.getCmp('idIsFollowUpMeetingMentorPendingMeetingDetailNo');

        IsFollowUpMeetingNo.setChecked(true);
        IsFollowUpMeetingNo.setDisabled(false);
        IsFollowUpMeetingYes.setDisabled(false);

        Ext.getCmp('idFollowUpMeetingIDTemp').setValue(0);

        Ext.getCmp('idFollowUpListMentorPendingMeetingDetail').setDisabled(false);
        Ext.getCmp('idLabelFollowUpListMentorPendingMeetingDetail').setHidden(true);
        Ext.getCmp('idFieldsetFollowUpListMentorPendingMeetingDetail').setHidden(true);

        var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));

        if (MentorLoginDetail != null) {
            Ext.getCmp('idUserNameWaitScreen').setHtml(MentorLoginDetail.MentorName);
        }

        var MenteeName = Ext.getCmp('idMenteeNameMentorPendingMeetingDetail');
        var MainTopics = Ext.getCmp('idMainTopicsMentorPendingMeetingDetail');
        var MeetingPlace = Ext.getCmp('idMeetingPlaceMentorPendingMeetingDetail');
        var MeetingDateTime = Ext.getCmp('idMeetingDateTimeMentorPendingMeetingDetail');
        var MenteeEmail = Ext.getCmp('idMeenteeEmailMentorPendingMeetingDetail');
        var MenteePhoneNo = Ext.getCmp('idMeenteePhoneMentorPendingMeetingDetail');
        var isPhoneDisable = Ext.getCmp('idIsPhoneSMSCapableMentorPendingMeetingDetail');
        var MenteeComment = Ext.getCmp('idMenteeCommentMentorPendingMeetingDetail');
        var MentorComment = Ext.getCmp('idMentorCommentMentorPendingMeetingDetail');
        var MeetingType = Ext.getCmp('idMainTypeMentorPendingMeetingDetail');
        Ext.getCmp('idBtnCancelMeetingMentorPendingMeetingDetail').setText('CANCEL MEETING');

        Ext.getCmp('idBtnStartNewMeetingMentorPendingMeetingDetail').setHidden(false).setDisabled(false).setText('BEGIN NEW MEETING');
        Ext.getCmp('idBtnWaitForAcknowledgementMentorPendingMeetingDetail').setHidden(true);

        if (PendingMeetingRecord.FollowUpMeetingFor > 0) {
            IsFollowUpMeetingYes.setChecked(true);
            IsFollowUpMeetingYes.setDisabled(true);
            IsFollowUpMeetingNo.setDisabled(true);
            Ext.getCmp('idFollowUpListMentorPendingMeetingDetail').setValue(PendingMeetingRecord.FollowUpMeetingFor);

        }

        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        if (PendingMeetingRecord.InitiatedBy == 0) {
            $(".mentor_pending_metting_detail").css("background-color", "#feffdd");
            Ext.getCmp('idInfoMentorPendingMettingDetailScreen').setHtml('Meeting Request from ' + PendingMeetingRecord.MenteeName);
            Ext.getCmp('idLabelMenteeEmailMentorPendingMeetingDetail').setHidden(true);
            Ext.getCmp('idLabelMenteePhoneNoMentorPendingMeetingDetail').setHidden(true);
            Ext.getCmp('idLabelMenteeNameMentorPendingMeetingDetail').setHtml(Mentor.Global.MENTEE_NAME + ' Name');
            Ext.getCmp('idLabelMenteeCommentsMentorPendingMeetingDetail').setHtml(Mentor.Global.MENTEE_NAME + ' Comment');
            Ext.getCmp('idLabelMentorCommentsMentorPendingMeetingDetail').setHtml(Mentor.Global.MENTOR_NAME + ' Comment');
            MenteeEmail.setHidden(true);
            MenteePhoneNo.setHidden(true);
            isPhoneDisable.setHidden(true);
            if (PendingMeetingRecord.Status == "Accepted") {
                Ext.getCmp('idBtnStartNewMeetingMentorPendingMeetingDetail').setHidden(false);
                Ext.getCmp('idBtnRescheduleMeetingMentorPendingMeetingDetail').setHidden(false);
                Ext.getCmp('idBtnCancelMeetingMentorPendingMeetingDetail').setHidden(false);
            } else if (PendingMeetingRecord.Status == "Cancelled" || PendingMeetingRecord.Status == "Rejected") {
                Ext.getCmp('idBtnStartNewMeetingMentorPendingMeetingDetail').setHidden(true);
                Ext.getCmp('idBtnRescheduleMeetingMentorPendingMeetingDetail').setHidden(true);
                Ext.getCmp('idBtnCancelMeetingMentorPendingMeetingDetail').setHidden(true);
            } else {
                Ext.getCmp('idBtnStartNewMeetingMentorPendingMeetingDetail').setHidden(true).setDisabled(true).setText('Pending Acknowledgement');
                Ext.getCmp('idBtnWaitForAcknowledgementMentorPendingMeetingDetail').setHidden(false);
                Ext.getCmp('idBtnWaitForAcknowledgementMentorPendingMeetingDetail').setText("Waiting for acceptance by " + PendingMeetingRecord.MenteeName);

                Ext.getCmp('idBtnRescheduleMeetingMentorPendingMeetingDetail').setHidden(false);
                Ext.getCmp('idBtnCancelMeetingMentorPendingMeetingDetail').setHidden(false);
                Ext.getCmp('idBtnCancelMeetingMentorPendingMeetingDetail').setText('CANCEL MEETING');
            }
        } else {
            $(".mentor_pending_metting_detail").css("background-color", "#d3ddff");
            Ext.getCmp('idInfoMentorPendingMettingDetailScreen').setHtml('I send meeting to ' + PendingMeetingRecord.MenteeName);
            Ext.getCmp('idBtnStartNewMeetingMentorPendingMeetingDetail').setHidden(false);
            Ext.getCmp('idBtnRescheduleMeetingMentorPendingMeetingDetail').setHidden(false);
            Ext.getCmp('idBtnCancelMeetingMentorPendingMeetingDetail').setHidden(false);
            Ext.getCmp('idLabelMenteeNameMentorPendingMeetingDetail').setHtml(Mentor.Global.MENTEE_NAME + ' Name');
            Ext.getCmp('idLabelMenteeEmailMentorPendingMeetingDetail').setHtml(Mentor.Global.MENTEE_NAME + ' Email');
            Ext.getCmp('idLabelMenteePhoneNoMentorPendingMeetingDetail').setHtml(Mentor.Global.MENTEE_NAME + ' Phone No');
            Ext.getCmp('idLabelMenteeCommentsMentorPendingMeetingDetail').setHtml(Mentor.Global.MENTEE_NAME + ' Comment');
            Ext.getCmp('idLabelMentorCommentsMentorPendingMeetingDetail').setHtml(Mentor.Global.MENTOR_NAME + ' Comment');
        }

        MenteeName.setValue("");
        MainTopics.setValue("");
        MeetingPlace.setValue("");
        MeetingDateTime.setValue("");
        MenteeEmail.setValue("");
        MenteePhoneNo.setValue("");
        isPhoneDisable.setChecked(0);
        MenteeComment.setValue("");
        MentorComment.setValue("");
        MeetingType.setValue("");

        MenteeName.setValue(PendingMeetingRecord.MenteeName);
        MainTopics.setValue(PendingMeetingRecord.TopicDescription);
        MeetingPlace.setValue(PendingMeetingRecord.MeetingPlaceName);
        MeetingDateTime.setValue(PendingMeetingRecord.MeetingDateTime);
        MenteeEmail.setValue(PendingMeetingRecord.InviteeEmail);
        MenteePhoneNo.setValue(PendingMeetingRecord.InviteePhone);
        isPhoneDisable.setValue(PendingMeetingRecord.SmsCapable);
        MenteeComment.setValue(PendingMeetingRecord.ManteeComments);
        MentorComment.setValue(PendingMeetingRecord.MentorComments);
        MeetingType.setValue(PendingMeetingRecord.MeetingTypeName);
    },
    doReschedule: function () {
        //        alert("doReschedule");
        var viewport = Ext.Viewport,
                mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("mentorRescheduleMeeting");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "mentorRescheduleMeeting"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

        var btnRescheduleMeeting = Ext.getCmp('idBtnRescheduleMeetingMentorPendingMeetingDetail');
        var InvitationData = Ext.getStore('MentorPendingMeeting').getAt(parseInt(btnRescheduleMeeting.getItemId())).data;
        Ext.getStore('MeetingType').load();


        var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));

        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        Ext.getCmp('idLabelMenteeNameMentorRescheduleMeetingDetail').setHtml(Mentor.Global.MENTEE_NAME + ' Name');
        Ext.getCmp('idLabelMenteeEmailMentorRescheduleMeetingDetail').setHtml(Mentor.Global.MENTEE_NAME + ' Email');
        Ext.getCmp('idLabelMenteePhoneNoMentorRescheduleMeetingDetail').setHtml(Mentor.Global.MENTEE_NAME + ' Phone No');
        Ext.getCmp('idLabelMenteeCommentsMentorRescheduleMeetingDetail').setHtml(Mentor.Global.MENTEE_NAME + ' Reason for Requested Reschedule');
        Ext.getCmp('idLabelMentorCommentsMentorRescheduleMeetingDetail').setHtml(Mentor.Global.MENTOR_NAME + ' Comment When Accepted');
        Ext.getCmp('idLabelMenteeContactMentorRescheduleMeetingDetail').setHtml('Contact ' + Mentor.Global.MENTEE_NAME);

        Ext.getCmp('idLabelRescheduleCommentRescheduleMeetingDetail').setHtml(Mentor.Global.MENTOR_NAME + "'s Reason/Comments for Reschedule<span class='spanAsterisk'>*</span>");
        Ext.getCmp('idRescheduleCommentRescheduleMeetingDetail').setValue("");
        var MeetingTypeMentor = Ext.getCmp('idMainTypeMenteeRescheduleMeetingDetail').setHidden(true);
        var MeetingType = Ext.getCmp('idLabelMainTypeMentorRescheduleMeetingDetail');
        var MeetingPlaceMentor = Ext.getCmp('idMeetingPlaceMenteeRescheduleMeetingDetail').setHidden(true);
        var MeetingPlace = Ext.getCmp('idLabelSelectMeetingPlaceReschedulePendingRequest');

        var MenteeName = Ext.getCmp('idMenteeNameMentorRescheduleMeetingDetail');
        var MainTopics = Ext.getCmp('idMainTopicsMentorRescheduleMeetingDetail');
        var MeetingPlace = Ext.getCmp('idMeetingPlaceMentorRescheduleMeetingDetail');
        var MenteeEmail = Ext.getCmp('idMeenteeEmailMentorRescheduleMeetingDetail');
        var MenteePhoneNo = Ext.getCmp('idMeenteePhoneMentorRescheduleMeetingDetail');
        var isPhoneDisable = Ext.getCmp('idIsPhoneSMSCapableMentorRescheduleMeetingDetail');
        var MenteeComment = Ext.getCmp('idMenteeCommentMentorRescheduleMeetingDetail');
        var MentorComment = Ext.getCmp('idMentorCommentMentorRescheduleMeetingDetail');
        var MeetingType = Ext.getCmp('idMainTypeMentorRescheduleMeetingDetail');
        var RescheduleSubmitButton = Ext.getCmp('idBtnRescheduleMeetingSubmit');
        RescheduleSubmitButton.setItemId("" + btnRescheduleMeeting.getItemId());

        var MeetingDate = Ext.getCmp('datePickerReschedulePendingRequest');
        var MeetingDateText = Ext.getCmp('dateTimeReschedulePendingRequest');
        MeetingDateText.show();
        var MeetingTime = Ext.getCmp('timePickerReschedulePendingRequest');
        var MeetingTimeText = Ext.getCmp('timeReschedulePendingRequest');
        MeetingTimeText.show();

        MenteeName.setValue("");
        MainTopics.setValue("");
        MeetingPlace.setValue("");
        MenteeEmail.setValue("");
        MenteePhoneNo.setValue("");
        isPhoneDisable.setChecked(0);
        MenteeComment.setValue("");
        //MentorComment.setValue("").setDisabled(false);
        MentorComment.setValue("");
        MeetingType.setValue("");

        //MeetingDate.setValue("");
        MeetingDateText.setValue("");
        MeetingTime.setValue("");
        MeetingTimeText.setValue("");

        MenteeName.setValue(InvitationData.MenteeName);
        Ext.getCmp('idInfoMentorNameMentorRescheduleMeetingDetail').setHtml('Meeting Request from ' + InvitationData.MenteeName); //Avinash
        MainTopics.setValue(InvitationData.TopicDescription);
        MeetingPlace.setValue(InvitationData.MeetingPlaceName);
        MeetingType.setValue(InvitationData.MeetingTypeName);

        var DateTime = InvitationData.MeetingDateTime.split(" ");
        Mentor.Global.Current_date = Date.parse(InvitationData.MeetingDateTime);
        //console.log(DateTime[0]);
        //console.log(DateTime[1]);

        //MeetingDate.setValue(Date.parse(InvitationData.MeetingDateTime));
        MeetingDateText.setValue((DateTime[0]));

        MeetingTime.setValue(Date.parse(InvitationData.MeetingDateTime));
        MeetingTimeText.setValue(DateTime[1]);

        MenteeEmail.setValue(InvitationData.InviteeEmail);
        MenteePhoneNo.setValue(InvitationData.InviteePhone);
        isPhoneDisable.setValue(InvitationData.SmsCapable);
        MenteeComment.setValue(InvitationData.ManteeComments);
        MentorComment.setValue(InvitationData.MentorComments);
    },
    doCancel: function () {
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));

        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        var mainMenu = mainPanel.down("mentorCancelMeeting");
        if (!mainMenu) {
            mainMenu = mainPanel.add({xtype: "mentorCancelMeeting"});
        }
        Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
        mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

        var CancellationCommentLabel = Ext.getCmp("idLabelMenteeCommentCancelMeetingDetail");
        CancellationCommentLabel.setHtml("Reason For Cancellation<span class='spanAsterisk'>*</span>");

        var btnCancelMeeting = Ext.getCmp('idBtnCancelMeetingMentorPendingMeetingDetail');

        var InvitationData = Ext.getStore('MentorPendingMeeting').getAt(parseInt(btnCancelMeeting.getItemId())).data;

        var CancelSubmitMeeting = Ext.getCmp('idbtnCancelPendingMeeting');
        CancelSubmitMeeting.setItemId("" + btnCancelMeeting.getItemId());

        var MainTopics = Ext.getCmp('idMainTopicsMentorCancelMeetingDetail');
        var MentorMenteeTag = Ext.getCmp('idLabelMenteeNameMentorCancelMeetingDetail');
        var MentorMenteeName = Ext.getCmp('idMenteeNameMentorCancelMeetingDetail');
        var CancelationReason = Ext.getCmp('menteeCancellationCommentCancelMeetingScreen');

        MainTopics.setValue("");
        MentorMenteeTag.setHtml("");
        MentorMenteeName.setValue("");
        CancelationReason.setValue("");

        MainTopics.setValue(InvitationData.TopicDescription);
        Ext.getCmp('idInfoMenteeNameMentorCancelMeetingDetail').setHtml('Meeting Request from ' + InvitationData.MenteeName);

        if (InvitationData.MeetingDateTime == "") {
            Ext.getCmp("idLabelMeetingDateTimeMentorCancelMeetingDetail").setHidden(true);
            Ext.getCmp("idLabelMeetingTimeMentorCancelMeetingDetail").setHidden(true);
            Ext.getCmp("idMeetingDateTimeMentorCancelMeetingDetail").setHidden(true);
            Ext.getCmp("idMeetingTimeMentorCancelMeetingDetail").setHidden(true);
        } else {
            Ext.getCmp("idLabelMeetingDateTimeMentorCancelMeetingDetail").setHidden(false);
            Ext.getCmp("idLabelMeetingTimeMentorCancelMeetingDetail").setHidden(false);
            Ext.getCmp("idMeetingDateTimeMentorCancelMeetingDetail").setHidden(false).setValue(InvitationData.MeetingDateTime.split(' ')[0]);
            Ext.getCmp("idMeetingTimeMentorCancelMeetingDetail").setHidden(false).setValue(InvitationData.MeetingDateTime.split(' ')[1]);
        }
        Ext.getCmp('cancelMeetingTitle').setTitle('Cancel Meeting');
        Ext.getCmp('idbtnBackCancelPendingMeeting').setText('Do not cancel meeting');

        if (MentorLoginUser != null) {
            MentorMenteeTag.setHtml(Mentor.Global.MENTEE_NAME + " Name");
            MentorMenteeName.setValue(InvitationData.MenteeName);

            if (InvitationData.InitiatedBy == 0) {
                //if (InvitationData.Status == "Accepted" || InvitationData.Status == "Cancelled" || InvitationData.Status == "Rejected") {
                Ext.getCmp('cancelMeetingTitle').setTitle('Cancel Meeting');
                Ext.getCmp('idbtnBackCancelPendingMeeting').setText('Do not cancel meeting');
                /*} else {
                 Ext.getCmp('cancelMeetingTitle').setTitle('Cancel Invite');
                 Ext.getCmp('idbtnBackCancelPendingMeeting').setText('Do not cancel invite');
                 }*/
            }

        } else if (MenteeLoginUser != null) {
            MentorMenteeTagsetHtml(Mentor.Global.MENTOR_NAME + " Name");
        }

    },
    doRescheduleRequest: function () {
        var me = this;
        var GLOBAL = Mentor.Global;

        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";

            var RescheduleSubmitMeeting = Ext.getCmp('idBtnRescheduleMeetingSubmit');
            var Record = Ext.getStore('MentorPendingMeeting').getAt(parseInt(RescheduleSubmitMeeting.getItemId())).data;
            var MeetingDate = Ext.getCmp('dateTimeReschedulePendingRequest').getValue();
            var MeetingTime = Ext.getCmp('timeReschedulePendingRequest').getValue();
            var MeetingType = Ext.getCmp('idMainTypeMentorRescheduleMeetingDetail').getValue();
            var MeetingPlace = Ext.getCmp('idMeetingPlaceMentorRescheduleMeetingDetail').getValue();
            var RescheduleReason = Ext.getCmp('idRescheduleCommentRescheduleMeetingDetail').getValue();
            if (MeetingDate == "" || MeetingTime == "") {
                Ext.Msg.alert('MELS', "Both date and time fields need to be set when you accept the meeting invite.");
                return;
            } else if (RescheduleReason == "") {
                Ext.Msg.alert('MELS', "Reason for Rescheduling is required.");
                return;
            }
            var data = {
                InvitationId: Record.InvitationId,
                MeetingDateTime: MeetingDate + " " + MeetingTime,
                MeetingTypeID: MeetingType,
                MeetingPlaceID: MeetingPlace,
                RescheduleComments: RescheduleReason,
                UserType: UserType
            };
            console.log(data)
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";

            var RescheduleSubmitMeeting = Ext.getCmp('idBtnRescheduleMeetingSubmit');
            //var Record = Ext.getStore('WaitScreenMentor').getAt(parseInt(RescheduleSubmitMeeting.getItemId())).data;
            var Record = Ext.getStore(Ext.getCmp('idStoreName').getValue()).getAt(parseInt(RescheduleSubmitMeeting.getItemId())).data;
            var RescheduleReason = Ext.getCmp('idRescheduleCommentRescheduleMeetingDetail').getValue();
            if (RescheduleReason == "") {
                Ext.Msg.alert('MELS', "Reason for Rescheduling is required.");
                return;
            }
            var data = {
                InvitationId: Record.InvitationId,
                RescheduleComments: RescheduleReason,
                UserType: UserType
            };
        }

        {
            Ext.Viewport.setMasked({
                xtype: "loadmask",
                message: "Please wait"
            });

            Ext.Ajax.request({
                url: GLOBAL.SERVER_URL,
                method: "POST",
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/json'
                },
                xhr2: true,
                disableCaching: false,
                jsonData: {
                    method: "rescheduleMeeting",
                    body: data
                },
                success: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    var data = Ext.decode(responce.responseText);
                    console.log(data);
                    d = data.rescheduleMeeting.data;

                    if (data.rescheduleMeeting.Error == 1) {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.rescheduleMeeting.Message);
                        }, 100);
                        return;
                    } else if (data.rescheduleMeeting.Error == 2) {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.rescheduleMeeting.Message);
                        }, 100);

                    } else {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.rescheduleMeeting.Message, function () {
                                var View = Mentor.Global.NavigationStack.pop();
                                View = Mentor.Global.NavigationStack.pop();
                                var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                                mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});

                                if (UserType == "0") {
                                    me.getMentorPendingMeeting();
                                } else {
                                    me.getMentorForWaitScreen();
                                }
                            });
                        }, 100);
                    }
                },
                failure: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    console.log(responce);
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            });
        }
    },
    btnCancelPendingMeeting: function () {
        var me = this;
        var GLOBAL = Mentor.Global;
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var UserID;
        var UserType;

        if (MentorLoginUser != null) {
            UserID = MentorLoginUser.Id;
            UserType = "0";
            var CancelSubmitMeeting = Ext.getCmp('idbtnCancelPendingMeeting');
            var Record = Ext.getStore('MentorPendingMeeting').getAt(parseInt(CancelSubmitMeeting.getItemId())).data;
        } else if (MenteeLoginUser != null) {
            UserID = MenteeLoginUser.Id;
            UserType = "1";
            var CancelMeetingMentee = Ext.getCmp('idBtnCancelMeetingMentee');
            //var Record = Ext.getStore('WaitScreenMentor').getAt(parseInt(CancelMeetingMentee.getItemId())).data;
            var Record = Ext.getStore(Ext.getCmp('idStoreName').getValue()).getAt(parseInt(CancelMeetingMentee.getItemId())).data;
        }

        var CancellationComment = Ext.getCmp('menteeCancellationCommentCancelMeetingScreen').getValue();

        if (CancellationComment == "") {
            Ext.Msg.alert('MELS', "Reason for cancellation is required");
            return;
        } else {
            Ext.Viewport.setMasked({
                xtype: "loadmask",
                message: "Please wait"
            });

            var data = {
                InvitationId: Record.InvitationId,
                cancellationReason: CancellationComment,
                UserType: UserType
            };

            Ext.Ajax.request({
                url: GLOBAL.SERVER_URL,
                method: "POST",
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/json'
                },
                xhr2: true,
                disableCaching: false,
                jsonData: {
                    method: "cancelMeeting",
                    body: data
                },
                success: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    var data = Ext.decode(responce.responseText);
                    console.log(data);
                    d = data.cancelMeeting.data;

                    if (data.cancelMeeting.Error == 1) {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.cancelMeeting.Message);
                        }, 100);
                        return;
                    } else if (data.cancelMeeting.Error == 2) {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.cancelMeeting.Message);
                        }, 100);

                    } else {
                        Ext.Function.defer(function () {
                            Ext.Msg.alert('MELS', data.cancelMeeting.Message, function () {
                                var View = Mentor.Global.NavigationStack.pop();
                                View = Mentor.Global.NavigationStack.pop();
                                var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
                                mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});

                                if (UserType == '0') {
                                    me.getMentorPendingMeeting();
                                } else if (UserType == '1') {
                                    //me.getMentorWaitScreen();
                                }
                            });
                        }, 100);
                    }
                },
                failure: function (responce) {
                    Ext.Viewport.setMasked(false); //Avinash
                    console.log(responce);
                    var connectionExist = Mentor.Global.doesConnectionExist();
                    var error = (!connectionExist) ? Mentor.Global.INTERNET_ERROR_MESSAGE : Mentor.Global.ERROR_MESSAGE;
                    Ext.Function.defer(function () {
                        Ext.Msg.alert('MELS', error);
                    }, 100);
                }
            });
        }
    },
    btnBackMentorPendingMeetingDetail: function () {
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    btnBackMentorRescheduleMeeting: function () {
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    btnBackMentorCancelMeeting: function () {
        var View = Mentor.Global.NavigationStack.pop();
        var viewport = Ext.Viewport, mainPanel = viewport.down("#mainviewport");
        mainPanel.animateActiveItem(View, {type: "slide", direction: "right"});
    },
    //Clear Searach Mentor Metting History
    onClearSearchMentorMettingHistory: function () {
        var store = Ext.getStore('MeetingHistory');
        store.clearFilter();
    },
    onClearSearchMentorTeams: function () {
        var store = Ext.getStore('TeamListing');
        store.clearFilter();
    },
    onClearSearchUserNotes: function () {
        var store = Ext.getStore('UserNotes');
        store.clearFilter();
    },
    //Clear Searach Mentee Metting History
    onClearSearchMenteeMettingHistory: function () {
        var store = Ext.getStore('MeetingHistory');
        store.clearFilter();
    },
    onClearSearchTeam: function () {
        var store = Ext.getStore('TeamMembers');
        store.clearFilter();
    },
    ValidateEmail: function (mail) {
        if (/^\w+([\+.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
            return (true);
        }
        return (false);
        //return (true);
    }
});
