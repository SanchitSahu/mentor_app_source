Ext.define('Mentor.store.SourceList', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.SourceList",
        autoLoad: false,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idSourceList'
        }
    }
});