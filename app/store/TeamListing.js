Ext.define('Mentor.store.TeamListing', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.TeamListing",
        autoLoad: true,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idTeamListing'
        }
    }
});