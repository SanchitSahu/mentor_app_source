Ext.define('Mentor.store.TeamMembers', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.TeamMembers",
        autoLoad: true,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idTeamMembers'
        }
    }
});