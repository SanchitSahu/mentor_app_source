Ext.define('Mentor.store.WaitScreenMentor', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.WaitScreenMentor",
        autoLoad: true,
        autoSync: true
    }
});