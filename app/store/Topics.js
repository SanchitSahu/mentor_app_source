Ext.define('Mentor.store.Topics', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.Topics",
        autoLoad: false,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idTopics'
        }
    }
});