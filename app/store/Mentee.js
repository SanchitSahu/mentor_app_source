Ext.define('Mentor.store.Mentee', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.Mentee",
        autoLoad: false,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idMentee'
        }
    }
});