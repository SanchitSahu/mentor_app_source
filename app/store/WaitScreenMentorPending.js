Ext.define('Mentor.store.WaitScreenMentorPending', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.WaitScreenMentorPending",
        autoLoad: true,
        autoSync: true
    }
});