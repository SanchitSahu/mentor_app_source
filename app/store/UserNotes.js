Ext.define('Mentor.store.UserNotes', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.UserNotes",
        autoLoad: false,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idUserNotes'
        }
    }
});