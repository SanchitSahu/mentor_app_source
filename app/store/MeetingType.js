Ext.define('Mentor.store.MeetingType', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.MeetingType",
        autoLoad: true,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idMeetingType'
        }
    }
});