Ext.define('Mentor.store.WaitScreenMentorAccepted', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.WaitScreenMentorAccepted",
        autoLoad: true,
        autoSync: true
    }
});