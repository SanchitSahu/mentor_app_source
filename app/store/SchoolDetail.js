Ext.define('Mentor.store.SchoolDetail', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.SchoolDetail",
        autoLoad: false,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idSchoolDetail'
        }
    }
});