Ext.define('Mentor.store.MentorPendingMeeting', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.MentorPendingMeeting",
        autoLoad: true,
        autoSync: true
    }
});