Ext.define('Mentor.store.SubTopic', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.SubTopic",
        autoLoad: false,
        autoSync: true,
        proxy: {
            type: 'localstorage',
            id: 'idSubTopic'
        }
    }
});