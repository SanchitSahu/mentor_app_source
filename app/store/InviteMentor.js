Ext.define('Mentor.store.InviteMentor', {
    extend: 'Ext.data.Store',
    config: {
        model: "Mentor.model.InviteMentor",
        autoLoad: true,
        autoSync: true
    }
});