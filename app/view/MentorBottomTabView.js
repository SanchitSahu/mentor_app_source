Ext.define('Mentor.view.MentorBottomTabView', {
    extend: 'Ext.TabPanel',
    xtype: 'mentorBottomTabView',
    id: 'idMentorBottomTabView',
    requires: [
        'Mentor.view.MeetingHistory'
    ],
    config: {
        ui: 'light',
        tabBar: {
            docked: 'bottom',
            layout: {
                pack: 'center'
            },
            defaults: {
                flex: 1
            }
        },
        items: [
            {
                //xtype : 'mentorPendingRequest',
                xtype: 'mentorInvitesTabView',
                iconCls: 'pendingInvitesIconMentorBottomTabView',
                iconMask: true,
                title: Mentor.Global.MENTOR_PENDING_TITLE,
                //id: 'idPendingInvitesTabInvitesMentorBottomTabView',
                badgeText: ''
            },
            {
                xtype: 'meetinghistory',
                iconCls: 'mettingHistoryIconMentorBottomTabView',
                iconMask: true,
                Cls: 'mettingHistoryMentorBottomTabView',
                id: 'mettingHistoryMentorBottomTabView'
            },
            {
                xtype: 'instantMeeting',
                iconCls: 'instantMeetingIconMentorBottomTabView',
                iconMask: true,
                Cls: 'instantMeetingMentorBottomTabView',
                id: 'instantMeetingMentorBottomTabView'
            },
            {
                xtype: 'mentorprofile',
                iconCls: 'myProfileIconMentorBottomTabView',
                iconMask: true,
                title: 'My Profile'
            }
        ],
        listeners: [
            {
                event: "activeitemchange",
                fn: "activeitemchange"
            },
            {
                event: "activate",
                fn: "onPageActivate"
            }

        ]
    },
    activeitemchange: function (a, value, oldValue, eOpts) {
        /*if("itemIdMeetinghistory" == oldValue._itemId)
         Mentor.Global.TabNavigationStack.push(0);
         else if("itemIdmentorPendingRequest" == oldValue._itemId)
         Mentor.Global.TabNavigationStack.push(1);
         else if("itemIdProfile" == oldValue._itemId)
         Mentor.Global.TabNavigationStack.push(2);*/
    },
    onPageActivate: function () {
        var tabbar = this.getTabBar();
        var setImpromptuTimer = setInterval(function () {
            if (Mentor.Global.IMPROMPTU_NAME != '') {
                clearInterval(setImpromptuTimer);
                tabbar.getItems().items[2].setTitle(Mentor.Global.IMPROMPTU_NAME);
            }
        });
    }
});