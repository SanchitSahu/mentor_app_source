Ext.define('Mentor.view.MentorActionItems', {
    extend: 'Ext.Panel',
    xtype: 'mentor_action_items',
    requires: [
    ],
    config: {
        cls: 'mentor_action_items',
        //scrollable: "vertical",
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'container'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Mentor ToDo Items',
                id: 'mentor_action_items_title',
                cls: 'my-titlebar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'btnBackCLS',
                        docked: 'left',
                        action: 'btnBackMentorActionItems'
                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('mentor_action');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'label',
                id: 'idUserNameEnterpreneur',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                hidden: true
            },
            {
                xtype: 'label',
                html: Mentor.Global.VERSION_NAME,
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true
            },
            {
                xtype: 'panel',
                margin: '20 0 0 0',
                layout: {
                    type: 'vbox'
                },
                items: [
                    {
                        xtype: 'label',
                        html: 'Mentor ToDo Items',
                        style: 'margin-left:10px;margin-right:10px;border: 0px solid #eee;border-left-width: 0;border-bottom: 1px solid #66cc66;font-weight: bold;',
                        id: 'idLabelMentorActionItems',
                        hidden: true
                    },
                    {
                        xtype: 'panel',
                        id: 'idMenteeActionItemHeader',
                        margin: '10 0 0 0',
                        layout: {
                            type: 'hbox'
                        },
                        style: 'border-bottom: 1px solid #bfbfbf;padding-bottom: 5px;',
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin-left: 30px; font-size: smaller;color:#7f7f7f'
                            },
                            {
                                xtype: 'label',
                                html: 'ToDo',
                                style: 'margin-left: 9px; font-size: smaller;color:#7f7f7f'
                            },
                            {
                                xtype: 'label',
                                html: 'Done',
                                style: 'margin-left: 10px; font-size: smaller;color:#7f7f7f'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        margin: '10 0 0 0',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'panel',
                                margin: '0 0 0 10',
                                id: 'idMentorActionItemComments'
                            },
                            {
                                xtype: 'panel',
                                id: 'idMentorActionItem',
                                margin: '0 0 0 10'
                            },
                            {
                                //style : 'width: 85%;'
                                xtype: 'panel',
                                margin: '0 0 0 10',
                                id: 'idMentorActionItemDoneOnMeetingUpdate'
                            }
                        ]
                    },
                    /*{
                     
                     xtype: 'fieldset',
                     title: 'Satisfaction with Session Results',
                     style : 'border-bottom: 1px solid #66cc66;',
                     items: [
                     {
                     
                     xtype : 'selectfield',
                     id : 'idFeedbackSelectFieldMentorActionTaken',
                     options: [
                     {text: 'VERY GOOD',  value: 'VERY GOOD'},
                     {text: 'GOOD',  value: 'GOOD'},
                     {text: 'OK', value: 'OK'},
                     {text: 'POOR',  value: 'POOR'},
                     {text: 'VERY POOR',  value: 'VERY POOR'}
                     ]
                     
                     },
                     ]
                     
                     },*/
                    {
                        xtype: 'panel',
                        margin: '10 0 0 0 ',
                        style: 'background-color: #f2f2f2;',
                        hidden: true,
                        items: [
                            {
                                xtype: 'label',
                                html: 'Satisfaction with meeting',
                                margin: '20 0 0 0',
                                cls: 'reviewFormLabelCLS',
                                style: 'text-align: -webkit-center;'
                            },
                            {
                                xtype: 'label',
                                html: '1 being very poor and 5 being very good.',
                                style: 'text-align: -webkit-center;font-weight: bold;font-size:14px;color: #808080;'
                            },
                            {
                                //style: 'background:red',
                                xtype: 'ratingfield',
                                name: 'usefull',
                                label: ' ',
                                labelWidth: '0%',
                                value: 0,
                                id: 'idFeedbackSelectFieldMentorActionTaken',
                                labelCls: 'background:red',
                                style: 'background-color: #f2f2f2;'
                            }
                        ]
                    },
                    /*{
                     xtype: 'fieldset',
                     title: 'End Session',
                     style : 'margin-top: -20px;',
                     items:[{
                     xtype : 'datetimepickerfield',
                     placeHolder : 'End Session',
                     id : 'endSession',
                     dateTimeFormat  : 'Y-m-d H:i:s',
                     minHours:1, 
                     maxHours: 24,
                     value: new Date(),
                     }]
                     },*/
                    /*{
                     xtype: 'checkboxfield',
                     name : 'color',
                     value: '0',
                     label: 'Do Research',
                     checked: true,
                     labelWidth: 200,
                     labelCls : 'radio-btn-cls'
                     },
                     {
                     xtype: 'checkboxfield',
                     name : 'color',
                     value: '1',
                     label: 'Arrange Meetings with Other',
                     labelWidth: 200,
                     labelCls : 'radio-btn-cls'
                     },
                     {
                     xtype: 'checkboxfield',
                     name : 'color',
                     value: '0',
                     label: 'Review Material',
                     labelWidth: 200,
                     labelCls : 'radio-btn-cls'
                     },
                     {
                     xtype: 'fieldset',
                     title: 'Elapsed Time',
                     items: [
                     {
                     
                     xtype : 'timepickerfield',
                     placeHolder : 'OK'
                     
                     },
                     ]
                     },*/
                    {
                        xtype: 'label',
                        style: 'font-weight: bold;',
                        margin: '10 10 0 10 ',
                        html: 'Comment',
                        hidden: true
                    },
                    {
                        xtype: 'textareafield',
                        name: 'mentorComment',
                        id: 'mentorCommentMentorActionItems',
                        cls: 'textFieldDisableCLS',
                        maxRows: 2,
                        disabled: false,
                        inputCls: 'inputCLS',
                        clearIcon: false,
                        margin: '0 10 0 10 ',
                        hidden: true
                    },
                    {
                        xtype: 'panel',
                        margin: '10 0 0 0',
                        style: 'background-color: #f2f2f2;',
                        items: [
                            {
                                xtype: 'label',
                                html: 'Rate your satisfaction with the meeting',
                                margin: '20 0 0 0',
                                cls: 'reviewFormLabelCLS',
                                style: 'text-align: -webkit-center;'
                            },
                            {
                                xtype: 'label',
                                html: '(This rating will NOT be seen by Mentee)',
                                style: 'text-align: -webkit-center;font-weight: bold;font-size:12px;color: #808080;'
                            },
                            {
                                xtype: 'label',
                                margin: '10 0 0 0',
                                html: 'From Very Unsatisfied to Very Satisfied',
                                style: 'text-align: -webkit-center;font-weight: bold;font-size:16px;color: #808080;'
                            },
                            /*{
                             //style: 'background:red',
                             xtype: 'ratingfield',
                             name: 'usefull1',
                             label: '',
                             labelWidth: '0%',
                             value: 0,
                             labelCls: 'background:red',
                             //style: 'background: transparent;'
                             }*/
                            {
                                xtype: 'ratingfield',
                                name: 'mentorActionRatting',
                                label: ' ',
                                labelWidth: '0%',
                                value: 0,
                                //id: 'idSatisfactionWithMeeting',
                                labelCls: 'rattingCLS',
                                style: 'background-color: #f2f2f2;'
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            pack: 'center'
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'SUBMIT',
                                cls: 'signin-btn-cls',
                                pressedCls: 'press-btn-cls',
                                width: '50%',
                                id: 'btnSubmitMentorActionItems',
                                action: "btnNextMentorActionItems"
                            }
                        ]
                    }

                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            }
        ]
    },
    onPageActivate: function () {

        var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
        var MentorActionItemStore = Ext.getStore('MentorAction').load();
        var MentorActionPanel = Ext.getCmp('idMentorActionItem');
        MentorActionPanel.removeAll();

        var MentorActionCommentsStore = Ext.getStore('MentorActionComments').load();
        console.log(MentorActionCommentsStore);

        var MentorActionItemComments = Ext.getCmp('idMentorActionItemComments');
        MentorActionItemComments.removeAll();

        var MentorActionPanelDoneOnNewMeeting = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
        MentorActionPanelDoneOnNewMeeting.removeAll();

        //Ext.getCmp('idMenteeActionItemHeader').setHidden(true); // Display headder

        var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        Ext.getCmp('idUserNameEnterpreneur').setHtml(MentorLoginDetail.MentorName);

        //ByDefault
        Ext.getCmp('idFeedbackSelectFieldMentorActionTaken').setValue("OK");
        Ext.getCmp('btnSubmitMentorActionItems').setText('SUBMIT');
        Ext.getCmp('btnSubmitMentorActionItems').setDisabled(false);

        //Ext.getCmp('endSession').setValue(new Date());

        for (i = 0; i < MentorActionItemStore.data.length; i++) {
            var checked = false;
            if (MeetingDetail != null) {
                Ext.getCmp('idMenteeActionItemHeader').setHidden(false); // Hide headder
                var MentorActionItems = MeetingDetail.MentorActionDetail.split(',');
                for (j = 0; j < MentorActionItems.length; j++) {
                    var MentorAction = MentorActionItems[j].split('~')
                    if (MentorAction[0] == MentorActionItemStore.data.getAt(i).data.MentorActionID) {
                        checked = true;
                        break;
                    }
                }

                var checkBoxField = Ext.create('Ext.field.Checkbox', {
                    //label: MentorActionItemStore.data.getAt(i).data.MentorActionName,
                    //labelWidth: 280,
                    name: 'recorded_stream',
                    value: MentorActionItemStore.data.getAt(i).data.MentorActionID,
                    itemId: MentorActionItemStore.data.getAt(i).data.MentorActionName,
                    labelCls: 'radio-btn-cls',
                    checked: checked,
                    labelWidth: '90%',
                    labelAlign: 'right',
                    listeners: {
                        'check': function (radio, e, eOpts) {
                            //me.radioHandler(radio.getValue());
                            if (radio.getItemId().toLowerCase() == "no action needed") {
                                var MentorActionPanel = Ext.getCmp('idMentorActionItem');
                                for (i = 0; i < MentorActionPanel.getItems().length; i++) {
                                    if (MentorActionPanel.getItems().getAt(i).isChecked()) {
                                        if (MentorActionPanel.getItems().getAt(i).getItemId().toLowerCase() != "no action needed") {
                                            MentorActionPanel.getItems().getAt(i).setChecked(false);
                                        }
                                    }
                                }
                            } else {
                                var MentorActionPanel = Ext.getCmp('idMentorActionItem');
                                for (i = 0; i < MentorActionPanel.getItems().length; i++) {
                                    if (MentorActionPanel.getItems().getAt(i).isChecked()) {
                                        if (MentorActionPanel.getItems().getAt(i).getItemId().toLowerCase() == "no action needed") {
                                            MentorActionPanel.getItems().getAt(i).setChecked(false);
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
                MentorActionPanel.add(checkBoxField);
            } else {
                var checkBoxField = Ext.create('Ext.field.Checkbox', {
                    //label: MentorActionItemStore.data.getAt(i).data.MentorActionName,
                    //labelWidth: 280,
                    name: 'recorded_stream',
                    value: MentorActionItemStore.data.getAt(i).data.MentorActionID,
                    itemId: MentorActionItemStore.data.getAt(i).data.MentorActionName,
                    labelCls: 'radio-btn-cls',
                    checked: checked,
                    labelWidth: '90%',
                    labelAlign: 'right',
                    listeners: {
                        'check': function (radio, e, eOpts) {
                            //me.radioHandler(radio.getValue());
                            if (radio.getItemId().toLowerCase() == "no action needed")
                            {
                                var MentorActionPanel = Ext.getCmp('idMentorActionItem');
                                var MentorActionItemsDonePanel = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
                                for (var i = 0; i < MentorActionPanel.getItems().length; i++) {
                                    //if(MentorActionPanel.getItems().getAt(i).isChecked())
                                    {
                                        if (MentorActionPanel.getItems().getAt(i).getItemId().toLowerCase() != "no action needed") {
                                            MentorActionPanel.getItems().getAt(i).setChecked(false);
                                        } else {
                                            MentorActionItemsDonePanel.getItems().getAt(i).setDisabled(false);
                                        }

                                    }
                                }
                            } else {
                                var MentorActionPanel = Ext.getCmp('idMentorActionItem');
                                var MentorActionItemsDonePanel = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
                                for (var i = 0; i < MentorActionPanel.getItems().length; i++) {
                                    if (MentorActionPanel.getItems().getAt(i).isChecked()) {
                                        if (MentorActionPanel.getItems().getAt(i).getItemId().toLowerCase() == "no action needed") {
                                            MentorActionPanel.getItems().getAt(i).setChecked(false);
                                        } else {
                                            MentorActionItemsDonePanel.getItems().getAt(i).setDisabled(false);
                                        }
                                    } else if (MentorActionItemsDonePanel.getItems().getAt(i).isChecked() == false) {
                                        MentorActionItemsDonePanel.getItems().getAt(i).setDisabled(true);
                                    }
                                }
                            }
                        },
                        'uncheck': function (radio, e, eOpts) {
                            var MentorActionPanel = Ext.getCmp('idMentorActionItem');
                            var MentorActionItemsDonePanel = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
                            for (var i = 0; i < MentorActionPanel.getItems().length; i++) {
                                if (!MentorActionPanel.getItems().getAt(i).getChecked()) {
                                    MentorActionItemsDonePanel.getItems().getAt(i).setChecked(false);
                                    MentorActionItemsDonePanel.getItems().getAt(i).setDisabled(true);
                                }
                            }
                        }
                    }
                });
                MentorActionPanel.add(checkBoxField);

                //Action Item Done for New Meeting
                var checked = false;
                var Disable = true;

                var checkBoxFieldActionDone = Ext.create('Ext.field.Checkbox', {
                    name: 'recorded_stream',
                    value: MentorActionItemStore.data.getAt(i).data.MentorActionID,
                    label: MentorActionItemStore.data.getAt(i).data.MentorActionName,
                    labelCls: 'radio-btn-cls',
                    checked: checked,
                    disabled: Disable,
                    labelWidth: '90%',
                    labelAlign: 'right',
                    listeners: {
                        'check': function (radio, e, eOpts) {
                            //me.radioHandler(radio.getValue());
                        }
                    }
                });
                MentorActionPanelDoneOnNewMeeting.add(checkBoxFieldActionDone);

                var MeetingId = Ext.decode(localStorage.getItem('SaveStartedMeetingID'));
                var cls = 'addnote';
                var icon = 'add_icon_filled.png';
                var isContinue = false;
                for (var c = 0; c < MentorActionCommentsStore.data.length; c++) {
                    if (isContinue)
                        continue;
                    if (MentorActionCommentsStore.data.getAt(c).data.MeetingID == MeetingId.SaveStartedMeetingID) {
                        if (MentorActionCommentsStore.data.getAt(c).data.MentorActionItemID == MentorActionItemStore.data.getAt(i).data.MentorActionID) {
                            cls = 'editnote_' + MentorActionCommentsStore.data.getAt(c).data.MentorActionItemCommentID;
                            icon = 'edit_note.png';
                        }
                    }
                }

                var buttonFieldActionComment = Ext.create('Ext.Img', {
                    src: 'resources/images/' + icon,
                    height: 25,
                    width: 25,
                    itemId: MentorActionItemStore.data.getAt(i).data.MentorActionID,
                    cls: cls,
                    listeners: {
                        'tap': function (radio, e, eOpts) {
                            var popup = Mentor.app.application.getController('AppDetail').addCommentPopup(0, radio.getItemId(), radio.getCls()[0], MeetingId.SaveStartedMeetingID);
                            Ext.Viewport.add(popup);
                            popup.show();
                        }
                    }
                });
                MentorActionItemComments.add(buttonFieldActionComment);
            }
        }

        // From Review Screen
        if (MeetingDetail != null) {
            Ext.getCmp('idFeedbackSelectFieldMentorActionTaken').setValue(MeetingDetail.MeetingFeedback);
            Ext.getCmp('btnSubmitMentorActionItems').setText('Update');
            //Ext.getCmp('btnSubmitMentorActionItems').setDisabled(true);
            //Ext.getCmp('endSession').setValue(new Date(MeetingDetail.MeetingEndDatetime));

            var MentorActionPanelDoneOnMettingUpdate = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
            MentorActionPanelDoneOnMettingUpdate.removeAll();
            for (i = 0; i < MentorActionItemStore.data.length; i++) {
                var checked = false;
                var Disable = true;
                if (MeetingDetail != null) {
                    var MentorActionItems = MeetingDetail.MentorActionDetail.split(',');
                    var MentorActionItemsDone = "";
                    if (MeetingDetail.MentorActionItemDone != null) {
                        MentorActionItemsDone = MeetingDetail.MentorActionItemDone.split(',');
                    }
                    for (j = 0; j < MentorActionItems.length; j++) {
                        var MentorAction = MentorActionItems[j].split('~')
                        if (MentorAction[0] == MentorActionItemStore.data.getAt(i).data.MentorActionID) {
                            for (k = 0; k < MentorActionItems.length; k++) {
                                if (MentorActionItemsDone[k] == MentorActionItemStore.data.getAt(i).data.MentorActionID) {
                                    checked = true;
                                }
                            }
                            Disable = false;
                            break;
                        }
                    }
                }

                var checkBoxField = Ext.create('Ext.field.Checkbox', {
                    name: 'recorded_stream',
                    value: MentorActionItemStore.data.getAt(i).data.MentorActionID,
                    label: MentorActionItemStore.data.getAt(i).data.MentorActionName,
                    labelCls: 'radio-btn-cls',
                    checked: checked,
                    disabled: Disable,
                    labelWidth: '90%',
                    labelAlign: 'right',
                    listeners: {
                        'check': function (radio, e, eOpts) {
                            //me.radioHandler(radio.getValue());
                        }
                    }
                });
                MentorActionPanelDoneOnMettingUpdate.add(checkBoxField);
            }
        } else {
            /*var MentorActionPanelDoneOnMettingUpdate = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
             MentorActionPanelDoneOnMettingUpdate.removeAll();*/
        }
        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        //Ext.getCmp('idLabelMentorActionItems').setHtml(Mentor.Global.MENTOR_NAME + ' Action Items');
    },
    onPagePainted: function () {
        Ext.getCmp('mentorCommentMentorActionItems').reset();
        Ext.getCmp('mentor_action_items_title').setTitle(Mentor.Global.MENTOR_NAME + ' ToDo Items');
    }
});
