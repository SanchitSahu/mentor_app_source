Ext.define('Mentor.view.AddTeamMembers', {
    extend: 'Ext.Panel',
    xtype: 'addteammembers',
    requires: [],
    config: {
        cls: 'mentor_action_items',
        //scrollable: "vertical",
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'container'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Tasks',
                id: 'action_add_teammembers',
                cls: 'my-titlebar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'btnBackCLS',
                        docked: 'left',
                        // action: 'btnBackActionAddTeamMembers'
                        listeners: {
                            tap: function () {
                                Mentor.app.application.getController('AppDetail').btnBackActionTeamDetail();
                            }
                        }
                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('add_team_member');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    },
                    {
                        iconCls: 'btnBackCLS',
                        docked: 'right',
                        style: 'visibility:hidden'
                    }
                ]
            },

            {
                xtype: 'panel',
                margin: '20 0 0 0',
                layout: {
                    type: 'vbox'
                },
                items: [
                    {
                        xtype: 'label',
                        // html: 'Mentee List',
                        //style: 'margin-left:10px;margin-right:10px;border: 0px solid #eee;border-left-width: 0;border-bottom: 1px solid #66cc66;font-weight: bold;',
                        style: 'border-bottom: 1px solid #bfbfbf;padding-bottom: 5px;padding-left: 15px;',
                        id: 'idLabelTeamMenteeAddTeamMembers'
                        //hidden: true
                    },

                    {
                        xtype: 'panel',
                        id: 'idMenteeTeamActionItem',
                        margin: '0 0 0 10'
                    },
                    {
                        xtype: 'label',
                        margin: '10 0 0 0',
                        // html: 'Mentor List',
                        //style: 'margin-left:10px;margin-right:10px;border: 0px solid #eee;border-left-width: 0;border-bottom: 1px solid #66cc66;font-weight: bold;',
                        style: 'border-bottom: 1px solid #bfbfbf;padding-bottom: 5px;padding-left: 15px;',
                        id: 'idLabelAddTeamMembers'
                        //hidden: true
                    },
                    {
                        xtype: 'panel',
                        id: 'idMentorActionItem',
                        margin: '0 0 0 10'
                    },
                    {
                        xtype: 'button',
                        text: 'Submit',
                        cls: 'signin-btn-cls',
                        pressedCls: 'press-btn-cls',
                        id: 'btnSubmitAddTeamMember',
                        action: "btnSubmitAddTeamMember",
                        margin: '20'
                    }
                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            }
        ]
    },
    onPageActivate: function () {
        var MenteeListStore = Ext.getStore('MenteeForTeam').load();
        var MentorListStore = Ext.getStore('MentorForTeam').load();

        var MentorTeamActionPanel = Ext.getCmp('idMentorActionItem');
        MentorTeamActionPanel.removeAll();
        var MenteeTeamActionPanel = Ext.getCmp('idMenteeTeamActionItem');
        MenteeTeamActionPanel.removeAll();

        for (var i = 0; i < MenteeListStore.data.length; i++) {
            var checked = false;
            if (MenteeListStore.data.getAt(i).data.MenteeID >= 0) {
                var checkBoxField = Ext.create('Ext.field.Checkbox', {

                    name: 'recorded_stream',
                    itemId: MenteeListStore.data.getAt(i).data.MenteeID,
                    value: MenteeListStore.data.getAt(i).data.MenteeName,
                    label: MenteeListStore.data.getAt(i).data.MenteeName,
                    labelCls: 'radio-btn-cls',
                    checked: checked,
                    labelWidth: '90%',
                    labelAlign: 'right'
                });
                MenteeTeamActionPanel.add(checkBoxField);
            }
        }

        for (i = 0; i < MentorListStore.data.length; i++) {
            var checked = false;
            if (MentorListStore.data.getAt(i).data.MentorID >= 0) {
                var checkBoxField = Ext.create('Ext.field.Checkbox', {
                    name: 'recorded_stream',
                    itemId: MentorListStore.data.getAt(i).data.MentorID,
                    value: MentorListStore.data.getAt(i).data.MentorName,
                    label: MentorListStore.data.getAt(i).data.MentorName,
                    labelCls: 'radio-btn-cls',
                    checked: checked,
                    labelWidth: '90%',
                    labelAlign: 'right'
                });
                MentorTeamActionPanel.add(checkBoxField);
            }
        }
        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        Ext.getCmp('idLabelAddTeamMembers').setHtml(Mentor.Global.MENTOR_NAME + ' List');
        Ext.getCmp('idLabelTeamMenteeAddTeamMembers').setHtml(Mentor.Global.MENTEE_NAME + ' List');
    },
    onPagePainted: function () {}
});
