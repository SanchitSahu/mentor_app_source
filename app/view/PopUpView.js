Ext.define('Mentor.view.PopUpView', {
    extend: 'Ext.Panel',
    xtype: 'popview',
    requires: [],
    config: {
        cls: 'signUp',
        defaults: {
            xtype: 'formpanel'
        },
        items: [
            {
                xtype: 'calendar',
                id: 'calendarViewID',
                floating: true,
                centered: true,
                width: 300,
                height: 400,
                fullscreen: true,
                viewConfig: {
                    mode: 'month',
                    weekStart: 1,
                    value: Mentor.Global.Current_date
                }
            }
        ]
    },
    listeners: [
        {
            event: "activate",
            fn: "onPageActivate"
        },
        {
            event: "painted",
            fn: "onPagePainted"
        }
    ],
    onPagePainted: function () {
        //Mentor.app.application.getController('AppDetail').getMentorForWaitScreen();
    }
});