Ext.define('Mentor.view.DeclineResponses', {
    extend: 'Ext.Panel',
    xtype: 'declineresponses',
    requires: [
    ],
    config: {
        cls: 'forgotpassword',
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'formpanel'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Reasons Declined',
                cls: 'my-titlebar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'btnBackCLS',
                        action: 'btnBackAcceptDeclineResponses',
                        docked: 'left'
                    },
//                    {
//                        iconCls: 'addIcon',
//                        docked: 'left',
//                        listeners: {
//                            element: 'element',
//                            tap: function () {
//                                var popup = Mentor.app.application.getController('AppDetail').addNotePopup();
//                                Ext.Viewport.add(popup);
//                                popup.show();
//                            }
//                        }
//                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        //style: 'visibility:hidden'
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('decline_responses');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    },
                    {
                        iconCls: 'refreshIcon',
                        docked: 'right',
                        handler: function () {
                            Mentor.app.application.getController('Main').getDeclineResponses(true);
                        }
                    }
                ]
            },
            {
                xtype: 'label',
                html: 'Reasons<span class="spanAsterisk">*</span>',
                style: 'font-weight: bold;',
                margin: '10 10 3 10'
            },
            {
                xtype: 'panel',
                margin: '10 0 0 0',
                items: [
                    {
                        xtype: 'panel',
                        id: 'idMentorDeclineResponses',
                        margin: '0 0 0 10'
                    }
                ]
            },
            {
                xtype: 'panel',
                flex: 1,
                style: 'margin-top:10px;',
                items: [
                    {
                        xtype: 'fieldset',
                        id: 'idMentorCommetnsFieldSetAcceptRejectPendingRequest',
                        title: Mentor.Global.MENTOR_NAME + " Comments",
                        items: [
                            {
                                xtype: 'textareafield',
                                id: 'commentsRejectPendingRequest',
                                placeHolder: Mentor.Global.MENTOR_NAME + ' Comments',
                                inputCls: 'inputCLS',
                                clearIcon: false
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                margin: '10 10 10 10',
                items: [
                    {
                        xtype: 'button',
                        text: 'OK',
                         cls: 'signin-btn-cls',
                        pressedCls : 'press-btn-cls',
                        action: 'doSubmitDeclineRequest',
                        flex: 1,
                        margin: '0 5 0 0'
                    },
                    {
                        xtype: 'button',
                        text: 'Cancel',
                        //cls: 'decline-btn-cls',
                         pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        action: 'btnBackAcceptDeclineResponses',
                        flex: 1,
                        margin: '0 0 0 5'
                    }
                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            },
            {
                event: "deactivate",
                fn: "onPageDeactivate"
            }
        ]
    },
    onPageDeactivate: function () {
        var MentorDeclineResponsesID;
        var MentorDeclineResponsesName;
        var isChecked = 0;
        var data = null;
        var MentorDeclineResponses = Ext.getCmp('idMentorDeclineResponses');
        for (i = 0; i < MentorDeclineResponses.getItems().length; i++) {
            if (MentorDeclineResponses.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    MentorDeclineResponsesID = MentorDeclineResponses.getItems().getAt(i).getValue();
                    MentorDeclineResponsesName = MentorDeclineResponses.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                } else {
                    MentorDeclineResponsesID = MentorDeclineResponsesID + "," + MentorDeclineResponses.getItems().getAt(i).getValue();
                    MentorDeclineResponsesName = MentorDeclineResponsesName + ";" + MentorDeclineResponses.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                }
            }
        }
        if (isChecked != 0) {
            data = {
                MentorDeclineResponsesID: MentorDeclineResponsesID,
                MentorDeclineResponsesName: MentorDeclineResponsesName
            };
            data = Ext.encode(data);
        }
        localStorage.setItem('CacheDeclineResponse', data);
    },
    onPageActivate: function () {
        var CachedDeclineResponse = Ext.decode(localStorage.getItem('CacheDeclineResponse'));
        localStorage.setItem('CacheDeclineResponse', null);

        var MentorDeclineResponsesStore = Ext.getStore('DeclineResponses');
        var MentorDeclineResponses = Ext.getCmp('idMentorDeclineResponses');
        MentorDeclineResponses.removeAll();
        for (i = 0; i < MentorDeclineResponsesStore.data.length; i++) {
            var checked = false;
            if (CachedDeclineResponse != null) {
                var SelectedDeclineResponse = CachedDeclineResponse;
                if (SelectedDeclineResponse != "" && SelectedDeclineResponse.MentorDeclineResponsesName != null && SelectedDeclineResponse.MentorDeclineResponsesName != '') {
                    SelectedDeclineResponse = SelectedDeclineResponse.MentorDeclineResponsesName.split(";");
                    if (SelectedDeclineResponse.indexOf(MentorDeclineResponsesStore.data.getAt(i).data.ResponseName) != -1) {
                        checked = true;
                    }
                }
            }
            var checkBoxField = Ext.create('Ext.field.Checkbox', {
                name: 'recorded_stream',
                value: MentorDeclineResponsesStore.data.getAt(i).data.ResponseID,
                itemId: MentorDeclineResponsesStore.data.getAt(i).data.ResponseName,
                label: MentorDeclineResponsesStore.data.getAt(i).data.ResponseName,
                labelCls: 'radio-btn-cls',
                checked: checked,
                labelWidth: '90%',
                labelAlign: 'right'
            });
            MentorDeclineResponses.add(checkBoxField);
        }

        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        Ext.getCmp('commentsRejectPendingRequest').setValue('');
        Ext.getCmp('commentsRejectPendingRequest').setPlaceHolder(Mentor.Global.MENTOR_NAME + ' Comments');
        Ext.getCmp('idMentorCommetnsFieldSetAcceptRejectPendingRequest').setTitle(Mentor.Global.MENTOR_NAME + ' Comments');
    }
});
