Ext.define('Mentor.view.Profile', {
    extend: 'Ext.Panel',
    xtype: 'mentorprofile',
    itemId: 'itemIdProfile',
    requires: [],
    config: {
        cls: 'mentorprofile',
        //scrollable: "vertical",
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'container'
        },
        items: [
            {
                xtype: 'panel',
                id: 'idTopHeaderProfileScreen',
                layout: {
                    type: 'vbox'
                },
                items: [
                    {
                        //docked: 'top',
                        xtype: 'toolbar',
                        title: 'Profile',
                        cls: 'my-titlebar',
                        //style:'background:transparent;',
                        id: 'idToolbarProfileScreen',
                        defaults: {
                            iconMask: true
                        },
                        items: [
                            /*{
                             //style : 'visibility: hidden;',
                             iconCls: 'btnBackCLS',
                             id:'profileBackBtn',
                             docked: 'left',
                             handler: function () {
                             var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
                             if (MentorLoginUser != null) {
                             var TabBar = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar();
                             TabBar.setActiveTab(0);
                             Ext.getCmp('idMentorBottomTabView').setActiveItem(0);
                             } else {
                             Ext.getCmp('idMenteeBottomTabView').setActiveItem(0);
                             }
                             }
                             },*/
                            {
                                xtype: 'button',
                                id: 'listButton',
                                iconCls: 'leftMenuIconCLS',
                                iconMask: true,
                                docked: 'left',
                                ui: 'plain',
                                handler: function () {
                                    if (Ext.Viewport.getMenus().left.isHidden()) {
                                        Ext.Viewport.showMenu('left');
                                    } else
                                    {
                                        Ext.Viewport.hideMenu('left');
                                    }
                                }
                            },
                            {
                                iconCls: 'goToNotesIcon',
                                docked: 'left',
                                id: 'addnotes',
                                action: 'addnotes'
                            },
                            {
                                iconCls: 'helpIcon',
                                docked: 'right',
                                listeners: {
                                    element: 'element',
                                    tap: function () {
                                        var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('profile');
                                        Ext.Viewport.add(popup);
                                        popup.show();
                                    }
                                }
                            },
                            {
                                iconCls: 'refreshIcon',
                                docked: 'right',
                                //action : 'btnSaveProfile',
                                action: "btnGetUpdatedProfile",
                                id: "idProfileRefresh",
                                listeners: {
                                    element: 'element',
                                    tap: function () {
                                        var popup = Mentor.app.application.getController('UpdateProfilePicture').btnGetUpdatedTeamData();
                                    }
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        cls: 'mettingHistoryToolBar',
                        id: 'idImagePanelProfileScreen',
                        layout: {
                            type: 'vbox',
                            pack: 'center',
                            align: 'middle'
                        },
                        items: [
                            {
                                //docked: 'top',
                                xtype: 'panel',
                                defaults: {
                                    iconMask: true
                                },
                                layout: {
                                    type: 'hbox'
                                },
                                items: [
                                    {
                                        xtype: 'panel',
                                        margin: '0 0 0 8',
                                        layout: {
                                            type: 'vbox',
                                            pack: 'center',
                                            align: 'middle'
                                        },
                                        items: [
                                            {
                                                xtype: "button",
                                                action: "takeProfilePhoto",
                                                id: 'id_ProfilePicture',
                                                cls: "photo-preview-cls",
                                                margin: 10
                                            },
                                            {
                                                xtype: 'label',
                                                id: 'idUserNameProfileScreen',
                                                style: 'color: white;margin-bottom: 10px;'
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'panel',
                                        margin: '0 0 0 8',
                                        layout: {
                                            type: 'vbox',
                                            pack: 'center',
                                            align: 'middle'
                                        },
                                        items: [
                                            {
                                                xtype: "button",
                                                //action: "takeProfilePhoto",
                                                id: 'id_TeamPicture',
                                                cls: "team-photo-preview-cls",
                                                margin: 10,
                                                listeners: {
                                                    tap: function () {
                                                        var popup = Mentor.app.application.getController('AppDetail').teamMenuPopup();
                                                        Ext.Viewport.add(popup);
                                                        popup.show();
                                                    }
                                                }
                                            },
                                            {
                                                xtype: 'label',
                                                id: 'idTeamNameProfileScreen',
                                                html: 'No Team',
                                                style: 'color: white;margin-bottom: 10px;'
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                xtype: 'label',
                                id: 'idSchoolNameProfileScreen',
                                style: 'color: white;margin-bottom: 10px;'
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'panel',
                layout: {
                    type: 'vbox'
                },
                items: [
                    {
                        xtype: 'panel',
                        margin: '10 15 10 15',
                        items: [
                            {
                                //docked: 'bottom',
                                xtype: 'label',
                                html: Mentor.Global.VERSION_NAME,
                                style: 'text-align: right;padding-right: 10px;padding: 0 10px 0 0;',
                                hidden: false
                            },
                            {
                                xtype: 'panel',
                                cls: 'userInputTextProfileCLS',
                                items: [
                                    {
                                        xtype: 'label',
                                        html: 'Email',
                                        cls: 'userProfileLabelCLS'
                                    },
                                    {
                                        //label : 'Email',
                                        xtype: 'textfield',
                                        id: 'email',
                                        disabled: false,
                                        clearIcon: false,
                                        inputCls: 'profileInputCLS'
                                    }
                                ]
                            },

                            /* {
                             xtype: 'image',
                             style: 'border-bottom: 1px #cacaca solid;'
                             },*/
                            {
                                xtype: 'panel',
                                cls: 'userInputTextProfileCLS',
                                margin: '15 0 0 0',
                                items: [
                                    {
                                        xtype: 'label',
                                        html: 'Username',
                                        cls: 'userProfileLabelCLS'
                                    },
                                    {
                                        //label : 'Username',
                                        xtype: 'textfield',
                                        id: 'userName',
                                        disabled: false,
                                        clearIcon: false,
                                        inputCls: 'profileInputCLS'
                                    }
                                ]
                            },
                            /*{
                             xtype: 'image',
                             style: 'border-bottom: 1px #cacaca solid;'
                             },*/
                            {
                                xtype: 'panel',
                                cls: 'userInputTextProfileCLS',
                                margin: '15 0 0 0',
                                items: [
                                    {
                                        xtype: 'label',
                                        html: 'Password',
                                        cls: 'userProfileLabelCLS'
                                    },
                                    {
                                        //label : 'Password',
                                        xtype: 'textfield',
                                        id: 'password',
                                        disabled: false,
                                        clearIcon: false,
                                        //inputCls: 'inputCLS',
                                        autoCapitalize: false,
                                        autoComplete: false,
                                        autoCorrect: false,
                                        inputCls: 'profileInputCLS'
                                    }
                                ]
                            },
                            /*{
                             xtype: 'image',
                             style: 'border-bottom: 1px #cacaca solid;'
                             },*/
                            {
                                xtype: 'panel',
                                cls: 'userInputTextProfileCLS',
                                margin: '15 0 0 0',
                                items: [
                                    {
                                        xtype: 'label',
                                        html: 'Confirm Password',
                                        cls: 'userProfileLabelCLS'
                                    },
                                    {
                                        //label : 'Password',
                                        xtype: 'textfield',
                                        id: 'cnfpassword',
                                        disabled: false,
                                        clearIcon: false,
                                        //inputCls: 'inputCLS',
                                        inputCls: 'profileInputCLS',
                                        autoCapitalize: false,
                                        autoComplete: false,
                                        autoCorrect: false
                                    }
                                ]
                            },
                            /*{
                             xtype: 'image',
                             style: 'border-bottom: 1px #cacaca solid;'
                             },*/
                            {
                                xtype: 'panel',
                                cls: 'userInputTextProfileCLS',
                                margin: '15 0 0 0',
                                items: [
                                    {
                                        xtype: 'label',
                                        html: 'Phone',
                                        cls: 'userProfileLabelCLS'
                                    },
                                    {
                                        //label : 'Phone',
                                        xtype: 'textfield',
                                        id: 'phone',
                                        disabled: false,
                                        clearIcon: false,
                                        //inputCls: 'inputCLS',
                                        inputCls: 'profileInputCLS',
                                        listeners: {
                                            keyup: function (field, value) {

                                            }
                                        }
                                    }
                                ]
                            },
                            /*{
                             xtype: 'image',
                             style: 'border-bottom: 1px #cacaca solid;'
                             },*/
                            {
                                xtype: 'label',
                                html: 'Enable Text Messaging (aka SMS) on this phone?',
                                cls: 'userProfileLabelCLS',
                                margin: '10 0 0 0',
                                style: 'background:transparent'
                            },
                            {
                                xtype: 'panel',
                                margin: '0 0 0 8',
                                layout: {
                                    type: 'hbox'
                                },
                                style: 'background:transparent',
                                items: [
                                    {
                                        xtype: 'radiofield',
                                        name: 'idIsPhoneSMSCapableProfileScreen',
                                        label: "Yes",
                                        labelCls: 'radio-btn-cls',
                                        labelWidth: '80%',
                                        labelAlign: 'right',
                                        hidden: false,
                                        value: true,
                                        style: 'background:transparent'
                                    },
                                    {
                                        xtype: 'radiofield',
                                        name: 'idIsPhoneSMSCapableProfileScreen',
                                        label: "No",
                                        labelCls: 'radio-btn-cls',
                                        labelWidth: '80%',
                                        labelAlign: 'right',
                                        hidden: false,
                                        value: false,
                                        checked: true,
                                        style: 'background:transparent'
                                    }
                                ]
                            },
                            /*{
                             xtype: 'image',
                             style: 'border-bottom: 1px #cacaca solid;'
                             },*/
                            {
                                xtype: 'panel',
                                cls: 'userInputTextProfileCLS',
                                margin: '15 0 0 0',
                                items: [
                                    {
                                        xtype: 'label',
                                        html: 'Source',
                                        cls: 'userProfileLabelCLS'
                                    },
                                    {
                                        xtype: 'selectfield',
                                        id: 'idSourceProfileScreen',
                                        store: 'SourceList',
                                        displayField: 'SourceName',
                                        valueField: 'SourceID',
                                        inputCls: 'profileSelectFieldInputCLS'
                                    }
                                ]
                            }
                                    /*{
                                     xtype: 'image',
                                     style: 'border-bottom: 1px #cacaca solid;'
                                     }*/
                        ]
                    },
                    {
                        xtype: 'panel',
                        style: 'background : #ebebeb;padding: 10px 30px;',
                        margin: '10 2 0 2',
                        items: [
                            {
                                xtype: 'label',
                                html: 'Add Interests',
                                cls: 'profileAddInterestCLS',
                                id: 'addInterestProfile'
                            },
                            {
                                xtype: 'label',
                                html: 'Tap on any interests to add them to your profile.',
                                cls: 'profileAddInterestDetailCLS'
                            }
                        ]
                    },
                    {
                        xtype: 'button',
                        text: 'Save Contact Info',
                        id: 'idButtonUpdateProfile',
                        cls: 'signin-btn-cls',
                        action: "btnUpdateProfile",
                        margin: '10 0 0 0',
                        hidden: true
                    },
                    {
                        xtype: 'label',
                        html: 'Add Skills',
                        id: 'idLabelAddSkillProfile',
                        style: 'margin-left:10px;margin-right:10px;margin-top:20px;border: 0px solid #eee;border-left-width: 0;border-bottom: 1px solid #66cc66;font-weight: bold;',
                        hidden: true
                    },
                    {
                        xtype: 'panel',
                        id: 'idSkillsProfile',
                        margin: '10 30 5 30',
                        defaults: {
                            style: {
                                float: 'left',
                                pack: 'center'
                            }
                        },
                        control: {
                            'button[action="addInterest"]': {
                                tap: function (element) {
                                    if (element.getCls() == 'profileSkillsActiveCLS')
                                        element.setCls('profileSkillsInActiveCLS');
                                    else
                                        element.setCls('profileSkillsActiveCLS');
                                }
                            }
                        }
                    },
                    {
                        xtype: 'panel',
                        layout: {
                            type: 'hbox'
                        },
                        margin: '0 10',
                        items: [
                            {
                                xtype: 'button',
                                text: 'SAVE',
                                id: 'idButtonAddSkillProfile',
                                cls: 'signin-btn-cls',
                                pressedCls: 'press-btn-cls',
                                action: "btnAddSkills",
                                hidden: false,
                                flex: 1
                            },
                            {
                                xtype: 'button',
                                text: 'CANCEL',
                                //cls: 'signin-btn-cls',
                                pressedCls: 'press-grey_btn-cls',
                                cls: 'grey-btn-cls',
                                hidden: false,
                                flex: 1,
                                //style: 'background:rgb(253, 84, 71)',
                                handler: function () {
                                    var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
                                    if (MentorLoginUser != null)
                                        Mentor.app.application.getController('LeftMenu').displayPendingTabMentee();
                                    else
                                        Mentor.app.application.getController('LeftMenu').displayInviteTabMentee();
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'button',
                        text: 'Access Subadmin Panel',
                        id: 'idButtonSubadminPanelProfile',
                        cls: 'accesssubadmin-btn-cls',
                        action: "openWebview",
                        hidden: true
                    },
                    {
                        xtype: 'button',
                        text: 'Signout',
                        id: 'idButtonSignOutProfile',
                        cls: 'signin-btn-cls',
                        action: "",
                        hidden: true,
                        handler: function () {
                            Ext.Msg.confirm("MELS", "Are you sure you want to sign out?", function (btn) {
                                if (btn == 'yes') {
                                    Mentor.app.application.getController('Main').btnSignOut();
                                }
                            });
                        }
                    }
                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            },
            {
                event: "beforeDestroy",
                fn: "onPageDeactivate"
            }
        ]
    },
    onPageActivate: function () {
        var SkillsStore = Ext.getStore('Skills').load();
        var SkillsPanel = Ext.getCmp('idSkillsProfile');
        var me = this;
        SkillsPanel.removeAll();

        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        var Skills;
        var SchoolName = localStorage.getItem("SchoolName");
        SchoolName = SchoolName.charAt(0).toUpperCase() + SchoolName.slice(1);
        Ext.getCmp('idSchoolNameProfileScreen').setHtml(SchoolName);
        if (MentorLoginUser != null) {
            if (MentorLoginUser.IsSubadmin == "1") {
                //Ext.getCmp('idButtonSubadminPanelProfile').setHidden(false);
                Ext.getCmp('idButtonSubadminPanelProfile').setHidden(true);
            } else {
                Ext.getCmp('idButtonSubadminPanelProfile').setHidden(true);
            }

            Ext.getCmp('idUserNameProfileScreen').setHtml(MentorLoginUser.MentorName);
            Ext.getCmp('userName').setValue(MentorLoginUser.MentorName);
            Ext.getCmp('email').setValue(MentorLoginUser.MentorEmail);
            Ext.getCmp('phone').setValue(MentorLoginUser.MentorPhone);
            //Ext.getCmp('password').setValue(MentorLoginUser.Password);
            Ext.getCmp('password').setValue('');
            Ext.getCmp('cnfpassword').setValue('');
            Skills = MentorLoginUser.Skills;
            if (MentorLoginUser.MentorImage != "") {
                me.down("button[action=takeProfilePhoto]").element.setStyle("backgroundImage", 'url("' + MentorLoginUser.MentorImage + '")');
                me.down("button[action=takeProfilePhoto]").element.setStyle("backgroundRepeat", 'no-repeat');
            }

            Ext.getCmp('addInterestProfile').setHtml('Skills Possessed');
            //Ext.getCmp('idIsPhoneSMSCapableProfileScreen').setChecked(MentorLoginUser.DisableSMS); // Default Unchecked
            //Ext.ComponentQuery.query('radiofield[name=idIsPhoneSMSCapableProfileScreen]')[0].setGroupValue(MentorLoginUser.DisableSMS)
            if (MentorLoginUser.DisableSMS == 1) {
                Ext.ComponentQuery.query('radiofield[name=idIsPhoneSMSCapableProfileScreen]')[0].setGroupValue(false); // Default Unchecked
            } else {
                Ext.ComponentQuery.query('radiofield[name=idIsPhoneSMSCapableProfileScreen]')[0].setGroupValue(true); // Default Unchecked
            }

            try {
                Ext.getCmp('idSourceProfileScreen').setValue(MentorLoginUser.MentorSourceID);
            } catch (e) {

            }

            //Avinash (Left Menu)
            var TeamDetail = Ext.decode(localStorage.getItem("TeamAllDetails"));
            try {

                Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("resources/images/group_avatar.png")');
                if (TeamDetail.OwnTeamDetail != false) {
                    Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("' + TeamDetail.OwnTeamDetail[0].TeamFullImage + '")');
                    Ext.getCmp('idTeamNameProfileScreen').setHtml(TeamDetail.OwnTeamDetail[0].TeamName);

                } else if (TeamDetail.MemberTeamDetail != false) {
                    Ext.ComponentQuery.query('#id_TeamPicture')[0].element.setStyle("backgroundImage", 'url("' + TeamDetail.MemberTeamDetail[0].TeamFullImage + '")');
                    Ext.getCmp('idTeamNameProfileScreen').setHtml(TeamDetail.MemberTeamDetail[0].TeamName);

                } else {
                    Ext.getCmp('idTeamNameProfileScreen').setHtml('No Team');
                }

            } catch (e) {
                console.log(e);
            }


        } else if (MenteeLoginUser != null) {
            Ext.getCmp('idUserNameProfileScreen').setHtml(MenteeLoginUser.MenteeName);
            Ext.getCmp('userName').setValue(MenteeLoginUser.MenteeName);
            Ext.getCmp('email').setValue(MenteeLoginUser.MenteeEmail);
            Ext.getCmp('phone').setValue(MenteeLoginUser.MenteePhone);
            //Ext.getCmp('password').setValue(MenteeLoginUser.Password);
            Ext.getCmp('password').setValue('');
            Ext.getCmp('cnfpassword').setValue('');

            Ext.getCmp('idLabelAddSkillProfile').setHtml("Add Interests");
            //Ext.getCmp('idButtonAddSkillProfile').setText("Add Interests");
            Ext.getCmp('addInterestProfile').setHtml('Skills Needed');

            Skills = MenteeLoginUser.Skills;
            if (MenteeLoginUser.MenteeImage != "") {
                me.down("button[action=takeProfilePhoto]").element.setStyle("backgroundImage", 'url("' + MenteeLoginUser.MenteeImage + '")');
                me.down("button[action=takeProfilePhoto]").element.setStyle("backgroundRepeat", 'no-repeat');
            }

            //Disaply check box for SMS in Mentee Profile only
            //Ext.getCmp('idIsPhoneSMSCapableProfileScreen').setChecked(MenteeLoginUser.DisableSMS); // Default Unchecked
            //Ext.ComponentQuery.query('radiofield[name=idIsPhoneSMSCapableProfileScreen]')[0].setGroupValue(MenteeLoginUser.DisableSMS)
            if (MenteeLoginUser.DisableSMS == 1) {
                Ext.ComponentQuery.query('radiofield[name=idIsPhoneSMSCapableProfileScreen]')[0].setGroupValue(false); // Default Unchecked
            } else {
                Ext.ComponentQuery.query('radiofield[name=idIsPhoneSMSCapableProfileScreen]')[0].setGroupValue(true); // Default Unchecked
            }

            try {
                Ext.getCmp('idSourceProfileScreen').setValue(MenteeLoginUser.MenteeSourceID);
            } catch (e) {

            }
        }

        for (i = 0; i < SkillsStore.data.length; i++) {
            var checked = false;
            var avtiveSkillCLS = 'profileSkillsInActiveCLS';

            var SkillsArray = Skills.split(',');
            for (j = 0; j < SkillsArray.length; j++) {
                var SkillsID = SkillsArray[j].split('~');
                if (SkillsID[0] == SkillsStore.data.getAt(i).data.SkillID)
                {
                    checked = true;
                    avtiveSkillCLS = 'profileSkillsActiveCLS';
                    break;
                }
            }

            var radioField = Ext.create('Ext.Button', {
                //name: 'recorded_stream',
                //labelCls : 'profileSkillsCLS',
                //labelWidth: 250,
                //checked : checked,
                //margin : 2,
                id: SkillsStore.data.getAt(i).data.SkillID,
                text: SkillsStore.data.getAt(i).data.SkillName,
                cls: avtiveSkillCLS,
                action: 'addInterest'
            });
            SkillsPanel.add(radioField);
        }
    },
    onPagePainted: function () {
        Mentor.app.application.getController('UpdateProfilePicture').getSourceList();
        Mentor.app.application.getController('UpdateProfilePicture').btnGetUpdatedProfile();
        Mentor.app.application.getController('UpdateProfilePicture').btnGetUpdatedTeamData();
        clearInterval(Mentor.Global.SET_INTERVAL_ID);
        Mentor.Global.SET_INTERVAL_ID = 0;
        /*Mentor.Global.SET_INTERVAL_ID = setInterval(function () {
         Mentor.app.application.getController('UpdateProfilePicture').btnGetUpdatedProfile(true);
         }, Mentor.Global.SET_INTERVAL_TIME);*/
    },
    onPageDeactivate: function () {
        var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        if (MentorLoginUser != null) {
            if (MentorLoginUser.MentorEmail == "") {
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', "Please update profile with your details to have full access to app", function () {

                    });
                }, 100);
                return;
            }
        } else if (MenteeLoginUser != null) {
            if (MenteeLoginUser.MenteeEmail == "") {
                Ext.Function.defer(function () {
                    Ext.Msg.alert('MELS', "Please update profile with your details to have full access to app", function () {

                    });
                }, 100);
                return;
            }
        }
        return false;
    }
});
