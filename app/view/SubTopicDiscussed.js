Ext.define('Mentor.view.SubTopicDiscussed', {
    extend: 'Ext.Panel',
    xtype: 'subtopicdiscusses',
    requires: [],
    config: {
        cls: 'meetingdetails',
        //scrollable: "vertical",
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'container'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Subtopics Discussed',
                cls: 'my-titlebar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        //iconCls: 'arrow_left',
                        iconCls: 'btnBackCLS',
                        action: 'btnBackSubtopicDiscussed',
                        docked: 'left'
                    },
//                    {
//                        iconCls: 'addIcon',
//                        //style: "visibility:hidden;"
//                        listeners: {
//                            element: 'element',
//                            tap: function () {
//                                var popup = Mentor.app.application.getController('AppDetail').addNotePopup();
//                                Ext.Viewport.add(popup);
//                                popup.show();
//                            }
//                        }
//                    },
                    {
                        iconCls: 'goToNotesIcon',
                        //docked: 'left',
                        //style: 'visibility:hidden'
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('subtopic_discussed');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'label',
                id: 'idUserNameEnterpreneur',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                hidden: true
            },
            {
                xtype: 'label',
                html: Mentor.Global.VERSION_NAME,
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true
            },
            {
                xtype: 'panel',
                margin: '20 0 0 0',
                layout: {
                    type: 'vbox'
                },
                items: [
//                    {
//                        xtype: 'label',
//                        html: 'Subtopics Discussed',
//                        style: 'margin-left:10px;margin-right:10px;border: 0px solid #eee;border-left-width: 0;border-bottom: 1px solid #66cc66;font-weight: bold;'
//                    },
                    {
                        xtype: 'panel',
                        id: 'idSubtopicDiscussed'
                    }
                ]
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'button',
                        text: 'OK',
                        ressedCls : 'press-btn-cls',
                        cls: 'signin-btn-cls',
                        action: "btnNextSubtopicDiscussed",
                        flex: 1
                    },
                    {
                        //id: 'idBtnUpdateMeetingDetails',
                        xtype: 'button',
                        text: 'Back',
                        pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        flex: 1,
                        hidden: false,
                        action: 'btnBackSubtopicDiscussed' //UpdateMeetingDetail Controller
                    }
                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "deactivate",
                fn: "onPageDeactivate"
            }
        ]
    },
    onPageDeactivate: function () {
        var SubTopicID;
        var SubTopicDescription;
        var isChecked = 0;
        var data = null;
        var MentorSubtopics = Ext.getCmp('idSubtopicDiscussed');
        for (i = 0; i < MentorSubtopics.getItems().length; i++) {
            if (MentorSubtopics.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    SubTopicID = MentorSubtopics.getItems().getAt(i).getValue();
                    SubTopicDescription = MentorSubtopics.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                } else {
                    SubTopicID = SubTopicID + "," + MentorSubtopics.getItems().getAt(i).getValue();
                    SubTopicDescription = SubTopicDescription + "," + MentorSubtopics.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                }
            }
        }
        if (isChecked != 0) {
            data = {
                SubTopicID: SubTopicID,
                SubTopicDescription: SubTopicDescription
            };
            data = Ext.encode(data);
        }
        localStorage.setItem('CacheMentorSubtopicDiscussed', data);
    },
    onPageActivate: function () {
        var CacheMentorSubtopicDiscussed = Ext.decode(localStorage.getItem('CacheMentorSubtopicDiscussed'));
        localStorage.setItem('CacheMentorSubtopicDiscussed', null);

        if (CacheMentorSubtopicDiscussed != null) {
            if (CacheMentorSubtopicDiscussed.SubTopicDescription != null) {
                SelectedSubtopics = CacheMentorSubtopicDiscussed.SubTopicDescription.split(",");
            }
        } else {
            var SelectedSubtopics = Ext.getCmp('mainIssueDiscussedMentorReviewForm').getValue();
            if (SelectedSubtopics != "") {
                SelectedSubtopics = SelectedSubtopics.split(",\n");
            }
        }
        var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History

        var MeetingDetailSubTopicStore = Ext.getStore('SubTopic').load();
        var MeetingDetailPanel = Ext.getCmp('idSubtopicDiscussed');
        MeetingDetailPanel.removeAll();

        var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        Ext.getCmp('idUserNameEnterpreneur').setHtml(MentorLoginDetail.MentorName);

        // If Subtopics available or not
        if (MeetingDetailSubTopicStore.data.length <= 0)
        {
            MeetingDetailPanel.setHtml("No Subtopics.");
            MeetingDetailPanel.setStyle('margin:10px;');
        } else {
            MeetingDetailPanel.setHtml("");
            MeetingDetailPanel.setStyle('margin:0px;');
        }

        for (i = 0; i < MeetingDetailSubTopicStore.data.length; i++) {
            var checked = false;
            if (SelectedSubtopics.indexOf(MeetingDetailSubTopicStore.data.getAt(i).data.SubTopicDescription) != -1) {
                checked = true;
            }

            var checkBoxField = Ext.create('Ext.field.Checkbox', {
                name: 'recorded_stream',
                value: MeetingDetailSubTopicStore.data.getAt(i).data.SubTopicID,
                label: MeetingDetailSubTopicStore.data.getAt(i).data.SubTopicDescription,
                itemId: MeetingDetailSubTopicStore.data.getAt(i).data.SubTopicDescription,
                labelCls: 'radio-btn-cls',
                cls : 'checkboxitem-cls',
                labelWidth: '90%',
                checked: checked,
                labelAlign: 'right'
            });
            MeetingDetailPanel.add(checkBoxField);
        }
    }
});