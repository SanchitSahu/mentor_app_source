Ext.define('Mentor.view.TeamListing', {
    extend: 'Ext.dataview.List',
    xtype: 'teamlisting',
    itemId: 'itemIdTeamListing',
    requires: [],
    config: {
        title: 'Teams',
        cls: 'meetinghistory',
        store: 'TeamListing',
        disableSelection: true,
        id:'idTeamListing',
        itemTpl: '<div class="myContent Invitation"  style = "background: #ffffff;padding: 10px;display: flex;min-height:70px;box-shadow: 1px 1px 4px 0.2px #888888;margin-left:10px;margin-right:10px;">' +
                    '<div style="width:20%; float:left; ">' +
                        '<img src={TeamFullImage} style="border-radius: 50%;width: 65px;height: 65px;">' +
                    '</div>' +
                    '<div style="width:80%;float:left;padding: 0px 0px !important;vertical-align: bottom;min-height: 16px;display: table;height: 65px;margin-left:10px;">' +
                        '<div style = "vertical-align: middle;float: none; display: table-cell;">'+
                            '<div style="width:100%">'+
                                '<div style="width:50%; float:left;font-size: 14px;color: black;"><b>{TeamName}</b>'+
                                '</div>' +
                                 '<div style=" float:right;">' +
                                '</div>'+
                            '</div>' +
                            '<div style="width:100%;float:left;">'+
                                '<span style="font-size:13px;color: black;">{TeamDescription}'+
                                '</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>',
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            },
            {
                event: 'itemtap',
                fn: function (view, index, item, record, e) {
                    Mentor.app.application.getController('AppDetail').displayTeamDetail(view, index, item, record, e);
                }
            }
        ],
        items: [
            {
                //title: 'Meeting History',
                docked: 'top',
                xtype: 'panel',
                cls: 'mettingHistoryToolBar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        xtype: 'toolbar',
                        docked: 'top',
                        cls: 'my-titlebar',
                        title: 'Teams',
                        defaults: {
                            iconMask: true
                        },
                        items: [
                            {
                                iconCls: 'btnBackCLS',
                                action: 'btnBackActionTeamListing',
                                docked: 'left'
                            },
                            {
                                iconCls: 'goToNotesIcon',
                                docked: 'left',
                                id: 'addnotes',
                                action: 'addnotes'
                            },
                            {
                                iconCls: 'helpIcon',
                                docked: 'right',
                                listeners: {
                                    element: 'element',
                                    tap: function () {
                                        var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('team_listing');
                                        Ext.Viewport.add(popup);
                                        popup.show();
                                    }
                                }
                            },
                            {
                                iconCls: 'refreshIcon',
                                action: 'btnRefreshTeamListing',
                                docked: 'right'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        cls: 'searchFieldCls',
                        items: [
                            {
                                xtype: 'searchfield',
                                placeHolder: 'Search team',
                                itemId: 'searchBoxMentorTeams',
                                cls : 'searchNotesCLS',
                                inputCls : 'searchNotesInputCLS'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    onPageActivate: function () {
        Mentor.Global.TabNavigationStack = 0;
    },
    onPagePainted: function () {
        //Mentor.app.application.getController('AppDetail').getMeetingHistory();
        clearInterval(Mentor.Global.SET_INTERVAL_ID);
        Mentor.Global.SET_INTERVAL_ID = 0;
        Mentor.Global.SET_INTERVAL_ID = setInterval(function () {
            //Mentor.app.application.getController('AppDetail').getMeetingHistory();
        }, Mentor.Global.SET_INTERVAL_TIME);
    }
});
