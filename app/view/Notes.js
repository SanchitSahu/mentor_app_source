Ext.define('Mentor.view.Notes', {
    extend: 'Ext.dataview.List',
    xtype: 'mentor_notes',
    id: 'idMentorNotes',
    requires: [],
    config: {
        title: 'Notes',
        cls: 'meetinghistory',
        store: 'UserNotes',
        disableSelection: true,
        style: {
            'border': '0'
        },
        id: 'noteListing',
        itemTpl: new Ext.XTemplate(
                '<div style ="box-shadow: 1px 1px 4px 0.2px #888888;margin-left:10px;margin-right:10px;">' +
                '<div class="myContent noteList" style="/*display:flex;*/min-height:65px;padding: 10px;">' +
                '<div style="width:100%; float:left; padding: 0px 0px !important; vertical-align:bottom; min-height:16px;">' +
                //'<div style="width:100%">' +
                '<div style="width:55%;float:left;font-size: 14px;color: black;line-height: 26px;white-space: nowrap;">' +
                //'<b>{[this.shortenString(values.NoteTitle, 31)]}</b>' +
                '<b>{NoteTitle}</b>' +
                '</div>' +
                '<div style="width: 45%;float:right;text-align: right;">' +
                '<span style="vertical-align: baseline;"><img src="./resources/images/clock_notelist.png"  height="13px" width="13px" /></span>' +
                '<span style = "margin-left:3px;/*vertical-align: text-top;*/font-size: 12px;color: black;">{UpdatedDate}</span>' +
                '</div>' +
                //'</div>' +
                '</div>' +
                //Section to show the Note Description
                //'<div style="width:100%;float:left;">' +
                //'<span style="font-size:11px;color: black;">{[this.shortenString(values.NoteDescription, 135)]}</span>' +
                //'<div>' +
                '<tpl if="NoteUserType != \'1\'">' +
                '<img src="./resources/images/share_{Access}.png" height="20px" width="20px" style="float:left" />' +
                '</tpl>' +
                //'<img src="./resources/images/share_{Access}.png" height="20px" width="20px" style="float:left" />' +
                //'<img src="./resources/images/share.png" class="share" height="20px" width="20px" style="float:right" />' +
                //'<img src="./resources/images/delete.png" class="delete" height="20px" width="20px" style="float:right;margin-right:10px;" />' +
                //'<img src="./resources/images/edit_note.png" class="edit" height="20px" width="20px" style="float:right;margin-right:10px;" />' +
                '</div>' +
                //'</div>' +
                '<div style="clear: both;"></div>' +
                '</div>',
                {
                    shortenString: function (name, length) {
                        return name;
                        name = name.replace(/\\/g, "");
                        return Ext.String.ellipsis(name, length, false);
                    }
                }
        ),
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            },
            {
                event: 'itemtap',
                //fn: function (view, index, item, e) {
                fn: function (view, index, item, record, e) {
                    if (Ext.dom.Element.fly(e.target).hasCls('edit')) {
                        var editNotePopup = Mentor.app.application.getController('AppDetail').editNotePopup(view, index);
                        Ext.Viewport.add(editNotePopup);
                        editNotePopup.show();
                    } else if (Ext.dom.Element.fly(e.target).hasCls('share')) {
                        //Mentor.app.application.getController('AppDetail').displayDetail(view, index, item, e);
                    } else if (Ext.dom.Element.fly(e.target).hasCls('delete')) {
                        Ext.Msg.confirm("MELS", "Are you sure you want to delete this note?", function (btn) {
                            if (btn == 'yes') {
                                Mentor.app.application.getController('AppDetail').deleteNotePopup(view, index);
                            }
                        });
                    } else {
                        var viewNotePopup = Mentor.app.application.getController('AppDetail').viewNotePopup(view, index);
                        Ext.Viewport.add(viewNotePopup);
                        viewNotePopup.show();
                    }
                    return;
                }
            }
        ],
        items: [
            {
                docked: 'top',
                xtype: 'panel',
                cls: 'mettingHistoryToolBar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        xtype: 'toolbar',
                        docked: 'top',
                        cls: 'my-titlebar',
                        title: 'Notes List',
                        defaults: {
                            iconMask: true
                        },
                        items: [
                            {
                                iconCls: 'btnBackCLS',
                                action: 'btnBackMentorNotes',
                                docked: 'left'
                            },
//                            {
//                                iconCls: 'addIcon',
//                                docked: 'left',
//                                listeners: {
//                                    element: 'element',
//                                    tap: function () {
//                                        var popup = Mentor.app.application.getController('AppDetail').addNotePopup();
//                                        Ext.Viewport.add(popup);
//                                        popup.show();
//                                    }
//                                }
//                            },
                            {
                                iconCls: 'helpIcon',
                                docked: 'right',
                                listeners: {
                                    element: 'element',
                                    tap: function () {
                                        var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('notes_list');
                                        Ext.Viewport.add(popup);
                                        popup.show();
                                    }
                                }
                            },
                            {
                                iconCls: 'refreshIcon',
                                action: 'btnRefreshUserNotes',
                                docked: 'right'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        cls: 'searchFieldCls',
                        margin: '0 20 10 20',
                        items: [
                            {
                                xtype: 'searchfield',
                                placeHolder: 'Search notes',
                                itemId: 'searchBoxMentorNotes',
                                cls: 'searchNotesCLS',
                                inputCls: 'searchNotesInputCLS'
                            },
                            {
                                xtype: 'button',
                                //html: 'Add',
                                cls: 'noteListAddButton',
                                docked: 'right',
                                iconCls: 'noteListAddButtonIcon',
                                margin: '0 0 0 7',
                                listeners: {
                                    element: 'element',
                                    tap: function () {
                                        var popup = Mentor.app.application.getController('AppDetail').addNotePopup('add');
                                        Ext.Viewport.add(popup);
                                        popup.show();
                                    }
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    },
    onPageActivate: function () {
    },
    onPagePainted: function () {
        Mentor.app.application.getController('Main').getUserNotes();
        clearInterval(Mentor.Global.SET_INTERVAL_ID);
        Mentor.Global.SET_INTERVAL_ID = 0;
        Mentor.Global.SET_INTERVAL_ID = setInterval(function () {
            //Mentor.app.application.getController('Main').getUserNotes()
        }, Mentor.Global.SET_INTERVAL_TIME);
    }
});