Ext.define('Mentor.view.MenteeReviewForm', {
    extend: 'Ext.form.Panel',
    xtype: 'menteereviewform',
    id: 'idMenteeReviewFormScreen',
    requires: [],
    config: {
        cls: 'menteereviewform',
        //scrollable: "vertical",
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'formpanel'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Meeting Edit Form',
                cls: 'my-titlebar-review_form',
                id: 'idMenteeReviewFormTitle',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'btnBackCLS',
                        docked: 'left',
                        action: 'btnBackMenteeReviewForm'
                    },
//                    {
//                        iconCls: 'addIcon',
//                        //style: "visibility:hidden;"
//                        listeners: {
//                            element: 'element',
//                            tap: function () {
//                                var popup = Mentor.app.application.getController('AppDetail').addNotePopup();
//                                Ext.Viewport.add(popup);
//                                popup.show();
//                            }
//                        }
//                    },
                    {
                        //docked: 'left',
                        //style: 'visibility:hidden'
                        iconCls: 'goToNotesIcon',
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('review_form');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'label',
                id: 'idUserNameEnterpreneur',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                hidden: true
            },
            {
                xtype: 'label',
                html: Mentor.Global.VERSION_NAME,
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true
            },
            {
                xtype: 'panel',
                margin: '10 20',
                layout: {
                    type: 'vbox'
                },
                items: [
                    {
                        xtype: 'panel',
                        margin: '10 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Meeting With Whom',
                                style: 'font-weight: normal;color:#161718',
                                cls: 'appLabelCls'
                            },
                            {
                                /*action : 'btnAddMeetingDetail',
                                 disabled :false,
                                 listeners : {
                                 element : 'element',
                                 tap : function() {
                                 Mentor.app.application.getController('AppDetail').changeMenteeDetail();
                                 }
                                 }*/
                                xtype: 'textfield',
                                name: 'meetingWithWhom',
                                id: 'meetingWithWhomMenteeReviewForm',
                                disabled: true,
                                disabledCls: 'textFieldDisableCLS',
                                inputCls: 'inputDisabledCLS'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        id: 'menteeMeetingTypePanel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [

                            {
                                xtype: 'label',
                                html: 'Meeting Type',
                                style: 'font-weight: normal;color:#161718',
                                cls: 'appLabelCls'
                            },
                            {
                                /*disabled :false,
                                 listeners : {
                                 element : 'element',
                                 tap : function() {
                                 Mentor.app.application.getController('AppDetail').changeMenteeDetail();
                                 }
                                 }*/
                                xtype: 'textfield',
                                name: 'meetingType',
                                id: 'meetingTypeMenteeReviewForm',
                                disabled: true,
                                disabledCls: 'textFieldDisableCLS',
                                inputCls: 'inputDisabledCLS'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        id: 'menteeMeetingPlacePanel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Meeting Place',
                                /*cls: 'reviewFormLabelCLS',
                                 flex: 2*/
                                style: 'font-weight: normal;color:#161718',
                                cls: 'appLabelCls'
                            },
                            {
                                /*disabled :false,
                                 listeners : {
                                 element : 'element',
                                 tap : function() {
                                 Mentor.app.application.getController('AppDetail').changeMenteeDetail();
                                 }
                                 }*/
                                xtype: 'textfield',
                                name: 'meetingType',
                                id: 'meetingPlaceMenteeReviewForm',
                                disabled: true,
                                disabledCls: 'textFieldDisableCLS',
                                inputCls: 'inputDisabledCLS'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Meeting Main Topic',
                                /* cls: 'reviewFormLabelCLS',
                                 flex: 2*/
                                style: 'font-weight: normal;color:#161718',
                                cls: 'appLabelCls'
                            },
                            {
                                /*disabled :false,
                                 listeners : {
                                 element : 'element',
                                 tap : function() {
                                 Mentor.app.application.getController('AppDetail').changeTimingDetail();
                                 }
                                 }*/
                                xtype: 'textfield',
                                name: 'meetingSubject',
                                id: 'meetingSubjectMenteeReviewForm',
                                disabled: true,
                                disabledCls: 'textFieldDisableCLS',
                                inputCls: 'inputDisabledCLS'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        id: 'menteeMeetingSubTopicPanel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Subtopics Discussed',
                                /* cls: 'reviewFormLabelCLS',
                                 flex: 2*/
                                style: 'font-weight: normal;color:#161718',
                                cls: 'appLabelCls'
                            },
                            {
                                xtype: 'textareafield',
                                name: 'mainIssueDiscussed',
                                id: 'mainIssueDiscussedMenteeReviewForm',
                                disabled: true,
                                disabledCls: 'textFieldDisableCLS',
                                inputCls: 'inputDisabledCLS',
                                maxRows: 2
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        layout: {
                            type: 'hbox'
                        },
                        margin: '20 0 0 0 ',
                        items: [
                            {
                                xtype: 'panel',
                                id: 'menteeMeetingStartTimePanel',
                                margin: '0 5 0 0 ',
                                layout: {
                                    type: 'vbox'
                                },
                                flex: 1,
                                items: [
                                    {
                                        xtype: 'label',
                                        html: 'Meeting Start Time',
                                        /*cls: 'reviewFormLabelCLS',
                                         flex: 2*/
                                        style: 'font-weight: normal;color:#161718',
                                        cls: 'appLabelCls'
                                    },
                                    {
                                        /*disabled :false,
                                         listeners : {
                                         element : 'element',
                                         tap : function() {
                                         Mentor.app.application.getController('AppDetail').changeTimingDetail();
                                         }
                                         }*/
                                        xtype: 'textfield',
                                        name: 'meetingStartTime',
                                        id: 'meetingStartTimeMenteeReviewForm',
                                        disabled: true,
                                        disabledCls: 'textFieldDisableCLS',
                                        inputCls: 'inputDisabledCLS',
                                        style: 'font-size: 17px;'
                                    }
                                ]
                            },
                            {
                                xtype: 'panel',
                                id: 'menteeMeetingEndTimePanel',
                                margin: '0 0 0 5',
                                layout: {
                                    type: 'vbox'
                                },
                                flex: 1,
                                items: [
                                    {
                                        xtype: 'label',
                                        html: 'Meeting End Time',
                                        /* cls: 'reviewFormLabelCLS',
                                         flex: 2*/
                                        style: 'font-weight: normal;color:#161718',
                                        cls: 'appLabelCls'
                                    },
                                    {
                                        /*disabled :false,
                                         listeners : {
                                         element : 'element',
                                         tap : function() {
                                         Mentor.app.application.getController('AppDetail').changeTimingDetail();
                                         }
                                         }*/
                                        xtype: 'textfield',
                                        name: 'meetingEndTime',
                                        id: 'meetingEndTimeMenteeReviewForm',
                                        disabled: true,
                                        disabledCls: 'textFieldDisableCLS',
                                        inputCls: 'inputDisabledCLS',
                                        style: 'font-size: 17px;'
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        id: 'menteeMeetingElapsedPanel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Elapsed Time (Minutes)',
                                /*cls: 'reviewFormLabelCLS',
                                 flex: 2*/
                                style: 'font-weight: normal;color:#161718',
                                cls: 'appLabelCls'
                            },
                            {
                                /*clearIcon  : false,
                                 listeners : {
                                 element : 'element',
                                 tap : function() {
                                 Mentor.app.application.getController('AppDetail').changeTimingDetail();
                                 }
                                 }*/
                                xtype: 'textfield',
                                name: 'meetingElapsedTime',
                                id: 'meetingElapsedTimeMenteeReviewForm',
                                disabled: true,
                                cls: 'textFieldDisableCLS',
                                inputCls: 'inputDisabledCLS',
                                disabledCls: 'textFieldDisableCLS'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        id: 'menteeMeetingMentorActionItemPanel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'ToDo Item By Mentor',
                                /*cls: 'reviewFormLabelCLS',
                                 flex: 2,*/
                                style: 'font-weight: normal;color:#161718',
                                cls: 'appLabelCls',
                                id: 'idLabelActionItemByMentor'
                            },
                            {
                                /*disabled :false,
                                 listeners : {
                                 element : 'element',
                                 tap : function() {
                                 Mentor.app.application.getController('AppDetail').changeMentorActionItemsDetail();
                                 }
                                 }*/
                                xtype: 'textareafield',
                                name: 'actionItemsByMentor',
                                id: 'actionItemsByMentorMenteeReviewForm',
                                disabled: true,
                                disabledCls: 'textFieldDisableCLS',
                                inputCls: 'inputDisabledCLS',
                                maxRows: 2
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        id: 'menteeMeetingMenteeActionItemPanel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'ToDo Items By Mentee',
                                /* cls: 'reviewFormLabelCLS',
                                 flex: 2,*/
                                style: 'font-weight: normal;color:#161718',
                                cls: 'appLabelCls',
                                id: 'idLabelActionItemByMentee'
                            },
                            {
                                //cls : 'textFieldDisableCLS',
                                //maxRows: 2,
                                //disabled :false,
                                xtype: 'textareafield',
                                name: 'actionItemsByMentee',
                                id: 'actionItemsByMenteeMenteeReviewForm',
                                disabled: false,
                                cls: 'textFieldDisableCLS',
                                clearIcon: false,
                                maxRows: 2,
                                inputCls: 'inputActiveDisableCLS',
                                //flex: 3,
                                listeners: {
                                    element: 'element',
                                    tap: function () {
                                        Mentor.app.application.getController('AppDetail').changeMenteeActionItemsDetail();
                                    }
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Mentor Comment',
                                /*cls: 'reviewFormLabelCLS',
                                 flex: 2,*/
                                style: 'font-weight: normal;color:#161718',
                                cls: 'appLabelCls',
                                id: 'idLabelMentorComment'
                            },
                            {
                                xtype: 'textareafield',
                                name: 'mentorComment',
                                id: 'mentorCommentMentorReviewForm',
                                cls: 'textFieldDisableCLS',
                                maxRows: 2,
                                disabled: true,
                                clearIcon: false
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        margin: '20 0 0 0 ',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'Mentee Comment',
                                /*cls: 'reviewFormLabelCLS',
                                 flex: 2,*/
                                style: 'font-weight: normal;color:#161718',
                                cls: 'appLabelCls',
                                id: 'idLabelMenteeComment'
                            },
                            {
                                xtype: 'textareafield',
                                name: 'mentorComment',
                                id: 'menteeCommentMenteeReviewForm',
                                cls: 'textFieldDisableCLS',
                                maxRows: 2,
                                disabled: false,
                                inputCls: 'inputCLS',
                                clearIcon: false
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        id: 'menteeMeetingRatingPanel',
                        margin: '10 0 0 0 ',
                        style: 'background-color: #f2f2f2;',
                        items: [
                            {
                                xtype: 'label',
                                html: 'Rate your satisfaction with the meeting',
                                margin: '20 0 0 0',
                                cls: 'reviewFormLabelCLS',
                                style: 'text-align: -webkit-center;'
                            },
                            {
                                xtype: 'label',
                                id: 'satisfactionRatingText',
                                style: 'text-align: -webkit-center;font-weight: bold;font-size:14px;color: #808080;'
                            },
                            {
                                xtype: 'label',
                                html: 'From Very Unsatisfied to Very Satisfied',
                                style: 'text-align: -webkit-center;font-weight: bold;font-size:14px;color: #808080;'
                            },
                            {
                                xtype: 'label',
                                html: '{1-5}  Clear and understandable feedback and advice.',
                                margin: '10 0 0 0',
                                style: 'text-align: -webkit-center;font-weight: bold;font-size:14px;color: #808080;'
                            },
                            {
                                xtype: 'ratingfield',
                                name: 'usefull',
                                label: ' ',
                                labelWidth: '0%',
                                value: 0,
                                id: 'idSatisfactionClearAndUnderstandableMenteeReviewForm',
                                labelCls: 'background:red',
                                style: 'background-color: #f2f2f2;'
                            },
                            {
                                xtype: 'label',
                                html: '{1-5} Applicable to overall subject area.',
                                margin: '10 0 0 0',
                                style: 'text-align: -webkit-center;font-weight: bold;font-size:14px;color: #808080;'
                            },
                            {
                                xtype: 'ratingfield',
                                name: 'usefull',
                                label: ' ',
                                labelWidth: '0%',
                                value: 0,
                                id: 'idSatisfactionOverallSubjectMenteeReviewForm',
                                labelCls: 'background:red',
                                style: 'background-color: #f2f2f2;'
                            },
                            {
                                xtype: 'label',
                                html: '{1-5} Relevant to my particular problem.',
                                margin: '10 0 0 0',
                                style: 'text-align: -webkit-center;font-weight: bold;font-size:14px;color: #808080;'
                            },
                            {
                                xtype: 'ratingfield',
                                name: 'usefull',
                                label: ' ',
                                labelWidth: '0%',
                                value: 0,
                                id: 'idSatisfactionReleveantProblemMenteeReviewForm',
                                labelCls: 'background:red',
                                style: 'background-color: #f2f2f2;'
                            },
                            {
                                xtype: 'label',
                                html: '{1-5} Executable/Actionable practical advice and feedback.',
                                margin: '10 0 0 0',
                                style: 'text-align: -webkit-center;font-weight: bold;font-size:14px;color: #808080;'
                            },
                            {
                                xtype: 'ratingfield',
                                name: 'usefull',
                                label: ' ',
                                labelWidth: '0%',
                                value: 0,
                                id: 'idSatisfactionAdviceAndFeedbackMenteeReviewForm',
                                labelCls: 'background:red',
                                style: 'background-color: #f2f2f2;'
                            }
                        ]
                    }
                ]
            },
//            {
//                xtype: 'button',
//                text: 'Submit',
//                id: 'idBtnSubmitMenteeReviewForm',
//                cls: 'signin-btn-cls',
//                action: "doSubmitMenteeReviewForm"
//            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                margin: '20 10',
                items: [
                    {
                        xtype: 'button',
                        text: 'UPDATE',
                        cls: 'signin-btn-cls',
                        pressedCls: 'press-btn-cls',
                        id: 'idBtnSubmitMenteeReviewForm',
                        action: "doSubmitMenteeReviewForm",
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        text: 'CANCEL',
                        pressedCls: 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        action: 'btnBackMenteeReviewForm',
                        id: 'idBtnBackMenteeReviewForm',
                        flex: 1
                    }
                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            }
        ]
    },
    onPageActivate: function () {
        localStorage.setItem('CacheEditMenteeActionItems', null);
        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        Ext.getCmp('idLabelActionItemByMentor').setHtml('ToDo Item By ' + Mentor.Global.MENTOR_NAME);
        Ext.getCmp('idLabelActionItemByMentee').setHtml('ToDo Items By ' + Mentor.Global.MENTEE_NAME);
        Ext.getCmp('idLabelMenteeComment').setHtml(Mentor.Global.MENTEE_NAME + ' Post-Meeting Comments');
        Ext.getCmp('idLabelMentorComment').setHtml(Mentor.Global.MENTOR_NAME + ' Comment');
    },
    onPagePainted: function () {
        clearInterval(Mentor.Global.SET_INTERVAL_ID);
        Mentor.Global.SET_INTERVAL_ID = 0;
        Ext.getCmp('satisfactionRatingText').setHtml('(This rating will not be seen by the ' + Mentor.Global.MEETING_DETAIL.MentorName + ')');
    }
});
