Ext.define('Mentor.view.MeetingEditMentorActionItems', {
    extend: 'Ext.Panel',
    xtype: 'meetingedit_mentor_action_items',
    requires: [],
    config: {
        cls: 'mentor_action_items',
        //scrollable: "vertical",
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'container'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Mentor ToDo Items',
                id: 'mentor_action_items_title',
                cls: 'my-titlebar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'btnBackCLS',
                        docked: 'left',
                        action: 'btnBackMeetingEditMentorActionItems'

                    },
                    {
                        iconCls: 'goToNotesIcon',
                        //docked: 'left',
                        //style: 'visibility:hidden'
                        id: 'addnotes',
                        action: 'addnotes'
                    },
//                    {
//                        iconCls: 'addIcon',
//                        listeners: {
//                            element: 'element',
//                            tap: function () {
//                                var popup = Mentor.app.application.getController('AppDetail').addNotePopup();
//                                Ext.Viewport.add(popup);
//                                popup.show();
//                            }
//                        }
//                    },
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('mentor_action');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'label',
                id: 'idUserNameEnterpreneur',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                hidden: true
            },
            {
                xtype: 'label',
                html: Mentor.Global.VERSION_NAME,
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true
            },
            {
                xtype: 'panel',
                margin: '20 0 0 0',
                layout: {
                    type: 'vbox'
                },
                items: [
                    {
                        xtype: 'label',
                        html: 'Mentor ToDo Items',
                        style: 'margin-left:10px;margin-right:10px;border: 0px solid #eee;border-left-width: 0;border-bottom: 1px solid #66cc66;font-weight: bold;',
                        id: 'idLabelMentorActionItems',
                        hidden: true
                    },
                    {
                        xtype: 'panel',
                        id: 'idMenteeActionItemHeader',
                        //margin: '10 0 0 0',
                        layout: {
                            type: 'hbox'
                        },
                         style : 'border-bottom: 1px solid #bfbfbf;padding-bottom: 5px;',
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin-left: 30px; font-size: smaller;'
                            },
                            {
                                xtype: 'label',
                                html: 'ToDo',
                                style: 'margin-left: 9px; font-size: smaller;'
                            },
                            {
                                xtype: 'label',
                                html: 'Done',
                                style: 'margin-left: 17px; font-size: smaller;'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        margin: '10 0 0 0',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'panel',
                                margin: '0 0 0 10',
                                id: 'idMentorActionItemComments'
                            },
                            {
                                xtype: 'panel',
                                id: 'idMentorActionItem',
                                margin: '0 0 0 10'
                            },
                            {
                                xtype: 'panel',
                                margin: '0 0 0 10',
                                id: 'idMentorActionItemDoneOnMeetingUpdate'
                            }
                        ]
                    },
                    {
                        xtype: 'label',
                        style: 'font-weight: bold;',
                        margin: '10 20 0 20 ',
                        html: 'Comment'
                    },
                    {
                        xtype: 'textareafield',
                        name: 'mentorComment',
                        id: 'mentorCommentMentorActionItems',
                        cls: 'textFieldDisableCLS',
                        maxRows: 2,
                        disabled: false,
                        inputCls: 'inputCLS',
                        clearIcon: false,
                        margin: '0 20 0 20 '
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox'
                        },
                        margin: '0 10 0 10 ',
                        items: [
                            {
                                xtype: 'button',
                                text: 'OK',
                               pressedCls : 'press-btn-cls',
                                cls: 'signin-btn-cls',
                                action: "btnNextMeetingEditMentorActionItems",
                                flex: 1
                            },
                            {
                                xtype: 'button',
                                text: 'BACK',
                                pressedCls : 'press-grey_btn-cls',
                                cls: 'grey-btn-cls',
                                flex: 1,
                                hidden: false,
                                action: 'btnBackMeetingEditMentorActionItems'
                            }
                        ]
                    }
                    //{
                    //    xtype: 'button',
                    //    text: 'SUBMIT',
                    //    cls: 'signin-btn-cls',
                    //    id: 'btnSubmitMentorActionItems',
                    //    action: "btnNextMentorActionItems"
                    //},
                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            },
            {
                event: "deactivate",
                fn: "onPageDeactivate"
            }
        ]
    },
    onPageDeactivate: function () {
        var MentorActionItemID;
        var MentorActionItemName;
        var MentorActionItemIDDone;
        var MentorActionItemNameDone;
        var isChecked = 0;
        var isCheckedDone = 0;
        var data = null;
        var MentorActionItems = Ext.getCmp('idMentorActionItem');

        for (i = 0; i < MentorActionItems.getItems().length; i++) {
            if (MentorActionItems.getItems().getAt(i).isChecked()) {
                if (isChecked == 0) {
                    MentorActionItemID = MentorActionItems.getItems().getAt(i).getValue();
                    MentorActionItemName = MentorActionItems.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                } else {
                    MentorActionItemID = MentorActionItemID + "," + MentorActionItems.getItems().getAt(i).getValue();
                    MentorActionItemName = MentorActionItemName + "," + MentorActionItems.getItems().getAt(i).getItemId();
                    isChecked = isChecked + 1;
                }
            }
        }

        var MentorActionItemsDone = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
        for (i = 0; i < MentorActionItemsDone.getItems().length; i++) {
            if (MentorActionItemsDone.getItems().getAt(i).isChecked()) {
                if (isCheckedDone == 0) {
                    MentorActionItemIDDone = MentorActionItemsDone.getItems().getAt(i).getValue();
                    MentorActionItemNameDone = MentorActionItemsDone.getItems().getAt(i).getItemId();
                    isCheckedDone = isCheckedDone + 1;
                } else {
                    MentorActionItemIDDone = MentorActionItemIDDone + "," + MentorActionItemsDone.getItems().getAt(i).getValue();
                    MentorActionItemNameDone = MentorActionItemNameDone + "," + MentorActionItemsDone.getItems().getAt(i).getItemId();
                    isCheckedDone = isCheckedDone + 1;
                }
            }
        }
        if (isChecked != 0) {
            data = {
                MentorActionItemID: MentorActionItemID,
                MentorActionItemName: MentorActionItemName,
                MentorActionItemIDDone: MentorActionItemIDDone,
                MentorActionItemNameDone: MentorActionItemNameDone
            };
            data = Ext.encode(data);
        }
        localStorage.setItem('CacheEditMentorActionItems', data);
    },
    onPageActivate: function () {
        var CacheEditMentorActionItems = Ext.decode(localStorage.getItem('CacheEditMentorActionItems'));
        localStorage.setItem('CacheEditMentorActionItems', null);

        var MentorActionCommentsStore = Ext.getStore('MentorActionComments').load();

        var MentorActionItemStore = Ext.getStore('MentorAction').load();
        var MentorActionPanel = Ext.getCmp('idMentorActionItem');
        MentorActionPanel.removeAll();

        var MentorActionPanelDoneOnNewMeeting = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
        MentorActionPanelDoneOnNewMeeting.removeAll();

        var MentorActionItemComments = Ext.getCmp('idMentorActionItemComments');
        MentorActionItemComments.removeAll();

        var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        Ext.getCmp('idUserNameEnterpreneur').setHtml(MentorLoginDetail.MentorName);

        for (i = 0; i < MentorActionItemStore.data.length; i++) {
            var checked = false;
            if (CacheEditMentorActionItems == null) {
                var SelectedActionItems = Ext.decode(localStorage.getItem("storeMentorActionItems"));
            } else {
                var SelectedActionItems = CacheEditMentorActionItems;
            }
            if (SelectedActionItems != "" && SelectedActionItems.MentorActionItemName != null && SelectedActionItems.MentorActionItemName != '') {
                SelectedActionItems = SelectedActionItems.MentorActionItemName.split(",");
                if (SelectedActionItems.indexOf(MentorActionItemStore.data.getAt(i).data.MentorActionName) != -1) {
                    checked = true;
                }
            }

            var checkBoxField = Ext.create('Ext.field.Checkbox', {
                name: 'recorded_stream',
                value: MentorActionItemStore.data.getAt(i).data.MentorActionID,
                itemId: MentorActionItemStore.data.getAt(i).data.MentorActionName,
                labelCls: 'radio-btn-cls',
                checked: checked,
                labelWidth: '90%',
                labelAlign: 'right',
                listeners: {
                    'check': function (radio, e, eOpts) {
                        if (radio.getItemId().toLowerCase() == "no action needed") {
                            var MentorActionPanel = Ext.getCmp('idMentorActionItem');
                            var MentorActionItemsDonePanel = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
                            for (var i = 0; i < MentorActionPanel.getItems().length; i++) {
                                {
                                    if (MentorActionPanel.getItems().getAt(i).getItemId().toLowerCase() != "no action needed") {
                                        MentorActionPanel.getItems().getAt(i).setChecked(false);
                                    } else {
                                        MentorActionItemsDonePanel.getItems().getAt(i).setDisabled(false);
                                    }
                                }
                            }
                        } else {
                            var MentorActionPanel = Ext.getCmp('idMentorActionItem');
                            var MentorActionItemsDonePanel = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
                            for (var i = 0; i < MentorActionPanel.getItems().length; i++) {
                                if (MentorActionPanel.getItems().getAt(i).isChecked()) {
                                    if (MentorActionPanel.getItems().getAt(i).getItemId().toLowerCase() == "no action needed") {
                                        MentorActionPanel.getItems().getAt(i).setChecked(false);
                                    } else {
                                        MentorActionItemsDonePanel.getItems().getAt(i).setDisabled(false);
                                    }
                                } else if (MentorActionItemsDonePanel.getItems().getAt(i).isChecked() == false) {
                                    MentorActionItemsDonePanel.getItems().getAt(i).setDisabled(true);
                                }
                            }
                        }
                    },
                    'uncheck': function (radio, e, eOpts) {
                        var MentorActionPanel = Ext.getCmp('idMentorActionItem');
                        var MentorActionItemsDonePanel = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
                        for (var i = 0; i < MentorActionPanel.getItems().length; i++) {
                            if (!MentorActionPanel.getItems().getAt(i).getChecked()) {
                                MentorActionItemsDonePanel.getItems().getAt(i).setChecked(false);
                                MentorActionItemsDonePanel.getItems().getAt(i).setDisabled(true);
                            }
                        }
                    }
                }
            });
            MentorActionPanel.add(checkBoxField);

            if (CacheEditMentorActionItems == null) {
                var SelectedActionItemsDone = Ext.decode(localStorage.getItem("storeMentorActionItems"));
            } else {
                SelectedActionItemsDone = CacheEditMentorActionItems;
            }
            var doneChecked = false;

            if (SelectedActionItemsDone != null && SelectedActionItemsDone != "" && SelectedActionItemsDone.MentorActionItemNameDone != null) {
                SelectedActionItemsDone = SelectedActionItemsDone.MentorActionItemNameDone.split(",");
                if (SelectedActionItemsDone.indexOf(MentorActionItemStore.data.getAt(i).data.MentorActionName) != -1) {
                    doneChecked = true;
                }
            }

            var Disable = (checked == true) ? false : true;

            var checkBoxFieldActionDone = Ext.create('Ext.field.Checkbox', {
                name: 'recorded_stream',
                value: MentorActionItemStore.data.getAt(i).data.MentorActionID,
                label: MentorActionItemStore.data.getAt(i).data.MentorActionName,
                itemId: MentorActionItemStore.data.getAt(i).data.MentorActionName,
                labelCls: 'radio-btn-cls',
                checked: doneChecked,
                disabled: Disable,
                labelWidth: '90%',
                labelAlign: 'right',
                listeners: {
                    'check': function (radio, e, eOpts) {
                    }
                }
            });
            MentorActionPanelDoneOnNewMeeting.add(checkBoxFieldActionDone);


            var cls = 'addnote';
            var icon = 'add_icon_filled.png';
            var isContinue = false;
            for (var c = 0; c < MentorActionCommentsStore.data.length; c++) {
                if (isContinue)
                    continue;
                if (MentorActionCommentsStore.data.getAt(c).data.MeetingID == Mentor.Global.MEETING_DETAIL.MeetingID) {
                    if (MentorActionCommentsStore.data.getAt(c).data.MentorActionItemID == MentorActionItemStore.data.getAt(i).data.MentorActionID) {
                        cls = 'editnote_' + MentorActionCommentsStore.data.getAt(c).data.MentorActionItemCommentID;
                        icon = 'edit_note.png';
                    }
                }
            }

            var buttonFieldActionComment = Ext.create('Ext.Img', {
                src: 'resources/images/' + icon,
                height: 25,
                width: 25,
                itemId: MentorActionItemStore.data.getAt(i).data.MentorActionID,
                cls: cls,
                listeners: {
                    'tap': function (radio, e, eOpts) {
                        var popup = Mentor.app.application.getController('AppDetail').addCommentPopup(0, radio.getItemId(), radio.getCls()[0], Mentor.Global.MEETING_DETAIL.MeetingID);
                        Ext.Viewport.add(popup);
                        popup.show();
                    }
                }
            });
            MentorActionItemComments.add(buttonFieldActionComment);
        }
    },
    onPagePainted: function () {
        Ext.getCmp('mentorCommentMentorActionItems').reset();
        Ext.getCmp('mentor_action_items_title').setTitle(Mentor.Global.MENTOR_NAME + ' ToDo Items');
    }
});