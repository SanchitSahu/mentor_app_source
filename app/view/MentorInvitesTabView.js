Ext.define('Mentor.view.MentorInvitesTabView', {
    extend: 'Ext.TabPanel',
    xtype: 'mentorInvitesTabView',
    id: 'idMentorInvitesTabView',
    config: {
        tabBar: {
            docked: 'top',
            layout: {
                pack: 'center'
            },
            defaults: {
                flex: 1
            }
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: Mentor.Global.MENTOR_PENDING_TITLE, //title: 'Pending',
                cls: 'mettingHistoryToolBar',
                id: 'invitesTab',
                //cls : 'invitesTab',
                defaults: {
                    iconMask: true
                },
                items: [
                    //{
                    //    iconCls: 'btnBackCLS',
                    //    iconCls: 'addMettingMentorMettingHistory',
                    //    action: 'btnBackMentorInvitesTabView',
                    //    docked: 'left',
                    //    style : 'visibility: hidden;',
                    //    handler:function(){
                    //        Ext.getCmp('idMenteeBottomTabView').setActiveItem(0);
                    //    }
                    //},
                    {
                        xtype: 'button',
                        id: 'listButton',
                        iconCls: 'leftMenuIconCLS',
                        iconMask: true,
                        docked: 'left',
                        ui: 'plain',
                        handler: function () {
                            if (Ext.Viewport.getMenus().left.isHidden()) {
                                Ext.Viewport.showMenu('left');
                            } else
                            {
                                Ext.Viewport.hideMenu('left');
                            }
                        }
                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    /*{
                     iconCls: 'refreshIcon',
                     action: '',
                     docked: 'left',
                     style: "visibility:hidden"
                     },*/
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('pending');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    },
                    {
                        //style: "margin-left:10px",
                        iconCls: 'refreshIcon',
                        action: '',
                        docked: 'right',
                        id: 'idRefreshMentorInvitesTab',
                        handler: function () {
                            var TabBar = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar();
                            var TabBarPos = TabBar.items.indexOf(TabBar.getActiveTab());
                            if (TabBarPos == 1) {
                                Mentor.app.application.getController('AppDetail').btnGetPendingRequest();
                            } else if (TabBarPos == 0) {
                                Mentor.app.application.getController('AppDetail').getMentorPendingMeeting();
                            } else if (TabBarPos == 2) {
                                Mentor.app.application.getController('AppDetail').btnInviteForMeetingMentor();
                            }
                        }
                    }
                ]
            },
            {
                //xtype : 'waitscreen',
                //title: 'Pending Meetings',
                title: Mentor.Global.MENTOR_PENDING_MEETING_TITLE,
                xtype: 'mentorPendingMettingList'
            },
            {
                //title: 'Pending Invites',
                xtype: 'mentorPendingRequest',
                title: Mentor.Global.MENTOR_PENDING_REQUEST_TITLE,
                id: 'idMentorPendingRequest'
            },
            {
                //title: 'Pending Invites',
                xtype: 'invitementee',
                title: Mentor.Global.MENTOR_ISSUE_INVITE_TITLE,
                id: 'idMentorIssueInvite'
            }
//            {
//                //title: 'Pending Invites',
//                xtype: 'instantMeeting',
//                //title: Mentor.Global.MENTOR_ISSUE_INVITE_TITLE,
//                title: 'Instant',
//                id: 'idMentorInstantMeeting'
//            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            }
        ]
    },
    onPageActivate: function () {},
    onPagePainted: function () {
        clearInterval(Mentor.Global.SET_INTERVAL_ID);
        Mentor.Global.SET_INTERVAL_ID = 0;
        Mentor.Global.SET_INTERVAL_ID = setInterval(function () {
            var TabBar = Ext.getCmp('idPendingInvitesTabInvitesMentorBottomTabView').getTabBar();
            var TabBarPos = TabBar.items.indexOf(TabBar.getActiveTab());
            if (TabBarPos == 0) {
                Ext.getCmp('idRefreshMentorInvitesTab').fireAction('tap', '', Mentor.app.application.getController('AppDetail').getMentorPendingMeeting(true));
            } else if (TabBarPos == 1) {
                Ext.getCmp('idRefreshMentorInvitesTab').fireAction('tap', '', Mentor.app.application.getController('AppDetail').btnGetPendingRequest(true));
            } else if (TabBarPos == 2) {
                Ext.getCmp('idRefreshMentorInvitesTab').fireAction('tap', '', Mentor.app.application.getController('AppDetail').btnInviteForMeetingMentor(true));
            }
        }, Mentor.Global.SET_INTERVAL_TIME);
    }
});