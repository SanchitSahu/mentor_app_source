Ext.define('Mentor.view.InviteStatusList', {
    extend: 'Ext.dataview.List',
    xtype: 'inviteStatusList',
    requires: [],
    config: {
        cls: 'meetinghistory',
        store: 'WaitScreenMentor',
        /*itemTpl: '<div class="myContent {Status}Invitation" style = "display: flex;min-height:55px;">'+ 
         '<div style="width:20%; float:left; ">'+ 
         '<img src={MentorImage} id="cricle" height="50px" width="50px" style = "border-radius: 50%;">'+ 
         '</div>'+ 
         '<div style="width:80%;float:left;padding: 0px 0px !important;vertical-align: bottom;min-height: 16px;">'+
         
         '<div style="width:100%"><div style="width:100%; float:left;font-size: 14px;"><b>{MentorName}</b></div>' +
         '<div style="width:50%; float:right;">'+
         '<span style = "float:right;font-size: 11px;">{Status}</span>'+
         '</div></div>' +
         
         //Main Topics
         '<div style="width:100%;float:left;margin-top:5px;">'+
         '<span style="float:left;font-size:11px;color: black;">Main Topic : {TopicDescription}</span>'+
         
         '<div style="float:right;">'+
         '<span style="float:left;vertical-align: baseline;"><img src="./resources/images/clock.png"  height="20px" width="20px"></span><span style="vertical-align: text-top;float:right;font-size:11px;color: black;">{inviteTime}</span></div>'+
         
         '<div>'+
         '</div>',*/
        itemTpl: '<div class="myContent {Status}Invitation" style = "display: flex;min-height:55px;">' +
                '<div style="width:20%; float:left; ">' +
                '<img src="{MentorImage}" id="circle" height="50px" width="50px" style = "border-radius: 50%;" />' +
                '</div>' +
                '<div style="width:80%;float:left;padding: 0px 0px !important;vertical-align: bottom;min-height: 16px;">' +
                '<div style="width:100%">' +
                '<div style="width:80%; float:left;font-size: 14px;"><b>{MentorName}</b></div>' +
                '<div style="width:20%; float:right;">' +
                //'<span style = "float:right;font-size: 11px;">{(Status == "Rejected") ? "Declined" : "Status"}</span>' +
                '<span style = "float:right;font-size: 11px;"><tpl if="Status == \'Rejected\'">Declined<tpl else >{Status}</tpl></span>' +
                '</div>' +
                //Main Topics
                '<div style="width:100%;float:left;margin-top:5px;">' +
                '<span style="float:left;font-size:11px;color: black;">Main Topic : {TopicDescription}</span>' +
                '<div style="float:right;">' +
                '<span style="float:left;vertical-align: baseline;">' +
                '<img src="./resources/images/clock.png"  height="20px" width="20px">' +
                '</span>' +
                '<span style="vertical-align: text-top;float:right;font-size:11px;color: black;">{inviteTime}</span>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>',
        items: [
            {
                xtype: 'label',
                id: 'idUserNameInviteStatusList',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                docked: 'top',
                hidden: true
            },
            {
                xtype: 'label',
                html: Mentor.Global.VERSION_NAME,
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true
            }
        ],
        listeners: [{
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            },
            {
                event: "itemtap",
                fn: function (view, index, item, e) {
                    Mentor.app.application.getController('AppDetail').displayInvitationSendDetailInWaitScreen(view, index, item, e);
                }
            }
        ]
    },
    onPageActivate: function () {
        //Check The Login User and display the name on the top header
        var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginDetail = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));

        if (MentorLoginDetail != null) {
            Ext.getCmp('idUserNameInviteStatusList').setHtml(MentorLoginDetail.MentorName);
        } else if (MenteeLoginDetail != null) {
            Ext.getCmp('idUserNameInviteStatusList').setHtml(MenteeLoginDetail.MenteeName);
        }
    },
    onPagePainted: function () {
        Mentor.app.application.getController('AppDetail').getMentorForWaitScreen();
    }
});