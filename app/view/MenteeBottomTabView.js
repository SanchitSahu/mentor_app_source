Ext.define('Mentor.view.MenteeBottomTabView', {
    extend: 'Ext.TabPanel',
    xtype: 'menteeBottomTabView',
    id: 'idMenteeBottomTabView',
    requires: [
        'Mentor.view.MeetingHistory'
    ],
    config: {
        ui: 'light',
        tabBar: {
            docked: 'bottom',
            layout: {
                pack: 'center'
            },
            defaults: {
                flex: 1
            }
        },
        items: [
            {
                /*listeners : {
                 element : 'element',
                 painted : function() {
                 Mentor.app.application.getController('AppDetail').btnGetPendingRequest();
                 }
                 }*/
                xtype: 'menteeInvitesTabView',
                iconCls: 'pendingInvitesIconMentorBottomTabView',
                iconMask: true,
                title: 'Invites'
            },
            {
                xtype: 'meetinghistorymentee',
                iconCls: 'mettingHistoryIconMentorBottomTabView',
                iconMask: true,
                Cls: 'mettingHistoryMentorBottomTabView',
                id: 'mettingHistoryMentorBottomTabView',
                title: 'History'
            },
            {
                xtype: 'mentorprofile',
                iconCls: 'myProfileIconMentorBottomTabView',
                iconMask: true,
                title: 'My Profile'
            }
        ]
    }
});