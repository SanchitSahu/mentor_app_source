Ext.define('Mentor.view.Login', {
    extend: 'Ext.Panel',
    xtype: 'login',
    requires: [],
    config: {
        cls: 'loginCLS',
        defaults: {
            xtype: 'formpanel'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Sign In',
                cls: 'my-titlebar',
                hidden: true
            },
            {
                xtype: 'button',
                iconCls: 'loginHelpIcon',
                cls: 'loginHelpCls',
                action: 'loginHelpIcon'
            },
            {
                xtype: 'panel',
                cls: 'login-top',
                paddingTop: 10,
                layout: {
                    type: 'hbox',
                    pack: 'center'
                }
            },
            {
                xtype: 'panel',
                flex: 1,
                style: 'margin-top:70px;',
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                items: [
                    {
                        xtype: 'image',
                        src: 'resources/images/app_logo.png',
                        height: 90,
                        width: 90,
                        flex: 1
//                        margin: 'auto',

                    },
                    {
                        xtype: 'label',
                        html: 'Sign In',
                        cls: 'signInLabelLoginCLS',
                        margin: '24 0 24 0',
                        style: "text-align:left",
                        flex: 1
                    }
                ]
            },
            {
                xtype: 'panel',
                flex: 1,
                style: 'margin-top:40px;',
                layout: {
                    type: 'vbox',
                    pack: 'center'
                },
                items: [
                    {
                        xtype: 'selectfield',
                        id: 'idSchoolDetailSelectFieldLoginScreen',
                        store: 'SchoolDetail',
                        displayField: 'school_name',
                        valueField: 'subdomain_id',
                        margin: '0 40 0 40',
                        cls: 'schoolNameLoginCLS',
                        listeners: {
                            change: function (field, value) {
                                if (value instanceof Ext.data.Model) {
                                    value = value.get(field.getValueField());
                                }
                                console.log(value);
                                var Store = Ext.getStore('SchoolDetail');
                                var record = Store.findRecord('subdomain_id', value);
                                if (record != null) {
                                    Mentor.Global.SERVER_URL = "http://" + record.data.subdomain_name + Mentor.Global.URL_PART;
                                    localStorage.setItem("SchoolName", record.data.subdomain_name);
                                    localStorage.setItem("ServerURL", Mentor.Global.SERVER_URL);

                                    Mentor.Global.WEBVIEW_URL = "http://" + record.data.subdomain_name + Mentor.Global.WEBVIEW_URL_PART;
                                    localStorage.setItem("WebviewURL", Mentor.Global.WEBVIEW_URL);
                                }
                            }
                        }
                    },
                    {
                        xtype: 'emailfield',
                        name: 'email',
                        id: 'emailLogin',
                        placeHolder: 'UserName',
                        cls: 'userNameLoginCLS',
                        margin: '20 40 0 40'
                    },
                    {
                        xtype: 'passwordfield',
                        name: 'password',
                        id: 'passwordLogin',
                        placeHolder: 'Password',
                        cls: 'passwordLoginCLS',
                        margin: '20 40 0 40'
                    }
                ]
            },
            {
                xtype: 'panel',
                layout: {
                    pack: 'center'
                },
                items: [
                    {
                        xtype: 'button',
                        text: 'SIGN IN',
                        id: 'id_btn_singin',
                        cls: 'signin-btn-cls',
                        pressedCls: 'press-btn-cls',
                        //labelCls : 'btn-label-cls',
                        action: "doLogin",
                        margin: '20 100 0 100'
                    },
                    {
                        xtype: 'button',
                        text: 'Forgot Password',
                        ui: 'plain',
                        cls: 'forgotpassword-btn-cls',
                        action: 'doForgotPassword'
                    }
                ]
            },
            {
                xtype: 'label',
                html: Mentor.Global.VERSION_NAME,
                style: 'text-align: right;margin-bottom:10px;margin-left:10px;font-size:15px;',
                docked: 'bottom'
            }
        ]
    }
});