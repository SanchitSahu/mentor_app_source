Ext.define('Mentor.view.RescheduleMeeting', {
    extend: 'Ext.Panel',
    xtype: 'mentorRescheduleMeeting',
    id: 'idMentorRescheduleMeeting',
    requires: [],
    config: {
        cls: 'reschedule_meeting',
        //scrollable: "vertical",
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'container'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Reschedule Meeting',
                cls: 'my-titlebar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'btnBackCLS',
                        action: 'btnBackMentorRescheduleMeeting',
                        docked: 'left'
                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        //style: 'visibility:hidden'
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('reschedule_meeting');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'panel',
                layout: {
                    type: 'vbox'
                },
                margin: '0 10',
                items: [
                    {
                        xtype: 'label',
                        id: 'idInfoMentorNameMentorRescheduleMeetingDetail',
                        style: 'text-align: center;padding: 15px;border-bottom:1px solid #dadada;color:#181717;font-size: 17px;',
                        html: 'Meeting Request from Mentee1'
                    },
                    {
                        xtype: 'label',
                        //html : 'Mentee Name',
                        style: 'font-weight: normal;',
                        margin: '20 10 0 10',
                        id: 'idLabelMenteeNameMentorRescheduleMeetingDetail',
                        cls: 'appLabelCls',
                        hidden: true
                    },
                    {
                        /*disabled :false,
                         listeners : {
                         element : 'element',
                         tap : function() {
                         Mentor.app.application.getController('AppDetail').changeMenteeDetail();
                         }
                         }*/
                        //name: 'waitScreenMeetingStatus',
                        xtype: 'textfield',
                        id: 'idMenteeNameMentorRescheduleMeetingDetail',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        margin: '0 10 0 10',
                        hidden: true
                    },
                    {
                        xtype: 'label',
                        html: 'Main Topics',
                        style: 'font-weight: normal;',
                        margin: '20 10 0 10',
                        cls: 'appLabelCls'
                    },
                    {
                        /*disabled :false,
                         listeners : {
                         element : 'element',
                         tap : function() {
                         Mentor.app.application.getController('AppDetail').changeMenteeDetail();
                         }
                         }*/
                        //name: 'waitScreenMeetingStatus',
                        xtype: 'textfield',
                        id: 'idMainTopicsMentorRescheduleMeetingDetail',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        margin: '0 10 0 10'
                    },
                    {
                        xtype: 'panel',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'fieldset',
                                title: "Meeting Date",
                                style: 'font-weight: normal;',
                                flex: 1,
                                items: [
                                    {
                                        xtype: 'textfield',
                                        disabled: true,
                                        disabledCls: 'textFieldDisableCLS',
                                        inputCls: 'inputDisableCLS',
                                        id: 'dateTimeReschedulePendingRequestViewOnly',
                                        hidden: true
                                    },
                                    {
                                        xtype: 'textfield',
                                        disabled: true,
                                        hidden: true,
                                        disabledCls: 'textFieldDisableAcceptAndRejectMenteeRequestCLS',
                                        inputCls: 'textFieldDateTimeAcceptAndRejectMenteeRequestCLS',
                                        id: 'dateTimeReschedulePendingRequest',
                                        listeners: {
                                            element: 'element',
                                            tap: function () {
                                                var input = $('#timeReschedulePendingRequest');
                                                input.clockpicker('hide');
                                                var popup = new Ext.Panel({
                                                    floating: true,
                                                    centered: true,
                                                    modal: true,
                                                    hideOnMaskTap: true,
                                                    autoDestroy: true,
                                                    width: 310,
                                                    height: 410,
                                                    items: [
                                                        {
                                                            xtype: 'popview',
                                                            listeners: {
                                                                selectionchange: function (a, newDate, oldDate, eOpts)
                                                                {
                                                                    console.log(newDate);
                                                                    //var MeetingTime = Ext.getCmp('timePickerReschedulePendingRequest').getValue();
                                                                    var MeetingTime = '';
                                                                    var currentDate = '';
                                                                    var selectedDate = '';
                                                                    var Year = '';
                                                                    var Month = '';
                                                                    var date = '';
                                                                    var MeetingDate = '';
                                                                    var SelectedMeetingDate = '';

                                                                    MeetingTime = Ext.getCmp('timeReschedulePendingRequest').getValue();
                                                                    currentDate = new Date();
                                                                    currentDate.setHours(0, 0, 0, 0);
                                                                    selectedDate = new Date(newDate);
                                                                    selectedDate.setHours(0, 0, 0, 0);
                                                                    if (currentDate > selectedDate) {
                                                                        Ext.Msg.alert("MELS", "Please select future date");
                                                                        popup.hide();
                                                                        Ext.Viewport.remove(popup);
                                                                    } else {
                                                                        Year = newDate.getFullYear();
                                                                        Month = (newDate.getMonth() + 1);
                                                                        if (Month < 10)
                                                                            Month = "0" + Month;
                                                                        date = newDate.getDate();
                                                                        if (date < 10)
                                                                            date = "0" + date;
                                                                        SelectedMeetingDate = Year + "-" + Month + "-" + date;
                                                                        if (MeetingTime != "") {
                                                                            MeetingDate = SelectedMeetingDate;
                                                                            MeetingDate = Date.parse(MeetingDate + " " + MeetingTime);
                                                                            //var MeetingDate = new Date(MeetingDate + ' ' + MeetingTime.getHours() + ':' + MeetingTime.getMinutes())
                                                                            if (new Date() > MeetingDate) {
                                                                                Ext.Msg.alert("MELS", "Please select future date.");
                                                                            } else {
                                                                                Mentor.Global.Current_date = new Date(MeetingDate);
                                                                                Ext.getCmp('dateTimeReschedulePendingRequest').setValue(SelectedMeetingDate);
                                                                            }
                                                                            popup.hide();
                                                                            Ext.Viewport.remove(popup);
                                                                        } else {
                                                                            Ext.getCmp('dateTimeReschedulePendingRequest').setValue(SelectedMeetingDate);
                                                                            Mentor.Global.Current_date = Date.parse(SelectedMeetingDate);
                                                                            popup.hide();
                                                                            Ext.Viewport.remove(popup);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    ],
                                                    listeners: {
                                                        hide: function (a, b) {
                                                            popup.hide();
                                                            Ext.Viewport.remove(popup);
                                                        }
                                                    }
                                                });
                                                Ext.Viewport.add(popup);
                                                Ext.getCmp('calendarViewID').setViewConfig({value: Mentor.Global.Current_date});
                                                Ext.getCmp('calendarViewID').setActiveItem(2);
                                                Ext.getCmp('calendarViewID').setActiveItem(2);
                                                Ext.getCmp('calendarViewID').setActiveItem(-2);
                                                Ext.getCmp('calendarViewID').setActiveItem(-2);
                                                popup.show();
                                            }
                                        }
                                    }
                                ]
                            },
                            {
                                xtype: 'fieldset',
                                title: "Meeting Time",
                                flex: 1,
                                items: [
                                    {
                                        xtype: 'textfield',
                                        disabled: true,
                                        disabledCls: 'textFieldDisableCLS',
                                        inputCls: 'inputDisableCLS',
                                        id: 'timePickerReschedulePendingRequestViewOnly',
                                        hidden: true
                                    },
                                    {
                                        xtype: 'datetimepickerfield',
                                        name: '24hrdt',
                                        id: 'timePickerReschedulePendingRequest',
                                        hidden: true,
                                        dateTimeFormat: 'H:i',
                                        picker: {
                                            yearFrom: new Date().getFullYear(),
                                            yearTo: new Date(new Date().setYear((new Date().getFullYear() + 1))).getFullYear(),
                                            minuteInterval: 1,
                                            ampm: false,
                                            slotOrder: ['hour', 'minute']
                                        }
                                    },
                                    {
                                        xtype: 'textfield',
                                        disabled: false,
                                        hidden: true,
                                        disabledCls: 'textFieldDisableAcceptAndRejectMenteeRequestCLS',
                                        inputCls: 'textFieldDateTimeAcceptAndRejectMenteeRequestCLS',
                                        id: 'timeReschedulePendingRequest',
                                        clearIcon: false,
                                        readOnly: true,
                                        listeners: {
                                            element: 'element',
                                            tap: function () {
                                                //Ext.getCmp('timeReschedulePendingRequest').setReadOnly(true);
                                                /*var Timepicker = Ext.getCmp('timePickerReschedulePendingRequest').getPicker();
                                                 Timepicker.show();*/
                                                var input = $('#timeReschedulePendingRequest');
                                                input.clockpicker({
                                                    beforeDone: function () {
                                                        Ext.getCmp("TimePickerReschedulePendingOldValue").setValue(Ext.getCmp('timeReschedulePendingRequest').getValue());
                                                    },
                                                    afterDone: function () {
                                                        var MeetingDate = Ext.getCmp('dateTimeReschedulePendingRequest').getValue();
                                                        var value = $('#timeReschedulePendingRequest input').val();
                                                        console.log(value);
                                                        if (value.length > 6 || typeof value.length == "undefined") {
                                                            return;
                                                        }
                                                        if (MeetingDate != "") {
                                                            MeetingDate = Date.parse(MeetingDate + " " + value);
                                                            if (new Date() > MeetingDate) {
                                                                var old_value = Ext.getCmp('TimePickerReschedulePendingOldValue').getValue();
                                                                Ext.getCmp('timeReschedulePendingRequest').setValue(null);
                                                                Ext.Msg.alert("MELS", "Please select future time");
                                                            } else {
                                                                Ext.getCmp("TimePickerReschedulePendingOldValue").setValue(this.value);
                                                                Ext.getCmp('timeReschedulePendingRequest').setValue(value);
                                                            }
                                                        } else {
                                                            Ext.getCmp('timeReschedulePendingRequest').setValue(value);
                                                        }
                                                    }
                                                });
                                            },
                                            blur: function () {
                                                var input = $('#timeReschedulePendingRequest');
                                                input.clockpicker('hide');
                                            }
                                        }
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        xtype: 'textfield',
                        id: 'TimePickerReschedulePendingOldValue',
                        hidden: true
                    },
                    {
                        xtype: 'label',
                        html: 'Meeting Type',
                        style: 'font-weight: normal;',
                        margin: '0 10 0 10',
                        cls: 'appLabelCls'
                    },
                    {
                        xtype: 'fieldset',
                        //title: "Meeting Type<span class='spanAsterisk'>*</span>",
                        title: "",
                        id: "idLabelMainTypeMentorRescheduleMeetingDetail",
                        style: 'margin-top:-20px;',
                        items: [
                            {
                                xtype: 'selectfield',
                                id: 'idMainTypeMentorRescheduleMeetingDetail',
                                store: 'MeetingType',
                                displayField: 'MeetingTypeName',
                                valueField: 'MeetingTypeID',
                                // autoSelect: false,
                                // placeHolder: '-- Select Meeting Type --',
                                listeners: {
                                    change: function (field, value) {
                                        if (value instanceof Ext.data.Model) {
                                            value = value.get(field.getValueField());
                                        }
                                        console.log(value);
                                        var Store = Ext.getStore('MeetingType');
                                        var record = Store.findRecord('MeetingTypeID', value);
                                        if (record != null) {
                                            if (record.data.MeetingTypeID == "") {
                                                Ext.getCmp('idMeetingPlaceMentorRescheduleMeetingDetail').setDisabled(false);
                                                Ext.getCmp('idMeetingPlaceMentorRescheduleMeetingDetail').removeCls('inputDisableCLS');
                                            } else if (record.data.MeetingTypeName != "In Person") {
                                                Ext.getCmp('idMeetingPlaceMentorRescheduleMeetingDetail').setDisabled(true);
                                                Ext.getCmp('idMeetingPlaceMentorRescheduleMeetingDetail').setValue(3);
                                                Ext.getCmp('idMeetingPlaceMentorRescheduleMeetingDetail').addCls('inputDisableCLS');
                                            } else {
                                                Ext.getCmp('idMeetingPlaceMentorRescheduleMeetingDetail').setDisabled(false);
                                                Ext.getCmp('idMeetingPlaceMentorRescheduleMeetingDetail').removeCls('inputDisableCLS');
                                            }
                                        }
                                    }
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'textfield',
                        //name: 'waitScreenMeetingStatus',
                        id: 'idMainTypeMenteeRescheduleMeetingDetail',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        margin: '0 10 20 10'
                    },
                    {
                        xtype: 'label',
                        html: 'Meeting Place',
                        style: 'font-weight: normal;',
                        margin: '0 10 0 10',
                        cls: 'appLabelCls'
                    },
                    {
                        xtype: 'fieldset',
                        //title: "Meeting Place<span class='spanAsterisk'>*</span>",
                        title: "",
                        id: 'idLabelSelectMeetingPlaceReschedulePendingRequest',
                        style: 'margin-top:-20px;',
                        items: [
                            {
                                // autoSelect: false,
                                // placeHolder: '-- Select Meeting Place --',
                                // inputCls: 'inputDisableCLS'
                                xtype: 'selectfield',
                                id: 'idMeetingPlaceMentorRescheduleMeetingDetail',
                                store: 'MeetingPlace',
                                displayField: 'MeetingPlaceName',
                                valueField: 'MeetingPlaceID'
                            }
                        ]
                    },
                    {
                        //name: 'waitScreenMeetingStatus',
                        xtype: 'textfield',
                        id: 'idMeetingPlaceMenteeRescheduleMeetingDetail',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        margin: '0 10 0 10'
                    },
                    {
                        xtype: 'label',
                        //html : 'Mentee Email',
                        style: 'font-weight: normal;',
                        margin: '0 10 0 10',
                        id: 'idLabelMenteeEmailMentorRescheduleMeetingDetail',
                        cls: 'appLabelCls',
                        hidden: true
                    },
                    {
                        xtype: 'textfield',
                        id: 'idMeenteeEmailMentorRescheduleMeetingDetail',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        margin: '0 10 0 10',
                        hidden: true
                    },
                    {
                        //html : 'Mentee Phone No',
                        xtype: 'label',
                        style: 'font-weight: normal;',
                        margin: '20 10 0 10',
                        id: 'idLabelMenteePhoneNoMentorRescheduleMeetingDetail',
                        cls: 'appLabelCls',
                        hidden: true
                    },
                    {
                        xtype: 'textfield',
                        id: 'idMeenteePhoneMentorRescheduleMeetingDetail',
                        disabled: true,
                        disabledCls: 'textFieldDisableCLS',
                        inputCls: 'inputDisableCLS',
                        margin: '0 10 0 10',
                        hidden: true
                    },
                    {
                        xtype: 'panel',
                        margin: '10 0 0 10',
                        layout: {
                            type: 'hbox'
                        },
                        hidden: true,
                        items: [
                            {
                                xtype: 'checkboxfield',
                                id: 'idIsPhoneSMSCapableMentorRescheduleMeetingDetail',
                                label: "Is above number Text Message(aka SMS) capable?",
                                labelCls: 'radio-btn-cls',
                                labelWidth: '90%',
                                labelAlign: 'right',
                                disabled: true,
                                cls: 'wordWrapSpan',
                                style: 'background: transparent;'
                            }
                        ]
                    },
                    {
                        //html : 'Contact Mentee',
                        xtype: 'label',
                        style: 'font-weight: normal;',
                        margin: '20 10 0 10',
                        id: 'idLabelMenteeContactMentorRescheduleMeetingDetail',
                        cls: 'appLabelCls',
                        hidden: true
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox'
                        },
                        hidden: true,
                        margin: '10 20 0 20',
                        id: 'idContainerMenteeContactMentorRescheduleMeetingDetail',
                        items: [
                            {
                                xtype: 'button',
                                text: 'EMAIL',
                                //cls : 'tick',
                                cls: 'signin-btn-cls',
                                pressedCls: 'press-btn-cls',
                                flex: 1,
                                margin: '0 5 0 0'
                            },
                            {
                                xtype: 'button',
                                text: 'CALL',
                                //cls : 'tick',
                                cls: 'signin-btn-cls',
                                pressedCls: 'press-btn-cls',
                                flex: 1,
                                margin: '0 5 0 5'
                            },
                            {
                                xtype: 'button',
                                text: 'SMS',
                                //cls : 'tick',
                                cls: 'signin-btn-cls',
                                pressedCls: 'press-btn-cls',
                                flex: 1,
                                margin: '0 0 0 5'
                            }
                        ]

                    },
                    {
                        //html : Mentor.Global.MENTEE_NAME+' Comment',
                        xtype: 'label',
                        margin: '10 10 0 10',
                        style: 'font-weight: normal;',
                        id: 'idLabelMenteeCommentsMentorRescheduleMeetingDetail',
                        cls: 'appLabelCls',
                        hidden: true
                    },
                    {
                        //name: 'mentorComment',
                        xtype: 'textareafield',
                        id: 'idMenteeCommentMentorRescheduleMeetingDetail',
                        disabledCls: 'textFieldDisableCLS',
                        cls: 'textFieldDisableCLS',
                        maxRows: 2,
                        disabled: false,
                        margin: '0 10 0 10',
                        //inputCls: 'inputDisableCLS',
                        inputCls: 'inputCLS',
                        clearIcon: false,
                        hidden: true
                    },
                    {
                        xtype: 'label',
                        margin: '20 10 0 10',
                        style: 'font-weight: normal;',
                        id: 'idLabelMentorCommentsMentorRescheduleMeetingDetail',
                        cls: 'appLabelCls'
                    },
                    {
                        xtype: 'textareafield',
                        name: 'mentorComment',
                        id: 'idMentorCommentMentorRescheduleMeetingDetail',
                        cls: 'textFieldDisableCLS',
                        maxRows: 2,
                        disabled: true,
                        margin: '0 10 0 10',
                        inputCls: 'inputDisableCLS',
                        clearIcon: false
                    },
//                    {
//                        xtype: 'textareafield',
//                        name: 'mentorComment',
//                        id: 'idMentorCommentMentorRescheduleMeetingDetail',
//                        cls: 'textFieldDisableCLS',
//                        maxRows: 2,
//                        disabled: true,
//                        margin: '0 10 20 10',
//                        inputCls: 'inputCLS',
//                        clearIcon: false,
//                    },
                    {
                        xtype: 'label',
                        margin: '20 10 0 10',
                        style: 'font-weight: normal;',
                        id: 'idLabelRescheduleCommentRescheduleMeetingDetail',
                        html: 'Reason for Reschedule',
                        cls: 'appLabelCls'
                    },
                    {
                        xtype: 'textareafield',
                        name: 'rescheduleComment',
                        id: 'idRescheduleCommentRescheduleMeetingDetail',
                        cls: 'textFieldDisableCLS',
                        maxRows: 2,
                        disabled: false,
                        margin: '0 10 20 10',
                        inputCls: 'inputCLS',
                        clearIcon: false
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            pack: 'center'
                        },
                        margin: '0 10 10 10',
                        items: [
                            {
                                xtype: 'button',
                                text: 'SUBMIT',
                                //cls : 'tick',
                                cls: 'signin-btn-cls',
                                pressedCls: 'press-btn-cls',
                                id: 'idBtnRescheduleMeetingSubmit',
                                action: 'doRescheduleRequest',
                                flex: 1,
                                margin: '0 5 0 0'
                            },
                            {
                                xtype: 'button',
                                text: 'CANCEL',
                                //cls : 'cancel',
                                pressedCls: 'press-grey_btn-cls',
                                cls: 'grey-btn-cls',
                                action: 'btnBackMentorRescheduleMeeting',
                                flex: 1,
                                margin: '0 0 0 5'
                            }
                        ]
                    }
                ]
            }
        ],
        listeners: [
//            {
//                event: "activate",
//                fn: "onPageActivate"
//            },
//            {
//                event: "painted",
//                fn: "onPagePainted"
//            },
//            {
//                event: "itemtap",
//                fn: function (view, index, item, e) {
//                    Mentor.app.application.getController('AppDetail').displayMentorRescheduleMeeting(view, index, item, e);
//                }
//            },
            /*itemtap: function(view, index, item, e)
             {
             alert(11);
             Mentor.app.application.getController('AppDetail').displayInvitationSendDetailInWaitScreen(view, index, item, e);
             
             },*/
        ]

    },
    onPageActivate: function () {
        var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginDetail = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
        if (MentorLoginDetail != null) {
            Ext.getCmp('idUserNameInviteStatusList').setHtml(MentorLoginDetail.MentorName);
        } else if (MenteeLoginDetail != null) {
            Ext.getCmp('idUserNameInviteStatusList').setHtml(MenteeLoginDetail.MenteeName);
        }
    },
    onPagePainted: function () {
//        Mentor.app.application.getController('AppDetail').getMentorRescheduleMeeting();
    }
});
