Ext.define("Mentor.view.SubadminWebView", {
    extend: 'Ext.TabPanel',
    xtype: 'subadminwebview',
    id: 'subadminWebView',
    config: {
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        ui: 'light',
        tabBar: {
            docked: 'bottom',
            layout: {
                pack: 'center'
            },
            defaults: {
                flex: 1
            }
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Subadmin Panel',
                id: 'webviewTab',
                cls: 'invitesTab',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'btnBackCLS',
                        action: 'btnBackMentorNotes',
                        docked: 'left'
                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        //style: 'visibility:hidden'
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    {
                        iconCls: 'refreshIcon',
                        docked: 'left',
                        style: 'visibility:hidden'
                    },
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var TabBar = Ext.getCmp('subadminWebView').getTabBar();
                                var TabBarPos = TabBar.items.indexOf(TabBar.getActiveTab());
                                if (TabBarPos == 0) {
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('subadmin_add_mentor');
                                } else if (TabBarPos == 1) {
                                    //if 1
                                    //var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('subadmin_add_mentee');
                                    
                                    //if 2
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('subadmin_relationship');
                                } else if (TabBarPos == 2) {
                                    var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('subadmin_relationship');
                                }

                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    },
                    {
                        iconCls: 'refreshIcon',
                        action: '',
                        docked: 'right',
                        id: 'refreshscreen',
                        handler: function () {
                            var pageHeight = $('html').height() - $('#webviewTab').height() - $('.x-container.x-tabbar.x-tabbar-dark.x-tabbar-light.x-dock-item.x-docked-bottom.x-stretched').height();
                            var AppConfig = Ext.decode(localStorage.getItem(Mentor.Global.APPLICATION_CONFIGURATION));
                            Ext.Viewport.setMasked({xtype: "loadmask", message: "Please wait"});
                            var TabBar = Ext.getCmp('subadminWebView').getTabBar();
                            var TabBarPos = TabBar.items.indexOf(TabBar.getActiveTab());
                            if (TabBarPos == 0) {
                                Ext.getCmp('AddMentorWebView').setHtml('<iframe width="' + $('html').width() + '" id="AddMentorWebViewIframe" height="' + pageHeight + '" src =""></iframe>');
                                Ext.getCmp('AddMentorWebView').setHtml('<iframe width="' + $('html').width() + '" id="AddMentorWebViewIframe" height="' + pageHeight + '" src ="' + Mentor.Global.WEBVIEW_URL + '?Email=' + encodeURIComponent(AppConfig.AdminEmail) + '&Password=' + AppConfig.AdminPassword + '&Page=mentors&callFromApp=true&loggedInMentor=' + AppConfig.LoggenInUserId + '\"></iframe>');
                                $('iframe#AddMentorWebViewIframe').load(function () {
                                    Ext.Viewport.setMasked(false);
                                });
                            } else if (TabBarPos == 1) {
                                //if 1
                                //Ext.getCmp('AddMenteeWebView').setHtml('<iframe width="' + $('html').width() + '" id="AddMenteeWebViewIframe" height="' + pageHeight + '" src =""></iframe>');
                                //Ext.getCmp('AddMenteeWebView').setHtml('<iframe width="' + $('html').width() + '" id="AddMenteeWebViewIframe" height="' + pageHeight + '" src ="' + Mentor.Global.WEBVIEW_URL + '?Email=' + encodeURIComponent(AppConfig.AdminEmail) + '&Password=' + AppConfig.AdminPassword + '&Page=mentees/add&callFromApp=true\"></iframe>');
                                //$('iframe#AddMenteeWebViewIframe').load(function () {
                                //    Ext.Viewport.setMasked(false);
                                //});

                                //if 2
                                Ext.getCmp('RelationshipWebView').setHtml('<iframe width="' + $('html').width() + '" id="RelationshipWebViewIframe" height="' + pageHeight + '" src =""></iframe>');
                                Ext.getCmp('RelationshipWebView').setHtml('<iframe width="' + $('html').width() + '" id="RelationshipWebViewIframe" height="' + pageHeight + '" src ="' + Mentor.Global.WEBVIEW_URL + '?Email=' + encodeURIComponent(AppConfig.AdminEmail) + '&Password=' + AppConfig.AdminPassword + '&Page=relationship&callFromApp=true\"></iframe>');
                                $('iframe#RelationshipWebViewIframe').load(function () {
                                    Ext.Viewport.setMasked(false);
                                });
                            } else if (TabBarPos == 2) {
                                Ext.getCmp('RelationshipWebView').setHtml('<iframe width="' + $('html').width() + '" id="RelationshipWebViewIframe" height="' + pageHeight + '" src =""></iframe>');
                                Ext.getCmp('RelationshipWebView').setHtml('<iframe width="' + $('html').width() + '" id="RelationshipWebViewIframe" height="' + pageHeight + '" src ="' + Mentor.Global.WEBVIEW_URL + '?Email=' + encodeURIComponent(AppConfig.AdminEmail) + '&Password=' + AppConfig.AdminPassword + '&Page=relationship&callFromApp=true\"></iframe>');
                                $('iframe#RelationshipWebViewIframe').load(function () {
                                    Ext.Viewport.setMasked(false);
                                });
                            }
                        }
                    }
                ]
            },
            {
                iconCls: 'AddMentorIconWebviewBottomTabView',
                iconMask: true,
                id: 'AddMentorWebView',
                title: 'SubAdmin'
            },
            /*{
             iconCls: 'AddMentorIconWebviewBottomTabView',
             iconMask: true,
             id: 'AddMenteeWebView',
             title: 'Add Mentee'
             },*/
            {
                iconCls: 'RelationshipIconWebviewBottomTabView',
                iconMask: true,
                id: "RelationshipWebView",
                title: 'Relationship'
            }
        ],
        listeners: [
            {
                event: "deactivate",
                fn: "onPageDeactivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            }
        ]
    },
    onPagePainted: function () {
        var AppConfig = Ext.decode(localStorage.getItem(Mentor.Global.APPLICATION_CONFIGURATION));
        Ext.Viewport.setMasked({xtype: "loadmask", message: "Please wait"});
        clearInterval(Mentor.Global.SET_INTERVAL_ID);
        Mentor.Global.SET_INTERVAL_ID = 0;
        var pageHeight = $('html').height() - $('#webviewTab').height() - $('.x-container.x-tabbar.x-tabbar-dark.x-tabbar-light.x-dock-item.x-docked-bottom.x-stretched').height();

        Ext.getCmp('AddMentorWebView').setHtml('<iframe width="' + $('html').width() + '" id="AddMentorWebViewIframe" height="' + pageHeight + '" src ="' + Mentor.Global.WEBVIEW_URL + '?Email=' + encodeURIComponent(AppConfig.AdminEmail) + '&Password=' + AppConfig.AdminPassword + '&Page=mentors&callFromApp=true&loggedInMentor=' + AppConfig.LoggenInUserId + '\"></iframe>');

        var TabBar = Ext.getCmp('subadminWebView').getTabBar();
        TabBar.on('activetabchange', function () {
            Ext.Viewport.setMasked({xtype: "loadmask", message: "Please wait"});
            if (TabBar.items.indexOf(TabBar.getActiveTab()) == 0) {
                Ext.getCmp('AddMentorWebView').setHtml('<iframe width="' + $('html').width() + '" id="AddMentorWebViewIframe" height="' + pageHeight + '" src =""></iframe>');
                Ext.getCmp('AddMentorWebView').setHtml('<iframe width="' + $('html').width() + '" id="AddMentorWebViewIframe" height="' + pageHeight + '" src ="' + Mentor.Global.WEBVIEW_URL + '?Email=' + encodeURIComponent(AppConfig.AdminEmail) + '&Password=' + AppConfig.AdminPassword + '&Page=mentors&callFromApp=true&loggedInMentor=' + AppConfig.LoggenInUserId + '\"></iframe>');
                $('iframe#AddMentorWebViewIframe').load(function () {
                    Ext.Viewport.setMasked(false);
                });
            } else if (TabBar.items.indexOf(TabBar.getActiveTab()) == 1) {
                //if 1
                //Ext.getCmp('AddMenteeWebView').setHtml('<iframe width="' + $('html').width() + '" id="AddMenteeWebViewIframe" height="' + pageHeight + '" src =""></iframe>');
                //Ext.getCmp('AddMenteeWebView').setHtml('<iframe width="' + $('html').width() + '" id="AddMenteeWebViewIframe" height="' + pageHeight + '" src ="' + Mentor.Global.WEBVIEW_URL + '?Email=' + encodeURIComponent(AppConfig.AdminEmail) + '&Password=' + AppConfig.AdminPassword + '&Page=mentees/add&callFromApp=true\"></iframe>');
                //$('iframe#AddMenteeWebViewIframe').load(function () {
                //    Ext.Viewport.setMasked(false);
                //});

                //if 2
                Ext.getCmp('RelationshipWebView').setHtml('<iframe width="' + $('html').width() + '" id="RelationshipWebViewIframe" height="' + pageHeight + '" src =""></iframe>');
                Ext.getCmp('RelationshipWebView').setHtml('<iframe width="' + $('html').width() + '" id="RelationshipWebViewIframe" height="' + pageHeight + '" src ="' + Mentor.Global.WEBVIEW_URL + '?Email=' + encodeURIComponent(AppConfig.AdminEmail) + '&Password=' + AppConfig.AdminPassword + '&Page=relationship&callFromApp=true\"></iframe>');
                $('iframe#RelationshipWebViewIframe').load(function () {
                    Ext.Viewport.setMasked(false);
                });
            } else if (TabBar.items.indexOf(TabBar.getActiveTab()) == 2) {
                Ext.getCmp('RelationshipWebView').setHtml('<iframe width="' + $('html').width() + '" id="RelationshipWebViewIframe" height="' + pageHeight + '" src =""></iframe>');
                Ext.getCmp('RelationshipWebView').setHtml('<iframe width="' + $('html').width() + '" id="RelationshipWebViewIframe" height="' + pageHeight + '" src ="' + Mentor.Global.WEBVIEW_URL + '?Email=' + encodeURIComponent(AppConfig.AdminEmail) + '&Password=' + AppConfig.AdminPassword + '&Page=relationship&callFromApp=true\"></iframe>');
                $('iframe#RelationshipWebViewIframe').load(function () {
                    Ext.Viewport.setMasked(false);
                });
            }
        }, this);

        $('iframe#AddMentorWebViewIframe').load(function () {
            Ext.Viewport.setMasked(false);
        });
    },
    onPageDeactivate: function () {
        Ext.getCmp('RelationshipWebView').setHtml("");
        Ext.getCmp('AddMentorWebView').setHtml("");
        //Ext.getCmp('AddMenteeWebView').setHtml("");
    }
});