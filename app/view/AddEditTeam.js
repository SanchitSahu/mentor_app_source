Ext.define('Mentor.view.AddEditTeam', {
    extend: 'Ext.Panel',
    xtype: 'addeditteam',
    itemId: 'idAddEditTeam',
    requires: [],
    config: {
        cls: 'mentorprofile',
        //scrollable: "vertical",
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'container'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Add a New Team',
                cls: 'my-titlebar',
                id: 'addEditTeamScreenTitle',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        //style : 'visibility: hidden;',
                        iconCls: 'btnBackCLS',
                        docked: 'left',
                        listeners: {
                            tap: function () {
                                Mentor.app.application.getController('AppDetail').btnBackActionTeamDetail();
                            }
                        }
                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('add_edit_team');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    },
                    {
                        iconCls: 'refreshIcon',
                        docked: 'right'
                                //action : 'btnSaveProfile',
                                //action: "btnGetUpdatedProfile",
                                //id: "idProfileRefresh"
                    }
                ]
            },
            {
                xtype: 'panel',
                cls: 'mettingHistoryToolBar',
                layout: {
                    type: 'vbox',
                    pack: 'center',
                    align: 'middle'
                },
                items: [
                    {
                        //docked: 'top',
                        xtype: 'panel',
                        defaults: {
                            iconMask: true
                        },
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'panel',
                                margin: '0 0 0 8',
                                layout: {
                                    type: 'vbox',
                                    pack: 'center',
                                    align: 'middle'
                                },
                                items: [
                                    {
                                        xtype: "button",
                                        action: "takeTeamProfilePhoto",
                                        id: 'id_TeamProfilePicture',
                                        cls: "team-photo-preview-cls",
                                        margin: 10
                                    },
                                    {
                                        xtype: "label",
                                        hidden: true,
                                        id: 'id_TeamProfilePictureHidden'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'panel',
                margin: '20',
                items: [
                    {
                        xtype: 'panel',
                        cls: 'userInputTextProfileCLS',
                        margin: '15 0 0 0',
                        items: [
                            {
                                xtype: 'label',
                                html: 'Team Name <span class="spanAsterisk">*</span>',
                                cls: 'userProfileLabelCLS',
                                id: 'idLabelTeamName'
                            },
                            {
                                xtype: 'textfield',
                                id: 'idTeamName',
                                disabled: false,
                                clearIcon: false,
                                inputCls: 'profileInputCLS'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        cls: 'userInputTextProfileCLS',
                        margin: '15 0 0 0',
                        items: [
                            {
                                xtype: 'label',
                                html: 'Team Description <span class="spanAsterisk">*</span>',
                                cls: 'userProfileLabelCLS',
                                id: 'idLabelTeamDesc'
                            },
                            {
                                xtype: 'textfield',
                                disabled: false,
                                clearIcon: false,
                                inputCls: 'profileInputCLS',
                                id: 'idTeamDesc'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        cls: 'userInputTextProfileCLS',
                        margin: '15 0 0 0',
                        items: [
                            {
                                xtype: 'label',
                                html: 'Max Members',
                                cls: 'userProfileLabelCLS',
                                id: 'idLabelRequiredMembers'
                            },
                            {
                                xtype: 'sliderfield',
                                disabled: false,
                                clearIcon: false,
                                inputCls: 'inputCLS',
                                id: 'idRequiredMembers',
                                value: 2,
                                minValue: 2,
                                listeners: {
                                    change: function (me, sl, thumb, newValue) {
                                        Ext.getCmp('idSliderValue').setValue(newValue);
                                    }
                                }
                            },
                            {
                                xtype: 'numberfield',
                                id: 'idSliderValue',
                                disabled: false,
                                clearIcon: false,
                                inputCls: 'inputCLS',
                                value: 3,
                                listeners: {
                                    keyup: function (me, e, eOpts) {
                                        var App_Config = Ext.decode(localStorage.getItem('applicationConfiguration'));
                                        if (me.getValue() > App_Config.maxMember) {
                                            me.setValue(App_Config.maxMember);
                                        }
                                        Ext.getCmp('idRequiredMembers').setValue(me.getValue());
                                    },
                                    change: function (me, newValue) {
                                        var App_Config = Ext.decode(localStorage.getItem('applicationConfiguration'));
                                        if (me.getValue() > App_Config.maxMember) {
                                            me.setValue(App_Config.maxMember);
                                        }
                                        Ext.getCmp('idRequiredMembers').setValue(newValue);
                                    }
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        style: 'background : #f2f2f2;padding: 10px 30px;',
                        margin: '10 -20 0 -20',
                        items: [
                            {
                                xtype: 'label',
                                html: 'Required Skills <span class="spanAsterisk">*</span>',
                                cls: 'profileAddInterestCLS',
                                id: 'addSkillsTeam'
                            },
                            {
                                xtype: 'label',
                                html: 'One or more skills required by Team members',
                                cls: 'profileAddInterestDetailCLS',
                                id: 'idAddEditSkillForTeam'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        id: 'idSkillsTeam',
                        margin: '10 0 20 0',
                        defaults: {
                            style: {
                                float: 'left',
                                pack: 'center'
                            }
                        },
                        control: {
                            'button[action="addRequiredSkills"]': {
                                tap: function (element) {
                                    if (element.getCls() == 'profileSkillsActiveCLS')
                                        element.setCls('profileSkillsInActiveCLS');
                                    else
                                        element.setCls('profileSkillsActiveCLS');
                                }
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'panel',
                layout: {
                    type: 'hbox'
                },
                margin: '0 10 0 10',
                items: [
                    {
                        xtype: 'button',
                        text: 'View Team Members',
                        id: 'idButtonViewTeamMember',
                        cls: 'signin-btn-cls',
                        pressedCls: 'press-btn-cls',
                        action: "btnViewTeamMembers",
                        hidden: true,
                        flex: 1
                    }
                ]
            },
            {
                xtype: 'panel',
                layout: {
                    type: 'hbox'
                },
                margin: '10 10 20 10',
                items: [
                    {
                        xtype: 'button',
                        text: 'SAVE',
                        id: 'idButtonAddTeam',
                        cls: 'signin-btn-cls',
                        pressedCls: 'press-btn-cls',
                        action: "btnAddTeam",
                        hidden: false,
                        flex: 1,
                        style: 'margin-top : 10px'
                    },
                    {
                        xtype: 'button',
                        text: 'CANCEL',
                        pressedCls: 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        hidden: false,
                        flex: 1,
                        style: {

                            'margin-top': '10px'
                        },
                        //style: 'background:,;',
                        listeners: {
                            tap: function () {
                                Mentor.app.application.getController('AppDetail').btnBackActionTeamDetail();
                            }
                        }
                    }
                ]
            }

        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            }
        ]
    },
    onPageActivate: function () {
        var App_Config = Ext.decode(localStorage.getItem('applicationConfiguration'));
        Ext.getCmp('idRequiredMembers').setMaxValue(App_Config.maxMember);
        Ext.getCmp('idSliderValue').setMaxValue(App_Config.maxMember);

        var SkillsStore = Ext.getStore('Skills').load();
        var SkillsPanel = Ext.getCmp('idSkillsTeam');
        SkillsPanel.removeAll();

        var Skills = '';
        var ownerTeamInfo = Ext.decode(localStorage.getItem('TeamAllDetails'));
        if (ownerTeamInfo.OwnTeamDetail != false) {
            Skills = ownerTeamInfo.OwnTeamDetail[0].Skills;
        }

        for (i = 0; i < SkillsStore.data.length; i++) {
            var avtiveSkillCLS = 'profileSkillsInActiveCLS';

            var SkillsArray = Skills.split(',');
            for (j = 0; j < SkillsArray.length; j++) {
                var SkillsID = SkillsArray[j].split('~');
                if (SkillsID[0] == SkillsStore.data.getAt(i).data.SkillID)
                {
                    avtiveSkillCLS = 'profileSkillsActiveCLS';
                    break;
                }
            }

            var radioField = Ext.create('Ext.Button', {
                id: 'skill_' + SkillsStore.data.getAt(i).data.SkillID,
                text: SkillsStore.data.getAt(i).data.SkillName,
                cls: avtiveSkillCLS,
                action: 'addRequiredSkills'
            });
            SkillsPanel.add(radioField);
        }
    },
    onPagePainted: function () {
        clearInterval(Mentor.Global.SET_INTERVAL_ID);
        Mentor.Global.SET_INTERVAL_ID = 0;
    }
});
