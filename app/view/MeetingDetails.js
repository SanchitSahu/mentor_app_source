Ext.define('Mentor.view.MeetingDetails', {
    extend: 'Ext.Panel',
    xtype: 'meetingdetails',
    requires: [],
    config: {
        cls: 'meetingdetails',
        //scrollable: "vertical",
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'container'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Subtopics Discussed',
                cls: 'my-titlebar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'btnBackCLS',
                        action: 'btnBackMeetingDetails',
                        docked: 'left'
                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('subtopic_discussed');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'label',
                id: 'idUserNameEnterpreneur',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                hidden: true
            },
            {
                xtype: 'label',
                html: Mentor.Global.VERSION_NAME,
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true
            },
            {
                xtype: 'panel',
                margin: '20 0 0 0',
                layout: {
                    type: 'vbox'
                },
                items: [
                    {
                        xtype: 'label',
                        html: 'Metting Main Topics',
                        style: 'margin-left:10px;color: #000000;'
                    },
                    {
                        xtype: 'label',
                        html: 'Metting Main Topics',
                        id: 'idMeetingMainTopicsMeetingDetailScreen',
                        style: 'padding-left:10px;color: #808080;border-bottom: 1px solid #bfbfbf;padding-bottom:10px;font-size:16px;'
                    },
                    {
                        xtype: 'panel',
                        id: 'idMeetingDetailSubTopic'
                    },
                    /*{
                     xtype: 'checkboxfield',
                     name : 'color',
                     value: '0',
                     label: 'Sales',
                     checked: true,
                     labelWidth: 200,
                     labelCls : 'radio-btn-cls'
                     },
                     {
                     xtype: 'checkboxfield',
                     name : 'color',
                     value: '1',
                     label: 'Marketing',
                     labelWidth: 200,
                     labelCls : 'radio-btn-cls'
                     },
                     {
                     xtype: 'checkboxfield',
                     name : 'color',
                     value: '0',
                     label: 'Human Resources',
                     labelWidth: 200,
                     labelCls : 'radio-btn-cls'
                     },
                     {
                     xtype: 'checkboxfield',
                     name : 'color',
                     value: '1',
                     label: 'Financing',
                     labelWidth: 200,
                     labelCls : 'radio-btn-cls'
                     },
                     {
                     xtype: 'checkboxfield',
                     name : 'color',
                     value: '1',
                     label: 'Plans',
                     labelWidth: 200,
                     labelCls : 'radio-btn-cls'
                     },*/
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            pack: 'center'
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'NEXT',
                                cls: 'signin-btn-cls',
                                action: "btnNextMeetingDetails",
                                //flex: 1,
                                pressedCls: 'press-btn-cls',
                                width: '50%'
                            },
                            {
                                xtype: 'button',
                                text: 'UPDATE',
                                cls: 'signin-btn-cls',
                                pressedCls: 'press-btn-cls',
                                width: '50%',
                                //flex: 1,
                                hidden: true,
                                id: 'idBtnUpdateMeetingDetails',
                                action: 'btnUpdateMeetingDetails' //UpdateMeetingDetail Controller
                            }
                        ]
                    }
                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            }
        ]
    },
    onPageActivate: function () {
        var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History

        var MeetingDetailSubTopicStore = Ext.getStore('SubTopic').load();
        var MeetingDetailPanel = Ext.getCmp('idMeetingDetailSubTopic');
        MeetingDetailPanel.removeAll();

        var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        Ext.getCmp('idUserNameEnterpreneur').setHtml(MentorLoginDetail.MentorName);


        /*for(i = 0 ; i<MeetingDetailSubTopicStore.data.length;i++){
         var checkBoxField = Ext.create('Ext.field.Checkbox', {
         name: 'recorded_stream',
         value: MeetingDetailSubTopicStore.data.getAt(i).data.SubTopicID,
         label: MeetingDetailSubTopicStore.data.getAt(i).data.SubTopicDescription,
         labelCls : 'radio-btn-cls',
         labelWidth: 250,
         listeners: {
         'check': function(radio, e, eOpts) {
         //me.radioHandler(radio.getValue());
         }
         }
         });
         MeetingDetailPanel.add(checkBoxField);
         }*/


        // If Subtopics available or not
        if (MeetingDetailSubTopicStore.data.length <= 0)
        {
            MeetingDetailPanel.setHtml("No Subtopics.");
            MeetingDetailPanel.setStyle('margin:10px;');
        } else {
            MeetingDetailPanel.setHtml("");
            MeetingDetailPanel.setStyle('margin:0px;');
        }

        for (i = 0; i < MeetingDetailSubTopicStore.data.length; i++) {
            var checked = false;
            if (MeetingDetail == null) {
                var MeetingDetailsItem = Ext.decode(localStorage.storeMeetingDetailSubTopics);
                if (MeetingDetailsItem != null && MeetingDetailsItem.SubTopicsID.length > 0) {
                    var Suptopics = MeetingDetailsItem.SubTopicsID.split(',');
                    if (Suptopics.indexOf(MeetingDetailSubTopicStore.data.getAt(i).data.SubTopicID) != -1) {
                        checked = true;
                    }
                }
//                var MeetingDetailsName;
//                for (j = 0; j < MeetingDetailsItem.length; j++) {
//                    var MeetingDetailID = MeetingDetailsItem[j].split('~')
//                    if (MeetingDetailID[0] == MeetingDetailSubTopicStore.data.getAt(i).data.SubTopicID)
//                    {
//                        checked = true;
//
//                    }
//                }


                /*if(i == 0)
                 checked = false;*/
                Ext.getCmp('idBtnUpdateMeetingDetails').setHidden(true);
            } else {
                // from Meeting History Screen
                var MeetingDetailsItem = MeetingDetail.SubTopicDescription.split(',');
                var MeetingDetailsName;
                for (j = 0; j < MeetingDetailsItem.length; j++) {
                    var MeetingDetailID = MeetingDetailsItem[j].split('~');
                    if (MeetingDetailID[0] == MeetingDetailSubTopicStore.data.getAt(i).data.SubTopicID) {
                        checked = true;
                    }
                }
                /*if(MeetingDetail.MeetingSubTopicID == MeetingDetailSubTopicStore.data.getAt(i).data.SubTopicID)
                 {
                 checked = true;
                 }*/
                Ext.getCmp('idBtnUpdateMeetingDetails').setHidden(false);

            }

            var checkBoxField = Ext.create('Ext.field.Checkbox', {
                name: 'recorded_stream',
                value: MeetingDetailSubTopicStore.data.getAt(i).data.SubTopicID,
                label: MeetingDetailSubTopicStore.data.getAt(i).data.SubTopicDescription,
                labelCls: 'radio-btn-cls',
                labelWidth: '90%',
                checked: checked,
                labelAlign: 'right',
                cls: 'checkboxitem-cls',
                listeners: {
                    'check': function (radio, e, eOpts) {
                        //me.radioHandler(radio.getValue());
                    }
                }
            });
            MeetingDetailPanel.add(checkBoxField);
        }
    }
});