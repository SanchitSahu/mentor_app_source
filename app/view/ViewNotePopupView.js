Ext.define('Mentor.view.ViewNotePopupView', {
    extend: 'Ext.Panel',
    xtype: 'viewnotepopview',
    requires: [],
    config: {
        cls: 'viewNotePopupScreen',
        defaults: {
        },
        items: [
            {
                xtype: 'textfield',
                id: 'idViewNoteID',
                hidden: true
            },
            {
                xtype: 'textfield',
                id: 'idViewNoteMeetingID',
                hidden: true
            },
            {
                xtype: 'label',
                html: 'Note Title',
                style: 'font-weight: bold;',
                padding: '35 10 3 10',
                hidden: true
            },
            {
                xtype: 'label',
                id: 'idViewNoteTitle',
                //cls: 'textFieldDisableCLS',
                inputCls: 'inputCLS',
                margin: '0 10 0 10',
                clearIcon: false,
                hidden: true
            },
            {
                xtype: 'label',
                html: "Note Content",
                style: 'font-weight: bold;',
                padding: '20 10 3 10'
            },
            {
                xtype: 'textareafield',
                height: '250px',
                style: "background-color: #FFFF76;",
                id: 'idViewNoteDescription',
                margin: '0 10 0 10',
                disabled: false,
                clearIcon: false
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                margin: '5 0 10 0',
                items: [
                    {
                        xtype: 'button',
                        text: 'EDIT',
                        id: 'idEditBtnViewNotePopup',
                        //cls : 'tick',
                        //cls: 'signin-btn-cls-note',
                        cls: 'signin-btn-cls',
                        pressedCls: 'press-btn-cls',

                        //action: "doSubmitAddEditNote",
                        flex: 1,
                        margin: '10 5 0 5',
                        listeners: {
                            tap: function () {
                                var NoteID = Ext.getCmp('idViewNoteID').getValue();
                                var popupObject = Ext.getCmp('viewNotePopUp');
                                popupObject.hide();

                                var editNotePopup = Mentor.app.application.getController('AppDetail').editNotePopup('', '', NoteID);
                                Ext.Viewport.add(editNotePopup);
                                editNotePopup.show();
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'DELETE',
                        id: 'idDeleteBtnViewNotePopup',
                        //cls : 'cancel',
                        //cls: 'decline-btn-cls-note',
                        pressedCls: 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',

                        flex: 1,
                        margin: '10 5 0 5',
                        listeners: {
                            tap: function () {
                                Ext.Msg.confirm("MELS", "Are you sure you want to delete this note?", function (btn) {
                                    if (btn == 'yes') {
                                        var NoteID = Ext.getCmp('idViewNoteID').getValue();
                                        var popupObject = Ext.getCmp('viewNotePopUp');
                                        popupObject.hide();
                                        Mentor.app.application.getController('AppDetail').deleteNotePopup('', '', NoteID);
                                    }
                                });
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'SHARE',
                        id: 'idShareBtnViewNotePopup',
                        //cls : 'cancel',
                        //cls: 'share-btn-cls-note',
                        cls: 'signin-btn-cls',
                        pressedCls: 'press-btn-cls',

                        flex: 1,
                        margin: '10 5 0 5',
                        listeners: {
                            tap: function () {
                                Ext.Msg.show({
                                    title: 'Share Note',
                                    message: 'What access you wish to set for your Note?',
                                    buttons: [
                                        {text: 'Public', itemId: 'public', ui: 'action'},
                                        {text: 'Private', itemId: 'private', ui: 'action'},
                                        {text: 'Cancel', itemId: 'cancel', ui: 'action'}
                                    ],
                                    fn: function (buttonId) {
                                        if (buttonId == 'cancel') {
                                            return false;
                                        } else {
                                            var NoteID = Ext.getCmp('idViewNoteID').getValue();
                                            Mentor.app.application.getController('AppDetail').shareNotePopup(buttonId, NoteID);
                                        }

                                    }
                                });
                            }
                        }
                    }
                ]
            }
        ]
    },
    listeners: [
        {
            event: "activate",
            fn: "onPageActivate"
        },
        {
            event: "painted",
            fn: "onPagePainted"
        }
    ],
    onPagePainted: function () {}
});