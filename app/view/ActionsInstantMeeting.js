Ext.define('Mentor.view.ActionsInstantMeeting', {
    extend: 'Ext.Panel',
    xtype: 'action_instant_meeting',
    requires: [
    ],
    config: {
        cls: 'mentor_action_items',
        //scrollable: "vertical",
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'container'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Tasks',
                id: 'action_instant_meeting_items_title',
                cls: 'my-titlebar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        iconCls: 'btnBackCLS',
                        docked: 'left',
                        action: 'btnBackActionInstantMeeting'
                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    {
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('mentor_action');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    },
                    {
                        iconCls: 'btnBackCLS',
                        docked: 'right',
                        style: 'visibility:hidden'
                    }
                ]
            },
            {
                xtype: 'label',
                id: 'idUserNameEnterpreneur',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                hidden: true
            },
            {
                xtype: 'panel',
                margin: '10 0 0 0',
                layout: {
                    type: 'vbox'
                },
                items: [
                    {
                        xtype: 'label',
                        html: 'Mentee ToDo Items',
                        style: 'margin-left:10px;margin-right:10px;border: 0px solid #eee;border-left-width: 0;border-bottom: 1px solid #66cc66;font-weight: bold;',
                        id: 'idLabelTeamMenteeActionInstantMeeting',
                        hidden: true
                    },
                    {
                        xtype: 'panel',
                        id: 'idMenteeTeamActionItemHeader',
                        margin: '10 0 0 0',
                        layout: {
                            type: 'hbox'
                        },
                         style : 'border-bottom: 1px solid #bfbfbf;padding-bottom: 5px;',
                        items: [
                            {
                                xtype: 'label',
                                html: 'ToDo',
                                style: 'margin-left: 9px; font-size: smaller;'
                            },
                            {
                                xtype: 'label',
                                html: 'Done',
                                style: 'margin-left: 10px; font-size: smaller;'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        margin: '10 0 0 0',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'panel',
                                id: 'idMenteeTeamActionItem',
                                margin: '0 0 0 10'
                            },
                            {
                                //style : 'width: 85%;'
                                xtype: 'panel',
                                margin: '0 0 0 10',
                                id: 'idMenteeTeamActionItemDoneOnMeetingUpdate'
                            }
                        ]
                    },
                    /*{
                        xtype: 'label',
                        html: 'Mentor ToDo Items',
                        style: 'margin-left:10px;margin-right:10px;border: 0px solid #eee;border-left-width: 0;border-bottom: 1px solid #66cc66;font-weight: bold;',
                        id: 'idLabelActionInstantMeeting',
                        hidden: true
                    },
                    {
                        xtype: 'panel',
                        id: 'idMenteeActionItemHeader',
                        margin: '10 0 0 0',
                         hidden: true,
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'label',
                                html: 'ToDo',
                                style: 'margin-left: 9px; font-size: smaller;'
                            },
                            {
                                xtype: 'label',
                                html: 'Done',
                                style: 'margin-left: 10px; font-size: smaller;'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        margin: '10 0 0 0',
                        layout: {
                            type: 'hbox'
                        },
                         hidden: true,
                        items: [
                            {
                                xtype: 'panel',
                                id: 'idMentorActionItem',
                                margin: '0 0 0 10'
                            },
                            {
                                //style : 'width: 85%;'
                                xtype: 'panel',
                                margin: '0 0 0 10',
                                id: 'idMentorActionItemDoneOnMeetingUpdate'
                            }
                        ]
                    },*/
                    {
                        xtype: 'panel',
                        layout: {
                            type: 'hbox',
                            pack : 'center'
                        },
                        items:[
                            {
                                xtype: 'button',
                                text: 'NEXT',
                                cls: 'signin-btn-cls',
                                 pressedCls : 'press-btn-cls',
                                id: 'btnSubmitActionInstantMeeting',
                                action: "btnNextActionInstantMeeting",
                                style: 'width:50%;'
                            }
                        ]
                    }
                    
                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            }
        ]
    },
    onPageActivate: function () {
        var MentorActionItemStore = Ext.getStore('MentorAction').load();
        var MenteeTeamActionItemStore = Ext.getStore('EnterpreneurAction').load();
        
        /*var MentorActionPanel = Ext.getCmp('idMentorActionItem');
        MentorActionPanel.removeAll();*/

        var MenteeTeamActionPanel = Ext.getCmp('idMenteeTeamActionItem');
        MenteeTeamActionPanel.removeAll();

        /*var MentorActionPanelDoneOnNewMeeting = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
        MentorActionPanelDoneOnNewMeeting.removeAll();*/

        var MenteeTeamActionPanelDoneOnNewMeeting = Ext.getCmp('idMenteeTeamActionItemDoneOnMeetingUpdate');
        MenteeTeamActionPanelDoneOnNewMeeting.removeAll();

        var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        Ext.getCmp('idUserNameEnterpreneur').setHtml(MentorLoginDetail.MentorName);

       /* for (i = 0; i < MentorActionItemStore.data.length; i++) {
            var checked = false;

            var checkBoxField = Ext.create('Ext.field.Checkbox', {
                name: 'recorded_stream',
                value: MentorActionItemStore.data.getAt(i).data.MentorActionID,
                itemId: MentorActionItemStore.data.getAt(i).data.MentorActionName,
                labelCls: 'radio-btn-cls',
                checked: checked,
                labelWidth: '90%',
                labelAlign: 'right',
                listeners: {
                    'check': function (radio, e, eOpts) {
                        //me.radioHandler(radio.getValue());
                        if (radio.getItemId().toLowerCase() == "no action needed")
                        {
                            var MentorActionPanel = Ext.getCmp('idMentorActionItem');
                            var MentorActionItemsDonePanel = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
                            for (var i = 0; i < MentorActionPanel.getItems().length; i++) {
                                //if(MentorActionPanel.getItems().getAt(i).isChecked())
                                {
                                    if (MentorActionPanel.getItems().getAt(i).getItemId().toLowerCase() != "no action needed") {
                                        MentorActionPanel.getItems().getAt(i).setChecked(false);
                                    } else {
                                        MentorActionItemsDonePanel.getItems().getAt(i).setDisabled(false);
                                    }

                                }
                            }
                        } else {
                            var MentorActionPanel = Ext.getCmp('idMentorActionItem');
                            var MentorActionItemsDonePanel = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
                            for (var i = 0; i < MentorActionPanel.getItems().length; i++) {
                                if (MentorActionPanel.getItems().getAt(i).isChecked()) {
                                    if (MentorActionPanel.getItems().getAt(i).getItemId().toLowerCase() == "no action needed") {
                                        MentorActionPanel.getItems().getAt(i).setChecked(false);
                                    } else {
                                        MentorActionItemsDonePanel.getItems().getAt(i).setDisabled(false);
                                    }
                                } else if (MentorActionItemsDonePanel.getItems().getAt(i).isChecked() == false) {
                                    MentorActionItemsDonePanel.getItems().getAt(i).setDisabled(true);
                                }
                            }
                        }
                    },
                    'uncheck': function (radio, e, eOpts) {
                        var MentorActionPanel = Ext.getCmp('idMentorActionItem');
                        var MentorActionItemsDonePanel = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
                        for (var i = 0; i < MentorActionPanel.getItems().length; i++) {
                            if (!MentorActionPanel.getItems().getAt(i).getChecked()) {
                                MentorActionItemsDonePanel.getItems().getAt(i).setChecked(false);
                                MentorActionItemsDonePanel.getItems().getAt(i).setDisabled(true);
                            }
                        }
                    }
                }
            });
            MentorActionPanel.add(checkBoxField);

            //Action Item Done for New Meeting
            var checked = false;
            var Disable = true;

            var checkBoxFieldActionDone = Ext.create('Ext.field.Checkbox', {
                name: 'recorded_stream',
                value: MentorActionItemStore.data.getAt(i).data.MentorActionID,
                label: MentorActionItemStore.data.getAt(i).data.MentorActionName,
                itemId: MentorActionItemStore.data.getAt(i).data.MentorActionName,
                labelCls: 'radio-btn-cls',
                checked: checked,
                disabled: Disable,
                labelWidth: '90%',
                labelAlign: 'right',
                listeners: {
                    'check': function (radio, e, eOpts) {
                        //me.radioHandler(radio.getValue());
                    }
                }
            });
            MentorActionPanelDoneOnNewMeeting.add(checkBoxFieldActionDone);
        }*/


        for (i = 0; i < MenteeTeamActionItemStore.data.length; i++) {
            var checked = false;

            var checkBoxField = Ext.create('Ext.field.Checkbox', {
                //label: MentorActionItemStore.data.getAt(i).data.MentorActionName,
                //labelWidth: 280,
                name: 'recorded_stream',
                value: MenteeTeamActionItemStore.data.getAt(i).data.MenteeActionID,
                itemId: MenteeTeamActionItemStore.data.getAt(i).data.MenteeActionName,
                labelCls: 'radio-btn-cls',
                checked: checked,
                labelWidth: '90%',
                labelAlign: 'right',
                listeners: {
                    'check': function (radio, e, eOpts) {
                        //me.radioHandler(radio.getValue());
                        if (radio.getItemId().toLowerCase() == "no action needed")
                        {
                            var MenteeTeamActionPanel = Ext.getCmp('idMenteeTeamActionItem');
                            var MentorActionItemsDonePanel = Ext.getCmp('idMenteeTeamActionItemDoneOnMeetingUpdate');
                            for (var i = 0; i < MenteeTeamActionPanel.getItems().length; i++) {
                                //if(MenteeTeamActionPanel.getItems().getAt(i).isChecked())
                                {
                                    if (MenteeTeamActionPanel.getItems().getAt(i).getItemId().toLowerCase() != "no action needed") {
                                        MenteeTeamActionPanel.getItems().getAt(i).setChecked(false);
                                    } else {
                                        MentorActionItemsDonePanel.getItems().getAt(i).setDisabled(false);
                                    }

                                }
                            }
                        } else {
                            var MenteeTeamActionPanel = Ext.getCmp('idMenteeTeamActionItem');
                            var MentorActionItemsDonePanel = Ext.getCmp('idMenteeTeamActionItemDoneOnMeetingUpdate');
                            for (var i = 0; i < MenteeTeamActionPanel.getItems().length; i++) {
                                if (MenteeTeamActionPanel.getItems().getAt(i).isChecked()) {
                                    if (MenteeTeamActionPanel.getItems().getAt(i).getItemId().toLowerCase() == "no action needed") {
                                        MenteeTeamActionPanel.getItems().getAt(i).setChecked(false);
                                    } else {
                                        MentorActionItemsDonePanel.getItems().getAt(i).setDisabled(false);
                                    }
                                } else if (MentorActionItemsDonePanel.getItems().getAt(i).isChecked() == false) {
                                    MentorActionItemsDonePanel.getItems().getAt(i).setDisabled(true);
                                }
                            }
                        }
                    },
                    'uncheck': function (radio, e, eOpts) {
                        var MenteeTeamActionPanel = Ext.getCmp('idMenteeTeamActionItem');
                        var MentorActionItemsDonePanel = Ext.getCmp('idMenteeTeamActionItemDoneOnMeetingUpdate');
                        for (var i = 0; i < MenteeTeamActionPanel.getItems().length; i++) {
                            if (!MenteeTeamActionPanel.getItems().getAt(i).getChecked()) {
                                MentorActionItemsDonePanel.getItems().getAt(i).setChecked(false);
                                MentorActionItemsDonePanel.getItems().getAt(i).setDisabled(true);
                            }
                        }
                    }
                }
            });
            MenteeTeamActionPanel.add(checkBoxField);

            //Action Item Done for New Meeting
            var checked = false;
            var Disable = true;

            var checkBoxFieldActionDone = Ext.create('Ext.field.Checkbox', {
                name: 'recorded_stream',
                value: MenteeTeamActionItemStore.data.getAt(i).data.MenteeActionID,
                label: MenteeTeamActionItemStore.data.getAt(i).data.MenteeActionName,
                itemId: MenteeTeamActionItemStore.data.getAt(i).data.MenteeActionName,
                labelCls: 'radio-btn-cls',
                checked: checked,
                disabled: Disable,
                labelWidth: '90%',
                labelAlign: 'right',
                listeners: {
                    'check': function (radio, e, eOpts) {
                        //me.radioHandler(radio.getValue());
                    }
                }
            });
            MenteeTeamActionPanelDoneOnNewMeeting.add(checkBoxFieldActionDone);

        }
        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        
        //Ext.getCmp('idLabelActionInstantMeeting').setHtml(Mentor.Global.MENTOR_NAME + ' ToDo Items');
        //Ext.getCmp('idLabelActionInstantMeeting').setHidden(false);

    },
    onPagePainted: function () {
        console.log(Mentor.Global.INSTANT_MEETING_DATA);
        clearInterval(Mentor.Global.INSTANT_MEETING_INTERVAL_ID);
    }
});
