Ext.define('Mentor.view.InstantMeetingTab', {
    extend: 'Ext.Panel',
    xtype: 'instantMeetingTab',
    requires: [],
    config: {
        cls: 'forgotpassword',
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'formpanel'
        },
        items: [
            {
                xtype: 'fieldset',
                title: "Meeting With <span class='spanAsterisk'>*</span>",
                style: 'margin-top:-10px;margin-left: 20px;margin-right: 20px;margin-bottom: 0px;',
                items: [
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            pack: 'center'
                        },
                        margin: '0 5 15 5',
                        items: [
                            {
                                xtype: 'radiofield',
                                name: 'color',
                                id: 'radioMentee',
                                value: 'mentee',
                                hidden: true
                            },
                            {
                                xtype: 'radiofield',
                                id: 'radioTeam',
                                name: 'color',
                                value: 'team',
                                hidden: true
                            },
                            {
                                xtype: 'panel',
                                flex: 1,
                                layout: {
                                    type: 'vbox',
                                    pack: 'center',
                                    align: 'middle'
                                },
                                items: [
                                    {
                                        xtype: 'panel',
                                        margin: '5 5',
                                        items: [
                                            {
                                                xtype: 'button',
                                                iconCls: 'userIcon',
                                                cls: 'toggleInstant-btn-cls',
                                                id: 'menteeBtn',
                                                style: "text-align:center",
                                                listeners: {
                                                    element: 'element',
                                                    tap: function () {
                                                        Ext.getCmp('radioMentee').check();
                                                        Ext.getCmp('teamBtn').removeCls('team selected');
                                                        Ext.getCmp('menteeBtn').addCls('mentee selected');
                                                        Ext.getCmp('idLabelSelectMentorInstantMeetingScreen').setHidden(false);
                                                        Ext.getCmp('idTeamSelectFieldInstantMeetingScreen').setValue('');
                                                        Ext.getCmp('idLabelSelectTeamInstantMeetingScreen').setHidden(true);
                                                    }
                                                }
                                            },
                                            {
                                                xtype: "label",
                                                id: "MenteeLabel",
                                                flex: 1,
                                                style: "text-align:center;font-size: 16px;"
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                xtype: 'panel',
                                flex: 1,
                                items: [
                                    {
                                        xtype: "label",
                                        html: "Or",
                                        centered: true
                                    }
                                ]
                            },
                            {
                                xtype: 'panel',
                                flex: 1,
                                layout: {
                                    type: 'vbox',
                                    pack: 'center',
                                    align: 'middle'
                                },
                                items: [
                                    {
                                        xtype: 'panel',
                                        margin: '5 5',
                                        items: [
                                            {
                                                xtype: 'button',
                                                iconCls: 'teamIcon',
                                                cls: 'toggleInstant-btn-cls',
                                                id: 'teamBtn',
                                                flex: 1,
                                                style: "text-align:center",
                                                listeners: {
                                                    element: 'element',
                                                    tap: function () {
                                                        Ext.getCmp('radioTeam').check();
                                                        Ext.getCmp('menteeBtn').removeCls('mentee selected');
                                                        Ext.getCmp('teamBtn').addCls('team selected');
                                                        Ext.getCmp('idLabelSelectMentorInstantMeetingScreen').setHidden(true);
                                                        Ext.getCmp('idMenteeSelectFieldInstantMeetingScreen').setValue(-1);
                                                        Ext.getCmp('idLabelSelectTeamInstantMeetingScreen').setHidden(false);
                                                    }
                                                }
                                            },
                                            {
                                                xtype: "label",
                                                html: "TEAM",
                                                flex: 1,
                                                style: "text-align:center;font-size: 16px;"
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]

            },
            {
                xtype: 'fieldset',
                hidden: true,
                title: 'Select Mentee',
                margin: '0 20 0 20',
                id: 'idLabelSelectMentorInstantMeetingScreen',
                items: [
                    {
                        xtype: 'selectfield',
                        id: 'idMenteeSelectFieldInstantMeetingScreen',
                        store: 'InviteMentee',
                        displayField: 'MenteeName',
                        placeHolder: '-- Select Mentee --',
                        valueField: 'MenteeID',
                        listeners: {
                            change: function (field, value) {
                                if (value instanceof Ext.data.Model) {
                                    value = value.get(field.getValueField());
                                }
                                var Store = Ext.getStore('InviteMentee');
                                var record = Store.findRecord('MenteeID', value);
                                if (record != null) {
                                    if (record.data.rStatus == "TRUE") {
//                                        Ext.getCmp('idSendBtnInstantMeetingScreen').setDisabled(true);
//                                        Ext.getCmp('idSendBtnInstantMeetingScreen').setText("Invitation Still Pending");
//                                        Ext.getCmp('idTopicsSelectFieldInstantMeetingScreen').setDisabled(true);
//                                        Ext.getCmp('mentorCommentInstantMeetingScreen').setDisabled(true);
                                    } else {
//                                        Ext.getCmp('idSendBtnInstantMeetingScreen').setDisabled(false);
//                                        Ext.getCmp('idSendBtnInstantMeetingScreen').setText("Send Invitation");
//                                        Ext.getCmp('idTopicsSelectFieldInstantMeetingScreen').setDisabled(false);
//                                        Ext.getCmp('mentorCommentInstantMeetingScreen').setDisabled(false);
                                    }
                                }
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'fieldset',
                title: 'Select Team <span class="spanAsterisk">*</span>',
                hidden: true,
                margin: '0 20 0 20',
                id: 'idLabelSelectTeamInstantMeetingScreen',
                items: [
                    {
                        xtype: 'selectfield',
                        id: 'idTeamSelectFieldInstantMeetingScreen',
                        store: 'TeamDropdown',
                        displayField: 'TeamName',
                        placeHolder: '-- Select Team --',
                        valueField: 'TeamId',
                        options: [
                            {text: '-- Select Team --', value: 'asc'}
                        ]
//                        listeners: {
//                            change: function (field, value) {
//                                if (value instanceof Ext.data.Model) {
//                                    value = value.get(field.getValueField());
//                                }
//                                var Store = Ext.getStore('InviteMentee');
//                                var record = Store.findRecord('MenteeID', value);
//                                if (record != null) {
//                                    if (record.data.rStatus == "TRUE") {
//                                        Ext.getCmp('idSendBtnInstantMeetingScreen').setDisabled(true);
//                                        Ext.getCmp('idSendBtnInstantMeetingScreen').setText("Invitation Still Pending");
//                                        Ext.getCmp('idTopicsSelectFieldInstantMeetingScreen').setDisabled(true);
//                                        Ext.getCmp('mentorCommentInstantMeetingScreen').setDisabled(true);
//                                    } else {
//                                        Ext.getCmp('idSendBtnInstantMeetingScreen').setDisabled(false);
//                                        Ext.getCmp('idSendBtnInstantMeetingScreen').setText("Send Invitation");
//                                        Ext.getCmp('idTopicsSelectFieldInstantMeetingScreen').setDisabled(false);
//                                        Ext.getCmp('mentorCommentInstantMeetingScreen').setDisabled(false);
//                                    }
//                                }
//                            }
//                        }
                    }
                ]
            },
            {
                xtype: 'fieldset',
                title: 'Select Main Topic <span class="spanAsterisk">*</span>',
                margin: '0 20 0 20',
                items: [
                    {
                        xtype: 'selectfield',
                        id: 'idTopicsSelectFieldInstantMeetingScreen',
                        store: 'Topics',
                        placeHolder: '-- Select Main Topic --',
                        displayField: 'TopicDescription',
                        valueField: 'TopicID'
                    }
                ]
            },
            {
                xtype: 'fieldset',
                title: "Meeting Type <span class='spanAsterisk'>*</span>",
                //style: 'margin-top:-10px;',
                margin: '0 20 0 20',
                items: [
                    {
                        xtype: 'selectfield',
                        id: 'idMeetingTypeSelectFieldInstantMeetingScreen',
                        store: 'MeetingType',
                        displayField: 'MeetingTypeName',
                        valueField: 'MeetingTypeID',
                        listeners: {
                            change: function (field, value) {
                                if (value instanceof Ext.data.Model) {
                                    value = value.get(field.getValueField());
                                }
                                var Store = Ext.getStore('MeetingType');
                                var record = Store.findRecord('MeetingTypeID', value);
                                if (record.data.MeetingTypeID == "") {
                                    Ext.getCmp('idMeetingPlaceSelectFieldInstantMeetingScreen').setDisabled(false);
                                    Ext.getCmp('idMeetingPlaceSelectFieldInstantMeetingScreen').removeCls('inputDisableCLS');
                                } else if (record.data.MeetingTypeName != "In Person") {
                                    Ext.getCmp('idMeetingPlaceSelectFieldInstantMeetingScreen').setDisabled(true);
                                    Ext.getCmp('idMeetingPlaceSelectFieldInstantMeetingScreen').setValue(3);
                                    Ext.getCmp('idMeetingPlaceSelectFieldInstantMeetingScreen').addCls('inputDisableCLS');
                                } else {
                                    Ext.getCmp('idMeetingPlaceSelectFieldInstantMeetingScreen').setDisabled(false);
                                    Ext.getCmp('idMeetingPlaceSelectFieldInstantMeetingScreen').removeCls('inputDisableCLS');
                                }
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'fieldset',
                title: "Meeting Place <span class='spanAsterisk'>*</span>",
                id: 'idLabelSelectMeetingPlaceInstantMeetingScreen',
                //style: 'margin-top:-30px;',
                margin: '0 20 0 20',
                items: [
                    {
                        xtype: 'selectfield',
                        id: 'idMeetingPlaceSelectFieldInstantMeetingScreen',
                        store: 'MeetingPlace',
                        displayField: 'MeetingPlaceName',
                        valueField: 'MeetingPlaceID'
                    }
                ]
            },
            {
                xtype: 'fieldset',
                title: "Meeting Start Time",
                id: 'idLabelmeetingStartTimeMentorInstantMeeting',
                margin: '0 20 0 20',
                items: [
                    {
                        xtype: 'textfield',
                        id: 'meetingStartTimeMentorInstantMeeting',
                        disabled: true,
                        //cls: 'textFieldDisableCLS',
                        clearIcon: false
                    }
                ]
            },
            {
                xtype: 'fieldset',
                title: "Meeting End Time",
                id: 'idLabelmeetingEndTimeMentorInstantMeeting',
                margin: '0 20 0 20',
                items: [
                    {
                        xtype: 'textfield',
                        name: 'meetingEndTime',
                        id: 'meetingEndTimeMentorInstantMeeting',
                        disabled: true,
                        //cls: 'textFieldDisableCLS',
                        clearIcon: false,
                        listeners: {
                            keyup: function (form, e) {
                                Mentor.app.application.getController('Main').calculateElapsedTimeEditForm();
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'fieldset',
                title: "Elapsed Time (mins):",
                margin: '0 20 0 20',
                items: [
                    {
                        xtype: 'textfield',
                        id: 'meetingElapsedTimeMentorInstantMeeting',
                        disabled: true,
                        value: 1
                    }
                ]
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                margin: '20',
                items: [
                    {
                        xtype: 'button',
                        text: 'NEXT',
                        cls: 'signin-btn-cls',
                        pressedCls: 'press-btn-cls',
                        id: 'idBtnNextInstantMeeting',
                        action: "doBtnNextInstantMeeting",
                        flex: 1,
                        margin: '0 100 0 100'
                    }
                ]
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                margin: '0 5 10 5',
                items: [
                    {
                        xtype: 'button',
                        text: 'Restart',
                        //cls: 'blue-btn-cls',
                        cls: 'signin-btn-cls',
                        pressedCls: 'press-btn-cls',
                        id: 'idBtnRestartInstantMeeting',
                        action: "doBtnRestartInstantMeeting",
                        flex: 1,
                        margin: '0 5',
                        listeners: {
                            tap: function () {
                                Ext.Msg.confirm("MELS", "Are you sure you want to Restart the meeting?", function (btn) {
                                    if (btn == 'yes') {
                                        var currentDate = new Date();
                                        var FullYear = currentDate.getFullYear();
                                        var Month = currentDate.getMonth() + 1;
                                        Month = (Month < 10) ? "0" + Month : Month;

                                        var date = currentDate.getDate();
                                        date = (date < 10) ? "0" + date : date;

                                        var Hours = currentDate.getHours();
                                        Hours = (Hours < 10) ? "0" + Hours : Hours;

                                        var Minutes = currentDate.getMinutes();
                                        var NewMinutes = '';

                                        Minutes = (Minutes == 0) ? 1 : (Minutes);
                                        NewMinutes = (parseInt(Minutes + 1));

                                        Minutes = (Minutes < 10) ? "0" + Minutes : Minutes;
                                        NewMinutes = (NewMinutes < 10) ? "0" + NewMinutes : NewMinutes;

                                        var Seconds = currentDate.getSeconds();
                                        Seconds = (Seconds < 10) ? "0" + Seconds : Seconds;

                                        Ext.getCmp('meetingStartTimeMentorInstantMeeting').setValue(FullYear + "-" + Month + "-" + date + " " + Hours + ":" + Minutes);
                                        Ext.getCmp('meetingEndTimeMentorInstantMeeting').setValue(FullYear + "-" + Month + "-" + date + " " + Hours + ":" + NewMinutes);
                                        clearInterval(Mentor.Global.INSTANT_MEETING_INTERVAL_ID);
                                        Ext.getCmp('meetingElapsedTimeMentorInstantMeeting').setValue(1);
                                        Mentor.Global.INSTANT_MEETING_INTERVAL_ID = setInterval(function () {
                                            var StartSessionDate = Date.parse(Ext.getCmp('meetingStartTimeMentorInstantMeeting').getValue());
                                            var EndSessionDate;

                                            EndSessionDate = new Date();
                                            StartSessionDate.setSeconds(EndSessionDate.getSeconds());

                                            var DateDiff = EndSessionDate - StartSessionDate;
                                            var diffHrs = parseInt((DateDiff % 86400000) / 3600000); // hours
                                            var diffMins = Math.floor(((DateDiff % 86400000) % 3600000) / 60000);
                                            diffMins = (diffMins == 0) ? 1 : (diffMins + 1);
                                            Ext.getCmp('meetingElapsedTimeMentorInstantMeeting').setValue(diffMins);
                                            diffMins = (diffMins < 10) ? "0" + diffMins : diffMins;
                                            diffHrs = (diffHrs < 10) ? "0" + diffHrs : diffHrs;

                                            StartSessionDate.setHours(StartSessionDate.getHours() + parseInt(diffHrs));
                                            StartSessionDate.setMinutes(StartSessionDate.getMinutes() + parseInt(diffMins));

                                            var FullYear = StartSessionDate.getFullYear();

                                            var Month = StartSessionDate.getMonth() + 1;
                                            Month = (Month < 10) ? "0" + Month : Month;

                                            var date = StartSessionDate.getDate();
                                            date = (date < 10) ? "0" + date : date;

                                            var Hours = StartSessionDate.getHours();
                                            Hours = (Hours < 10) ? "0" + Hours : Hours;

                                            var Minutes = StartSessionDate.getMinutes();
                                            Minutes = (Minutes < 10) ? "0" + Minutes : Minutes;

                                            var Seconds = StartSessionDate.getSeconds();
                                            Seconds = (Seconds < 10) ? "0" + Seconds : Seconds;

                                            Ext.getCmp('meetingEndTimeMentorInstantMeeting').setValue(FullYear + "-" + Month + "-" + date + " " + Hours + ":" + Minutes);
                                        }, 60000);
                                    }
                                });
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Cancel',
                        //cls: 'decline-btn-cls',
                        pressedCls: 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        id: 'idBtnCancelInstantMeeting',
                        action: "doBtnCancelInstantMeeting",
                        flex: 1,
                        margin: '0 5',
                        listeners: {
                            tap: function () {
                                Ext.Msg.confirm("MELS", "Are you sure you want to Cancel the meeting?", function (btn) {
                                    if (btn == 'yes') {


                                        Ext.getCmp('meetingElapsedTimeMentorInstantMeeting').setValue(1);
                                        clearInterval(Mentor.Global.INSTANT_MEETING_INTERVAL_ID);
                                        //Ext.getCmp('idMentorBottomTabView').setActiveItem(0);
                                        Mentor.app.application.getController('AppDetail').btnBackInstantMeetingScreen();
                                    }
                                });
                            }
                        }
                    }
                ]
            }
        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            },
            {
                event: "deactivate",
                fn: "onPageDeactivate"
            }
        ]
    },
    onPageActivate: function () {
        console.log('onPageActivate');
//        if (mentor.Global.CLEAR_IMPROMPTU) {
//            Ext.getCmp('idMenteeSelectFieldInstantMeetingScreen');
//            Ext.getCmp('idTeamSelectFieldInstantMeetingScreen');
//            Ext.getCmp('idTopicsSelectFieldInstantMeetingScreen');
//            Ext.getCmp('idMeetingTypeSelectFieldInstantMeetingScreen');
//            Ext.getCmp('idMeetingPlaceSelectFieldInstantMeetingScreen');
//            Ext.getCmp('meetingStartTimeMentorInstantMeeting');
//            Ext.getCmp('meetingEndTimeMentorInstantMeeting');
//            Ext.getCmp('meetingElapsedTimeMentorInstantMeeting');
//        }
        var TopicsStore = Ext.getStore('Topics').load();
        Ext.getCmp('idLabelSelectMentorInstantMeetingScreen').setTitle('Select ' + Mentor.Global.MENTEE_NAME + ' <span class="spanAsterisk">*</span>');
    },
    onPageDeactivate: function () {
        console.log('onPageDeactivate');

        //Avinash (Tab)
        //Mentor.Global.INSTANT_MEETING_DATA = "";
        //clearInterval(Mentor.Global.INSTANT_MEETING_INTERVAL_ID);
    },
    onPagePainted: function () {
//        Ext.getCmp('idToolbarInstantMeeting').setTitle(Mentor.Global.IMPROMPTU_NAME + ' Meeting');
        Ext.getCmp('MenteeLabel').setHtml(Mentor.Global.MENTEE_NAME.toUpperCase());
        console.log('onPagePainted');

        Ext.getCmp('teamBtn').removeCls('team selected');
        Ext.getCmp('menteeBtn').removeCls('mentee selected');
        Ext.getCmp('radioTeam').uncheck();
        Ext.getCmp('radioMentee').uncheck();
        Ext.getCmp('idLabelSelectMentorInstantMeetingScreen').setHidden(true);
        Ext.getCmp('idLabelSelectTeamInstantMeetingScreen').setHidden(true);

        var TopicsStore = Ext.getStore('Topics').load();
        Mentor.app.application.getController('AppDetail').btnInviteForMeetingMentor();
        Mentor.app.application.getController('UpdateProfilePicture').btnGetUpdatedTeamData();
        clearInterval(Mentor.Global.SET_INTERVAL_ID);
        Mentor.Global.SET_INTERVAL_ID = 0;

        if (Mentor.Global.INSTANT_MEETING_DATA == '') {
            var currentDate = new Date();
            var FullYear = currentDate.getFullYear();
            var Month = currentDate.getMonth() + 1;
            Month = (Month < 10) ? "0" + Month : Month;

            var date = currentDate.getDate();
            date = (date < 10) ? "0" + date : date;

            var Hours = currentDate.getHours();
            Hours = (Hours < 10) ? "0" + Hours : Hours;

            var Minutes = currentDate.getMinutes();
            var NewMinutes = '';

            Minutes = (Minutes == 0) ? 1 : (Minutes);
            NewMinutes = (parseInt(Minutes + 1));

            Minutes = (Minutes < 10) ? "0" + Minutes : Minutes;
            NewMinutes = (NewMinutes < 10) ? "0" + NewMinutes : NewMinutes;

            var Seconds = currentDate.getSeconds();
            Seconds = (Seconds < 10) ? "0" + Seconds : Seconds;

            Ext.getCmp('meetingStartTimeMentorInstantMeeting').setValue(FullYear + "-" + Month + "-" + date + " " + Hours + ":" + Minutes);
            Ext.getCmp('meetingEndTimeMentorInstantMeeting').setValue(FullYear + "-" + Month + "-" + date + " " + Hours + ":" + NewMinutes);
            Mentor.Global.INSTANT_MEETING_INTERVAL_ID = setInterval(function () {
                var StartSessionDate = Date.parse(Ext.getCmp('meetingStartTimeMentorInstantMeeting').getValue());
                var EndSessionDate;

                EndSessionDate = new Date();
                StartSessionDate.setSeconds(EndSessionDate.getSeconds());

                var DateDiff = EndSessionDate - StartSessionDate;
                var diffHrs = parseInt((DateDiff % 86400000) / 3600000); // hours
                var diffMins = Math.floor(((DateDiff % 86400000) % 3600000) / 60000);
                diffMins = (diffMins == 0) ? 1 : (diffMins + 1);
                Ext.getCmp('meetingElapsedTimeMentorInstantMeeting').setValue(diffMins);
                diffMins = (diffMins < 10) ? "0" + diffMins : diffMins;
                diffHrs = (diffHrs < 10) ? "0" + diffHrs : diffHrs;

                StartSessionDate.setHours(StartSessionDate.getHours() + parseInt(diffHrs));
                StartSessionDate.setMinutes(StartSessionDate.getMinutes() + parseInt(diffMins));

                var FullYear = StartSessionDate.getFullYear();

                var Month = StartSessionDate.getMonth() + 1;
                Month = (Month < 10) ? "0" + Month : Month;

                var date = StartSessionDate.getDate();
                date = (date < 10) ? "0" + date : date;

                var Hours = StartSessionDate.getHours();
                Hours = (Hours < 10) ? "0" + Hours : Hours;

                var Minutes = StartSessionDate.getMinutes();
                Minutes = (Minutes < 10) ? "0" + Minutes : Minutes;

                var Seconds = StartSessionDate.getSeconds();
                Seconds = (Seconds < 10) ? "0" + Seconds : Seconds;

                Ext.getCmp('meetingEndTimeMentorInstantMeeting').setValue(FullYear + "-" + Month + "-" + date + " " + Hours + ":" + Minutes);
            }, 60000);
        } else {

            Ext.getCmp('meetingStartTimeMentorInstantMeeting').setValue(Mentor.Global.INSTANT_MEETING_DATA.MeetingStartTime);
            Ext.getCmp('meetingEndTimeMentorInstantMeeting').setValue(Mentor.Global.INSTANT_MEETING_DATA.MeetingEndTime);

            Mentor.Global.INSTANT_MEETING_INTERVAL_ID = setInterval(function () {
                var StartSessionDate = Date.parse(Ext.getCmp('meetingStartTimeMentorInstantMeeting').getValue());
                var EndSessionDate;

                EndSessionDate = Date.parse(Ext.getCmp('meetingEndTimeMentorInstantMeeting').getValue());
                StartSessionDate.setSeconds(EndSessionDate.getSeconds());

                var DateDiff = EndSessionDate - StartSessionDate;
                var diffHrs = parseInt((DateDiff % 86400000) / 3600000); // hours
                var diffMins = Math.floor(((DateDiff % 86400000) % 3600000) / 60000); // minutes
                diffMins = (diffMins == 0) ? 1 : (diffMins + 1);
                Ext.getCmp('meetingElapsedTimeMentorInstantMeeting').setValue(diffMins);
                diffMins = (diffMins < 10) ? "0" + diffMins : diffMins;
                diffHrs = (diffHrs < 10) ? "0" + diffHrs : diffHrs;

                StartSessionDate.setHours(StartSessionDate.getHours() + parseInt(diffHrs));
                StartSessionDate.setMinutes(StartSessionDate.getMinutes() + parseInt(diffMins));

                var FullYear = StartSessionDate.getFullYear();
                var Month = StartSessionDate.getMonth() + 1;
                var date = StartSessionDate.getDate();

                var Hours = StartSessionDate.getHours();
                Hours = (Hours < 10) ? "0" + Hours : Hours;

                var Minutes = StartSessionDate.getMinutes();
                Minutes = (Minutes < 10) ? "0" + Minutes : Minutes;

                var Seconds = StartSessionDate.getSeconds();
                Seconds = (Seconds < 10) ? "0" + Seconds : Seconds;

                Ext.getCmp('meetingEndTimeMentorInstantMeeting').setValue(FullYear + "-" + Month + "-" + date + " " + Hours + ":" + Minutes);
            }, 60000);
        }
    }
});