Ext.define('Mentor.view.MentorPendingRequest', {
    extend: 'Ext.dataview.DataView',
    xtype: 'mentorPendingRequest',
    //id : 'idMentorPendingRequest',
    requires: [],
    config: {
        cls: 'meetinghistory',
        store: 'PendingRequest',
        inline: true,
        itemTpl:
                //'<div class="myContent" style = "display: flex;min-height:55px;">' +
                '<div class="myContent strippedListing invites" style = "max-width: 160px;min-height:220px;width:80%;margin:10px auto;box-shadow: 1px 1px 7px 2px #888888;">' +
                    '<div style="width:100%; float:left;text-align: center; ">' +
                        '<img src={MenteeImage} id="cricle" height="90px" width="90px" style = "border-radius: 100%;background-color:#ffffff;">' +
                    '</div>' +
                    '<div style="width:100%;float:left;padding: 0px 0px !important;vertical-align: bottom;min-height: 16px;">' +
                        '<div style="width:100%">' +
                            '<div style="width:100%; float:left;font-size: 14px;text-align: center;white-space: nowrap; overflow: hidden;text-overflow: ellipsis;">' +
                                /*'<b>Invitation from {MenteeName}</b>' +*/
                                '<b>{MenteeName}</b>'+
                            '</div>' +
                            //'<div style="width:100%;float:left;"><span style="font-size:11px;color: black;">Main Topic : {TopicDescription}</span>'+
                            '<div style="width:100%; text-align: center;">' +
                                '<span style = "width:100%;float:right;font-size: 11px;height:15px;">' +
                                    '<tpl if="Status == \'Rescheduled\'">' +
                                        '<tpl if="rescheduledBy == \'1\'">Reschedule requested' +
                                        '<tpl else>{Status}' +
                                        '</tpl>' +
                                    '</tpl>' +
                                '</span>' +
                            '</div>' +
                            '<div style="width:100%;float:left;margin-top:5px;">' +
                                '<span style="float:left;font-size:11px;color: black;width: 100%;text-align: center;white-space: nowrap; overflow: hidden;text-overflow: ellipsis; ">Main Topic : {TopicDescription}'+
                                '</span>' +
                                '<div style="clear: both;text-align: center;">' +
                                    /*'<span style="float:left;vertical-align: baseline;">' +
                                        '<img src="./resources/images/clock.png"  height="20px" width="20px">' +
                                    '</span>' +
                                    '<span style="vertical-align: text-top;float:right;font-size:11px;color: black;">{inviteTime}'
                                     +'</span>' +*/
                                    '<span style="padding-left:20px;position:relative;vertical-align: text-top;font-size:11px;color: black;">'+
                                        '<img src="./resources/images/clock.png" style="position:absolute;left:0;top:-1px;" height="15px" width="15px">{inviteTime}'+
                                    '</span>'+
                                '</div>' +
                                '<div  style = "text-align: center;font-size:14px">'+
                                    '<input class="btnViewDetail" type="button" id="btn{id}" value="View Detail">'+
                                '</div>'+
                            '<div>' +
                        '<div>' +
                    '<div>' +
                '</div>',
        listeners: [
            {
                event: "itemtap",
                fn: function (view, index, item, e)
                {
                    Mentor.app.application.getController('AppDetail').respondPendingRequest(view, index, item, e);
                }
            },
            {
                event: "painted",
                fn: "onPagePainted"
            }
        ],
        items: [
            /*	{
             docked: 'top',
             xtype: 'toolbar',
             title: 'Pending Invitations',
             cls:'my-titlebar',
             defaults: {
             iconMask: true
             },
             items:[
             {
             iconCls: 'refreshIcon',
             action : 'btnWaitScreen',
             docked: 'right',
             action : 'btnGetPendingRequest'
             },
             /*{
             iconCls: 'reply',
             action : 'btnAddMeetingDetail',
             docked: 'right',
             },*/
            /*{
             iconCls: 'btnBackCLS',
             action : 'btnBackMentorPendingRequest',
             docked: 'left',
             //style : 'visibility: hidden;',
             }
             ]
             },*/
            {
                xtype: 'label',
                id: 'idUserNameMentorPendingRequest',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                docked: 'top',
                hidden: true
            },
            {
                xtype: 'label',
                html: Mentor.Global.VERSION_NAME,
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true
            }
            /*{
             xtype : 'container',
             layout: {
             type: 'hbox',
             pack: 'center',
             
             },
             docked: 'top',
             items:[{
             xtype : 'button',
             label : 'Add New Meeting',
             cls : 'signin-btn-cls',
             text : 'Log new meeting details',
             style : 'width:45%;margin-bottom:10px;font-size: 15px;',
             action : 'btnAddMeetingDetail',
             },
             {
             xtype : 'button',
             label : 'Add New Meeting',
             cls : 'signin-btn-cls',
             text : 'Pending Request',
             style : 'width:45%;margin-bottom:10px;font-size: 15px;',
             action : 'btnGetPendingRequest',
             }
             ]
             
             }*/
        ]
    },
    onPagePainted: function () {
        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        Mentor.app.application.getController('AppDetail').btnGetPendingRequest();
    }
});
