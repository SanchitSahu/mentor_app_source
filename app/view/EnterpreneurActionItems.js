Ext.define('Mentor.view.EnterpreneurActionItems', {
    extend: 'Ext.Panel',
    xtype: 'enterpreneur_action_items',
    requires: [],
    config: {
        cls: 'enterpreneur_action_items',
        //scrollable: "vertical",
        scrollable: {
            direction: 'vertical',
            indicators: {
                y: {
                    autoHide: false
                }
            }
        },
        defaults: {
            xtype: 'container'
        },
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                title: 'Mentee ToDo Items',
                id: 'enterpreneur_action_items_title',
                cls: 'my-titlebar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        //iconCls: 'arrow_left',
                        iconCls: 'btnBackCLS',
                        action: 'btnBackEnterpreneurActionItems',
                        docked: 'left'
                    },
                    {
                        iconCls: 'goToNotesIcon',
                        docked: 'left',
                        id: 'addnotes',
                        action: 'addnotes'
                    },
                    {
                        //iconCls: 'arrow_left',
                        iconCls: 'helpIcon',
                        docked: 'right',
                        listeners: {
                            element: 'element',
                            tap: function () {
                                var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('mentee_action');
                                Ext.Viewport.add(popup);
                                popup.show();
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'label',
                id: 'idUserNameEnterpreneur',
                style: 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
                hidden: true
            },
            {
                xtype: 'label',
                id: 'idVersionEnterpreneur',
                style: 'text-align: right;padding-right: 10px;padding: 10px;',
                docked: 'bottom',
                hidden: true
            },
            {
                xtype: 'panel',
                margin: '20 0 0 0',
                layout: {
                    type: 'vbox'
                },
                items: [
                    {
                        xtype: 'label',
                        html: 'Enterpreneur ToDo Items',
                        style: 'margin-left:10px;margin-right:10px;border: 0px solid #eee;border-left-width: 0;border-bottom: 1px solid #66cc66;font-weight: bold;',
                        id: 'idLabelMenteeActionItemEnterpreneurActionItemScreen',
                        hidden: true
                    },
                    {
                        xtype: 'panel',
                        id: 'idEnterpreneurActionItemHeader',
                        margin: '10 0 0 0',
                        layout: {
                            type: 'hbox'
                        },
                        style: 'border-bottom: 1px solid #bfbfbf;padding-bottom: 5px;',
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin-left: 30px; font-size: smaller;'
                            },
                            {
                                xtype: 'label',
                                html: 'ToDo',
                                style: 'margin-left: 9px; font-size: smaller;color:#7f7f7f'
                            },
                            {
                                xtype: 'label',
                                html: 'Done',
                                style: 'margin-left: 10px; font-size: smaller;color:#7f7f7f'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        margin: '10 0 0 0',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'panel',
                                margin: '0 0 0 10',
                                id: 'idMenteeActionItemComments'
                            },
                            {
                                xtype: 'panel',
                                id: 'idEnterpreneurActionItem',
                                margin: '0 0 0 10'
                            },
                            {
                                //style : 'width: 85%;'
                                xtype: 'panel',
                                margin: '0 0 0 10',
                                id: 'idMenteeActionItemDoneOnMeetingUpdate'
                            }
                        ]
                    },
                    /*{
                     xtype: 'checkboxfield',
                     name : 'color',
                     value: '0',
                     label: 'Practice',
                     checked: true,
                     labelWidth: 200,
                     labelCls : 'radio-btn-cls'
                     },
                     {
                     xtype: 'checkboxfield',
                     name : 'color',
                     value: '1',
                     label: 'Market Validation',
                     labelWidth: 200,
                     labelCls : 'radio-btn-cls'
                     },
                     {
                     xtype: 'checkboxfield',
                     name : 'color',
                     value: '0',
                     label: 'Cold Calls',
                     labelWidth: 200,
                     labelCls : 'radio-btn-cls'
                     },
                     {
                     xtype: 'checkboxfield',
                     name : 'color',
                     value: '1',
                     label: 'Meeting with Client',
                     labelWidth: 200,
                     labelCls : 'radio-btn-cls'
                     },
                     {
                     xtype: 'checkboxfield',
                     name : 'color',
                     value: '1',
                     label: 'Develop Plans',
                     labelWidth: 200,
                     labelCls : 'radio-btn-cls'
                     },
                     {
                     xtype: 'checkboxfield',
                     name : 'color',
                     value: '1',
                     label: 'Write up Meeting Notes',
                     labelWidth: 200,
                     labelCls : 'radio-btn-cls'
                     },
                     {
                     xtype: 'checkboxfield',
                     name : 'color',
                     value: '1',
                     label: 'Develop Financial Projections',
                     labelWidth: 250,
                     labelCls : 'radio-btn-cls'
                     },*/
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            pack: 'center'
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'NEXT',
                                cls: 'signin-btn-cls',
                                pressedCls: 'press-btn-cls',
                                action: "btnNextEntrepreneurItems",
                                id: 'idBtnNextEntrepreneurItems',
                                //flex: 1
                                width: '50%'
                            }
                            /*{
                             xtype : 'button',
                             text : 'Update',
                             cls : 'signin-btn-cls',
                             flex:1,
                             hidden : true,
                             id : 'idBtnUpdateEntrepreneurItems',
                             action : 'btnUpdateEntrepreneurItems' //UpdateMeetingDetail Controller
                             
                             }*/
                        ]
                    }
                ]
            }

        ],
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            }
        ]
    },
    onPageActivate: function () {

        var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History

        var EnterpreneurActionItemStore = Ext.getStore('EnterpreneurAction').load();
        var MeetingDetailPanel = Ext.getCmp('idEnterpreneurActionItem');
        MeetingDetailPanel.removeAll();
        var MenteeActionPanelDoneOnNewMeeting = Ext.getCmp('idMenteeActionItemDoneOnMeetingUpdate');
        MenteeActionPanelDoneOnNewMeeting.removeAll();
        var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
        var MenteeLoginDetail = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));

        var MenteeActionCommentsStore = Ext.getStore('MenteeActionComments').load();

        var MenteeActionItemComments = Ext.getCmp('idMenteeActionItemComments');
        MenteeActionItemComments.removeAll();

        //Ext.getCmp('idEnterpreneurActionItemHeader').setHidden(true); // Display headder

        Ext.getCmp('idVersionEnterpreneur').setHtml(Mentor.Global.VERSION_NAME);
        if (MentorLoginDetail != null) {
            Ext.getCmp('idUserNameEnterpreneur').setHtml(MentorLoginDetail.MentorName);
        } else if (MenteeLoginDetail != null) {
            Ext.getCmp('idUserNameEnterpreneur').setHtml(MenteeLoginDetail.MenteeName);
            Ext.getCmp('idEnterpreneurActionItemHeader').setHidden(false); // Display headder
            if (MeetingDetail != null) {
                Ext.getCmp('idBtnNextEntrepreneurItems').setText('Update');
            }
        }

        for (i = 0; i < EnterpreneurActionItemStore.data.length; i++) {
            var checked = false;
            if (MeetingDetail != null) {
                var MenteeActionItems = MeetingDetail.MenteeActionDetail.split(',');
                var MenteeActionName;
                for (j = 0; j < MenteeActionItems.length; j++) {
                    var MenteeAction = MenteeActionItems[j].split('~');
                    if (MenteeAction[0] == EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID) {
                        checked = true;
                        break;
                    }
                }

                var checkBoxField = Ext.create('Ext.field.Checkbox', {
                    name: 'recorded_stream',
                    value: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID,
                    //label: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionName,
                    itemId: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionName,
                    labelCls: 'radio-btn-cls',
                    //labelWidth: 280,
                    checked: checked,
                    labelWidth: '90%',
                    labelAlign: 'right',
                    listeners: {
                        'check': function (radio, e, eOpts) {
                            //me.radioHandler(radio.getValue());
                            if (radio.getItemId().toLowerCase() == "no action needed") {
                                var EntrepreneurItemsPanel = Ext.getCmp('idEnterpreneurActionItem');
                                for (i = 0; i < EntrepreneurItemsPanel.getItems().length; i++) {
                                    if (EntrepreneurItemsPanel.getItems().getAt(i).isChecked()) {
                                        if (EntrepreneurItemsPanel.getItems().getAt(i).getItemId().toLowerCase() != "no action needed") {
                                            EntrepreneurItemsPanel.getItems().getAt(i).setChecked(false);
                                        }
                                    }
                                }
                            } else {
                                var EntrepreneurItemsPanel = Ext.getCmp('idEnterpreneurActionItem');
                                for (i = 0; i < EntrepreneurItemsPanel.getItems().length; i++) {
                                    if (EntrepreneurItemsPanel.getItems().getAt(i).isChecked()) {
                                        if (EntrepreneurItemsPanel.getItems().getAt(i).getItemId().toLowerCase() == "no action needed") {
                                            EntrepreneurItemsPanel.getItems().getAt(i).setChecked(false);
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
                MeetingDetailPanel.add(checkBoxField);
            } else
            {
                var Disable = true;
                var MeetingDetailsItem = Ext.decode(localStorage.storeEntrepreneurActionItems);
                if (MeetingDetailsItem != null && MeetingDetailsItem.MenteeActionItemID.length > 0) {
                    var MenteeActionItem = MeetingDetailsItem.MenteeActionItemID.split(',');
                    if (MenteeActionItem.indexOf(EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID) != -1) {
                        checked = true;
                        Disable = false;
                    }
                }
                var checkBoxField = Ext.create('Ext.field.Checkbox', {
                    name: 'recorded_stream',
                    value: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID,
                    //label: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionName,
                    itemId: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionName,
                    labelCls: 'radio-btn-cls',
                    //labelWidth: 280,
                    checked: checked,
                    labelWidth: '90%',
                    labelAlign: 'right',
                    listeners: {
                        'check': function (radio, e, eOpts) {
                            //me.radioHandler(radio.getValue());
                            if (radio.getItemId().toLowerCase() == "no action needed") {
                                var EntrepreneurItemsPanel = Ext.getCmp('idEnterpreneurActionItem');
                                var EntrepreneurItemsDonePanel = Ext.getCmp('idMenteeActionItemDoneOnMeetingUpdate');
                                for (var i = 0; i < EntrepreneurItemsPanel.getItems().length; i++) {
                                    //if(EntrepreneurItemsPanel.getItems().getAt(i).isChecked())
                                    //{
                                    if (EntrepreneurItemsPanel.getItems().getAt(i).getItemId().toLowerCase() != "no action needed") {
                                        EntrepreneurItemsPanel.getItems().getAt(i).setChecked(false);
                                    } else {
                                        EntrepreneurItemsDonePanel.getItems().getAt(i).setDisabled(false);
                                    }
                                    //}
                                }
                            } else {
                                var EntrepreneurItemsPanel = Ext.getCmp('idEnterpreneurActionItem');
                                var EntrepreneurItemsDonePanel = Ext.getCmp('idMenteeActionItemDoneOnMeetingUpdate');
                                for (var i = 0; i < EntrepreneurItemsPanel.getItems().length; i++) {
                                    if (EntrepreneurItemsPanel.getItems().getAt(i).isChecked()) {
                                        if (EntrepreneurItemsPanel.getItems().getAt(i).getItemId().toLowerCase() == "no action needed") {
                                            EntrepreneurItemsPanel.getItems().getAt(i).setChecked(false);
                                        } else {
                                            EntrepreneurItemsDonePanel.getItems().getAt(i).setDisabled(false);
                                        }
                                    } else if (EntrepreneurItemsPanel.getItems().getAt(i).isChecked() == false) {
                                        EntrepreneurItemsDonePanel.getItems().getAt(i).setDisabled(true);
                                    }
                                }
                            }
                        },
                        'uncheck': function (radio, e, eOpts) {
                            var EntrepreneurItemsPanel = Ext.getCmp('idEnterpreneurActionItem');
                            var EntrepreneurItemsDonePanel = Ext.getCmp('idMenteeActionItemDoneOnMeetingUpdate');
                            for (var i = 0; i < EntrepreneurItemsPanel.getItems().length; i++) {
                                if (!EntrepreneurItemsPanel.getItems().getAt(i).getChecked()) {
                                    EntrepreneurItemsDonePanel.getItems().getAt(i).setChecked(false);
                                    EntrepreneurItemsDonePanel.getItems().getAt(i).setDisabled(true);
                                }
                            }
                        }
                    }
                });
                MeetingDetailPanel.add(checkBoxField);
                //Action Item Done for New Meeting
                var checked = false;
                //var Disable = true;

                var MeetingDetailsItem = Ext.decode(localStorage.storeEntrepreneurActionItems);
                //if (MeetingDetailsItem != null && MeetingDetailsItem.MenteeActionItemDoneID.length > 0) {
                if (MeetingDetailsItem != null && MeetingDetailsItem.MenteeActionItemDoneID != null && MeetingDetailsItem.MenteeActionItemDoneID.length > 0) {
                    var MenteeActionItemDone = MeetingDetailsItem.MenteeActionItemDoneID.split(',');
                    if (MenteeActionItemDone.indexOf(EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID) != -1) {
                        checked = true;
                        //Disable = false;
                    }
                }
                var checkBoxFieldActionDone = Ext.create('Ext.field.Checkbox', {
                    name: 'recorded_stream',
                    value: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID,
                    label: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionName,
                    labelCls: 'radio-btn-cls',
                    checked: checked,
                    disabled: Disable,
                    //labelWidth: 200,
                    labelWidth: '90%',
                    labelAlign: 'right'
                });
                MenteeActionPanelDoneOnNewMeeting.add(checkBoxFieldActionDone);

                var MeetingId = Ext.decode(localStorage.getItem('SaveStartedMeetingID'));
                var cls = 'addnote';
                var icon = 'add_icon_filled.png';
                var isContinue = false;
                for (var c = 0; c < MenteeActionCommentsStore.data.length; c++) {
                    if (isContinue)
                        continue;
                    if (MenteeActionCommentsStore.data.getAt(c).data.MeetingID == MeetingId.SaveStartedMeetingID) {
                        if (MenteeActionCommentsStore.data.getAt(c).data.MenteeActionItemID == EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID) {
                            cls = 'editnote_' + MenteeActionCommentsStore.data.getAt(c).data.MenteeActionItemCommentID;
                            icon = 'edit_note.png';
                        }
                    }
                }

                var buttonFieldActionComment = Ext.create('Ext.Img', {
                    src: 'resources/images/' + icon,
                    height: 25,
                    width: 25,
                    itemId: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID,
                    cls: cls,
                    listeners: {
                        'tap': function (radio, e, eOpts) {
                            var popup = Mentor.app.application.getController('AppDetail').addCommentPopup(1, radio.getItemId(), radio.getCls()[0], MeetingId.SaveStartedMeetingID);
                            Ext.Viewport.add(popup);
                            popup.show();
                        }
                    }
                });
                MenteeActionItemComments.add(buttonFieldActionComment);
            }
        }

        //Action Item Done
        if (MeetingDetail != null) {
            var MenteeActionPanelDoneOnMettingUpdate = Ext.getCmp('idMenteeActionItemDoneOnMeetingUpdate');
            MenteeActionPanelDoneOnMettingUpdate.removeAll();
            for (i = 0; i < EnterpreneurActionItemStore.data.length; i++) {
                var checked = false;
                var Disable = true;
                var MenteeActionItems = MeetingDetail.MenteeActionDetail.split(',');
                var MenteeActionName;
                for (j = 0; j < MenteeActionItems.length; j++) {
                    var MenteeAction = MenteeActionItems[j].split('~');
                    var MenteeActionItemsDone = "";
                    if (MeetingDetail.MenteeActionItemDone != null) {
                        MenteeActionItemsDone = MeetingDetail.MenteeActionItemDone.split(',');
                    }
                    if (MenteeAction[0] == EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID) {
                        for (k = 0; k < MenteeActionItems.length; k++) {
                            if (MenteeActionItemsDone[k] == EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID) {
                                checked = true;
                            }
                        }
                        //checked = true;
                        Disable = false;
                        break;
                    }
                }

                var checkBoxField = Ext.create('Ext.field.Checkbox', {
                    name: 'recorded_stream',
                    value: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID,
                    label: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionName,
                    labelCls: 'radio-btn-cls',
                    checked: checked,
                    disabled: Disable,
                    //labelWidth: 200,
                    labelWidth: '90%',
                    labelAlign: 'right',
                    listeners: {
                        'check': function (radio, e, eOpts) {
                            //me.radioHandler(radio.getValue());
                        }
                    }
                });
                MenteeActionPanelDoneOnMettingUpdate.add(checkBoxField);
            }
        }

        //Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
        Ext.getCmp('idLabelMenteeActionItemEnterpreneurActionItemScreen').setHtml(Mentor.Global.MENTEE_NAME + ' ToDo Items');
    },
    onPagePainted: function () {
        Ext.getCmp('enterpreneur_action_items_title').setTitle(Mentor.Global.MENTEE_NAME + ' ToDo Items');
    }
});
