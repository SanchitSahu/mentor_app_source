Ext.define('Mentor.view.AddNotePopupView', {
    extend: 'Ext.Panel',
    xtype: 'addnotepopview',
    requires: [],
    config: {
        cls: 'addNotePopupScreen',
        defaults: {
        },
        items: [
            {
                xtype: 'textfield',
                id: 'idAddEditNoteID',
                hidden: true
            },
            {
                xtype: 'textfield',
                id: 'idAddEditNoteMeetingID',
                hidden: true
            },
            {
                xtype: 'label',
                html: 'Note Title',
                style: 'font-weight: bold;',
                padding: '35 10 3 10',
                hidden: true
            },
            {
                xtype: 'textfield',
                id: 'idAddEditNoteTitle',
                cls: 'textFieldDisableCLS',
                inputCls: 'inputCLS',
                margin: '0 10 0 10',
                clearIcon: false,
                hidden: true
            },
            {
                xtype: 'label',
                html: "Note Content",
                style: 'font-weight: bold;',
                padding: '20 10 3 10'
            },
            {
                xtype: 'textareafield',
                //height: '195px',
                style: "background-color: #ffffff;",
                id: 'idAddEditNoteDescription',
                margin: '0 10 0 10',
                cls: 'textFieldDisableCLS',
                disabled: false,
                inputCls: 'inputCLS',
                clearIcon: false
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                margin: '5 5 10 5',
                items: [
                    {
                        xtype: 'button',
                        text: 'SAVE',
                        //cls : 'tick',
                        //cls: 'signin-btn-cls-note',
                        cls: 'signin-btn-cls',
                        pressedCls : 'press-btn-cls',
                        action: "doSubmitAddEditNote",
                        flex: 1,
                        margin: '10 5 0 5'
                    },
                    {
                        xtype: 'button',
                        text: 'CANCEL',
                        //cls : 'cancel',
                        //cls: 'decline-btn-cls-note',
                        pressedCls : 'press-grey_btn-cls',
                        cls: 'grey-btn-cls',
                        flex: 1,
                        margin: '10 5 0 5',
                        listeners: {
                            tap: function () {
                                var popupObject = Ext.getCmp('addEditNotePopUp');
                                popupObject.hide();
                            }
                        }
                    }
                ]
            }
        ]
    },
    listeners: [
        {
            event: "activate",
            fn: "onPageActivate"
        },
        {
            event: "painted",
            fn: "onPagePainted"
        }
    ],
    onPagePainted: function () {}
});