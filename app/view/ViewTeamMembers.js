Ext.define('Mentor.view.ViewTeamMembers', {
    extend: 'Ext.dataview.List',
    xtype: 'viewteammembers',
    itemId: 'itemIdMeetinghistory',
    id: "viewteammembers",
    requires: [],
    config: {
        title: 'Team Members',
        cls: 'meetinghistory',
        store: 'TeamMembers',
        disableSelection: true,
        itemTpl: /*'<div class="myContent "  style = "background: #ffffff;padding: 10px;display: flex;min-height:70px;box-shadow: 1px 1px 4px 0.2px #888888;margin-left:10px;margin-right:10px;display: flex;">' +
                    '<div style="width:20%; float:left; ">' +
                        '<img src={UserImage} id="cricle" height="50px" width="50px" style = "border-radius: 50%;">' +
                    '</div>' +
                    '<div style="width:80%;float:left;padding: 0px 0px !important;vertical-align: bottom;min-height: 16px;">' +
                        '<div style="width:100%;vertical-align: middle;float: none; display: table-cell;">' +
                            '<div style="width:50%; float:left;font-size: 14px;color: black;">' +
                                '<tpl if="isOwner == \'0\'">' +
                                    '<b>{UserName}</b>' +
                                '<tpl else>' +
                                    '<b>{UserName} (Manager)</b>' +
                                '</tpl>' +
                            '</div>' +
                            '<div style=" float:right;">' +
                                '<tpl if="isOwner == \'0\'">' +
                                    '<span style="vertical-align: baseline;"><img src="./resources/images/delete.png" class="remove" height="40px" width="40px"></span>' +
                                     '<span style="vertical-align: baseline;"><img src="./resources/images/block.png" class="block"  height="40px" width="40px"></span>' +
                                '</tpl>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>',*/
                '<div class="myContent Invitation"  style = "background: #ffffff;padding: 10px;display: flex;min-height:70px;box-shadow: 1px 1px 4px 0.2px #888888;margin-left:10px;margin-right:10px;">' +
                    '<div style="width:20%; float:left; ">' +
                        '<img src={UserImage} style="border-radius: 50%;width: 65px;height: 65px;">' +
                    '</div>' +
                    '<div style="width:80%;float:left;padding: 0px 0px !important;vertical-align: bottom;min-height: 16px;display: table;height: 65px;margin-left:10px;">' +
                        '<div style = "vertical-align: middle;float: none; display: table-cell;">'+
                            '<div style="width:100%">'+
                                '<div style="width:50%; float:left;font-size: 14px;color: black;">'+
                                    '<tpl if="isOwner == \'0\'">' +
                                        '<b>{UserName}</b>' +
                                    '<tpl else>' +
                                        '<b>{UserName}</b></br>(Admin)' +
                                    '</tpl>' +
                                '</div>' +
                                '<div style=" float:right;">' +
                                    '<tpl if="isOwner == \'0\'">' +
                                        '<span style="vertical-align: baseline;"><img src="./resources/images/delete.png" class="remove" height="40px" width="40px"></span>' +
                                         '<span style="vertical-align: baseline;"><img src="./resources/images/block.png" class="block"  height="40px" width="40px"></span>' +
                                    '</tpl>' +
                                '</div>' +
                            '</div>' +
                            
                        '</div>' +
                    '</div>' +
                '</div>',
        listeners: [
            {
                event: "activate",
                fn: "onPageActivate"
            },
            {
                event: "painted",
                fn: "onPagePainted"
            },
            {
                event: 'itemtap',
                fn: function (view, index, item, record, e) {
                    aa = record;
                    var ownerTeamInfo = Ext.decode(localStorage.getItem('TeamAllDetails'));
                    if (Ext.dom.Element.fly(e.target).hasCls('remove')) {
                       Ext.Msg.confirm("MELS", "Are you sure you want to remove "+record.data.UserName+" from your "+ownerTeamInfo.OwnTeamDetail[0].TeamName+" team?" , function (btn) {
                            if (btn == 'yes') {
                                Mentor.app.application.getController('AppDetail').removeTeamMember(record);
                            }
                        });
                    } else if (Ext.dom.Element.fly(e.target).hasCls('block')) {
                        Ext.Msg.confirm("MELS", "Are you sure you want to Block "+record.data.UserName+" from your "+ownerTeamInfo.OwnTeamDetail[0].TeamName+" team?", function (btn) {
                            if (btn == 'yes') {
                                Mentor.app.application.getController('AppDetail').blockTeamMember(record);
                            }
                        });
                    } else {
                        return false;
                    }
                }
            }
        ],
        items: [
            {
                //title: 'Meeting History',
                docked: 'top',
                xtype: 'panel',
                cls: 'mettingHistoryToolBar',
                defaults: {
                    iconMask: true
                },
                items: [
                    {
                        xtype: 'toolbar',
                        docked: 'top',
                        cls: 'my-titlebar',
                        title: 'Team Members',
                        defaults: {
                            iconMask: true
                        },
                        items: [
                            {
                                //iconCls: 'addMettingMentorMettingHistory',
                                //style : 'visibility: hidden;',
                                /*handler:function(){
                                 Ext.getCmp('idMenteeBottomTabView').setActiveItem(0);
                                 }*/
                                iconCls: 'btnBackCLS',
                                action: 'btnBackMentorInvitesTabView',
                                docked: 'left',
                                listeners: {
                                    tap: function () {
                                        Mentor.app.application.getController('AppDetail').btnBackActionTeamDetail();
                                    }
                                }
                            },
                            {
                                //style: 'visibility:hidden'
                                iconCls: 'goToNotesIcon',
                                docked: 'left',
                                id: 'addnotes',
                                action: 'addnotes'
                            },
                            {
                                iconCls: 'helpIcon',
                                docked: 'right',
                                listeners: {
                                    element: 'element',
                                    tap: function () {
                                        var popup = Mentor.app.application.getController('AppDetail').addHelpPopup('team_members');
                                        Ext.Viewport.add(popup);
                                        popup.show();
                                    }
                                }
                            },
                            {
                                iconCls: 'refreshIcon',
                                action: 'btnRefreshTeamMembers',
                                docked: 'right'
                            }
                            /*{
                             iconCls: 'addMettingMentorMettingHistory',
                             action : 'btnAddMeetingDetail',
                             docked: 'right',
                             hidden:true,
                             },*/
                            /*{
                             xtype : 'textfield',
                             
                             }*/
                        ]
                    },
                    /*{
                     iconCls: 'home',
                     action : 'btnProfileMeetingDetail',
                     docked: 'left',
                     },*/


                    {
                        xtype: 'panel',
                        cls: 'searchFieldCls',
                        margin:'0 20 10 20',
                        items: [
                            {
                                //style : 'background: transparent !important;',
                                xtype: 'searchfield',
                                placeHolder: 'Search team member',
                                // itemId: 'searchBoxMentorMettingHistory'
                                id: 'searchBoxTeam',
                                itemId: 'searchBoxTeamMember',
                                cls : 'searchNotesCLS',
                                inputCls : 'searchNotesInputCLS'
                            },
                            {
                                xtype: 'button',
                                //html: 'Add',
                                id: 'addTeamMembersToTeam',
                                cls: 'noteListAddButton',
                                docked: 'right',
                                iconCls: 'noteListAddButtonIcon',
                                margin : '0 0 0 7',
                                hidden : true,
                                listeners: {
                                    tap: function () {
                                        Mentor.app.application.getController('AppDetail').btnAddTeamMembers(Ext.getCmp('addTeamMembersToTeam').getItemId());
                                    }

                                    /*tap: function () {
                                     Ext.Msg.show({
                                     title: 'Add Team Members',
                                     message: 'What access you wish to set for your Note?',
                                     buttons: [
                                     {text: 'Mentor', itemId: 'mentor', ui: 'action'},
                                     {text: 'Mentee', itemId: 'mentee', ui: 'action'},
                                     {text: 'Cancel', itemId: 'cancel', ui: 'action'}
                                     ],
                                     fn: function (buttonId) {
                                     if (buttonId == 'cancel') {
                                     return false;
                                     } else {
                                     
                                     // var NoteID = Ext.getCmp('idViewNoteID').getValue();
                                     // Mentor.app.application.getController('AppDetail').shareNotePopup(buttonId, NoteID);
                                     }
                                     
                                     }
                                     });
                                     
                                     }*/
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    },
    onPageActivate: function () {
        // Mentor.Global.TabNavigationStack = 0;

    },
    onPagePainted: function () {
        Mentor.app.application.getController('AppDetail').getTeamMembers();
        Mentor.app.application.getController('AppDetail').getAllMentorsMenteesForTeam();
        // Mentor.app.application.getController('AppDetail').getMeetingHistory();
        //
        // clearInterval(Mentor.Global.SET_INTERVAL_ID);
        // Mentor.Global.SET_INTERVAL_ID = 0;
        // Mentor.Global.SET_INTERVAL_ID = setInterval(function () {
        //     Mentor.app.application.getController('AppDetail').getMeetingHistory(true);
        // }, Mentor.Global.SET_INTERVAL_TIME);
    }
});