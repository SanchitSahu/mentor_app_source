Ext.define('Mentor.model.MeetingPlace', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'MeetingPlaceID', type: 'string'},
            {name: 'MeetingPlaceName', type: 'string'},
            {name: 'Weight', type: 'string'}
        ]
    }
});
