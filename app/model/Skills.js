Ext.define('Mentor.model.Skills', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'SkillID', type: 'string'},
            {name: 'SkillName', type: 'string'}
        ]
    }
});
