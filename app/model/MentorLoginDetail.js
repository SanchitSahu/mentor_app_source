Ext.define('Mentor.model.MentorLoginDetail', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'Id', type: 'string'},
            {name: 'MentorEmail', type: 'string'},
            {name: 'MentorName', type: 'string'},
            {name: 'MentorPhone', type: 'string'},
            {name: 'MentorStatus', type: 'string'},
            {name: 'Skills', type: 'string'}
        ]
    }
});
