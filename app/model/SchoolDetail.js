Ext.define('Mentor.model.SchoolDetail', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'subdomain_id', type: 'string'},
            {name: 'subdomain_name', type: 'string'},
            {name: 'school_name', type: 'string'}
        ]
    }
});
