Ext.define('Mentor.model.MentorAction', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'MentorActionID', type: 'string'},
            {name: 'MentorActionName', type: 'string'}
        ]
    }
});
