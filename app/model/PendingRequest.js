Ext.define('Mentor.model.PendingRequest', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'InvitationId', type: 'string'},
            {name: 'MenteeID', type: 'string'},
            {name: 'MenteeName', type: 'string'},
            {name: 'ManteeComments', type: 'ManteeComments'},
            {name: 'TopicDescription', type: 'TopicDescription'},
            {name: 'MenteeImage', type: 'String'},
            {name: 'inviteTime', type: 'String'},
            {name: 'InviteeEmail', type: 'String'},
            {name: 'InviteePhone', type: 'String'},
            {name: 'SmsCapable', type: 'String'},
            {name: 'rescheduledBy', type: 'String'},
            {name: 'RescheduleComments', type: 'String'},
            {name: 'Status', type: 'String'},
            {name: 'InitiatedBy', type: 'String'}
        ]
    }
});
