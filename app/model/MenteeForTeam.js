Ext.define('Mentor.model.MenteeForTeam', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'MenteeID', type: 'string'},
            {name: 'MenteeName', type: 'string'}
        ]
    }
});