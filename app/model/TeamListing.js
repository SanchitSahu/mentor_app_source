Ext.define('Mentor.model.TeamListing', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'TeamId', type: 'string'},
            {name: 'TeamName', type: 'string'},
            {name: 'TeamDescription', type: 'string'},
            {name: 'MaxMembers', type: 'string'},
            {name: 'OwnerName', type: 'string'},
            {name: 'Skills', type: 'string'},
            {name: 'TeamFullImage', type: 'string'},
            {name: 'TeamSkills', type: 'string'},
            {name: 'TeamOwnerUserID', type: 'string'},
            {name: 'TeamOwnerUserType', type: 'string'},
            {name: 'OwnerFullImage', type: 'string'}
        ]
    }
});
