Ext.define('Mentor.model.InviteMentee', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'MenteeID', type: 'string'},
            {name: 'MenteeName', type: 'string'},
            {name: 'rStatus', type: 'string'}
        ]
    }
});
