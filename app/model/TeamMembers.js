Ext.define('Mentor.model.TeamMembers', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'UserImage', type: 'string'},
            {name: 'UserName', type: 'string'},
            {name: 'UserID', type: 'string'},
            {name: 'UserType', type: 'string'},
            {name: 'TeamID', type: 'string'},
            {name: 'isOwner', type: 'string'}
        ]
    }
});