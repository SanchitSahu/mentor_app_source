Ext.define('Mentor.model.AcceptResponses', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'ResponseID', type: 'string'},
            {name: 'ResponseName', type: 'string'}
        ]
    }
});