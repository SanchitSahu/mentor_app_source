Ext.define('Mentor.model.UserNotes', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'NoteID', type: 'string'},
            {name: 'MeetingID', type: 'string'},
            {name: 'NoteUserType', type: 'string'},
            {name: 'NoteAuthor', type: 'string'},
            {name: 'NoteTitle', type: 'string'},
            {name: 'NoteDescription', type: 'string'},
            {name: 'Status', type: 'string'},
            {name: 'Weight', type: 'string'},
            {name: 'CreatedDate', type: 'string'},
            {name: 'UpdatedDate', type: 'string'},
            {name: 'Access', type: 'string'}
        ]
    }
});